<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHobiDetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hobby_det', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('id_hobi');
            $table->integer('id_kathobi_satu');
            $table->integer('id_kathobi_dua');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hobby_det');
    }
}
