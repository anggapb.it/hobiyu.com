<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLelangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lelangs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('userid');
            $table->string('nmitem');
            $table->integer('idkatitem');
            $table->string('kondisi_item',50);
            $table->float('hrgawal', 8, 2);
            $table->text('deskripsi');
            $table->text('photo_item');
            $table->timestamp('tgl_bts_penawaran');
            $table->string('notelp', 20);
            $table->string('beban_biaya_kirim');
            $table->integer('idstlelang');
            $table->string('pemenang_bid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lelangs');
    }
}
