<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barangs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kdbrg');
            $table->string('nmbrg');
            $table->integer('idkatbrg');
            $table->integer('stok');
            $table->integer('harga');
            $table->integer('berat');
            $table->string('kondisi_barang');
            $table->text('deskripsi_barang');
            $table->text('gambar_satu');
            $table->text('gambar_dua');
            $table->text('gambar_tiga');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barangs');
    }
}
