<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('noorder');
            $table->integer('idcustomer');
            $table->integer('idpenjual');
            $table->integer('idmethod_bayar');
            $table->integer('idkurir');
            $table->integer('ongkir');
            $table->integer('total');
            $table->text('alamat_kirim');
            $table->integer('status');
            $table->datetime('batas_bayar');
            $table->datetime('estimasi_kirim');
            $table->integer('idbank_transfer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
