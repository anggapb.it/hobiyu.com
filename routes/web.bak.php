<?php
  Route::view('/', 'welcome');
  Auth::routes();

  Route::get('/login/customer', 'Auth\LoginController@showCustomerLoginForm');
  Route::get('/login/writer', 'Auth\LoginController@showWriterLoginForm');
  Route::get('/register/customer', 'Auth\RegisterController@showCustomerRegisterForm');
  Route::get('/register/writer', 'Auth\RegisterController@showWriterRegisterForm');

  Route::post('/login/customer', 'Auth\LoginController@customerLogin');
  Route::post('/login/writer', 'Auth\LoginController@writerLogin');
  Route::post('/register/customer', 'Auth\RegisterController@createCustomer');
  Route::post('/register/writer', 'Auth\RegisterController@createWriter');

  Route::view('/home', 'home')->middleware('auth');
  Route::view('/customer', 'customer');
  Route::view('/writer', 'writer'); 