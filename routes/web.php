<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     //return view('welcome');
//     return view('    index');
// });

Route::get('/', 'HomeBaseWebController@index');
Route::get('/home/customer', 'HomeBaseWebController@homeCustomer');
Route::get('/customer/lelang', 'CustomerController@lelangCustomer')->name('customer.lelang');

Route::get('/customer/history-bid', 'CustomerController@historyBid')->name('customer.history-bid');
Route::get('/customer/get-history-bid', 'CustomerController@getHistoryBid')->name('customer.get-history-bid');
Route::get('/customer/formlelang', 'CustomerController@formLelang');
Route::post('/customer/store', 'CustomerController@storeLelang')->name('customer.store');
Route::get('/customer/saldo', 'CustomerController@saldo')->name('customer.saldo');
Route::get('/customer/toko', 'CustomerController@toko')->name('customer.toko');
Route::get('/customer/order', 'CustomerController@order')->name('customer.order');
Route::get('/customer/sales', 'CustomerController@sales')->name('customer.sales');
Route::get('/customer/forum', 'CustomerController@forum')->name('customer.forum');
Route::post('/customer/update-forum', 'CustomerController@updateForum')->name('customer.update-forum');
Route::get('/customer/formbrg', 'CustomerController@formBrg');
Route::post('/customer/insert-barang', 'CustomerController@insertBrg')->name('customer.insert-barang');
Route::post('/customer/update-barang', 'CustomerController@updateBrg')->name('customer.update-barang');
Route::post('/customer/batalorder/{id}', 'CustomerController@batalorder')->name('customer.batalorder');
Route::post('/customer/kirimorder/{id}', 'CustomerController@kirimorder')->name('customer.kirimorder');
Route::get('/customer/orderdet/{id}', 'CustomerController@orderdet')->name('customer.orderdet');
Route::get('/customer/orderdet-forsales/{id}', 'CustomerController@orderdet_forsales')->name('customer.orderdet-forsales');
Route::post('/customer/update-noresi', 'CustomerController@update_noresi')->name('customer.update-noresi');
Route::get('/customer/complaint', 'CustomerController@complaint')->name('customer.complaint');
Route::get('/customer/complaint-detail/{id}', 'CustomerController@complaint_detail')->name('customer.complaint-detail');
Route::post('/customer/insert-complaint', 'CustomerController@insertComplaint')->name('customer.insert-complaint');
Route::post('/customer/insert-answer-complaint/{id}', 'CustomerController@insertAnswerComplaint')->name('customer.insert-answer-complaint');

Route::post('/lelang/destroy/{id}', 'CustomerController@destroyLelang')->name('lelang.destroy');
Route::get('/lelang/edit', 'CustomerController@editLelang')->name('lelang.edit');
Route::get('/lelang/detail/{id}', 'HomeBaseWebController@detailLelang')->name('lelang.detail');

Route::get('/jual/{id}', 'JualController@index')->name('jual');
Route::get('/jual/detail/{id}', 'JualController@detail')->name('jual.detail');
Route::post('/jual/addcart', 'JualController@addCart')->name('jual.addcart');
Route::post('/jual/remove-cart/{id}', 'JualController@removeCart')->name('jual.remove-cart');
Route::get('/jual/cart/checkout', 'JualController@checkout')->name('jual.cart.checkout');
Route::post('/jual/cart/insert-checkout', 'JualController@insertCheckout')->name('jual.cart.insert-checkout');
Route::get('/jual/cart/success-checkout/{id}', 'JualController@successCheckout')->name('jual.cart.success-checkout');
Route::post('/jual/update-cart/{id}/{qty}', 'JualController@updateCart')->name('jual.update-cart');



Route::get('/customer/profile', 'CustomerController@profile')->name('customer.profile');
Route::post('/customer/update-profile/{id}', 'CustomerController@updateProfile')->name('customer.update-profile');
Route::post('/customer/insert-topup', 'CustomerController@insertTopup')->name('customer.insert-topup');
Route::get('/customer/kota/{id}', 'CustomerController@getKota')->name('customer.kota');
Route::get('/customer/kecamatan/{id}', 'CustomerController@getKec')->name('customer.kecamatan');
Route::get('/customer/kelurahan/{id}', 'CustomerController@getKel')->name('customer.kelurahan');
// Route::get('/lelang/bid', 'HomeBaseWebController@bid');
Route::post('/lelang/makebid', 'HomeBaseWebController@makeBid')->name('lelang.makebid');
Route::post('/lelang/update-exttime', 'CustomerController@addExttime')->name('lelang.update-exttime');
Route::post('/lelang/update-lelang', 'CustomerController@updateLelangNew')->name('lelang.update-lelang');
Route::get('/lelang/{id}', 'LelangController@index')->name('lelang');

Route::get('/lelang/hobbydet/{id}', 'LelangController@detail_hobby')->name('lelang.hobbydet');
//FORUM
Route::get('/forum/list/{id}', 'ForumController@list')->name('forum.list');
Route::get('/forum/post', 'ForumController@post')->name('forum.post');
Route::post('/forum/store', 'ForumController@store')->name('forum.store');
Route::get('/forum/detail/{id}', 'ForumController@detail')->name('forum.detail');
Route::post('/forum/comment/{id}', 'ForumController@comment')->name('forum.comment');

//END FORUM


Route::get('signup','HomeBaseWebController@signup');
//Route::resource('members', 'MembersController');

Route::get('member-login','Auth\MemberLoginController@showLoginForm');
Route::post('member-login', ['as' => 'member-login', 'uses' => 'Auth\MemberLoginController@login']);

Route::get('member-register','Auth\MemberLoginController@showRegisterPage');
Route::post('member-register', 'Auth\MemberLoginController@register')->name('member.register');



Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'master', 'namespace' => 'Master'], function(){
        Route::resource('bank', 'BankController');

        //USER
        Route::resource('user', 'UsersController');
        Route::post('user/store', 'UsersController@store');
        Route::get('user/delete/{id}', 'UsersController@destroy');

        Route::resource('bank', 'BanksController');
        Route::post('bank/store', 'BanksController@store');
        Route::get('bank/delete/{id}', 'BanksController@destroy');

        Route::resource('provinsi', 'ProvinsiController');
        Route::post('provinsi/store', 'ProvinsiController@store');
        Route::get('provinsi/delete/{id}', 'ProvinsiController@destroy');

        Route::resource('job', 'JobsController');
        Route::post('job/store', 'JobsController@store');
        Route::get('job/delete/{id}', 'JobsController@destroy');

        Route::resource('type-transaction', 'TypeTransactionController');
        Route::post('type-transaction/store', 'TypeTransactionController@store');
        Route::get('type-transaction/delete/{id}', 'TypeTransactionController@destroy');

        Route::resource('category-hobby', 'CategoryHobbyController');
        Route::post('category-hobby/store', 'CategoryHobbyController@store');
        Route::get('category-hobby/delete/{id}', 'CategoryHobbyController@destroy');

        Route::resource('hobby', 'HobbyController');
        Route::post('hobby/store', 'HobbyController@store');
        Route::get('hobby/delete/{id}', 'HobbyController@destroy');

        Route::resource('hobby-det', 'HobbyDetController');
        Route::post('hobby-det/store', 'HobbyDetController@store');
        Route::get('hobby-det/delete/{id}', 'HobbyDetController@destroy');
      
       
        Route::resource('bid', 'BidsController');
        Route::post('bid/store', 'BidsController@store');
        Route::get('bid/delete/{id}', 'BidsController@destroy');

        Route::resource('kota', 'KotaController');
        Route::post('kota/store', 'KotaController@store');
        Route::get('kota/listprovinsi', 'KotaController@show');
        Route::get('kota/delete/{id}', 'KotaController@destroy');

        Route::resource('kecamatan', 'KecamatanController');
        Route::post('kecamatan/store', 'KecamatanController@store');
        Route::get('kecamatan/listkota', 'KecamatanController@show');
        Route::get('kecamatan/delete/{id}', 'KecamatanController@destroy');

        Route::resource('kelurahan', 'KelurahanController');
        Route::post('kelurahan/store', 'KelurahanController@store');
        Route::get('kelurahan/listkecamatan', 'KelurahanController@show');
        Route::get('kelurahan/delete/{id}', 'KelurahanController@destroy');

        

        Route::resource('sponsor', 'SponsorController');
        Route::post('sponsor/store', 'SponsorController@store');
        Route::get('sponsor/delete/{id}', 'SponsorController@destroy');

         Route::get('kelipatan', 'KelipatanController@index');
         Route::post('kelipatan/store', 'KelipatanController@store');
         Route::post('kelipatan/update', 'KelipatanController@update');
         Route::get('kelipatan/delete/{id}', 'KelipatanController@destroy');
        Route::get('kelipatan/edit/{id}', 'KelipatanController@edit');
        
        Route::get('slider', 'SliderController@index')->name('master.slider');
        Route::post('slider/store', 'SliderController@store');
        Route::post('slider/update', 'SliderController@update');
        Route::get('slider/delete/{id}', 'SliderController@destroy');
       Route::get('slider/edit/{id}', 'SliderController@edit');

       Route::get('profile', 'ProfileController@index');
       Route::post('profile/store', 'ProfileController@store');
       Route::post('profile/update', 'ProfileController@update');
       Route::get('profile/delete/{id}', 'ProfileController@destroy');
      Route::get('profile/edit/{id}', 'ProfileController@edit'); 
      Route::get('profile/listbank', 'ProfileController@getbank');

      Route::resource('setting', 'SettingController');
        Route::post('setting/store', 'SettingController@store');
        Route::get('setting/delete/{id}', 'SettingController@destroy');

        Route::get('member', 'MemberController@index');
        // Route::post('member/store', 'MemberController@store');
        // Route::post('member/update', 'MemberController@update');
        Route::get('member/banned/{id}', 'MemberController@banned');
      // Route::get('member/cancel/{id}', 'MemberController@edit');
       Route::get('member/active/{id}', 'MemberController@active');
       Route::get('member/listmember', 'MemberController@getmember');


      Route::get('category-forum', 'CategoryForumController@index');
      Route::post('category-forum/store', 'CategoryForumController@store');
      Route::post('category-forum/update', 'CategoryForumController@update');
      Route::get('category-forum/delete/{id}', 'CategoryForumController@destroy');
      Route::get('category-forum/edit/{id}', 'CategoryForumController@edit');

      Route::resource('mstforum', 'MstForumController');
      Route::post('mstforum/store', 'MstForumController@store');
      Route::get('mstforum/delete/{id}', 'MstForumController@destroy');
       
   
       
    });

    Route::group(['prefix' => 'transaksi', 'namespace' => 'Transaksi'], function(){
        Route::resource('barang', 'BarangController');
        Route::post('barang/store', 'BarangController@store');
        Route::get('barang/delete/{id}', 'BarangController@destroy');
        Route::get('barang/listdethobi', 'BarangController@show');
        Route::get('barang/create', 'BarangController@create');

        Route::resource('list-lelang', 'TLelangController');
        Route::post('list-lelang/store', 'TLelangController@store');
        Route::post('list-lelang/update', 'TLelangController@update');
        Route::get('list-lelang/delete/{id}', 'TLelangController@destroy');
        Route::get('list-lelang/get-member/{id}', 'TLelangController@get_member_by_id');

    //    Route::get('topup-saldo','TopupController@index');
        Route::get('topup-saldo', 'TopupController@index');
        Route::post('topup-saldo/store', 'TopupController@store');
        Route::post('topup-saldo/update', 'TopupController@update');
        Route::get('topup-saldo/delete/{id}', 'TopupController@destroy');
      // Route::get('topup-saldo/cancel/{id}', 'TopupController@edit');
       Route::get('topup-saldo/confirm/{id}', 'TopupController@confirm');
       Route::get('topup-saldo/listmember', 'TopupController@getmember');
     //  Route::get('topup-saldo/{any}', 'TopupController@index')->where('any', '.*');
    //    Route::get('topup-saldo/{any?}', function () {
    //     return view('backend.transaksi.topup.index');
    //    })->where('any', '[\/\w\.-]*');

      Route::get('list-jual', 'TJualController@index');
      Route::get('list-jual/verifikasi/{id}', 'TJualController@verifikasi');
      Route::get('list-jual/orderdet/{id}', 'TJualController@orderdet');
      Route::post('list-jual/update', 'TJualController@update');

      Route::get('list-complaint', 'TComplaintController@index');
      Route::post('list-complaint/answer', 'TComplaintController@answer');
      Route::get('list-complaint/complaintdet/{id}', 'TComplaintController@complaintdet');

      
      // Route::get('list-forum', 'CategoryForumController@index');
      // Route::post('list-forum/store', 'CategoryForumController@store');
      // Route::post('list-forum/update', 'CategoryForumController@update');
      // Route::get('list-forum/delete/{id}', 'CategoryForumController@destroy');
      // Route::get('list-forum/edit/{id}', 'CategoryForumController@edit');
    
      
    });
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/login/customer', 'Auth\LoginController@showCustomerLoginForm');
Route::get('/register/customer', 'Auth\RegisterController@showCustomerRegisterForm');
Route::post('/login/customer', 'Auth\LoginController@customerLogin');
Route::get('/logout/customer', 'Auth\LoginController@customerLogout');
Route::post('/register/customer', 'Auth\RegisterController@createCustomer');


Route::post('/post/create', 'PostController@store');
Route::get('/post/edit/{id}', 'PostController@edit');
Route::post('/post/update/{id}', 'PostController@update');
Route::delete('/post/delete/{id}', 'PostController@delete');
Route::get('/posts', 'PostController@index');



