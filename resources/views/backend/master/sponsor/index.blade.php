@extends('layouts.app')


@section('content')
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="content-wrapper">
    
    <section class="content-header">
        <h1>
            Sponsor
            <small>Master</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Master</a></li>
            <li class="active">Sponsor</li>
        </ol>
        </section>

   <section class="content">    
    <div class="box">
        <div class="box-header">
              <h2 class="box-title">Data Sponsor</h2>

            <div class="box-body">
                     <a href="javascript:void(0)" class="btn-sm btn-info ml-3" id="create-new-btn">Add New Sponsor</a>
                    <br>
                    <br>
                 
                    <table class="table table-bordered table-striped" id="tbl_id">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>No</th>
                                <th>Banner</th>
                                <th>Nama Sponsor</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        </table>
                 
              </div>
        </div>    
    </div>
  </div>
</section>

<div class="modal fade" id="mdl_frm" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="FrmModal"></h4>
    </div>
    <div class="modal-body">
        <form method="post" id="sponsorForm" name="sponsorForm" class="form-horizontal" enctype="multipart/form-data">
           <input type="hidden" name="sponsor_id" id="sponsor_id">
           <div class="form-group">
                <label for="name" class="col-sm-1 control-label">Banner</label>
                <div class="col-sm-12">
                    <input type="file" class="form-control" id="banner" name="banner" required="">
                    <span>Ukuran Gambar : 384 x 900 (Maks. 8 Mb)</span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                <label for="name" class="control-label">Nama Sponsor</label>
                </div>    
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Sponsor" value="" required="">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                <label for="name" class="control-label">Link Url Sponsor</label>
                </div>    
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="link" name="link" placeholder="Website Sponsor">
                </div>
            </div>
           
            <label class="control-label">Type</label>
            <div class="form-group">   
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="type_sponsor" name="type_sponsor" placeholder="Type" value="" required="">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-1 control-label">Status</label>
                <div class="col-sm-12">
                <select class="form-control select2" id="id_status" name="id_status" required="" style="width: 100%;">
                     <option value="ACTIVE">Aktif</option>
                     <option value="NON ACTIVE">Tidak Aktif</option>
                </select>
                </div>
              </div>

            

            <label class="control-label">Tgl Aktif</label>
            <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" id="tglactive" name="tglactive" require="" class="form-control pull-right">
                </div>

            <label class="control-label">No. Kontrak</label>
            <div class="form-group">   
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="no_contract" name="no_contract" placeholder="No. Kontrak" value="" required="">
                </div>
            </div>

            <label class="control-label">Waktu Kontrak</label>
            <div class="form-group">   
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="time_contract" name="time_contract" placeholder="Waktu Kontrak" value="" required="">
                </div>
            </div>

           

            
            <div class="col-sm-offset-2 col-sm-10">
             <button type="submit" class="btn btn-primary" id="btn-save" value="create">Save changes
             </button>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        
    </div>
</div>
</div>
</div>




@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

<script type="text/javascript"> 
var SITEURL = '{{URL::to('')}}';
var valInsUp;
$(document).ready( function () {
    $('.select2').select2()

    $('#tglactive').datepicker({
      autoclose: true,
      dateFormat: 'dd-mm-yy',
    
    })

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  $('#tbl_id').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: SITEURL + "/master/sponsor",
          type: 'GET',
         },
         columns: [
                  {data: 'id', name: 'id', 'visible': false},
                  {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                  { data: 'banner', name: 'banner' },
                  { data: 'name', name: 'name' },
                  { data: 'id_status', name: 'id_status' },
                  {data: 'action', name: 'action', orderable: false},
               ],
        order: [[0, 'desc']]
      });



    /*  When user click add user button */
    $('#create-new-btn').click(function () {
        $('#btn-save').val("create-master");
        $('#sponsor_id').val('');
        $('#sponsorForm').trigger("reset");
        $('#FrmModal').html("Add New Sponsor");
        $('#mdl_frm').modal('show');
    });  

     /* When click edit user */
     $('body').on('click', '.edit-btn', function () {
      var sponsor_id = $(this).data('id');
      $.get(SITEURL+'/master/sponsor/' + sponsor_id +'/edit', function (data) {
         $('#name-error').hide();
         $('#FrmModal').html("Edit Sponsor");
          $('#btn-save').val("edit-master");
          $('#btn-save').html("Update Changes");
          $('#mdl_frm').modal('show');
          $('#sponsor_id').val(data.id);
          $('#name').val(data.name);
          $('#type_sponsor').val(data.type_sponsor);
          $('#id_status').val(data.id_status);
          $('#tglactive').val(data.tglactive);
          $('#no_contract').val(data.no_contract);
          $('#time_contract').val(data.time_contract);
          $('#status_contract').val(data.status_contract);
          $('#banner').val(data.banner);
          $('#link').val(data.link);
        
      })
   });

   $('body').on('click', '#delete-btn', function () {
         if (confirm("Hapus data?")) {
            var sponsor_id = $(this).data("id");
            confirm("Are You sure want to delete !");

            $.ajax({
                type: "get",
                url: SITEURL + "/master/sponsor/delete/"+sponsor_id,
                success: function (data) {
                var oTable = $('#tbl_id').dataTable(); 
                oTable.fnDraw(false);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }else {
            
        }
       
    }); 
    
   
        
  
});


$('#sponsorForm').on('submit',(function(e) {
 
 $.ajaxSetup({
  
 headers: {
  
   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  
 }
  
 });
  
 e.preventDefault();
  
 var formData = new FormData(this);
  
  
  
 $.ajax({
  
    type:'POST',
  
    url: SITEURL + "/master/sponsor/store",
  
    data:formData,
  
    cache:false,
  
    contentType: false,
  
    processData: false,
  
    success:function(data){
  
        
        $('#sponsorForm').trigger("reset");
                    $('#mdl_frm').modal('hide');
                    $('#btn-save').html('Save Changes');
                    var oTable = $('#tbl_id').dataTable();
                    oTable.fnDraw(false);
  
    },
  
    error: function(data){
  
        console.log('Error:', data);
                     $('#btn-save').html('Save Changes');
  
    }
  
 });
  
 }));


// if ($("#sponsorForm").length > 0) {
//    // if(valInsUp === 1){
//         $("#sponsorForm").validate({
//             submitHandler: function(form) {
        
//             var actionType = $('#btn-save').val();
//             $('#btn-save').html('Sending..');
            
//             $.ajax({
//                 data: $('#sponsorForm').serialize(),
//                 url: SITEURL + "/master/sponsor/store",
//                 type: "POST",
//                 dataType: 'json',
//                 success: function (data) {
        
//                     $('#sponsorForm').trigger("reset");
//                     $('#mdl_frm').modal('hide');
//                     $('#btn-save').html('Save Changes');
//                     var oTable = $('#tbl_id').dataTable();
//                     oTable.fnDraw(false);
                    
//                 },
//                 error: function (data) {
//                     console.log('Error:', data);
//                     $('#btn-save').html('Save Changes');
//                 }
//             });
//             }
//         });
  

// }
</script>
@endpush
