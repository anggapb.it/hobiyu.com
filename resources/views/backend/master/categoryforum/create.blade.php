<div class="modal fade" id="modal_catforum" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="catforumCrudModal"></h4>
    </div>
    <div class="modal-body">
        <form id="catforumForm" name="catforumForm" class="form-horizontal">
           <input type="hidden" name="catforum_id" id="catforum_id">
            <div class="form-group">
                <label for="name" class="col-sm-3 control-label">Category Forum</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="" required="">
                </div>
            </div>

           
            <div class="col-sm-offset-2 col-sm-10">
             <button type="submit" class="btn btn-primary" id="btn-save" value="create">Save changes
             </button>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        
    </div>
</div>
</div>
</div>

