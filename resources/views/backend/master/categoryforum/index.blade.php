@extends('layouts.app')

<meta name="csrf-token" content="{{ csrf_token() }}">
@section('content')
<!-- CSRF Token -->


<div class="content-wrapper">
    
    <section class="content-header">
        <h1>
            Category Forum
            <small>Master</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Master</a></li>
            <li class="active">Category Forum</li>
        </ol>
        </section>

   <section class="content">    
    <div class="box">
        <div class="box-header">
              <h2 class="box-title">Data Category Forum</h2>

            <div class="box-body">
                     <a href="javascript:void(0)" class="btn-sm btn-info ml-3" id="create-new-catforum">Add New Category Forum</a>
                    <br>
                    <br>
                 
                    <table class="table table-bordered table-striped" id="tbl_catforum">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>No</th>
                                <th>Name</th>
                                <th>Created at</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        </table>
                 
              </div>
        </div>    
    </div>
  </div>
</section>

@include('backend.master.categoryforum.create')


@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

<script type="text/javascript"> 
var SITEURL = '{{URL::to('')}}';
var valInsUp;
$(document).ready( function () {
 

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  $('#tbl_catforum').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: SITEURL + "/master/category-forum",
          type: 'GET',
         },
         columns: [
                  {data: 'id', name: 'id', 'visible': false},
                  {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                  { data: 'name', name: 'name' },
                  { data: 'created_at', name: 'created_at' },
                  {data: 'action', name: 'action', orderable: false},
               ],
        order: [[0, 'desc']]
      });



    /*  When user click add user button */
    $('#create-new-catforum').click(function () {
        valInsUp = 1;
        $('#btn-save').val("create-catforum");
        $('#catforum_id').val('');
        $('#catforumForm').trigger("reset");
        $('#catforumCrudModal').html("Add New Category Forum");
        $('#modal_catforum').modal('show');
    });  

     /* When click edit user */
     $('body').on('click', '.edit-catforum', function () {
        valInsUp = 2;
      var catforum_id = $(this).data('id');
      $.get(SITEURL+'/master/category-forum/edit/'+catforum_id, function (data) {
         $('#name-error').hide();
         $('#keterangan-error').hide();
         $('#catforumCrudModal').html("Edit Category Forum");
          $('#btn-save').val("edit-catforum");
          $('#btn-save').html("Update Changes");
          $('#modal_catforum').modal('show');
          $('#catforum_id').val(data.id);
          $('#name').val(data.name);
        
      })
   });

   $('body').on('click', '#delete-catforum', function () {
         if (confirm("Hapus data?")) {
            var catforum_id = $(this).data("id");
            confirm("Are You sure want to delete !");

            $.ajax({
                type: "get",
                url: SITEURL + "/master/category-forum/delete/"+catforum_id,
                success: function (data) {
                var oTable = $('#tbl_catforum').dataTable(); 
                oTable.fnDraw(false);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }else {
            
        }
       
    }); 
    
   
        
  
});
if ($("#catforumForm").length > 0) {
   // if(valInsUp === 1){
        $("#catforumForm").validate({
            submitHandler: function(form) {
        
            var actionType = $('#btn-save').val();
            $('#btn-save').html('Sending..');
            
            $.ajax({
                data: $('#catforumForm').serialize(),
                url: SITEURL + "/master/category-forum/store",
                type: "POST",
                dataType: 'json',
                success: function (data) {
        
                    $('#catforumForm').trigger("reset");
                    $('#modal_catforum').modal('hide');
                    $('#btn-save').html('Save Changes');
                    var oTable = $('#tbl_catforum').dataTable();
                    oTable.fnDraw(false);
                    
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#btn-save').html('Save Changes');
                }
            });
            }
        });
    // } else if(valInsUp === 2){
    //     alert('ini update');
    // }

}
</script>
@endpush
