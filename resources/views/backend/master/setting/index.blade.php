@extends('layouts.app')


@section('content')
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="content-wrapper">
    
    <section class="content-header">
        <h1>
            Setting
            <small>Master</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Master</a></li>
            <li class="active">Setting</li>
        </ol>
        </section>

   <section class="content">    
    <div class="box">
        <div class="box-header">
              <h2 class="box-title">Data Setting</h2>

            <div class="box-body">
                     <a href="javascript:void(0)" class="btn-sm btn-info ml-3" id="create-new-btn">Add New Setting</a>
                    <br>
                    <br>
                 
                    <table class="table table-bordered table-striped" id="tbl_id">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Nilai</th>
                                <th>Keterangan</th>
                                <th>Attribute</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        </table>
                 
              </div>
        </div>    
    </div>
  </div>
</section>

<div class="modal fade" id="mdl_frm" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="FrmModal"></h4>
    </div>
    <div class="modal-body">
        <form id="settingForm" name="settingForm" class="form-horizontal">
           <input type="hidden" name="setting_id" id="setting_id">
            <div class="form-group">
                <div class="col-sm-3">
                <label for="name" class="control-label">Nama</label>
                </div>    
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="name" name="name" required="">
                </div>
            </div>
           
            <div class="form-group">
                <div class="col-sm-3">
                <label for="name" class="control-label">Nilai</label>
                </div>    
                <div class="col-sm-12">
                    <input type="number" class="form-control" id="nilai" name="nilai" required="">
                </div>
            </div>
           
            <div class="form-group">
                <div class="col-sm-3">
                <label for="name" class="control-label">Keterangan</label>
                </div>    
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="keterangan" name="keterangan" >
                </div>
            </div>
           
            <div class="form-group">
                <div class="col-sm-3">
                <label for="name" class="control-label">Attribute</label>
                </div>    
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="attribute" name="attribute" >
                </div>
            </div>
           
       
            
            <div class="col-sm-offset-2 col-sm-10">
             <button type="submit" class="btn btn-primary" id="btn-save" value="create">Save changes
             </button>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        
    </div>
</div>
</div>
</div>




@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

<script type="text/javascript"> 
var SITEURL = '{{URL::to('')}}';
var valInsUp;
$(document).ready( function () {
    $('.select2').select2()

    $('#tglactive').datepicker({
      autoclose: true,
      dateFormat: 'dd-mm-yy',
    
    })

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  $('#tbl_id').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: SITEURL + "/master/setting",
          type: 'GET',
         },
         columns: [
                  {data: 'id', name: 'id', 'visible': false},
                  {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                  { data: 'name', name: 'name' },
                  { data: 'nilai', name: 'nilai' },
                  { data: 'keterangan', name: 'keterangan' },
                  { data: 'attribute', name: 'attribute' },
                  {data: 'action', name: 'action', orderable: false},
               ],
        order: [[0, 'desc']]
      });



    /*  When user click add user button */
    $('#create-new-btn').click(function () {
        $('#btn-save').val("create-master");
        $('#setting_id').val('');
        $('#settingForm').trigger("reset");
        $('#FrmModal').html("Add New Setting");
        $('#mdl_frm').modal('show');
    });  

     /* When click edit user */
     $('body').on('click', '.edit-btn', function () {
      var setting_id = $(this).data('id');
      $.get(SITEURL+'/master/setting/' + setting_id +'/edit', function (data) {
         $('#name-error').hide();
         $('#FrmModal').html("Edit Setting");
          $('#btn-save').val("edit-master");
          $('#btn-save').html("Update Changes");
          $('#mdl_frm').modal('show');
          $('#setting_id').val(data.id);
          $('#name').val(data.name);
          $('#nilai').val(data.nilai);
          $('#keterangan').val(data.keterangan);
          $('#attribute').val(data.attribute);
        
      })
   });

   $('body').on('click', '#delete-btn', function () {
         if (confirm("Hapus data?")) {
            var setting_id = $(this).data("id");
            confirm("Are You sure want to delete !");

            $.ajax({
                type: "get",
                url: SITEURL + "/master/setting/delete/"+setting_id,
                success: function (data) {
                var oTable = $('#tbl_id').dataTable(); 
                oTable.fnDraw(false);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }else {
            
        }
       
    }); 
    
   
        
  
});
if ($("#settingForm").length > 0) {
   // if(valInsUp === 1){
        $("#settingForm").validate({
            submitHandler: function(form) {
        
            var actionType = $('#btn-save').val();
            $('#btn-save').html('Sending..');
            
            $.ajax({
                data: $('#settingForm').serialize(),
                url: SITEURL + "/master/setting/store",
                type: "POST",
                dataType: 'json',
                success: function (data) {
        
                    $('#settingForm').trigger("reset");
                    $('#mdl_frm').modal('hide');
                    $('#btn-save').html('Save Changes');
                    var oTable = $('#tbl_id').dataTable();
                    oTable.fnDraw(false);
                    
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#btn-save').html('Save Changes');
                }
            });
            }
        });
  

}
</script>
@endpush
