@extends('layouts.app')


@section('content')
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="content-wrapper">
    
    <section class="content-header">
        <h1>
            Kelurahan
            <small>Master</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Master</a></li>
            <li class="active">Kelurahan</li>
        </ol>
        </section>

   <section class="content">    
    <div class="box">
        <div class="box-header">
              <h2 class="box-title">Data Kelurahan</h2>

            <div class="box-body">
                     <a href="javascript:void(0)" class="btn-sm btn-info ml-3" id="create-new-btn">Add New Kelurahan</a>
                    <br>
                    <br>
                 
                    <table class="table table-bordered table-striped" id="tbl_id">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>No</th>
                                <th>Kelurahan</th>
                                <th>Kecamatan</th>
                                <th>Created at</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        </table>
                 
              </div>
        </div>    
    </div>
  </div>
</section>

<div class="modal fade" id="mdl_frm" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="FrmModal"></h4>
    </div>
    <div class="modal-body">
        <form id="kelForm" name="kelForm" class="form-horizontal">
           <input type="hidden" name="kelurahan_id" id="kelurahan_id">
            <div class="form-group">
                <label for="name" class="col-sm-1 control-label">Kelurahan</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Nama Kelurahan" value="" required="">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-1 control-label">Kecamatan</label>
                <div class="col-sm-12">
                <select class="form-control select2" id="id_kecamatan" name="id_kecamatan" style="width: 100%;">
                     <option value="">Pilih Kecamatan</option>
                </select>
                </div>
              </div>
 
            <div class="col-sm-offset-2 col-sm-10">
             <button type="submit" class="btn btn-primary" id="btn-save" value="create">Save changes
             </button>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        
    </div>
</div>
</div>
</div>




@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

<script type="text/javascript"> 
var SITEURL = '{{URL::to('')}}';
var valInsUp;
$(document).ready( function () {
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  
    $.ajax({
            url: SITEURL + "/master/kelurahan/listkecamatan",
			type: 'GET',
			success: function(data) {
			//	var jsonResult = jQuery.parseJSON( data );
            JSON.stringify(data); //to string
            if(data.length > 0){
                for(i=0; i< data.length; i++){
                    var currenData = data[i];
                    var opt_kec = '<option value="'+ currenData.id +'">'+ currenData.name +'</option>';
				    $('#id_kecamatan').append(opt_kec);
                }
            }
            $('#id_kecamatan option:first-child').attr("selected", "selected");

			}
		});
 
    $('.select2').select2()

 
  $('#tbl_id').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: SITEURL + "/master/kelurahan",
          type: 'GET',
         },
         columns: [
                  {data: 'id', name: 'id', 'visible': false},
                  {data: 'id_kecamatan', name: 'id_kecamatan', 'visible': false},
                  {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                  { data: 'name', name: 'name' },
                  { data: 'nmkecamatan', name: 'nmkecamatan' },
                  { data: 'created_at', name: 'created_at' },
                  {data: 'action', name: 'action', orderable: false},
               ],
        order: [[0, 'desc']]
      });



    /*  When user click add user button */
    $('#create-new-btn').click(function () {
        $('#btn-save').val("create-master");
        $('#kelurahan_id').val('');
        $('#kelForm').trigger("reset");
        $('#FrmModal').html("Add New Kelurahan");
        $('#mdl_frm').modal('show');
    });  

     /* When click edit user */
     $('body').on('click', '.edit-btn', function () {
      var kelurahan_id = $(this).data('id');
      $.get(SITEURL+'/master/kelurahan/' + kelurahan_id +'/edit', function (data) {
         $('#name-error').hide();
         $('#FrmModal').html("Edit Kelurahan");
          $('#btn-save').val("edit-master");
          $('#btn-save').html("Update Changes");
          $('#mdl_frm').modal('show');
          $('#kelurahan_id').val(data.id);
          $('#name').val(data.name);
          $('#id_kecamatan').val(data.id_kecamatan);
        
      })
   });

   $('body').on('click', '#delete-btn', function () {
         if (confirm("Hapus data?")) {
            var kelurahan_id = $(this).data("id");
            confirm("Are You sure want to delete !");

            $.ajax({
                type: "get",
                url: SITEURL + "/master/kelurahan/delete/"+kelurahan_id,
                success: function (data) {
                var oTable = $('#tbl_id').dataTable(); 
                oTable.fnDraw(false);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }else {
            
        }
       
    }); 
    
   
        
  
});
if ($("#kelForm").length > 0) {
   // if(valInsUp === 1){
        $("#kelForm").validate({
            submitHandler: function(form) {
        
            var actionType = $('#btn-save').val();
            $('#btn-save').html('Sending..');
            
            $.ajax({
                data: $('#kelForm').serialize(),
                url: SITEURL + "/master/kelurahan/store",
                type: "POST",
                dataType: 'json',
                success: function (data) {
        
                    $('#kelForm').trigger("reset");
                    $('#mdl_frm').modal('hide');
                    $('#btn-save').html('Save Changes');
                    var oTable = $('#tbl_id').dataTable();
                    oTable.fnDraw(false);
                    
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#btn-save').html('Save Changes');
                }
            });
            }
        });
  

}


</script>
@endpush
