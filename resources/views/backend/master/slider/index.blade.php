@extends('layouts.app')

<meta name="csrf-token" content="{{ csrf_token() }}">
@section('content')
<!-- CSRF Token -->


<div class="content-wrapper">
    
    <section class="content-header">
        <h1>
            Slider
            <small>Master</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Master</a></li>
            <li class="active">Slider</li>
        </ol>
        </section>
        @if ($message = Session::get('success'))
              <div class="alert alert-success">
                  <p>{{ $message }}</p>
              </div>
          @endif
   <section class="content">    
    <div class="box">
        <div class="box-header">
              <h2 class="box-title">Data Slider</h2>

            <div class="box-body">
                     <a href="javascript:void(0)" class="btn-sm btn-info ml-3" id="create-new-slider">Add New Slider</a>
                    <br>
                    <br>
                 
                    <table class="table table-bordered table-striped" id="tbl_slider">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>No</th>
                                <th>Slider</th>
                                <th>Keterangan</th>
                              
                                <th>Created at</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        </table>
                 
              </div>
        </div>    
    </div>
  </div>
</section>

@include('backend.master.slider.create')


@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

<script type="text/javascript"> 
var SITEURL = '{{URL::to('')}}';
var valInsUp;
$(document).ready( function () {
 

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  $('#tbl_slider').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: SITEURL + "/master/slider",
          type: 'GET',
         },
         columns: [
                  {data: 'id', name: 'id', 'visible': false},
                  {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                  { data: 'slider_image', name: 'slider_image' },
                  { data: 'keterangan', name: 'keterangan' },
                  { data: 'created_at', name: 'created_at' },
                  {data: 'action', name: 'action', orderable: false},
               ],
        order: [[0, 'desc']]
      });



    /*  When user click add user button */
    $('#create-new-slider').click(function () {
        valInsUp = 1;
        $('#btn-save').val("create-slider");
        $('#slider_id').val('');
        $('#sliderForm').trigger("reset");
        $('#sliderCrudModal').html("Add New Slider");
        $('#modal_slider').modal('show');
    });  

     /* When click edit user */
     $('body').on('click', '.edit-slider', function () {
        valInsUp = 2;
      var slider_id = $(this).data('id');
      $.get(SITEURL+'/master/slider/edit/'+slider_id, function (data) {
         $('#name-error').hide();
         $('#keterangan-error').hide();
         $('#sliderCrudModal').html("Edit Slider");
          $('#btn-save').val("edit-slider");
          $('#btn-save').html("Update Changes");
          $('#modal_slider').modal('show');
          $('#slider_id').val(data.id);
          $('#keterangan').val(data.keterangan);
          $('#slider').val(data.slider);
        
      })
   });

   $('body').on('click', '#delete-slider', function () {
         if (confirm("Hapus data?")) {
            var slider_id = $(this).data("id");
            confirm("Are You sure want to delete !");

            $.ajax({
                type: "get",
                url: SITEURL + "/master/slider/delete/"+slider_id,
                success: function (data) {
                var oTable = $('#tbl_slider').dataTable(); 
                oTable.fnDraw(false);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }else {
            
        }
       
    }); 
    
   
        
  
});
// if ($("#sliderForm").length > 0) {
//    // if(valInsUp === 1){
//         $("#sliderForm").validate({
//             submitHandler: function(form) {
        
//             var actionType = $('#btn-save').val();
//             $('#btn-save').html('Sending..');
            
//             $.ajax({
//                 data: $('#sliderForm').serialize(),
//                 //enctype: 'multipart/form-data',
//                 processData: false,
//                 cache: false,
//                 url: SITEURL + "/master/slider/store",
//                 type: "POST",
//                 dataType: 'json',
//                 success: function (data) {
        
//                     $('#sliderForm').trigger("reset");
//                     $('#modal_slider').modal('hide');
//                     $('#btn-save').html('Save Changes');
//                     var oTable = $('#tbl_slider').dataTable();
//                     oTable.fnDraw(false);
                    
//                 },
//                 error: function (data) {
//                     console.log('Error:', data);
//                     $('#btn-save').html('Save Changes');
//                 }
//             });
//             }
//         });
    

// }

$('#sliderForm').on('submit',(function(e) {
 
 $.ajaxSetup({
  
 headers: {
  
   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  
 }
  
 });
  
 e.preventDefault();
  
 var formData = new FormData(this);
  
  
  
 $.ajax({
  
    type:'POST',
  
    url: SITEURL + "/master/slider/store",
  
    data:formData,
  
    cache:false,
  
    contentType: false,
  
    processData: false,
  
    success:function(data){
  
        
        $('#sliderForm').trigger("reset");
                    $('#modal_slider').modal('hide');
                    $('#btn-save').html('Save Changes');
                    var oTable = $('#tbl_slider').dataTable();
                    oTable.fnDraw(false);
  
    },
  
    error: function(data){
  
        console.log('Error:', data);
                    $('#btn-save').html('Save Changes');
  
    }
  
 });
  
 }));
</script>
@endpush
