<div class="modal fade" id="modal_slider" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="userCrudModal"></h4>
    </div>
    <div class="modal-body">
        <form method="post" id="sliderForm" name="sliderForm" class="form-horizontal" enctype="multipart/form-data">
           <input type="hidden" name="slider_id" id="slider_id">
            <div class="form-group">
                <label for="name" class="col-sm-1 control-label">Slider</label>
                <div class="col-sm-12">
                    <input type="file" class="form-control" id="slider_image" name="slider_image" required="">
                    <span>Ukuran Gambar : 847 x 370 (Maks. 8 Mb)</span>
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-sm-1 control-label">Keterangan</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan">
                </div>
            </div>

           
            <div class="col-sm-offset-2 col-sm-10">
             <button type="submit" class="btn btn-primary" id="btn-save" value="create">Save changes
             </button>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        
    </div>
</div>
</div>
</div>

