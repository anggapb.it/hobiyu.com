@extends('layouts.app')


@section('content')
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="content-wrapper">
    
    <section class="content-header">
        <h1>
            Hobby
            <small>Master</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Master</a></li>
            <li class="active">Hobby</li>
        </ol>
        </section>

   <section class="content">    
    <div class="box">
        <div class="box-header">
              <h2 class="box-title">Data Hobby</h2>

            <div class="box-body">
                     <a href="javascript:void(0)" class="btn-sm btn-info ml-3" id="create-new-btn">Add New Hobby</a>
                    <br>
                    <br>
                 
                    <table class="table table-bordered table-striped" id="tbl_id">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>No</th>
                                <th>Hobby</th>
                                <th>Keterangan</th>
                                <th>Status</th>
                                <th>Icon</th>
                                <th>Created at</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        </table>
                 
              </div>
        </div>    
    </div>
  </div>
</section>

<div class="modal fade" id="mdl_frm" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="FrmModal"></h4>
    </div>
    <div class="modal-body">
        <form id="hobbyForm" name="hobbyForm" class="form-horizontal">
           <input type="hidden" name="hobby_id" id="hobby_id">
            <div class="form-group">
                <div class="col-sm-3">
                <label for="name" class="control-label">Hobby</label>
                </div>    
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Hobby" value="" required="">
                </div>
            </div>

            <div class="form-group">
                <label for="keterangan" class="col-sm-1 control-label">Keterangan</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan" value="" required="">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-1 control-label">Icon</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="icon" name="icon" placeholder="Example : icon fa fa-music">
                </div>
                <div class="col-sm-12"><a href="#" onclick="window.open('https://adminlte.io/themes/AdminLTE/pages/UI/icons.html', '_blank');"> (Lihat Kode Icon)</a></div>
            </div>

            <div class="form-group">
                <label class="col-sm-1 control-label">Status</label>
                <div class="col-sm-12">
                <select class="form-control select2" id="id_status" name="id_status" required="" style="width: 100%;">
                     <option value="ACTIVE">Aktif</option>
                     <option value="NON ACTIVE">Tidak Aktif</option>
                </select>
                </div>
              </div>
 
            <div class="col-sm-offset-2 col-sm-10">
             <button type="submit" class="btn btn-primary" id="btn-save" value="create">Save changes
             </button>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        
    </div>
</div>
</div>
</div>




@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

<script type="text/javascript"> 
var SITEURL = '{{URL::to('')}}';
var valInsUp;
$(document).ready( function () {
 

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  $('#tbl_id').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: SITEURL + "/master/hobby",
          type: 'GET',
         },
         columns: [
                  {data: 'id', name: 'id', 'visible': false},
                  {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                  { data: 'name', name: 'name' },
                  { data: 'keterangan', name: 'keterangan' },
                  { data: 'id_status', name: 'id_status' },
                  { data: 'icon', name: 'icon' },
                  { data: 'created_at', name: 'created_at' },
                  {data: 'action', name: 'action', orderable: false},
               ],
        order: [[0, 'desc']]
      });



    /*  When user click add user button */
    $('#create-new-btn').click(function () {
        $('#btn-save').val("create-master");
        $('#hobby_id').val('');
        $('#hobbyForm').trigger("reset");
        $('#FrmModal').html("Add New Hobby");
        $('#mdl_frm').modal('show');
    });  

     /* When click edit user */
     $('body').on('click', '.edit-btn', function () {
      var hobby_id = $(this).data('id');
      $.get(SITEURL+'/master/hobby/' + hobby_id +'/edit', function (data) {
         $('#name-error').hide();
         $('#FrmModal').html("Edit Hobby");
          $('#btn-save').val("edit-master");
          $('#btn-save').html("Update Changes");
          $('#mdl_frm').modal('show');
          $('#hobby_id').val(data.id);
          $('#name').val(data.name);
          $('#keterangan').val(data.keterangan);
          $('#id_status').val(data.id_status);
        
      })
   });

   $('body').on('click', '#delete-btn', function () {
         if (confirm("Hapus data?")) {
            var hobby_id = $(this).data("id");
            confirm("Are You sure want to delete !");

            $.ajax({
                type: "get",
                url: SITEURL + "/master/hobby/delete/"+hobby_id,
                success: function (data) {
                var oTable = $('#tbl_id').dataTable(); 
                oTable.fnDraw(false);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }else {
            
        }
       
    }); 
    
   
        
  
});
if ($("#hobbyForm").length > 0) {
   // if(valInsUp === 1){
        $("#hobbyForm").validate({
            submitHandler: function(form) {
        
            var actionType = $('#btn-save').val();
            $('#btn-save').html('Sending..');
            
            $.ajax({
                data: $('#hobbyForm').serialize(),
                url: SITEURL + "/master/hobby/store",
                type: "POST",
                dataType: 'json',
                success: function (data) {
        
                    $('#hobbyForm').trigger("reset");
                    $('#mdl_frm').modal('hide');
                    $('#btn-save').html('Save Changes');
                    var oTable = $('#tbl_id').dataTable();
                    oTable.fnDraw(false);
                    
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#btn-save').html('Save Changes');
                }
            });
            }
        });
  

}
</script>
@endpush
