<div class="modal fade" id="modal_kelipatan" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="kelipatanCrudModal"></h4>
    </div>
    <div class="modal-body">
        <form id="kelipatanForm" name="kelipatanForm" class="form-horizontal">
           <input type="hidden" name="kelipatan_id" id="kelipatan_id">
            <div class="form-group">
                <label for="name" class="col-sm-1 control-label">Kelipatan</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Jumlah Kelipatan" value="" maxlength="50" required="">
                </div>
            </div>

           
            <div class="col-sm-offset-2 col-sm-10">
             <button type="submit" class="btn btn-primary" id="btn-save" value="create">Save changes
             </button>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        
    </div>
</div>
</div>
</div>

