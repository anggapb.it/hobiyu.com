@extends('layouts.app')

<meta name="csrf-token" content="{{ csrf_token() }}">
@section('content')
<!-- CSRF Token -->


<div class="content-wrapper">
    
    <section class="content-header">
        <h1>
            Kelipatan
            <small>Master</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Master</a></li>
            <li class="active">Kelipatan</li>
        </ol>
        </section>

   <section class="content">    
    <div class="box">
        <div class="box-header">
              <h2 class="box-title">Data Kelipatan</h2>

            <div class="box-body">
                     <a href="javascript:void(0)" class="btn-sm btn-info ml-3" id="create-new-kelipatan">Add New Kelipatan</a>
                    <br>
                    <br>
                 
                    <table class="table table-bordered table-striped" id="tbl_kelipatan">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>No</th>
                                <th>Kelipatan</th>
                                <th>Created at</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        </table>
                 
              </div>
        </div>    
    </div>
  </div>
</section>

@include('backend.master.kelipatan.create')


@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

<script type="text/javascript"> 
var SITEURL = '{{URL::to('')}}';
var valInsUp;
$(document).ready( function () {
 

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  $('#tbl_kelipatan').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: SITEURL + "/master/kelipatan",
          type: 'GET',
         },
         columns: [
                  {data: 'id', name: 'id', 'visible': false},
                  {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                  { data: 'name', name: 'name' },
                  { data: 'created_at', name: 'created_at' },
                  {data: 'action', name: 'action', orderable: false},
               ],
        order: [[0, 'desc']]
      });



    /*  When user click add user button */
    $('#create-new-kelipatan').click(function () {
        valInsUp = 1;
        $('#btn-save').val("create-kelipatan");
        $('#kelipatan_id').val('');
        $('#kelipatanForm').trigger("reset");
        $('#kelipatanCrudModal').html("Add New Kelipatan");
        $('#modal_kelipatan').modal('show');
    });  

     /* When click edit user */
     $('body').on('click', '.edit-kelipatan', function () {
        valInsUp = 2;
      var kelipatan_id = $(this).data('id');
      $.get(SITEURL+'/master/kelipatan/edit/'+kelipatan_id, function (data) {
         $('#name-error').hide();
         $('#keterangan-error').hide();
         $('#kelipatanCrudModal').html("Edit Kelipatan");
          $('#btn-save').val("edit-kelipatan");
          $('#btn-save').html("Update Changes");
          $('#modal_kelipatan').modal('show');
          $('#kelipatan_id').val(data.id);
          $('#name').val(data.name);
        
      })
   });

   $('body').on('click', '#delete-kelipatan', function () {
         if (confirm("Hapus data?")) {
            var kelipatan_id = $(this).data("id");
            confirm("Are You sure want to delete !");

            $.ajax({
                type: "get",
                url: SITEURL + "/master/kelipatan/delete/"+kelipatan_id,
                success: function (data) {
                var oTable = $('#tbl_kelipatan').dataTable(); 
                oTable.fnDraw(false);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }else {
            
        }
       
    }); 
    
   
        
  
});
if ($("#kelipatanForm").length > 0) {
   // if(valInsUp === 1){
        $("#kelipatanForm").validate({
            submitHandler: function(form) {
        
            var actionType = $('#btn-save').val();
            $('#btn-save').html('Sending..');
            
            $.ajax({
                data: $('#kelipatanForm').serialize(),
                url: SITEURL + "/master/kelipatan/store",
                type: "POST",
                dataType: 'json',
                success: function (data) {
        
                    $('#kelipatanForm').trigger("reset");
                    $('#modal_kelipatan').modal('hide');
                    $('#btn-save').html('Save Changes');
                    var oTable = $('#tbl_kelipatan').dataTable();
                    oTable.fnDraw(false);
                    
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#btn-save').html('Save Changes');
                }
            });
            }
        });
    // } else if(valInsUp === 2){
    //     alert('ini update');
    // }

}
</script>
@endpush
