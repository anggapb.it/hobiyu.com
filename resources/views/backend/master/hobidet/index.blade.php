@extends('layouts.app')


@section('content')
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="content-wrapper">
    
    <section class="content-header">
        <h1>
            Detail Hobby
            <small>Master</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Master</a></li>
            <li class="active">Detail Hobby</li>
        </ol>
        </section>

   <section class="content">    
    <div class="box">
        <div class="box-header">
              <h2 class="box-title">Data Detail Hobby</h2>

            <div class="box-body">
                     <a href="javascript:void(0)" class="btn-sm btn-info ml-3" id="create-new-btn">Add New Detail Hobby</a>
                    <br>
                    <br>
                 
                    <table class="table table-bordered table-striped" id="tbl_id">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>No</th>
                                <th>Detail Hobby</th>
                                <th>Hobby</th>
                                <th>Kategori 1</th>
                                <th>Kategori 2</th>
                                <th>Status</th>
                                <th>Created at</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        </table>
                 
              </div>
        </div>    
    </div>
  </div>
</section>

<div class="modal fade" id="mdl_frm" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="FrmModal"></h4>
    </div>
    <div class="modal-body">
        <form id="hobbydetForm" name="hobbydetForm" class="form-horizontal">
           <input type="hidden" name="hobby_det_id" id="hobby_det_id">
           
           <label class="control-label">Hobby</label>
           <div class="form-group">
                <div class="col-sm-12">
                <select class="form-control select2" id="id_hobby" name="id_hobby" required="" style="width: 100%;">
                     <option value="">Pilih Hobby</option>
                     @foreach($hobi as $list_hobi)
                        <option value="{{ $list_hobi->id }}" {{ isset($selected_hobi) && $selected_hobi->id == $list_hobi->id ? 'selected="selected"' : '' }}>{{ $list_hobi->name }}</option>
                     @endforeach
                </select>
                </div>
              </div>
              
            <label class="control-label">Detail Hobby</label>
            <div class="form-group">
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Nama Detail Hobby" value="" required="">
                </div>
            </div>

          

              <div class="form-group">
                <label class="col-sm-2 control-label">Kategori 1</label>
                <div class="col-sm-12">
                <select class="form-control cbkatsatu" id="id_kathobi_satu" name="id_kathobi_satu" required="" style="width: 100%;">
                     <option value="">Pilih Kategori 1</option>
                     @foreach($cathobi as $list_cathobi)
                        <option value="{{ $list_cathobi->id }}" {{ (isset($selected_cathobi) && $selected_cathobi->id == $list_cathobi->id) ? 'selected="selected"' : ''  }}>{{ $list_cathobi->name }}</option>
                    @endforeach
                </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Kategori 2</label>
                <div class="col-sm-12">
                <select class="form-control cbkatdua" id="id_kathobi_dua" name="id_kathobi_dua" style="width: 100%;">
                     <option value="">Pilih Kategori 2</option>
                     @foreach($cathobi as $list_cathobi)
                        <option value="{{ $list_cathobi->id }}" {{ (isset($selected_cathobi) && $selected_cathobi->id == $list_cathobi->id) ? 'selected="selected"' : ''  }}>{{ $list_cathobi->name }}</option>
                    @endforeach
                </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-1 control-label">Status</label>
                <div class="col-sm-12">
                <select class="form-control select2" id="id_status" name="id_status" required="" style="width: 100%;">
                     <option value="ACTIVE">Aktif</option>
                     <option value="NON ACTIVE">Tidak Aktif</option>
                </select>
                </div>
              </div>
 
 
            <div class="col-sm-offset-2 col-sm-10">
             <button type="submit" class="btn btn-primary" id="btn-save" value="create">Save changes
             </button>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        
    </div>
</div>
</div>
</div>




@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

<script type="text/javascript"> 
var SITEURL = '{{URL::to('')}}';
var valInsUp;
$(document).ready( function () {
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  
 
    $('.select2').select2();
    $('.cbkatsatu').select2();
    $('.cbkatdua').select2();

 
  $('#tbl_id').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: SITEURL + "/master/hobby-det",
          type: 'GET',
         },
         columns: [
                  {data: 'id', name: 'id', 'visible': false},
                  {data: 'id_hobi', name: 'id_hobi', 'visible': false},
                  {data: 'id_kathobi_satu', name: 'id_kathobi_satu', 'visible': false},
                  {data: 'id_kathobi_dua', name: 'id_kathobi_dua', 'visible': false},
                  {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                  { data: 'name', name: 'name' },
                  { data: 'nmhobi', name: 'nmhobi' },
                  { data: 'nmkathobi1', name: 'nmkathobi1' },
                  { data: 'nmkathobi2', name: 'nmkathobi2' },
                  { data: 'id_status', name: 'id_status' },
                  { data: 'created_at', name: 'created_at' },
                  {data: 'action', name: 'action', orderable: false},
               ],
        order: [[0, 'desc']]
      });



    /*  When user click add user button */
    $('#create-new-btn').click(function () {
        $('#btn-save').val("create-master");
        $('#hobby_det_id').val('');
        $('#hobbydetForm').trigger("reset");
        $('#FrmModal').html("Add New Detail Hobby");
        $('#mdl_frm').modal('show');
    });  

     /* When click edit user */
     $('body').on('click', '.edit-btn', function () {
      var hobby_det_id = $(this).data('id');
      $.get(SITEURL+'/master/hobby-det/' + hobby_det_id +'/edit', function (data) {
         $('#name-error').hide();
         $('#FrmModal').html("Edit Detail Hobby");
          $('#btn-save').val("edit-master");
          $('#btn-save').html("Update Changes");
          $('#mdl_frm').modal('show');
          $('#hobby_det_id').val(data.id);
          $('#name').val(data.name);
          $('#id_hobby').val(data.id_hobi);
          $('#id_kathobi_satu').val(data.id_kathobi_satu);
          $('.cbkatdua').val(data.id_kathobi_dua);
          $('#id_status').val(data.id_status);
        
      })
   });

   $('body').on('click', '#delete-btn', function () {
         if (confirm("Hapus data?")) {
            var hobby_det_id = $(this).data("id");
            confirm("Are You sure want to delete !");

            $.ajax({
                type: "get",
                url: SITEURL + "/master/hobby-det/delete/"+hobby_det_id,
                success: function (data) {
                var oTable = $('#tbl_id').dataTable(); 
                oTable.fnDraw(false);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }else {
            
        }
       
    }); 
    
   
        
  
});
if ($("#hobbydetForm").length > 0) {
   // if(valInsUp === 1){
        $("#hobbydetForm").validate({
            submitHandler: function(form) {
        
            var actionType = $('#btn-save').val();
            $('#btn-save').html('Sending..');
            
            $.ajax({
                data: $('#hobbydetForm').serialize(),
                url: SITEURL + "/master/hobby-det/store",
                type: "POST",
                dataType: 'json',
                success: function (data) {
        
                    $('#hobbydetForm').trigger("reset");
                    $('#mdl_frm').modal('hide');
                    $('#btn-save').html('Save Changes');
                    var oTable = $('#tbl_id').dataTable();
                    oTable.fnDraw(false);
                    
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#btn-save').html('Save Changes');
                }
            });
            }
        });
  

}


</script>
@endpush
