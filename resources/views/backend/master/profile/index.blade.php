@extends('layouts.app')

<meta name="csrf-token" content="{{ csrf_token() }}">
@section('content')
<!-- CSRF Token -->


<div class="content-wrapper">
    
    <section class="content-header">
        <h1>
            Profile
            <small>Master</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Master</a></li>
            <li class="active">Profile</li>
        </ol>
        </section>

   <section class="content">    
    <div class="box">
        <div class="box-header">
              <h2 class="box-title">Data Profile</h2>

            <div class="box-body">
                     <!-- <a href="javascript:void(0)" class="btn-sm btn-info ml-3" id="create-new-profile">Add New Profile</a> -->
                    <br>
                    <br>
                 
                    <table class="table table-bordered table-striped" id="tbl_profile">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>No</th>
                                <th>Brand</th>
                                <th>Phone 1</th>
                                <th>Phone 2</th>
                                <th>Alamat</th>
                                <th>Email</th>
                                <th>FB</th>
                                <th>Twitter</th>
                                <th>Youtube</th>
                                <th>Created at</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        </table>
                 
              </div>
        </div>    
    </div>
  </div>
</section>

@include('backend.master.profile.create')


@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

<script type="text/javascript"> 
var SITEURL = '{{URL::to('')}}';
var valInsUp;
$(document).ready( function () {
 

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $.ajax({
            url: SITEURL + "/master/profile/listbank",
			type: 'GET',
			success: function(data) {
			//	var jsonResult = jQuery.parseJSON( data );
            JSON.stringify(data); //to string
            if(data.length > 0){
                for(i=0; i< data.length; i++){
                    var currenData = data[i];
                    var opt_bank = '<option value="'+ currenData.id +'">'+ currenData.name +'</option>';
				    $('#idbank1').append(opt_bank);
				    $('#idbank2').append(opt_bank);
				    $('#idbank3').append(opt_bank);
                }
            }
            $('#idbank1 option:first-child').attr("selected", "selected");
            $('#idbank2 option:first-child').attr("selected", "selected");
            $('#idbank3 option:first-child').attr("selected", "selected");

			}
		});

  $('#tbl_profile').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: SITEURL + "/master/profile",
          type: 'GET',
         },
         columns: [
                  {data: 'id', name: 'id', 'visible': false},
                  {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                  { data: 'brand_name', name: 'brand_name' },
                  { data: 'phone1', name: 'phone1' },
                  { data: 'phone2', name: 'phone2' },
                  { data: 'address', name: 'address' },
                  { data: 'email', name: 'email' },
                  { data: 'facebook', name: 'facebook' },
                  { data: 'twitter', name: 'twitter' },
                  { data: 'youtube', name: 'youtube' },
                  { data: 'created_at', name: 'created_at' },
                  {data: 'action', name: 'action', orderable: false},
               ],
        order: [[0, 'desc']]
      });



    /*  When user click add user button */
    $('#create-new-profile').click(function () {
        valInsUp = 1;
        $('#btn-save').val("create-profile");
        $('#profile_id').val('');
        $('#profileForm').trigger("reset");
        $('#profileCrudModal').html("Add New Profile");
        $('#modal_profile').modal('show');
    });  

     /* When click edit user */
     $('body').on('click', '.edit-profile', function () {
        valInsUp = 2;
      var profile_id = $(this).data('id');
      $.get(SITEURL+'/master/profile/edit/'+profile_id, function (data) {
         $('#name-error').hide();
         $('#keterangan-error').hide();
         $('#profileCrudModal').html("Edit Profile");
          $('#btn-save').val("edit-profile");
          $('#btn-save').html("Update Changes");
          $('#modal_profile').modal('show');
          $('#profile_id').val(data.id);
          $('#brand_name').val(data.brand_name);
          $('#phone1').val(data.phone1);
          $('#phone2').val(data.phone2);
          $('#email').val(data.email);
          $('#facebook').val(data.facebook);
          $('#twitter').val(data.twitter);
          $('#youtube').val(data.youtube);
          $('#address').val(data.address);
          $('#idbank1').val(data.bank_satu);
          $('#idbank2').val(data.bank_dua);
          $('#idbank3').val(data.bank_tiga);
          $('#kdrek_satu').val(data.kdrek_satu);
          $('#kdrek_dua').val(data.kdrek_dua);
          $('#kdrek_tiga').val(data.kdrek_tiga);
          $('#atasnama_satu').val(data.atasnama_satu);
          $('#atasnama_dua').val(data.atasnama_dua);
          $('#atasnama_tiga').val(data.atasnama_tiga);
        
      })
   });

   $('body').on('click', '#delete-profile', function () {
         if (confirm("Hapus data?")) {
            var profile_id = $(this).data("id");
            confirm("Are You sure want to delete !");

            $.ajax({
                type: "get",
                url: SITEURL + "/master/profile/delete/"+profile_id,
                success: function (data) {
                var oTable = $('#tbl_profile').dataTable(); 
                oTable.fnDraw(false);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }else {
            
        }
       
    }); 
    
   
        
  
});
if ($("#profileForm").length > 0) {
   // if(valInsUp === 1){
        $("#profileForm").validate({
            submitHandler: function(form) {
        
            var actionType = $('#btn-save').val();
            $('#btn-save').html('Sending..');
            
            $.ajax({
                data: $('#profileForm').serialize(),
                url: SITEURL + "/master/profile/store",
                type: "POST",
                dataType: 'json',
                success: function (data) {
        
                    $('#profileForm').trigger("reset");
                    $('#modal_profile').modal('hide');
                    $('#btn-save').html('Save Changes');
                    var oTable = $('#tbl_profile').dataTable();
                    oTable.fnDraw(false);
                    
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#btn-save').html('Save Changes');
                }
            });
            }
        });
    // } else if(valInsUp === 2){
    //     alert('ini update');
    // }

}
</script>
@endpush
