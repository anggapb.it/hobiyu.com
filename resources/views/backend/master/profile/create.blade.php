<div class="modal fade" id="modal_profile" aria-hidden="true">
<div class="modal-dialog modal-lg">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="profileCrudModal"></h4>
    </div>
    <div class="modal-body">
        <form id="profileForm" name="profileForm" class="form-horizontal">
           <input type="hidden" name="profile_id" id="profile_id">

           <div class="form-row">
                    <div class="form-group col-md-12">
                    <label>Brand</label>
                    <input type="text" class="form-control" id="brand_name" name="brand_name"required="">
                    </div>
                   
                </div>
            

            <div class="form-row">
            <div class="form-group">
                <div class="col-sm-6">
                <label>Phone 1</label>
                    <input type="text" class="form-control" id="phone1" name="phone1" maxlength="50" >
                </div>
           
                <div class="col-sm-6">
                <label>Phone 2</label>
                    <input type="text" class="form-control" id="phone2" name="phone2" maxlength="50" >
                </div>
                </div>
            </div>
            
            <div class="form-row">
            <div class="form-group">
               
                <div class="col-sm-4">
                <label>Bank ke 1</label>
                    <select class="form-control select2" id="idbank1" name="idbank1" style="width: 100%;">
                        <option value="">Pilih..</option>
                    </select>
                </div>

                <div class="col-sm-4">
                <label>Kode Rek</label>
                    <input type="text" class="form-control" id="kdrek_satu" name="kdrek_satu" maxlength="50" >
                </div>

                <div class="col-sm-4">
                <label>Atasnama</label>
                    <input type="text" class="form-control" id="atasnama_satu" name="atasnama_satu" maxlength="50" >
                </div>
            </div>
            </div>
            
            <div class="form-row">
            <div class="form-group">
                
                <div class="col-sm-4">
                <label>Bank ke 2</label>
                <select class="form-control select2" id="idbank2" name="idbank2" style="width: 100%;">
                        <option value="">Pilih..</option>
                    </select>
                </div>

                <div class="col-sm-4">
                <label>Kode Rek</label>
                    <input type="text" class="form-control" id="kdrek_dua" name="kdrek_dua">
                </div>

                <div class="col-sm-4">
                <label>Atasnama</label>
                    <input type="text" class="form-control" id="atasnama_dua" name="atasnama_dua">
                </div>
            </div>
            </div>
            
            <div class="form-row">
            <div class="form-group">
                
                <div class="col-sm-4">
                <label>Bank ke 3</label>
                <select class="form-control select2" id="idbank3" name="idbank3" style="width: 100%;">
                        <option value="">Pilih..</option>
                    </select>
                </div>

                <div class="col-sm-4">
                <label>Kode Rek</label>
                    <input type="text" class="form-control" id="kdrek_tiga" name="kdrek_tiga">
                </div>

                <div class="col-sm-4">
                <label>Atasnama</label>
                    <input type="text" class="form-control" id="atasnama_tiga" name="atasnama_tiga">
                </div>
            </div>
            </div>

       
           
            <div class="form-group">
                <label for="name" class="col-sm-1 control-label">Alamat</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="address" name="address">
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-sm-1 control-label">Email</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="email" name="email">
                </div>
            </div>

            <div class="form-group">
                <label for="name" class="col-sm-1 control-label">Facebook</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="facebook" name="facebook">
                </div>
            </div>
           
            <div class="form-group">
                <label for="name" class="col-sm-1 control-label">Twitter</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="twitter" name="twitter">
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-sm-1 control-label">Youtube</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="youtube" name="youtube">
                </div>
            </div>

           
            <div class="col-sm-offset-2 col-sm-10">
             <button type="submit" class="btn btn-primary" id="btn-save" value="create">Save changes
             </button>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        
    </div>
</div>
</div>
</div>

