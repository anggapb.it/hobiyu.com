@extends('layouts.app')


@section('content')
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="content-wrapper">
    
    <section class="content-header">
        <h1>
            Kecamatan
            <small>Master</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Master</a></li>
            <li class="active">Kecamatan</li>
        </ol>
        </section>

   <section class="content">    
    <div class="box">
        <div class="box-header">
              <h2 class="box-title">Data Kecamatan</h2>

            <div class="box-body">
                     <a href="javascript:void(0)" class="btn-sm btn-info ml-3" id="create-new-btn">Add New Kecamatan</a>
                    <br>
                    <br>
                 
                    <table class="table table-bordered table-striped" id="tbl_id">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>No</th>
                                <th>Kecamatan</th>
                                <th>Kota / Kabupaten</th>
                                <th>Created at</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        </table>
                 
              </div>
        </div>    
    </div>
  </div>
</section>

<div class="modal fade" id="mdl_frm" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="FrmModal"></h4>
    </div>
    <div class="modal-body">
        <form id="kecForm" name="kecForm" class="form-horizontal">
           <input type="hidden" name="kecamatan_id" id="kecamatan_id">
            <div class="form-group">
                <label for="name" class="col-sm-1 control-label">Kecamatan</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Nama Kecamatan" value="" required="">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Kota / Kabupaten</label>
                <div class="col-sm-12">
                <select class="form-control select2" id="id_kota" name="id_kota" style="width: 100%;">
                     <option value="">Pilih Kota / Kabupaten</option>
                </select>
                </div>
              </div>
 
            <div class="col-sm-offset-2 col-sm-10">
             <button type="submit" class="btn btn-primary" id="btn-save" value="create">Save changes
             </button>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        
    </div>
</div>
</div>
</div>




@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

<script type="text/javascript"> 
var SITEURL = '{{URL::to('')}}';
var valInsUp;
$(document).ready( function () {
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  
    $.ajax({
            url: SITEURL + "/master/kecamatan/listkota",
			type: 'GET',
			success: function(data) {
			//	var jsonResult = jQuery.parseJSON( data );
            JSON.stringify(data); //to string
            if(data.length > 0){
                for(i=0; i< data.length; i++){
                    var currenData = data[i];
                    var opt_kota = '<option value="'+ currenData.id +'">'+ currenData.name +'</option>';
				    $('#id_kota').append(opt_kota);
                }
            }
            $('#id_kota option:first-child').attr("selected", "selected");

			}
		});
 
    $('.select2').select2()

 
  $('#tbl_id').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: SITEURL + "/master/kecamatan",
          type: 'GET',
         },
         columns: [
                  {data: 'id', name: 'id', 'visible': false},
                  {data: 'id_kota', name: 'id_kota', 'visible': false},
                  {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                  { data: 'name', name: 'name' },
                  { data: 'nmkota', name: 'nmkota' },
                  { data: 'created_at', name: 'created_at' },
                  {data: 'action', name: 'action', orderable: false},
               ],
        order: [[0, 'desc']]
      });



    /*  When user click add user button */
    $('#create-new-btn').click(function () {
        $('#btn-save').val("create-master");
        $('#kecamatan_id').val('');
        $('#kecForm').trigger("reset");
        $('#FrmModal').html("Add New Kecamatan");
        $('#mdl_frm').modal('show');
    });  

     /* When click edit user */
     $('body').on('click', '.edit-btn', function () {
      var kecamatan_id = $(this).data('id');
      $.get(SITEURL+'/master/kecamatan/' + kecamatan_id +'/edit', function (data) {
         $('#name-error').hide();
         $('#FrmModal').html("Edit Kecamatan");
          $('#btn-save').val("edit-master");
          $('#btn-save').html("Update Changes");
          $('#mdl_frm').modal('show');
          $('#kecamatan_id').val(data.id);
          $('#name').val(data.name);
          $('#id_kota').val(data.id_kota);
        
      })
   });

   $('body').on('click', '#delete-btn', function () {
         if (confirm("Hapus data?")) {
            var kecamatan_id = $(this).data("id");
            confirm("Are You sure want to delete !");

            $.ajax({
                type: "get",
                url: SITEURL + "/master/kecamatan/delete/"+kecamatan_id,
                success: function (data) {
                var oTable = $('#tbl_id').dataTable(); 
                oTable.fnDraw(false);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }else {
            
        }
       
    }); 
    
   
        
  
});
if ($("#kecForm").length > 0) {
   // if(valInsUp === 1){
        $("#kecForm").validate({
            submitHandler: function(form) {
        
            var actionType = $('#btn-save').val();
            $('#btn-save').html('Sending..');
            
            $.ajax({
                data: $('#kecForm').serialize(),
                url: SITEURL + "/master/kecamatan/store",
                type: "POST",
                dataType: 'json',
                success: function (data) {
        
                    $('#kecForm').trigger("reset");
                    $('#mdl_frm').modal('hide');
                    $('#btn-save').html('Save Changes');
                    var oTable = $('#tbl_id').dataTable();
                    oTable.fnDraw(false);
                    
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#btn-save').html('Save Changes');
                }
            });
            }
        });
  

}


</script>
@endpush
