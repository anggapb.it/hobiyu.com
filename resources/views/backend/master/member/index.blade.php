@extends('layouts.app')
<meta name="csrf-token" content="{{ csrf_token() }}">

@section('content')
<!-- CSRF Token -->


<div class="content-wrapper">
    
    <section class="content-header">
        <h1>
            Member
            <small>Master</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Master</a></li>
            <li class="active">Member</li>
        </ol>
        </section>

   <section class="content">    
    <div class="box">
        <div class="box-header">
              <h2 class="box-title">Data Member</h2>

            <div class="box-body">
                     <!-- <a href="javascript:void(0)" class="btn-sm btn-info ml-3" id="create-new-member">Add New Member</a> -->
                    <br>
                    <br>
                 
                    <table class="table table-bordered table-striped" id="tbl_member">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Alamat</th>
                                <th>Saldo Aktif</th>
                                <th>Kode Rek</th>
                                <th>Bank</th>
                                <th>Status</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        </table>
                 
              </div>
        </div>    
    </div>
  </div>
</section>

@include('backend.master.member.create')


@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

<script type="text/javascript"> 
var SITEURL = '{{URL::to('')}}';
var valInsUp;
$(document).ready( function () {
  
   

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $.ajax({
            url: SITEURL + "/master/member/listmember",
			type: 'GET',
			success: function(data) {
			//	var jsonResult = jQuery.parseJSON( data );
            JSON.stringify(data); //to string
            if(data.length > 0){
                for(i=0; i< data.length; i++){
                    var currenData = data[i];
                    var opt_kota = '<option value="'+ currenData.id +'">'+ currenData.name +'</option>';
				    $('#member').append(opt_kota);
                }
            }
            $('#member option:first-child').attr("selected", "selected");

			}
		});
 
    $('.select2').select2()
  $('#tbl_member').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: SITEURL + "/master/member",
          type: 'GET',
         
         },
         columns: [
                  {data: 'id', name: 'id', 'visible': false},
                  {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                  { data: 'name', name: 'name' },
                  { data: 'email', name: 'email' },
                  { data: 'phone', name: 'phone' },
                  { data: 'alamat', name: 'alamat' },
                  {data: 'saldo', name: 'saldo', orderable: false},
                  { data: 'kdrek', name: 'kdrek' },
                  { data: 'nmbank', name: 'nmbank' },
                  {data: 'status', name: 'status', orderable: false},
                  {data: 'action', name: 'action', orderable: false},
               ],
        order: [[0, 'desc']]
      });



    /*  When user click add user button */
    $('#create-new-member').click(function () {
        valInsUp = 1;
        $('#btn-save').val("create-topup");
        $('#member_id').val('');
        $('#topupForm').trigger("reset");
        $('#topupCrudModal').html("Add New Member");
        $('#modal_topup').modal('show');
    });  

   
   

   $('body').on('click', '#active-member', function (data) {
    
        if (confirm("Aktifkan Member?")) {
            var member_id = $(this).data("id");
          
                confirm("Apakah anda yakin akan mengaktifkan member?");

                $.ajax({
                    type: "get",
                    url: SITEURL + "/master/member/active/"+member_id,
                    success: function (data) {
                    var oTable = $('#tbl_member').dataTable(); 
                    oTable.fnDraw(false);
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
         
        }else {
            
        }
      
        
       
    }); 
  
  
    $('body').on('click', '#banned-member', function (data) {
    
        if (confirm("Banned Member?")) {
            var member_id = $(this).data("id");
          
                confirm("Apakah anda yakin akan banned member? ");

                $.ajax({
                    type: "get",
                    url: SITEURL + "/master/member/banned/"+member_id,
                    success: function (data) {
                    var oTable = $('#tbl_member').dataTable(); 
                    oTable.fnDraw(false);
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
         
        }else {
            
        }
      
        
       
    }); 
    
   
        
  
});
if ($("#topupForm").length > 0) {
   // if(valInsUp === 1){
        $("#topupForm").validate({
            submitHandler: function(form) {
        
            var actionType = $('#btn-save').val();
            $('#btn-save').html('Sending..');
            
            $.ajax({
                data: $('#topupForm').serialize(),
                url: SITEURL + "/master/member/store",
                type: "POST",
                dataType: 'json',
                success: function (data) {
        
                    $('#topupForm').trigger("reset");
                    $('#modal_topup').modal('hide');
                    $('#btn-save').html('Save Changes');
                    var oTable = $('#tbl_member').dataTable();
                    oTable.fnDraw(false);
                    
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#btn-save').html('Save Changes');
                }
            });
            }
        });
    // } else if(valInsUp === 2){
    //     alert('ini update');
    // }

}
</script>
@endpush
