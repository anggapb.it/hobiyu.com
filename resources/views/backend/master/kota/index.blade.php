@extends('layouts.app')


@section('content')
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="content-wrapper">
    
    <section class="content-header">
        <h1>
            Kota / Kabupaten
            <small>Master</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Master</a></li>
            <li class="active">Kota / Kabupaten</li>
        </ol>
        </section>

   <section class="content">    
    <div class="box">
        <div class="box-header">
              <h2 class="box-title">Data Kota / Kabupaten</h2>

            <div class="box-body">
                     <a href="javascript:void(0)" class="btn-sm btn-info ml-3" id="create-new-btn">Add New Kota / Kabupaten</a>
                    <br>
                    <br>
                 
                    <table class="table table-bordered table-striped" id="tbl_id">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>No</th>
                                <th>Kota / Kabupaten</th>
                                <th>Provinsi</th>
                                <th>Created at</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        </table>
                 
              </div>
        </div>    
    </div>
  </div>
</section>

<div class="modal fade" id="mdl_frm" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="FrmModal"></h4>
    </div>
    <div class="modal-body">
        <form id="kotaForm" name="kotaForm" class="form-horizontal">
           <input type="hidden" name="kota_id" id="kota_id">
            <div class="form-group">
                <label for="name" class="col-sm-3 control-label">Kota / Kabupaten</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Nama Kota / Kabupaten" value="" required="">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-1 control-label">Provinsi</label>
                <div class="col-sm-12">
                <select class="form-control select2" id="id_provinsi" name="id_provinsi" style="width: 100%;">
                     <option value="">Pilih Provinsi</option>
                </select>
                </div>
              </div>
 
            <div class="col-sm-offset-2 col-sm-10">
             <button type="submit" class="btn btn-primary" id="btn-save" value="create">Save changes
             </button>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        
    </div>
</div>
</div>
</div>




@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

<script type="text/javascript"> 
var SITEURL = '{{URL::to('')}}';
var valInsUp;
$(document).ready( function () {
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  
    $.ajax({
            url: SITEURL + "/master/kota/listprovinsi",
			type: 'GET',
			success: function(data) {
			//	var jsonResult = jQuery.parseJSON( data );
            JSON.stringify(data); //to string
            if(data.length > 0){
                for(i=0; i< data.length; i++){
                    var currenData = data[i];
                    var opt_prov = '<option value="'+ currenData.id +'">'+ currenData.name +'</option>';
				    $('#id_provinsi').append(opt_prov);
                }
            }
            $('#id_provinsi option:first-child').attr("selected", "selected");

			}
		});
 
    $('.select2').select2()

 
  $('#tbl_id').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: SITEURL + "/master/kota",
          type: 'GET',
         },
         columns: [
                  {data: 'id', name: 'id', 'visible': false},
                  {data: 'id_provinsi', name: 'id_provinsi', 'visible': false},
                  {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                  { data: 'name', name: 'name' },
                  { data: 'nmprovinsi', name: 'nmprovinsi' },
                  { data: 'created_at', name: 'created_at' },
                  {data: 'action', name: 'action', orderable: false},
               ],
        order: [[0, 'desc']]
      });



    /*  When user click add user button */
    $('#create-new-btn').click(function () {
        $('#btn-save').val("create-master");
        $('#kota_id').val('');
        $('#kotaForm').trigger("reset");
        $('#FrmModal').html("Add New Kota / Kabupaten");
        $('#mdl_frm').modal('show');
    });  

     /* When click edit user */
     $('body').on('click', '.edit-btn', function () {
      var kota_id = $(this).data('id');
      $.get(SITEURL+'/master/kota/' + kota_id +'/edit', function (data) {
         $('#name-error').hide();
         $('#FrmModal').html("Edit Kota / Kabupaten");
          $('#btn-save').val("edit-master");
          $('#btn-save').html("Update Changes");
          $('#mdl_frm').modal('show');
          $('#kota_id').val(data.id);
          $('#name').val(data.name);
          $('#id_provinsi').val(data.id_provinsi);
        
      })
   });

   $('body').on('click', '#delete-btn', function () {
         if (confirm("Hapus data?")) {
            var kota_id = $(this).data("id");
            confirm("Are You sure want to delete !");

            $.ajax({
                type: "get",
                url: SITEURL + "/master/kota/delete/"+kota_id,
                success: function (data) {
                var oTable = $('#tbl_id').dataTable(); 
                oTable.fnDraw(false);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }else {
            
        }
       
    }); 
    
   
        
  
});
if ($("#kotaForm").length > 0) {
   // if(valInsUp === 1){
        $("#kotaForm").validate({
            submitHandler: function(form) {
        
            var actionType = $('#btn-save').val();
            $('#btn-save').html('Sending..');
            
            $.ajax({
                data: $('#kotaForm').serialize(),
                url: SITEURL + "/master/kota/store",
                type: "POST",
                dataType: 'json',
                success: function (data) {
        
                    $('#kotaForm').trigger("reset");
                    $('#mdl_frm').modal('hide');
                    $('#btn-save').html('Save Changes');
                    var oTable = $('#tbl_id').dataTable();
                    oTable.fnDraw(false);
                    
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#btn-save').html('Save Changes');
                }
            });
            }
        });
  

}


// function load_provinsi_list(){
// 		$('#id_provinsi').empty();
// 		$('#id_provinsi').append('<option value="">Pilih Provinsi</option>');

//         $.ajaxSetup({
//             headers: {
//                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//             }
//         });
		
// 		$.ajax({
//             url: SITEURL + "/master/kota/listprovinsi",
// 			type: 'GET',
// 			success: function(data) {
// 				var jsonResult = jQuery.parseJSON( data );
//                 alert(jsonResult)
// 				// if(jsonResult.data[1] > 0){
// 				// 	for(i = 0; i < jsonResult.data; i++)
// 				// 	{
// 				// 		 currentdata = jsonResult.data[i];
// 				// 		var opt_prov = '<option value="'+ currentdata.id +'">'+ currentdata.name +'</option>';
// 				// 		$('#id_provinsi').append(opt_prov);
// 				// 	}
// 				// }
// 				// $('#id_provinsi option:first-child').attr("selected", "selected");
				
// 			}
// 		});
// 	}
</script>
@endpush
