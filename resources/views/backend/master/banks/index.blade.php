@extends('layouts.app')


@section('content')
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="content-wrapper">
    
    <section class="content-header">
        <h1>
            Bank
            <small>Master</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Master</a></li>
            <li class="active">Bank</li>
        </ol>
        </section>

   <section class="content">    
    <div class="box">
        <div class="box-header">
              <h2 class="box-title">Data Bank</h2>

            <div class="box-body">
                     <a href="javascript:void(0)" class="btn-sm btn-info ml-3" id="create-new-bank">Add New Bank</a>
                    <br>
                    <br>
                 
                    <table class="table table-bordered table-striped" id="tbl_bank">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>No</th>
                                <th>Bank</th>
                                <th>Keterangan</th>
                                <th>Created at</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        </table>
                 
              </div>
        </div>    
    </div>
  </div>
</section>

@include('backend.master.banks.create')


@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

<script type="text/javascript"> 
var SITEURL = '{{URL::to('')}}';
var valInsUp;
$(document).ready( function () {
 

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  $('#tbl_bank').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: SITEURL + "/master/bank",
          type: 'GET',
         },
         columns: [
                  {data: 'id', name: 'id', 'visible': false},
                  {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                  { data: 'name', name: 'name' },
                  { data: 'keterangan', name: 'keterangan' },
                  { data: 'created_at', name: 'created_at' },
                  {data: 'action', name: 'action', orderable: false},
               ],
        order: [[0, 'desc']]
      });



    /*  When user click add user button */
    $('#create-new-bank').click(function () {
        valInsUp = 1;
        $('#btn-save').val("create-bank");
        $('#bank_id').val('');
        $('#bankForm').trigger("reset");
        $('#userCrudModal').html("Add New Bank");
        $('#modal_bank').modal('show');
    });  

     /* When click edit user */
     $('body').on('click', '.edit-bank', function () {
        valInsUp = 2;
      var bank_id = $(this).data('id');
      $.get(SITEURL+'/master/bank/' + bank_id +'/edit', function (data) {
         $('#name-error').hide();
         $('#keterangan-error').hide();
         $('#userCrudModal').html("Edit Bank");
          $('#btn-save').val("edit-bank");
          $('#btn-save').html("Update Changes");
          $('#modal_bank').modal('show');
          $('#bank_id').val(data.id);
          $('#name').val(data.name);
          $('#keterangan').val(data.keterangan);
      })
   });

   $('body').on('click', '#delete-bank', function () {
         if (confirm("Hapus data?")) {
            var bank_id = $(this).data("id");
            confirm("Are You sure want to delete !");

            $.ajax({
                type: "get",
                url: SITEURL + "/master/bank/delete/"+bank_id,
                success: function (data) {
                var oTable = $('#tbl_bank').dataTable(); 
                oTable.fnDraw(false);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }else {
            
        }
       
    }); 
    
   
        
  
});
if ($("#bankForm").length > 0) {
   // if(valInsUp === 1){
        $("#bankForm").validate({
            submitHandler: function(form) {
        
            var actionType = $('#btn-save').val();
            $('#btn-save').html('Sending..');
            
            $.ajax({
                data: $('#bankForm').serialize(),
                url: SITEURL + "/master/bank/store",
                type: "POST",
                dataType: 'json',
                success: function (data) {
        
                    $('#bankForm').trigger("reset");
                    $('#modal_bank').modal('hide');
                    $('#btn-save').html('Save Changes');
                    var oTable = $('#tbl_bank').dataTable();
                    oTable.fnDraw(false);
                    
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#btn-save').html('Save Changes');
                }
            });
            }
        });
    // } else if(valInsUp === 2){
    //     alert('ini update');
    // }

}
</script>
@endpush
