<div class="modal fade" id="modal_bank" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="userCrudModal"></h4>
    </div>
    <div class="modal-body">
        <form id="bankForm" name="bankForm" class="form-horizontal">
           <input type="hidden" name="bank_id" id="bank_id">
            <div class="form-group">
                <label for="name" class="col-sm-1 control-label">Bank</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Nama Bank" value="" maxlength="50" required="">
                </div>
            </div>

           
            <div class="form-group">
                <label for="name" class="col-sm-1 control-label">Keterangan</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan" value="" required="">
                </div>
            </div>
 
            <div class="col-sm-offset-2 col-sm-10">
             <button type="submit" class="btn btn-primary" id="btn-save" value="create">Save changes
             </button>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        
    </div>
</div>
</div>
</div>

