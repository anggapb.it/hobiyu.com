@extends('layouts.app')


@section('content')
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="content-wrapper">
    
    <section class="content-header">
        <h1>
            Daftar Komplain
            <small>Transaksi</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Transaksi</a></li>
            <li class="active">Daftar Komplain</li>
        </ol>
        </section>

   <section class="content">    
    <div class="box">
        <div class="box-header">
              <h2 class="box-title">Data Daftar Komplain</h2>

            <div class="box-body">
                   
                 
                    <table class="table table-bordered table-striped" id="tbl_id">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>File</th>
                                <th>No Tiket</th>
                                <th>Member</th>
                                <th>Keterangan</th>
                                <th>Status</th>
                                <th>Created at</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        </table>
                 
              </div>
        </div>    
    </div>
  </div>
</section>

<div class="modal fade" id="mdl_frm" aria-hidden="true">
<div class="modal-dialog modal-lg">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="FrmModal"></h4>
    </div>
    <div class="modal-body">
            
    <form method="post" id="listcomplaintForm" class="form-horizontal" enctype="multipart/form-data">
           <input type="hidden" name="complaint_id" id="complaint_id">
      
           <div class="form-row">
                    <div class="form-group col-md-12">
                    <label>Jawaban</label><span style="color:red">*</span>
                    <textarea class="form-control" id="answer" name="answer" rows="5"></textarea>
                    </div>
                   
                </div>
        <div class="form-row">
                    <div class="form-group col-md-12">
                    <label>File (Maks. 8 Mb)</label>
                    <input type="file" class="form-control-file" id="file" name="file">
                    </div>
                   
                </div>


                <table class="table table-bordered table-striped" id="tblComplaintdet">
                        <thead>
                            <tr>
                              
                                <th>No. Urut</th>
                             
                                <th>Answer</th>
                                <th>File</th>
                                <th>User</th>
                               
                            </tr>
                        </thead>
                    
                  </table>        

           
                      

    </div>
    <div class="modal-footer">
    <div class="row">
            <div class="col-lg-11 margin-tb">
                <div class="pull-right">
                <button type="submit" class="btn btn-primary" id="btn-save">Submit</button>
                </div>
                
            </div>
            </div>
    </div>

    </form>
    
</div>
</div>
</div>




@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

<script type="text/javascript"> 
var SITEURL = '{{URL::to('')}}';
var orderidx;
$(document).ready( function () {
 

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  $('#tbl_id').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: SITEURL + "/transaksi/list-complaint",
          type: 'GET',
         },
         columns: [
                  {data: 'id', name: 'id', 'visible': false},
               //   {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                  { data: 'image', name: 'image' },
                  { data: 'notiket', name: 'notiket' },
                  { data: 'nmcustomer', name: 'nmcustomer' },
                  { data: 'judul', name: 'judul' },
                  { data: 'status', name: 'status' },      
                  { data: 'created_at', name: 'created_at' },
                  {data: 'action', name: 'action', orderable: false},
               ],
        order: [[0, 'desc']]
      });



     /* When click edit user */
     $('body').on('click', '.edit-btn', function () {
      var complaint_id = $(this).data('id');

     $('#tblComplaintdet').DataTable({
         processing: true,
         serverSide: true,
         destroy: true,
         paging: false,
         searching: false,
         ajax: {
          url: SITEURL + "/transaksi/list-complaint/complaintdet/"+complaint_id,
          type: 'GET',
         },
          columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
              
                    { data: 'answer', name: 'answer' },
                    { data: 'file', name: 'file' },
                    { data: 'iduser', name: 'iduser' },       
                ],
          order: [[0, 'desc']]
         });

         $('#name-error').hide();
         $('#mdl_frm').modal('show');
         $('#complaint_id').val($(this).data('id'));
         
      
   });
   
  
});

$('#listcomplaintForm').on('submit',(function(e) {
 
 $.ajaxSetup({
  
 headers: {
  
   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  
 }
  
 });
  
 e.preventDefault();
  
 var formData = new FormData(this);
  
  
  
 $.ajax({
  
    type:'POST',
  
    url: SITEURL + "/transaksi/list-complaint/answer",
  
    data:formData,
  
    cache:false,
  
    contentType: false,
  
    processData: false,
  
    success:function(data){
  
        
        $('#listcomplaintForm').trigger("reset");
                    //$('#mdl_frm').modal('hide');
                    var oTable = $('#tblComplaintdet').dataTable();
                    oTable.fnDraw(false);
  
    },
  
    error: function(data){
  
        console.log('Error:', data);
  
    }
  
 });
  
 }));
</script>
@endpush
