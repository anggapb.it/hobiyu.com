@extends('layouts.app')


@section('content')
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="content-wrapper">
    
    <section class="content-header">
        <h1>
            List Lelang
            <small>Transaksi</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Transaksi</a></li>
            <li class="active">List Lelang</li>
        </ol>
        </section>

   <section class="content">    
    <div class="box">
        <div class="box-header">
              <h2 class="box-title"> List Lelang</h2>

            <div class="box-body">
                     <!-- <a href="javascript:void(0)" class="btn-sm btn-info ml-3" id="create-new-barang">Add New List Lelang</a> -->
                     <!-- <a href="{{ url('transaksi/barang/create') }}" class="btn-sm btn-info ml-3" id="btn_add">Add New List Lelang</a> -->
                    <br>
                    <br>
                 
                    <table class="table table-bordered table-striped" id="tbl_list_lelang">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Owner</th>
                                <th>Item</th>
                                <th>Harga Awal</th>
                                <th>Deskripsi</th>
                                <th>Pemenang</th>
                                <th>Status</th>
                                <th>Created at</th>
                                <!-- <th>Action</th> -->
                            </tr>
                        </thead>
                        </table>
                 
              </div>
        </div>    
    </div>
  </div>
</section>

<div class="modal fade" id="modal_view_member" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Data Member</h4>
    </div>
    <div class="modal-body">
           <input type="hidden" name="list_lelang_id" id="list_lelang_id">

           <div class="form-row">
                    <div class="form-group col-md-6">
                    <label for="name">Nama</label>
                    <input type="text" class="form-control" id="name" name="name" readonly>
                    </div>
                    <div class="form-group col-md-6">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" name="email" readonly>
                    </div>
                </div>
                <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="phone">No. Hp</label>
                    <input type="text" class="form-control" id="phone" name="phone" readonly>
                    </div>
                    <div class="form-group col-md-6">
                    <label for="alamat">Alamat</label>
                    <textarea class="form-control" id="alamat" name="alamat" rows="3" readonly></textarea>
                    </div>
                   
                </div>
               
                <div class="form-row">
                    <div class="form-group col-md-6">
                    <label>Bank</label>
                    <input type="text" class="form-control" id="bank" name="bank" readonly>
                    </div>
                    <div class="form-group col-md-6">
                    <label>Kode Rekening</label>
                    <input type="text" class="form-control" id="kdrek" name="kdrek" readonly>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                    <label>Jenis Kelamin</label>
                    <input type="text" class="form-control" id="jkelamin" name="jkelamin" readonly>
                    </div>
                    <div class="form-group col-md-6">
                    <label>No. KTP</label>
                    <input type="text" class="form-control" id="ktp" name="ktp" readonly>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                    <label>Tempat Lahir</label>
                    <input type="text" class="form-control" id="tptlahir" name="tptlahir" readonly>
                    </div>
                    <div class="form-group col-md-6">
                    <label>Tgl. Lahir</label>
                    <input type="text" class="form-control" id="tgllahir" name="tgllahir" readonly>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                    <label>Pekerjaan</label>
                    <input type="text" class="form-control" id="pekerjaan" name="pekerjaan" readonly>
                    </div>
                    <div class="form-group col-md-6">
                    <label>Tgl. Aktivasi</label>
                    <input type="text" class="form-control" id="tglaktivasi" name="tglaktivasi" readonly>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                    <label>Provinsi</label>
                    <input type="text" class="form-control" id="provinsi" name="provinsi" readonly>
                    </div>
                    <div class="form-group col-md-6">
                    <label>Kota / Kabupaten</label>
                    <input type="text" class="form-control" id="kotkab" name="kotkab" readonly>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                    <label>Kecamatan</label>
                    <input type="text" class="form-control" id="kecamatan" name="kecamatan" readonly>
                    </div>
                    <div class="form-group col-md-6">
                    <label>Kelurahan</label>
                    <input type="text" class="form-control" id="kelurahan" name="kelurahan" readonly>
                    </div>
                </div>



         
      
    </div>
    <div class="modal-footer">
        
    </div>
</div>
</div>
</div>

@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

<script type="text/javascript"> 
var SITEURL = '{{URL::to('')}}';
var valInsUp;
$(document).ready( function () {

 

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $.ajax({
            url: SITEURL + "/transaksi/list-lelang/listdethobi",
			type: 'GET',
			success: function(data) {
			//	var jsonResult = jQuery.parseJSON( data );
            JSON.stringify(data); //to string
            if(data.length > 0){
                for(i=0; i< data.length; i++){
                    var currenData = data[i];
                    var opt_arr = '<option value="'+ currenData.id +'">'+ currenData.name +'</option>';
				    $('#id_kategori_brg').append(opt_arr);
                }
            }
            $('#id_kategori_brg option:first-child').attr("selected", "selected");

			}
		});
 
    $('.select2').select2();

  $('#tbl_list_lelang').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: SITEURL + "/transaksi/list-lelang",
          type: 'GET',
         },
         columns: [
                  {data: 'id', name: 'id', 'visible': true},
                  { data: 'nmcustomer', name: 'nmcustomer' },
                  { data: 'nmitem', name: 'nmitem' },
                  { data: 'hrgawal', name: 'hrgawal' },
                  { data: 'deskripsi', name: 'deskripsi' },
                  { data: 'view_pemenang', name: 'view_pemenang' },
                  { data: 'nmstlelang', name: 'nmstlelang' },
                  { data: 'created_at', name: 'created_at' },
                //   {data: 'action', name: 'action', orderable: false},
               ],
        order: [[0, 'desc']]
      }); 

     /* When click edit user */
//      $('body').on('click', '.verifikasi-lelang', function () {
//       var list_lelang_id = $(this).data('id');
//       $.get(SITEURL+'/transaksi/list-lelang/' + list_lelang_id +'/edit', function (data) {
//          $('#name-error').hide();
//       //   $('#keterangan-error').hide();
//          $('#listLelangModal').html("Verifikasi List Lelang");
//           $('#btn-save').val("verifikasi-lelang");
//           $('#btn-save').html("Update");
//           $('#modal_view_member').modal('show');
//           $('#list_lelang_id').val(data.id);
        
        
//           $('#nmitem').val(data.nmitem);
//           $('#nmkatitem').val(data.idkatitem);
//           $('#kondisi_item').val(data.kondisi_item);
//           $('#hrgawal').val(data.hrgawal);
//           $('#deskripsi').val(data.deskripsi);
//           $('#photo_item').val(data.photo_item);
//           $('#tgl_bts_penawaran').val(data.tgl_bts_penawaran);
//           $('#notelp').val(data.notelp);
//           $('#beban_biaya_kirim').val(data.beban_biaya_kirim);
//           $('#idstlelang').val(data.idstlelang);
//       })
//    });

   $('body').on('click', '.btn_view', function () {
    var id = $(this).data('id');
    $.get(SITEURL+'/transaksi/list-lelang/get-member/'+ id, function (data) {
        $('#name').val(data.name);
        $('#email').val(data.email);
        $('#phone').val(data.phone);
        $('#alamat').val(data.alamat);
        $('#bank').val(data.nmbank);
        $('#kdrek').val(data.kdrek);
        $('#jkelamin').val(data.kelamin);
        $('#ktp').val(data.noktp);
        $('#tptlahir').val(data.tpt_lahir);
        $('#tgllahir').val(data.tgl_lahir);
        $('#pekerjaan').val(data.nmpekerjaan);
        $('#tglaktivasi').val(data.tglaktivasi);
        $('#provinsi').val(data.provinsi);
        $('#kotkab').val(data.kotkab);
        $('#kecamatan').val(data.kecamatan);
        $('#kelurahan').val(data.kelurahan);
    });
    $('#modal_view_member').modal('show');
   });
    
   
        
  
});
if ($("#listLelangForm").length > 0) {
   // if(valInsUp === 1){
        $("#listLelangForm").validate({
            submitHandler: function(form) {
        
            var actionType = $('#btn-save').val();
            $('#btn-save').html('Sending..');
            
            $.ajax({
                data: $('#listLelangForm').serialize(),
                url: SITEURL + "/transaksi/list-lelang/update",
                type: "POST",
                enctype: 'multipart/form-data',
                processData: false,  // Important!
              //  contentType: false,
                cache: false,
                dataType: 'json',
                success: function (data) {
        
                    $('#listLelangForm').trigger("reset");
                    $('#modal_view_member').modal('hide');
                 //   $('#btn-save').html('Save');
                    var oTable = $('#tbl_list_lelang').dataTable();
                    oTable.fnDraw(false);
                    
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#btn-save').html('Save');
                }
            });
            }
        });
    // } else if(valInsUp === 2){
    //     alert('ini update');
    // }

}
</script>
@endpush
