@extends('layouts.app')


@section('content')
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="content-wrapper">
    
    <section class="content-header">
        <h1>
            Barang
            <small>Transaksi</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Transaksi</a></li>
            <li class="active">Barang</li>
        </ol>
        </section>

   <section class="content">    
    <div class="box">
        <div class="box-header">
              <h2 class="box-title">Data Barang</h2>

            <div class="box-body">
                     <!-- <a href="{{ url('transaksi/barang/create') }}" class="btn-sm btn-info ml-3" id="btn_add">Add New Barang</a> -->
                    <br>
                    <br>
                 
                    <table class="table table-bordered table-striped" id="tbl_barang">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Kode Barang</th>
                                <th>Barang</th>
                                <th>Kategori</th>
                                <th>Stok</th>
                                <th>Harga</th>
                                <th>Created at</th>
                                
                            </tr>
                        </thead>
                        </table>
                 
              </div>
        </div>    
    </div>
  </div>
</section>

<div class="modal fade" id="modal_barang" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="barangModal"></h4>
    </div>
    <div class="modal-body">
        <form id="barangForm" name="barangForm" class="form-horizontal" enctype="multipart/form-data">
           <input type="hidden" name="barang_id" id="barang_id">
          
           <div class="form-group">
                <label class="col-sm-1 control-label">Kategori</label>
                <div class="col-sm-12">
                <select class="form-control select2" id="id_kategori_brg" name="id_kategori_brg" style="width: 100%;">
                     <option value="">Pilih Kategori</option>
                </select>
                </div>
              </div>

            <div class="form-group">
                <label for="name" class="col-sm-1 control-label">Barang</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Nama Barang" value="" maxlength="50" required="">
                </div>
            </div>

           
            <div class="form-group">
                <label for="name" class="col-sm-1 control-label">Deskripsi</label>
                <div class="col-sm-12">
                    <textarea class="form-control textarea desc_brg" id="deskripsi_barang" name="deskripsi_barang" rows="10" cols="80"></textarea>
                </div>
            </div>

            <div class="form-group">
              <div class="col-sm-3">
                <label for="name" class="control-label">Stok</label>
               </div> 
                <div class="col-sm-12">
                    <input type="number" class="form-control" id="stok" name="stok" placeholder="Stok" value="" required="">
                </div>
            </div>
            <div class="form-group">
             <div class="col-sm-3">
                <label for="name" class="control-label">Harga</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" id="harga" name="harga" placeholder="Harga" value="" required="">
                </div>
            </div>
            <div class="form-group">
                  <div class="col-sm-3">
                <label for="name" class="control-label">Berat</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" id="berat" name="berat" placeholder="Berat">
                </div>
            </div>
            <div class="form-group">
              <div class="col-sm-3">
                <label for="name" class="control-label">Kondisi Barang</label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" id="cb_kondisi_brg" name="cb_kondisi_brg">
                        <option value="baru">Baru</option>
                        <option value="bekas">Bekas</option>
                      
                    </select>
                </div>
            </div>
          
            <div class="form-group">
              <div class="col-sm-3">
                <label for="name" class="control-label">Status Publish</label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" id="status" name="status">
                        <option value="A">Aktif</option>
                        <option value="N">Non Aktif</option>
                      
                    </select>
                </div>
            </div>
          
            <div class="form-group">
              <div class="col-sm-3">
                <label for="name" class="control-label">Photo Barang (1)</label>
                </div>
                <div class="col-sm-12">
                   <input type="file" id="image" name="image">
                </div>
            </div>
            <div class="form-group">
              <div class="col-sm-3">
                <label for="name" class="control-label">Photo Barang (2)</label>
                </div>
                <div class="col-sm-12">
                   <input type="file" id="file_dua" name="file_dua">
                </div>
            </div>
            <div class="form-group">
              <div class="col-sm-3">
                <label for="name" class="control-label">Photo Barang (3)</label>
                </div>
                <div class="col-sm-12">
                   <input type="file" id="file_tiga" name="file_tiga">
                </div>
            </div>



            <div class="col-sm-offset-2 col-sm-10">
             <button type="submit" class="btn btn-primary" id="btn-save" value="create">Save
             </button>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        
    </div>
</div>
</div>
</div>

@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

<script type="text/javascript"> 
var SITEURL = '{{URL::to('')}}';
var valInsUp;
$(document).ready( function () {

 

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $.ajax({
            url: SITEURL + "/transaksi/barang/listdethobi",
			type: 'GET',
			success: function(data) {
			//	var jsonResult = jQuery.parseJSON( data );
            JSON.stringify(data); //to string
            if(data.length > 0){
                for(i=0; i< data.length; i++){
                    var currenData = data[i];
                    var opt_arr = '<option value="'+ currenData.id +'">'+ currenData.name +'</option>';
				    $('#id_kategori_brg').append(opt_arr);
                }
            }
            $('#id_kategori_brg option:first-child').attr("selected", "selected");

			}
		});
 
    $('.select2').select2();

  $('#tbl_barang').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: SITEURL + "/transaksi/barang",
          type: 'GET',
         },
         columns: [
                  {data: 'id', name: 'id', 'visible': false},
                //  {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                  { data: 'kdbrg', name: 'kdbrg' },
                  { data: 'nmbrg', name: 'nmbrg' },
                  { data: 'nmkatbrg', name: 'nmkatbrg' },
                  { data: 'stok', name: 'stok' },
                  { data: 'harga', name: 'harga' },
                  { data: 'created_at', name: 'created_at' },
                //   {data: 'action', name: 'action', orderable: false},
               ],
        order: [[0, 'desc']]
      });



    /*  When user click add user button */
    $('#create-new-barang').click(function () {
       // valInsUp = 1;
        $('#btn-save').val("create-barang");
        $('#barang_id').val('');
        $('#barangForm').trigger("reset");
        $('#barangModal').html("Add New Barang");
        $('#modal_barang').modal('show');
        
    });  

     /* When click edit user */
     $('body').on('click', '.edit-barang', function () {
        valInsUp = 2;
      var barang_id = $(this).data('id');
      $.get(SITEURL+'/transaksi/barang/' + barang_id +'/edit', function (data) {
         $('#name-error').hide();
      //   $('#keterangan-error').hide();
         $('#barangModal').html("Edit Barang");
          $('#btn-save').val("edit-barang");
          $('#btn-save').html("Update Changes");
          $('#modal_barang').modal('show');
          $('#barang_id').val(data.id);
          $('#name').val(data.nmbrg);
          $('#id_kategori_brg').val(data.idkatbrg);
          $('#deskripsi_barang').text(data.deskripsi_barang);
          $('#cb_kondisi_brg').val(data.kondisi_barang);
          $('#stok').val(data.stok);
          $('#harga').val(data.harga);
          $('#berat').val(data.berat);
      })
   });

   $('body').on('click', '#delete-barang', function () {
         if (confirm("Hapus data?")) {
            var barang_id = $(this).data("id");
            confirm("Are You sure want to delete !");

            $.ajax({
                type: "get",
                url: SITEURL + "/transaksi/barang/delete/"+barang_id,
                success: function (data) {
                var oTable = $('#tbl_barang').dataTable(); 
                oTable.fnDraw(false);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }else {
            
        }
       
    }); 
    
   
        
  
});
if ($("#barangForm").length > 0) {
   // if(valInsUp === 1){
        $("#barangForm").validate({
            submitHandler: function(form) {
        
            var actionType = $('#btn-save').val();
            $('#btn-save').html('Sending..');
            
            $.ajax({
                data: $('#barangForm').serialize(),
                url: SITEURL + "/transaksi/barang/store",
                type: "POST",
                enctype: 'multipart/form-data',
                processData: false,  // Important!
              //  contentType: false,
                cache: false,
                dataType: 'json',
                success: function (data) {
        
                    $('#barangForm').trigger("reset");
                    $('#modal_barang').modal('hide');
                    $('#btn-save').html('Save');
                    var oTable = $('#tbl_barang').dataTable();
                    oTable.fnDraw(false);
                    
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#btn-save').html('Save');
                }
            });
            }
        });
    // } else if(valInsUp === 2){
    //     alert('ini update');
    // }

}
</script>
@endpush
