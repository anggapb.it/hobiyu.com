@extends('layouts.app')


@section('content')
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="content-wrapper">

<section class="content-header">
      <h1>
       From Input Barang
        <!-- <small>Preview</small> -->
      </h1>
     
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <!-- <h3 class="box-title">Select2</h3> -->

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
            <form action="{{ url('transaksi/barang/store') }}" enctype="multipart/form-data" method="POST">
              <div class="alert alert-danger print-error-msg" style="display:none">
                <ul></ul>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group">
                <label>Kategori</label>
                <select class="form-control select2" id="idkatbrg" name="idkatbrg" style="width: 100%;">
                     <option value="">Pilih Kategori</option>
                     @foreach($kategori as $list_kat)
                        <option value="{{ $list_kat->id }}" {{ (isset($selected_katbrg) && $selected_katbrg->id == $list_kat->id) ? 'selected="selected"' : ''  }}>{{ $list_kat->name }}</option>
                    @endforeach
                </select>
              </div>
          
              <!-- /.form-group -->
              <div class="form-group">
                <label>Barang</label>
                <input type="text" class="form-control" id="nmbrg" name="nmbrg" placeholder="Nama Barang" value="" maxlength="50" required="">
              </div>
              <div class="form-group">
                <label>Deskripsi</label>
                <textarea class="form-control textarea desc_brg" id="deskripsi_barang" name="deskripsi_barang" rows="10" cols="80"></textarea>
              </div>
              <div class="form-group">
                <label>Stok</label>
                <input type="number" class="form-control" id="stok" name="stok" placeholder="Stok" value="" required="">
              </div>
              <div class="form-group">
                <label>Harga</label>
                <input type="number" class="form-control" id="harga" name="harga" placeholder="Harga" value="" required="">
              </div>
              <div class="form-group">
                <label>Berat</label>
                <input type="number" class="form-control" id="berat" name="berat" placeholder="Berat">
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                <label>Kondisi Barang</label>
                <select class="form-control" id="kondisi_barang" name="kondisi_barang">
                        <option value="baru">Baru</option>
                        <option value="bekas">Bekas</option>
                      
                    </select>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label>Status Publish</label>
                <select class="form-control" id="status" name="status">
                        <option value="A">Aktif</option>
                        <option value="N">Non Aktif</option>
                      
                    </select>
              </div>
             
              <div class="form-group">
                <label>Photo Barang (1)</label>
                <input type="file" id="gambar_satu" name="gambar_satu">
              </div>
              <div class="form-group">
                <label>Photo Barang (2)</label>
                <input type="file" id="gambar_dua" name="gambar_dua">
              </div>
              <div class="form-group">
                <label>Photo Barang (2)</label>
                <input type="file" id="gambar_tiga" name="gambar_tiga">
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
          </div>
       
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        <button type="submit" class="btn btn-primary btn-save-brg" id="btn-save" value="create">Save
             </button>
        <a href="{{ url('transaksi/barang') }}" class="btn btn-danger" id="btn-save" value="create">Back
            </a>
        </div>
      </div>
      </form>
</section>
</div>

@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="http://malsup.github.com/jquery.form.js"></script>

<script type="text/javascript"> 
var SITEURL = '{{URL::to('')}}';
// $.ajaxSetup({
//       headers: {
//           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//       }
//   });
$(document).ready( function () {
    // $.ajax({
    //         url: SITEURL + "/transaksi/barang/listdethobi",
	// 		type: 'GET',
	// 		success: function(data) {
	// 		//	var jsonResult = jQuery.parseJSON( data );
    //         JSON.stringify(data); //to string
    //         if(data.length > 0){
    //             for(i=0; i< data.length; i++){
    //                 var currenData = data[i];
    //                 var opt_arr = '<option value="'+ currenData.id +'">'+ currenData.name +'</option>';
	// 			    $('#idkatbrg').append(opt_arr);
    //             }
    //         }
    //         $('#idkatbrg option:first-child').attr("selected", "selected");

	// 		}
	// 	});
    $('.select2').select2();
    $("body").on("click",".btn-save-brg",function(e){
    $(this).parents("form").ajaxForm(options);
  });
  var options = { 
    complete: function(response) 
    {
        if($.isEmptyObject(response.responseJSON.error)){
            $("input[name='nmbrg']").val('');
            alert('Upload gambar berhasil.');
            windows.location.reload();
        }else{
            printErrorMsg(response.responseJSON.error);
        }
    }
  };
  function printErrorMsg (msg) {
    $(".print-error-msg").find("ul").html('');
    $(".print-error-msg").css('display','block');
    $.each( msg, function( key, value ) {
        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
    });
  }
});

</script>
@endpush
