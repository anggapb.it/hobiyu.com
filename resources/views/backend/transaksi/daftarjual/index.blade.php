@extends('layouts.app')


@section('content')
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="content-wrapper">
    
    <section class="content-header">
        <h1>
            Daftar Jual
            <small>Transaksi</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Transaksi</a></li>
            <li class="active">Daftar Jual</li>
        </ol>
        </section>

   <section class="content">    
    <div class="box">
        <div class="box-header">
              <h2 class="box-title">Data Daftar Jual</h2>

            <div class="box-body">
                   
                 
                    <table class="table table-bordered table-striped" id="tbl_id">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>No.</th>
                                <th>No Transaksi</th>
                                <th>Customer</th>
                                <th>Total</th>
                                <th>Status</th>
                                <th>Created at</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        </table>
                 
              </div>
        </div>    
    </div>
  </div>
</section>

<div class="modal fade" id="mdl_frm" aria-hidden="true">
<div class="modal-dialog modal-lg">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="FrmModal"></h4>
    </div>
    <div class="modal-body">
            
        <form id="listJualForm" name="listJualForm" class="form-horizontal">
           <input type="hidden" name="order_id" id="order_id">
      


            <div class="form-row">
                    <div class="form-group col-md-6">
                    <label>No Order</label>
                    <input type="text" readOnly="true" class="form-control" id="noorder" name="noorder">
                    </div>
                   
                    <div class="form-group col-md-6">
                    <label>Customer</label>
                    <input type="text"  readOnly="true" class="form-control" id="nmcustomer" name="nmcustomer">
                    </div>
                </div>
            
            <div class="form-row">
                    <div class="form-group col-md-4">
                    <label>Total</label>
                    <input type="number"  readOnly="true" class="form-control" id="total" name="total">
                    </div>
                   
                    <div class="form-group col-md-8">
                    <label>Status</label>
                  
                    <select id="status" name="status" class="form-control">
                      
                        @foreach($storders as $item)
                        <option value="{{ $item->id }}" {{ isset($selected_item) && $selected_item->id == $item->id ? 'selected="selected"' : '' }}>{{ $item->name }}</option>
                      
                        @endforeach
                    </select>
                    </div>
                </div>

        
         
     

                  <table class="table table-bordered table-striped" id="tblOrderdet">
                        <thead>
                            <tr>
                              
                                <th>No.</th>
                                <th>Barang</th>
                                <th>Qty</th>
                                <th>Harga</th>
                                <th>Penjual</th>
                                <th>No Resi</th>
                            </tr>
                        </thead>
                    
                  </table>
                      

    </div>
    <div class="modal-footer">
    <div class="row">
            <div class="col-lg-11 margin-tb">
                <div class="pull-right">
                <button type="submit" class="btn btn-primary" id="btn-save">Submit</button>
                </div>
                
            </div>
            </div>
    </div>

    </form>
</div>
</div>
</div>




@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

<script type="text/javascript"> 
var SITEURL = '{{URL::to('')}}';
var orderidx;
$(document).ready( function () {
 

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  $('#tbl_id').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: SITEURL + "/transaksi/list-jual",
          type: 'GET',
         },
         columns: [
                  {data: 'id', name: 'id', 'visible': false},
                  {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                  { data: 'noorder', name: 'noorder' },
                  { data: 'nmcustomer', name: 'nmcustomer' },
                  { data: 'total', name: 'total' },
                  { data: 'nmstatus', name: 'nmstatus' },      
                 
                  { data: 'created_at', name: 'created_at' },
                  {data: 'action', name: 'action', orderable: false},
               ],
        order: [[0, 'desc']]
      });



     /* When click edit user */
     $('body').on('click', '.edit-btn', function () {
      var order_id = $(this).data('id');
      $.get(SITEURL+'/transaksi/list-jual/verifikasi/'+ order_id, function (data) {
         $('#name-error').hide();
         $('#FrmModal').html("Verifikasi Data Jual");
         

         $('#tblOrderdet').DataTable({
         processing: true,
         serverSide: true,
         destroy: true,
         paging: false,
         searching: false,
         ajax: {
          url: SITEURL + "/transaksi/list-jual/orderdet/"+order_id,
          type: 'GET',
         },
          columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                    { data: 'nmbrg', name: 'nmbrg' },
                    { data: 'qty', name: 'qty' },
                    { data: 'harga', name: 'harga' },
                    { data: 'nmpenjual', name: 'nmpenjual' },      
                    { data: 'noresi', name: 'noresi' },         
                ],
          order: [[0, 'desc']]
         });


          $('#mdl_frm').modal('show');
          $('#order_id').val(data.id);
          $('#noorder').val(data.noorder);
          $('#nmcustomer').val(data.nmcustomer);
          $('#status').val(data.status);
          $('#total').val(data.total);  
          orderidx = data.id;
      })
   });

 
   
        
  
});
if ($("#listJualForm").length > 0) {

    $("#listJualForm").validate({
            submitHandler: function(form) {
        
            var actionType = $('#btn-save').val();
            $('#btn-save').html('Update..');
            
            $.ajax({
                data: $('#listJualForm').serialize(),
                url: SITEURL + "/transaksi/list-jual/update",
                type: "POST",
                dataType: 'json',
                success: function (data) {
        
                    $('#listJualForm').trigger("reset");
                    $('#mdl_frm').modal('hide');
                    var oTable = $('#tbl_id').dataTable();
                    oTable.fnDraw(false);
                    
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
            }
        });   
        
  

}
</script>
@endpush
