<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<meta name="keywords" content="MediaCenter, Template, eCommerce">
<meta name="robots" content="all">
<title>Hobiyu - Sign Up</title>

<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

<!-- Customizable CSS -->
<link rel="stylesheet" href="{{asset('css/main.css')}}">
<link rel="stylesheet" href="{{asset('css/blue.css')}}">
<link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
<link rel="stylesheet" href="{{asset('css/owl.transitions.css')}}">
<link rel="stylesheet" href="{{asset('css/animate.min.css')}}">
<link rel="stylesheet" href="{{asset('css/rateit.css')}}">
<link rel="stylesheet" href="{{asset('css/bootstrap-select.min.css')}}">

<!-- Icons/Glyphs -->
<link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">

<!-- Fonts -->
<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
</head>
<body class="cnt-home">
<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1"> 
  
  <!-- ============================================== TOP MENU ============================================== -->
  <div class="top-bar animate-dropdown">
    <div class="container">
      <div class="header-top-inner">
        <div class="cnt-account">
       
        </div>
        <!-- /.cnt-account -->
        
        <!-- /.cnt-cart -->
       
      </div>
      <!-- /.header-top-inner --> 
    </div>
    <!-- /.container --> 
  </div>
  <!-- /.header-top --> 
  <!-- ============================================== TOP MENU : END ============================================== -->
<div class="main-header"><div class="container">
        <div class="col-xs-12 col-sm-12 col-md-3 logo-holder"> 
          <div class="logo"> <a href="{{ url('/') }}"> <img src="{{asset('images/logo.png')}}" alt="logo"> </a> </div>
       
	  		</div>	
	
    
</div>
  <!-- /.main-header --> 
  
  
  <!-- /.header-nav --> 
  <!-- ============================================== NAVBAR : END ============================================== --> 
  
</header>

<!-- ============================================== HEADER : END ============================================== -->
<div class="body-content">
		<div class="container">
		<div class="sign-in-page">
			<div class="row">
<!-- create a new account -->
<div class="col-md-6 col-sm-6 create-new-account">
	
	<p class="text title-tag-line">Daftar Member Baru.</p>
  @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    <form method="POST" action='{{ url("register/$url") }}' aria-label="{{ __('Register') }}">
		<div class="form-group">
    {{ csrf_field() }}
    {{ method_field('post') }}
		    <label class="info-title" for="name">Name <span>*</span></label>
		    <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}"  >
        @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
		</div>
		<div class="form-group">
	    	<label class="info-title" for="email">Email Address <span>*</span></label>
	    	<input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" >
        @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
    	</div>
       
        <div class="form-group">
		    <label class="info-title" for="phone">Phone Number <span>*</span></label>
		    <input type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" >
        @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
  	</div>
        <div class="form-group">
		    <label class="info-title" for="password">Password <span>*</span></label>
		    <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('password') }}" >
        @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
  	</div>
         <div class="form-group">
		    <label class="info-title" for="confirm_password">Confirm Password <span>*</span></label>
		    <input type="password" id="password-confirm" class="form-control" name="password_confirmation"  >
        @if ($errors->has('password_confirmation'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
		</div>
         <div class="form-group">
         <input type="checkbox" name="policy" value="1"> Saya telah membaca dan menyetujui <a href="javascript:void(0)" class="modalPolicy">Kebijakan Privasi</a> Hobiyu<br>
        @if ($errors->has('policy'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('policy') }}</strong>
                                    </span>
                                @endif
		</div>
	  	<button type="submit" class="btn-upper btn btn-primary checkout-page-button">Sign Up</button>
	</form>
	
	
</div>	
<!-- create a new account -->			</div><!-- /.row -->
		</div><!-- /.sigin-in-->
		<!-- ============================================== BRANDS CAROUSEL ============================================== -->

</div>

<div class="modal fade" id="modal_policy" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
   
      <!-- <form action="{{ route('customer.update-barang') }}" method="POST" enctype="multipart/form-data">
          @csrf -->
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Kebijakan Privasi Hobiyu</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
    
      <!-- Modal body -->
	  <div class="form-row">
                    <div class="form-group col-md-12">
		<center><h5><b>Peraturan, Tata Tertib dan kebijakan dalam registrasi account baru.</b></h5></center>
		<hr>
					<ul>
		<li>A. Setiap account  yang akan daftar ke hobiyu.com  wajib mengisi data yang benar dan sesuai dengan data identitas diri  (KTP)  & No HP yang aktif
</li><br>
		<li>B. Setiap account  yang sudah terdaftar wajib mematuhi tata tertib yang di keluarkan oleh hobiyu.com.
</li><br>
		<li>C. Barang yang di jual belikan atau lelang tidak diperbolehkan barang yang dilindungi atau yang  di larang oleh  Undang – Undang hukum  negara Republik Indonesia.
</li><br>
		<li>D. jika terjadi secara sengaja atau tidak sengaja, tetap melakukan jual/beli/lelang barang yang di larang oleh Undang – Undang hukum negara Republik Indonesia, admin akan langsung menghapus content yang di jual/beli/lelang tanpa konfirmasi terlebih dahulu,  serta akan  mem blokir account yang bersangkutan.  Dan segala bentuk resiko  hukum yang terjadi atas pelanggaran account tersebut, sepenuhnya menjadi  tanggung jawab oleh account  yang  bersangkutan.
</li><br>
		<li>E. Segala perkara proses transaksi antara account  penjual dan account pembeli / account  pelelang yang bermasalah,  (ketidak sesuaian barang, keterlambatan pengiriman, dll) sebaiknya jalan keluar tersebut  di lakukan dengan cara kekeluargaan.</li><br>
		<li>F. Hobiyu.com hanya sebagai media  fasilitator yang menjadi wadah  antara penjual dan pembeli. 
</li>
	</ul>
                    </div>
                   
                </div>
   


      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
      
 
    </div>
  </div>
</div>
<!-- /#top-banner-and-menu --> 

<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="footer color-bg">
@include('web.footerweb')
</footer>
<!-- ============================================================= FOOTER : END============================================================= --> 

<!-- For demo purposes – can be removed on production --> 

<!-- For demo purposes – can be removed on production : End --> 

<!-- JavaScripts placed at the end of the document so the pages load faster --> 
<script src="{{asset('js/jquery-1.11.1.min.js')}}"></script> 
<script src="{{asset('js/bootstrap.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-hover-dropdown.min.js')}}"></script> 
<script src="{{asset('js/owl.carousel.min.js')}}"></script> 
<script src="{{asset('js/echo.min.js')}}"></script> 
<script src="{{asset('js/jquery.easing-1.3.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-slider.min.js')}}"></script> 
<script src="{{asset('js/jquery.rateit.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/lightbox.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-select.min.js')}}"></script> 
<script src="{{asset('js/wow.min.js')}}"></script> 
<script src="{{asset('js/scripts.js')}}"></script>

<script>
$(document).ready( function () {
	$(".modalPolicy").click(function () {
		$('#modal_policy').modal('show');
	});
});
</script>
</body>
</html>