@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="{{ asset('ui-backend/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('ui-backend/datatables.net-bs/css/dataTables.bootstrap.css')}}">
   
    <!-- <link rel="stylesheet" href="{{ asset('plugins/datatables/smantic/dataTables.semanticui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables/smantic/responsive.semanticui.min.css') }}"> -->
    <link rel="stylesheet" href="{{ asset('ui-backend/sweetalert/sweetalert2.css') }}">
@append

@section('js')
<script src="{{ asset('ui-backend/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('ui-backend/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    <script src="{{ asset('ui-backend/datatables.net/jquery.dataTables.js') }}"></script>
   
    <!-- <script src="{{ asset('plugins/datatables/smantic/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/smantic/responsive.semanticui.min.js') }}"></script> -->
    <script src="{{ asset('ui-backend/sweetalert/sweetalert2.js') }}"></script>
@append

@section('scripts')
    <script type="text/javascript">
        // global
        var dt = "";
        var formRules = [];
        var initModal = function(){
            return false;
        };
        var unModal = function(){
            return false;
        };

        $.fn.form.settings.prompt = {
            empty                : 'The {name} field is required.',
            checked              : 'The {name} field is required.',
            email                : 'The {name} must be a valid email address.',
            url                  : 'The {name} format is invalid.',
            regExp               : 'The {name} is not formatted correctly',
            integer              : 'The {name} must be an integer.',
            decimal              : '{name} must be a decimal number',
            number               : 'The {name} must be a number.',
            is                   : '{name} must be "{ruleValue}"',
            isExactly            : '{name} must be exactly "{ruleValue}"',
            not                  : '{name} cannot be set to "{ruleValue}"',
            notExactly           : '{name} cannot be set to exactly "{ruleValue}"',
            contain              : '{name} cannot contain "{ruleValue}"',
            containExactly       : '{name} cannot contain exactly "{ruleValue}"',
            doesntContain        : '{name} must contain  "{ruleValue}"',
            doesntContainExactly : '{name} must contain exactly "{ruleValue}"',
            minLength            : 'The {name} must be at least {ruleValue} characters.',
            length               : '{name} must be at least {ruleValue} characters',
            exactLength          : '{name} must be exactly {ruleValue} characters',
            maxLength            : 'The {name} may not be greater than {ruleValue} characters.',
            match                : '{name} must match {ruleValue} field',
            different            : '{name} must have a different value than {ruleValue} field',
            creditCard           : '{name} must be a valid credit card number',
            minCount             : '{name} must have at least {ruleValue} choices',
            exactCount           : '{name} must have exactly {ruleValue} choices',
            maxCount             : '{name} must have {ruleValue} or less choices'
        };

    </script>
    @yield('rules')
    @yield('init-modal')
    
    @include('layouts.scripts.datatable')
    @include('layouts.scripts.actions')
@append

@section('content')
    @section('content-header')
        <div class="title-container">
           
            <h2 class="ui header">
                <div class="content">
                    {!! $title or '-' !!}
                    <div class="sub header">{!! $subtitle or ' ' !!}</div>
                </div>
            </h2>
        </div>
    @show

    <div class="ui clearing divider" style="border-top: none !important; margin:10px"></div>

    @section('content-body')
    <div class="ui grid">
        <div class="sixteen wide column main-content">
            <div class="ui segments">
                <div class="ui segment">
                    <form class="ui filter form">
                        <div class="inline fields">
                           
                            @section('filters')
                                <div class="field">
                                    <input name="filter[kode]" placeholder="Kode Area" type="text">
                                </div>
                                <div class="field">
                                    <input name="filter[area]" placeholder="Area" type="text">
                                </div>
                                <button type="button" class="ui teal icon filter button" data-content="Cari Data">
                                    <i class="search icon"></i>
                                </button>
                                <button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
                                    <i class="refresh icon"></i>
                                </button>
                            @show
                            <div style="margin-left: auto; margin-right: 1px;">
                                @section('toolbars')
                                @if($pagePerms == '' || auth()->user()->can($pagePerms.'-add'))
                                    <button type="button" class="ui blue add button">
                                        <i class="plus icon"></i>
                                        Add New
                                    </button>
                                @endif
                                {{-- <button type="button" class="ui green button">
                                    <i class="file excel outline icon"></i>
                                    Export Excel
                                </button> --}}
                                @show
                            </div>
                        </div>
                    </form>

                    @section('subcontent')
                        @if(isset($tableStruct))
                        <table id="listTable" class="ui celled compact red responsive table" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    @foreach ($tableStruct as $struct)
                                        <th class="center aligned">{{ $struct['title'] or $struct['name'] }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @yield('tableBody')
                            </tbody>
                        </table>
                        @endif
                    @show
                </div>
            </div>
        </div>
    </div>
    @show
@endsection

@section('modals')
<div class="ui {{ $modalSize or 'mini' }} modal" id="formModal">
    <div class="ui inverted loading dimmer">
        <div class="ui text loader">Loading</div>
    </div>
</div>
@append