<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta -->
@include('web.headweb')
</head>
<body class="cnt-home">
<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1"> 
  
  <!-- ============================================== TOP MENU ============================================== -->
  <div class="top-bar animate-dropdown">
    <div class="container">
      <div class="header-top-inner">
        <div class="cnt-account">
          <ul class="list-unstyled">
            <!-- <li><a href="#"><i class="icon fa fa-user"></i>My Account</a></li> -->
            
              @if(!Auth::guard('customer')->check()) 
              <li><a href="{{ url('login/customer') }}"><i class="icon fa fa-lock"></i>Login</a></li>
              <li><a href="{{ url('register/customer') }}"><i class="icon fa fa-user"></i>Daftar Member</a></li>
              @endif      
              @if(Auth::guard('customer')->check())  
              <li><a href="{{ url('customer/saldo') }}"><i class="icon fa fa-home"></i>Dashboard</a></li>
                <li><a href="#"><i class="icon fa fa-user"></i> 
                  Selamat Datang {{ Auth::guard('customer')->user()->name  }}
                    </li> </a>
                    <li><a href="{{ url('logout/customer') }}"><i class="icon fa fa-lock"></i> 
                     Logout
                    </a></li> 
              @endif      
              
          </ul>
        </div>
        <!-- /.cnt-account -->
        
        <!-- /.cnt-cart -->
        <div class="clearfix"></div>
      </div>
      <!-- /.header-top-inner --> 
    </div>
    <!-- /.container --> 
  </div>
  <!-- /.header-top --> 
  <!-- ============================================== TOP MENU : END ============================================== -->
  <div class="main-header">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 logo-holder"> 
          <!-- ============================================================= LOGO ============================================================= -->
          <div class="logo"> <a href="{{ url('/') }}"> <img src="{{asset('images/logo.png')}}" alt="logo"> </a> </div>
          <!-- /.logo --> 
          <!-- ============================================================= LOGO : END ============================================================= --> </div>
        <!-- /.logo-holder -->
        
        <div class="col-xs-12 col-sm-12 col-md-7 top-search-holder"> 
          <!-- /.contact-row --> 
          <!-- ============================================================= SEARCH AREA ============================================================= -->
          @include('web.searchbar')
          <!-- /.search-area --> 
          <!-- ============================================================= SEARCH AREA : END ============================================================= --> </div>
        <!-- /.top-search-holder -->
        
          @include('web.cart')
      <!-- /.row --> 
      
    </div>
    <!-- /.container --> 
    
  </div>
  <!-- /.main-header --> 
  
  <!-- ============================================== NAVBAR ============================================== -->
  @include('web.menuatas')
  <!-- /.header-nav --> 
  <!-- ============================================== NAVBAR : END ============================================== --> 
  
</header>
 
<!-- ============================================== HEADER : END ============================================== -->
<div class="body-content outer-top-xs" id="top-banner-and-menu">
  <div class="container">
    <div class="row"> 
      <!-- ============================================== SIDEBAR ============================================== -->
      <div class="col-xs-12 col-sm-12 col-md-3 sidebar"> 
     
        <!-- ================================== TOP NAVIGATION ================================== -->
       @include('web.sidemenu-category')
        <!-- /.side-menu --> 
        <!-- ================================== TOP NAVIGATION : END ================================== --> 
        
        <!-- ============================================== HOT DEALS ============================================== -->
      
        <!-- ============================================== HOT DEALS: END ============================================== --> 
        
        <!-- ============================================== SPECIAL OFFER ============================================== -->
        
        <!-- <div class="sidebar-widget outer-bottom-small wow fadeInUp">
          <h3 class="section-title">Barang di jual</h3>
          <div class="sidebar-widget-body outer-top-xs">
            <div class="owl-carousel sidebar-carousel special-offer custom-carousel owl-theme outer-top-xs">
              <div class="item">
                <div class="products special-product">
                  <div class="product">
                    <div class="product-micro">
                      <div class="row product-micro-row">
                        <div class="col col-xs-5">
                          <div class="product-image">
                            <div class="image"> <a href="#"> <img src="{{asset('images/img/bunga2.jpg')}}" alt=""> </a> </div>
                      
                            
                          </div>
                        </div>
                  
                        <div class="col col-xs-7">
                          <div class="product-info">
                            <h3 class="name"><a href="#">Bunga Hias <br>Artificial</a></h3>
                     
                            <div class="product-price"> <span class="price"> Rp. 90.000 </span> </div>
                           
                            
                          </div>
                        </div>
                   
                      </div>
               
                    </div>
                 
                    
                  </div>
                  <div class="product">
                    <div class="product-micro">
                      <div class="row product-micro-row">
                        <div class="col col-xs-5">
                          <div class="product-image">
                            <div class="image"> <a href="#"> <img src="{{asset('images/img/iguana.jpg')}}" alt=""> </a> </div>
                         
                            
                          </div>
                       
                        </div>
                      
                        <div class="col col-xs-7">
                          <div class="product-info">
                            <h3 class="name"><a href="#">Forrest Dragon <br>Iguana</a></h3>
                            <div class="product-price"> <span class="price"> Rp. 70.000 </span> </div>
                          
                            
                          </div>
                        </div>
                     
                      </div>
               
                    </div>
             
                  </div>
                
                </div>
              </div>
            </div>
          </div>
        </div> -->
        <!-- /.sidebar-widget --> 
        <!-- ============================================== SPECIAL OFFER : END ============================================== --> 
        
        <!-- ============================================== Testimonials============================================== -->
      
        
        <!-- ============================================== Testimonials: END ============================================== -->
        
        <!-- <div class="home-banner"> <img src="assets/images/img/03.jpg" alt="Image"> </div> -->
      </div>
      <!-- /.sidemenu-holder --> 
      <!-- ============================================== SIDEBAR : END ============================================== --> 
      
      <!-- ============================================== CONTENT ============================================== -->
      <div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder"> 
        <!-- ========================================== SECTION – HERO ========================================= -->
        
        <div id="hero">
          <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">  
          @foreach($sliders as $item) 
            <div class="item" style="background-image: url({{asset('uploads/slider/'.$item->slider)}});">
       
            </div>
            <!-- /.item --> 
          @endforeach  
          </div>
          <!-- /.owl-carousel --> 
        </div>
        
        <!-- ========================================= SECTION – HERO : END ========================================= --> 
        
        <!-- ============================================== INFO BOXES ============================================== -->
       
        <!-- /.info-boxes --> 
        <!-- ============================================== INFO BOXES : END ============================================== --> 
        <!-- ============================================== SCROLL TABS ============================================== -->
      
        <br>
        <br>
        <br>
        <br>
       
       
      
        <div id="lelang-will-end-tabs-slider" class="scroll-tabs outer-top-vs wow fadeInUp">
          <div class="more-info-tab clearfix ">
            <h3 class="new-product-title pull-left">Lelang Terbaru</h3>
          
          </div>
          
          <div class="tab-content outer-top-xs">
            <div class="tab-pane in active" id="all">
              <div class="product-slider">
                <div class="owl-carousel home-owl-carousel custom-carousel owl-theme" data-item="4">
                  
                @foreach($lelang_new as $lelang) 
                 
                   
                        <div class="item item-carousel">
                          <div class="products">
                            <div class="product">
                              <div class="product-image">
                                <div class="image"> <a href="{{url('/lelang/detail',$lelang->id)}}"><img  src="{{asset('uploads/item_lelang/'.$lelang->photo_item)}}" width="200" height="200" alt=""></a> </div>
                                <!-- /.image -->
                                @if($lelang->idstlelang != 3)
                                <div class="tag sale"><span>{{ $bid::where("idlelang", $lelang->id)->count() }} bid</span></div>
                                @else
                                <div class="tag new"><span>CLOSE</span></div>
                                @endif
                                
                              </div>
                              <!-- /.product-image -->
                              
                              <div class="product-info text-left">
                                <h3 class="name"><a href="{{url('/lelang/detail',$lelang->id)}}">{{ $lelang->nmitem }}</a></h3>
                                <!--<div class="rating rateit-small"></div>-->
                                <!-- <div class="description"></div> -->
                                <div class="product-price"> <span class="price"> Rp. {{ number_format($lelang->hrgawal,0,',','.') }} </span></div>
                                <!-- /.product-price --> 
                               <!--  <p>{{ \Carbon\Carbon::parse($lelang->tgl_bts_penawaran) }}</p> -->
                                 <h6 class="section-title"> <div style="color: #ff7878" data-countdown="{{ $lelang->tgl_bts_penawaran }}"></div></h6>
                                 <h6 class="section-title"> <div style="color: black"><span class="icon fa fa-map-marker"></span>&nbsp;{{$lelang->lokasi_barang}}</div></h6>
                               
                                
                                
                              </div>
                              <!-- /.product-info -->
                            
                              <!-- /.cart --> 
                            </div>
                            <!-- /.product --> 
                            
                          </div>
                          <!-- /.products --> 
                        </div>
                 
                 
                  <!-- /.item -->
                @endforeach  
                  




                </div>
                <!-- /.home-owl-carousel --> 
              </div>
              <!-- /.product-slider --> 
            </div>
            <!-- /.tab-pane -->
            
            
          </div>
          <!-- /.tab-content --> 
        </div>


        <div id="product-tabs-slider" class="scroll-tabs outer-top-vs wow fadeInUp">
          <div class="more-info-tab clearfix ">
            <h3 class="new-product-title pull-left">Lelang Akan Berakhir</h3>
           
          </div>
          
          <div class="tab-content outer-top-xs">
            <div class="tab-pane in active" id="all">
              <div class="product-slider">
                <div class="owl-carousel home-owl-carousel custom-carousel owl-theme" data-item="4">
                  
                @foreach($lelang_will_end as $lelang)
                   
                 
                           <div class="item item-carousel">
                    <div class="products">
                      <div class="product">
                        <div class="product-image">
                          <div class="image"> <a href="{{url('/lelang/detail',$lelang->id)}}"><img  src="{{asset('uploads/item_lelang/'.$lelang->photo_item)}}" width="200" height="200" alt=""></a> </div>
                          <!-- /.image -->
                          @if($lelang->idstlelang != 3)
                          <div class="tag sale"><span>{{ $bid::where("idlelang", $lelang->id)->count() }} bid</span></div>
                          @else
                          <div class="tag new"><span>CLOSE</span></div>
                          @endif
                        </div>
                        <!-- /.product-image -->
                        
                        <div class="product-info text-left">
                          <h3 class="name"><a href="{{url('/lelang/detail',$lelang->id)}}">{{ $lelang->nmitem }}</a></h3>
                          <!--<div class="rating rateit-small"></div>-->
                          <!-- <div class="description"></div> -->
                          <div class="product-price"> <span class="price"> Rp. {{ number_format($lelang->hrgawal,0,',','.') }} </span></div>
                           <h6 class="section-title"> <div style="color: #ff7878" data-countdown="{{ $lelang->tgl_bts_penawaran }}"></div></h6>
                           <h6 class="section-title"> <div style="color: black"><span class="icon fa fa-map-marker"></span>&nbsp;{{$lelang->lokasi_barang}}</div></h6>
                              
                          <!-- /.product-price --> 
                          
                          
                        </div>
                        <!-- /.product-info -->
                      
                        <!-- /.cart --> 
                      </div>
                      <!-- /.product --> 
                      
                    </div>
                    <!-- /.products --> 
                  </div>               
                    
                  <!-- /.item -->
                @endforeach  
                  




                </div>
                <!-- /.home-owl-carousel --> 
              </div>
              <!-- /.product-slider --> 
            </div>
            <!-- /.tab-pane -->
            
            
          </div>
          <!-- /.tab-content --> 
        </div>
        <!-- /.scroll-tabs --> 
        <!-- ============================================== SCROLL TABS : END ============================================== --> 


        <!-- ============================================== WIDE PRODUCTS ============================================== -->
        <div class="wide-banners wow fadeInUp outer-bottom-xs">
          <div class="row">
            <div class="col-md-12">
              <div class="wide-banner cnt-strip">
                <div class="image"> <img class="img-responsive" src="assets/images/img/sport.jpg" alt=""> </div>
                <div class="strip strip-text">
                  <div class="strip-inner">
                    <h2 class="text-right">Mens Sport Fashion<br>
                      <span class="shopping-needs">Murah dan Berkualitas</span></h2>
                  </div>
                </div>
                <div class="new-label">
                  <div class="text">NEW</div>
                </div>
                <!-- /.new-label --> 
              </div>
              <!-- /.wide-banner --> 
            </div>
            <!-- /.col --> 
            
          </div>
          <!-- /.row --> 
        </div>
        <!-- /.wide-banners --> 
        <!-- ============================================== WIDE PRODUCTS : END ============================================== --> 
        <!-- ============================================== BEST SELLER ============================================== -->
        
      
        <!-- /.sidebar-widget --> 
        <!-- ============================================== BEST SELLER : END ============================================== --> 
        
        <!-- ============================================== BLOG SLIDER ============================================== -->
        
        <!-- /.section --> 
        <!-- ============================================== BLOG SLIDER : END ============================================== --> 
        
        
        
      </div>
      <!-- /.homebanner-holder --> 
      <!-- ============================================== CONTENT : END ============================================== --> 
    </div>
    <!-- /.row --> 
    <!-- ============================================== BRANDS CAROUSEL ============================================== -->
    <div id="brands-carousel" class="logo-slider wow fadeInUp">
      <div class="logo-slider-inner">
        <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
          <div class="item m-t-15"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item m-t-10"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand3.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand6.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item--> 
        </div>
        <!-- /.owl-carousel #logo-slider --> 
      </div>
      <!-- /.logo-slider-inner --> 
      
    </div>
    <!-- /.logo-slider --> 
    <!-- ============================================== BRANDS CAROUSEL : END ============================================== --> 
  </div>
  <!-- /.container --> 
</div>
<!-- /#top-banner-and-menu --> 
@include('web.footerwebfix')
<script type="text/javascript">
   $(document).ready(function(){
        $('[data-countdown]').each(function() {
        var $this = $(this), finalDate = $(this).data('countdown');
        $this.countdown(finalDate, function(event) {
          $this.html(event.strftime('%D Hari %H:%M:%S'));
        });
      });
});

</script>
</body>
</html>