@foreach($items as $item)
	@if(is_null($item->perms) || auth()->user()->can($item->perms.'-view'))
		@if(!$item->hasChildren())
             <li><a href="{!! $item->url() !!}" tabindex="{{ $item->id }}"><i class="fa fa-book"></i> <span>{!! $item->title !!}</span></a></li>
		@else
        <li class="treeview" tabindex="{{ $item->id }}">
          <a href="#">
            <i class="fa fa-th"></i> <span>{!! $item->title !!}</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" tabindex="-1">
         
            @foreach ($item->children() as $child)
					@if(is_null($child->perms) || auth()->user()->can($child->perms.'-view'))
						@if(!$child->hasChildren())
                        <li><a href="{!! $child->url() !!}" class="fa fa-circle-o">
								<i class="{{ $child->icon }} icon"></i>&nbsp;&nbsp;{!! $child->title !!}
							</a></li>
						@else
                        <li class="treeview">
                            <a href="#"><i class="fa fa-circle-o"></i> {!! $child->title !!}
                                <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                               
                                @foreach ($child->children() as $grandChild)
										@if(is_null($grandChild->perms) || auth()->user()->can($grandChild->perms.'-view'))
											<a href="{!! $grandChild->url() !!}" class="fa fa-minus">&nbsp;&nbsp;{!! $grandChild->title !!}</a>
										@endif
									@endforeach
                            </ul>
                        </li>
							
						@endif
					@endif
				@endforeach
          </ul>
        </li>
		@endif
	@endif
@endforeach