<!DOCTYPE html>
<html lang="en">
<head>
@include('web.headweb')
</head>
<body class="cnt-home">
<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1"> 
  @include('web.headerwebcust')
</header>

<!-- ============================================== HEADER : END ============================================== -->
<div class="body-content outer-top-xs" id="top-banner-and-menu">
  <div class="container">
    <div class="row"> 
      <!-- ============================================== SIDEBAR ============================================== -->
      <div class="col-xs-12 col-sm-12 col-md-3 sidebar"> 
        
        <!-- ================================== TOP NAVIGATION ================================== -->
        <div class="sidebar-module-container">
          <div class="sidebar-filter"> 
            <!-- ============================================== SIDEBAR CATEGORY ============================================== -->
                @include('web.sidebarcust')            
            </div>
        
          </div>
          <!-- /.sidebar-filter --> 
        </div>
        <!-- /.side-menu --> 
        <!-- ================================== TOP NAVIGATION : END ================================== --> 
      </div>
      <!-- /.sidemenu-holder --> 
      <!-- ============================================== SIDEBAR : END ============================================== --> 
      
      <!-- ============================================== CONTENT ============================================== -->
      <div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder"> 
      
        
        <!-- ============================================== BLOG SLIDER ============================================== -->
        <section class="section latest-blog outer-bottom-vs wow fadeInUp">
          <h3 class="section-title">Jual Barang</h3>
          <br>
        <div id="container_list_lelang">  
          <div class="row">
            <div class="col-lg-11 margin-tb">
                <div class="pull-right">
                    <a class="btn btn-warning" href="{{ route('customer.toko') }}"> Back</a>
                </div>
                
            </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
            <br>
          <div class="frm_add_lelang">
          <form action="{{ route('customer.insert-barang') }}" method="POST" enctype="multipart/form-data">
          @csrf
         
                <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="idhobi">Kategori</label><span style="color:red">*</span>
                    <select id="idhobi" name="idhobi" class="form-control">
                        <option value="" selected>Pilih...</option>
                        @foreach($hobbies as $hobi)
                        <option value="{{ $hobi->id }}" {{ isset($selected_hobi) && $selected_hobi->id == $hobi->id ? 'selected="selected"' : '' }}>{{ $hobi->name }}</option>
                      
                        @endforeach
                     
                    </select>
                    </div>
                    <div class="form-group col-md-6">
                    <label for="idhobidet">Lebih Spesifik</label><span style="color:red">*</span>
                    <select id="idhobidet" name="idhobidet" class="form-control">
                        <!-- <option selected>Pilih...</option>
                        @foreach($hobidets as $hobidet)
                        <option value="{{ $hobidet->id }}" {{ isset($selected_hobidet) && $selected_hobidet->id == $hobidet->id ? 'selected="selected"' : '' }}>{{ $hobidet->name }}</option>
            
                        @endforeach -->
                     
                    </select>
                    </div>   
                    
                   
                </div>

          <div class="form-row">
                    <div class="form-group col-md-12">
                    <label>Nama Barang</label><span style="color:red">*</span>
                    <input type="text" class="form-control" id="nmbrg" name="nmbrg" placeholder="Nama barang yang akan dijual">
                    </div>
                   
                </div>
          <div class="form-row">
                  
                    <div class="form-group col-md-12">
                    <label>SKU</label><span style="color:red">*</span>
                    <input type="text" class="form-control" id="kdbrg" name="kdbrg" placeholder="SKU (Stock Keeping Unit) adalah nomor unik pada tiap barang">
                    </div>
                   
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                    <label>Stok</label><span style="color:red">*</span>
                    <input type="number" class="form-control" id="stok" name="stok">
                    </div>
                   
                    <div class="form-group col-md-6">
                    <label>Perkiraan Berat</label><span style="color:red">*</span>
                    <input type="number" class="form-control" id="berat" name="berat" placeholder="Satuan Gram (gr)">
                    </div>
                </div>
              
                <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="kondisi_barang">Kondisi</label><span style="color:red">*</span>
                    <select id="kondisi_barang" name="kondisi_barang" class="form-control">
                        <option selected>Pilih...</option>
                        <option>Baru</option>
                        <option>Bekas</option>
                    </select>
                    </div>
                    <div class="form-group col-md-6">
                    <label for="harga">Harga</label><span style="color:red">*</span>
                    <input type="number" class="form-control" id="harga" name="harga" placeholder="">
                    </div>
                   
                </div>
                <div class="form-group col-md-12">
                    <label for="deskripsi">Deskripsi</label><span style="color:red">*</span>
                    <textarea class="form-control" id="deskripsi" name="deskripsi" rows="5"></textarea>
                </div>

                 <div class="form-row">
                    <div class="form-group col-md-12">
                    <label>Lokasi Barang</label><span style="color:red">*</span>
                    <input type="text" class="form-control" id="lokasi_barang" name="lokasi_barang">
                    </div>
                 
                </div>    

                <div class="form-row">
                    <div class="form-group col-md-6">
                    <label for="photo_item">Photo Utama (Max. 8 Mb)</label><span style="color:red">*</span>
                    <input type="file" class="form-control-file" id="photo_item" name="photo_item" require="" >
                    </div>
                    <div class="form-group col-md-6">
                    <label>Photo Lainnya (Max. 8 Mb)</label>
                    <input type="file" class="form-control-file" id="photo_item2" name="photo_item2" >
                    </div>
                   
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                    <label>Photo Lainnya (Max. 8 Mb)</label>
                    <input type="file" class="form-control-file" id="photo_item3" name="photo_item3"  >
                    </div>
                              
                </div>
              
               
               

                <div class="form-row">
                    <div class="form-group col-md-6">
                    <label for="photo_item">Video 1  (Max. 15 Mb)</label>
                    <input type="file" class="form-control-file" id="video1" name="video1">
                    </div>
                    <div class="form-group col-md-6">
                    <label>Video 2  (Max. 15 Mb)</label>
                    <input type="file" class="form-control-file" id="video2" name="video2" >
                    </div>
                   
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                    <label for="photo_item">Video 3  (Max. 15 Mb)</label>
                    <input type="file" class="form-control-file" id="video3" name="video3" >
                    </div>
                  
                </div>

             
                <div class="row">
            <div class="col-lg-11 margin-tb">
                <div class="pull-right">
                <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                
            </div>
            </div>
              
              <br>
              <br>
                </form>
          </div>
            <!-- /.owl-carousel --> 
         
          <!-- /.blog-slider-container --> 
         </div> 
        </section>
        <!-- /.section --> 
        <!-- ============================================== BLOG SLIDER : END ============================================== --> 
        
        
        
      </div>
      <!-- /.homebanner-holder --> 
      <!-- ============================================== CONTENT : END ============================================== --> 
    </div>
    <!-- /.row --> 
    <!-- ============================================== BRANDS CAROUSEL ============================================== -->
    <div id="brands-carousel" class="logo-slider wow fadeInUp">
      <div class="logo-slider-inner">
        <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
          <div class="item m-t-15"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item m-t-10"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand3.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand6.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item--> 
        </div>
        <!-- /.owl-carousel #logo-slider --> 
      </div>
      <!-- /.logo-slider-inner --> 
      
    </div>
    <!-- /.logo-slider --> 
    <!-- ============================================== BRANDS CAROUSEL : END ============================================== --> 
  </div>
  <!-- /.container --> 
</div>
<!-- /#top-banner-and-menu --> 

<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="footer color-bg">
@include('web.footerweb')
</footer>
<!-- ============================================================= FOOTER : END============================================================= --> 

<!-- For demo purposes – can be removed on production --> 

<!-- For demo purposes – can be removed on production : End --> 

<!-- JavaScripts placed at the end of the document so the pages load faster --> 
<script src="{{asset('js/jquery-1.11.1.min.js')}}"></script> 
<script src="{{asset('js/bootstrap.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-hover-dropdown.min.js')}}"></script> 
<script src="{{asset('js/owl.carousel.min.js')}}"></script> 
<script src="{{asset('js/echo.min.js')}}"></script> 
<script src="{{asset('js/jquery.easing-1.3.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-slider.min.js')}}"></script> 
<script src="{{asset('js/jquery.rateit.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/lightbox.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-select.min.js')}}"></script> 
<script src="{{asset('js/wow.min.js')}}"></script> 
<script src="{{asset('js/scripts.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.id.js')}}"></script>
<script>
var SITEURL = '{{URL::to('')}}';
$(document).ready( function () {
 //   $('#userid').hide();

    $('#tgl_bts_penawaran').datetimepicker({
    //  language: 'id',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1
    
    })

    $('select[name="idhobi"]').on('change', function() {
          var idhobby = $(this).val();
          if(idhobby) {
              $.ajax({
               //   url: "{{ url('/lelang/hobbydet/') }}/"+idhobby, //'/lelang/detail_hobby/'+provID,
                  url: SITEURL +"/lelang/hobbydet/"+idhobby,
                  type: "GET",
                  dataType: "json",
                  success:function(data) {                      
                      $('select[name="idhobidet"]').empty();
                      // $.each(data, function(key, value) {
                      //     $('select[name="idhobidet"]').append('<option value="'+ key +'">'+ value +'</option>');
                      // });
                      $.each(data.dethobby, function(key, value) {
                      $('#idhobidet').append('<option value="'+ value.id+'">'+ value.name +'</option>');
                      });
                  }
              });
          }else{
              $('select[name="idhobidet"]').empty();
          }
      });
});   
</script>

</body>
</html>