<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta -->
<meta name="csrf-token" content="{{ csrf_token() }}">
@include('web.headweb')
</head>
<body class="cnt-home">
<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1"> 
  
  <!-- ============================================== TOP MENU ============================================== -->
  <div class="top-bar animate-dropdown">
    <div class="container">
      <div class="header-top-inner">
        <div class="cnt-account">
          <ul class="list-unstyled">
            <!-- <li><a href="#"><i class="icon fa fa-user"></i>My Account</a></li> -->
            
              @if(!Auth::guard('customer')->check()) 
              <li><a href="{{ url('login/customer') }}"><i class="icon fa fa-lock"></i>Login</a></li>
              <li><a href="{{ url('register/customer') }}"><i class="icon fa fa-user"></i>Daftar Member</a></li>
              @endif      
              @if(Auth::guard('customer')->check())  
              <li><a href="{{ url('home/customer') }}"><i class="icon fa fa-home"></i>Dashboard</a></li>
                <li><a href="#"><i class="icon fa fa-user"></i> 
                  Selamat Datang {{ Auth::guard('customer')->user()->name  }}
                    </li> </a>
                    <li><a href="{{ url('logout/customer') }}"><i class="icon fa fa-lock"></i> 
                     Logout
                    </a></li> 
              @endif      
              
          </ul>
        </div>
        <!-- /.cnt-account -->
        
        <!-- /.cnt-cart -->
        <div class="clearfix"></div>
      </div>
      <!-- /.header-top-inner --> 
    </div>
    <!-- /.container --> 
  </div>
  <!-- /.header-top --> 
  <!-- ============================================== TOP MENU : END ============================================== -->
  <div class="main-header">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 logo-holder"> 
          <!-- ============================================================= LOGO ============================================================= -->
          <div class="logo"> <a href="{{ url('/') }}"> <img src="{{asset('images/logo.png')}}" alt="logo"> </a> </div>
          <!-- /.logo --> 
          <!-- ============================================================= LOGO : END ============================================================= --> </div>
        <!-- /.logo-holder -->
        
        <div class="col-xs-12 col-sm-12 col-md-7 top-search-holder"> 
          <!-- /.contact-row --> 
          <!-- ============================================================= SEARCH AREA ============================================================= -->
          @include('web.searchbar')
          <!-- /.search-area --> 
          <!-- ============================================================= SEARCH AREA : END ============================================================= --> </div>
        <!-- /.top-search-holder -->
        
        @include('web.cart')
      <!-- /.row --> 
      
    </div>
    <!-- /.container --> 
    
  </div>
  <!-- /.main-header --> 
  
  <!-- ============================================== NAVBAR ============================================== -->
  @include('web.menuatas')
  <!-- /.header-nav --> 
  <!-- ============================================== NAVBAR : END ============================================== --> 
  
</header>

<!-- ============================================== HEADER : END ============================================== -->
<div id="ext-time">
<div class="body-content outer-top-xs" id="top-banner-and-menu">
  <div class="container">
    <div class="row"> 
      <!-- ============================================== SIDEBAR ============================================== -->
      <div class="col-xs-12 col-sm-12 col-md-3 sidebar"> 
        
        <!-- ================================== TOP NAVIGATION ================================== -->
        <div class="sidebar-module-container">
          <div class="sidebar-filter"> 
            <!-- ============================================== SIDEBAR CATEGORY ============================================== -->
                @include('web.sidebarcust')            
            </div>
        
          </div>
          <!-- /.sidebar-filter --> 
        </div>
        <!-- /.side-menu --> 
        <!-- ================================== TOP NAVIGATION : END ================================== --> 
      </div>
      <!-- /.sidemenu-holder --> 
      <!-- ============================================== SIDEBAR : END ============================================== --> 
      
      <!-- ============================================== CONTENT ============================================== -->
      <div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder"> 
      
        
        <!-- ============================================== BLOG SLIDER ============================================== -->
        <section class="section latest-blog outer-bottom-vs wow fadeInUp">
          <h3 class="section-title">My Store</h3>
          <br>
        <div id="container_list_lelang">  
          <div class="row">
            <div class="col-lg-11 margin-tb">
                <div class="pull-right">
                    <a class="btn-sm btn-primary" href="{{ url('customer/formbrg') }}"> Tambah Barang</a>
                </div>
            </div>
            </div>
          <div class="table-responsive">
          @if ($message = Session::get('success'))
              <div class="alert alert-success">
                  <p>{{ $message }}</p>
              </div>
          @endif
            <table class="table table-striped">
                <thead>
                    <tr>
                    <td><strong>No</strong></td>
                    <td><strong>Gambar</strong></td>
                    <td><strong>Nama</strong></td>
                    <td><strong>Harga</strong></td>              
                    <td><strong>Stok</strong></td>
                    <!-- <td><strong>Status</strong></td> -->
                    <td><strong>Action</strong></td>
                    </tr>
                </thead>
                <tbody>
                <!-- <tr> 
                            <td colspan="6">Belum ada barang yang anda jual...</td>
                           
                </tr>          -->
                    
                @foreach ($barangs as $barang)
  
                <tr>
                    <td>{{ ++$i }}</td>
                    <td><img src="{{asset('uploads/jual_barang/'.$barang->gambar_satu)}}" style="width:80px; height:80px;"/></td>
                    <td>{{ $barang->nmbrg }}</td>
                    <td>{{ number_format($barang->harga,0,',','.') }}</td>
                    <td>{{ $barang->stok }}</td>
                    <!-- <td>{{ $barang->status }}</td> -->
                    <td>
                    <button type="button" v-on:click="idbrg = {{$barang->id}}" class="btn btn-info btn-xs modalEditBrg" 
                          data-lokasi="{{$barang->lokasi_barang}}" 
                          data-harga="{{$barang->harga}}" 
                          data-deskripsi="{{$barang->deskripsi_barang}}" 
                          data-nmbrg="{{$barang->nmbrg}}" 
                          data-kdbrg="{{$barang->kdbrg}}" 
                          data-berat="{{$barang->berat}}" 
                          data-stok="{{$barang->stok}}" 
                          data-idhobi="{{$barang->idhobi}}" 
                          data-idhobidet="{{$barang->idhobidet}}" 
                          data-kondisi="{{$barang->kondisi_barang}}" 
                          data-status="{{$barang->status}}" 
                          data-id="{{$barang->id}}">
                      <span class="icon fa fa-pencil"> Edit</span></button>
                        <form action="{{ route('lelang.destroy',$barang->id) }}" method="POST">
                          @csrf

                            <!-- <button type="submit" class="btn btn-danger btn-xs">Delete</button> -->
                           
                        </form>
                    </td>
                </tr>
                @endforeach
                 
                </tbody>
                </table>
                {!! $barangs->links() !!}
             </div>
            <!-- /.owl-carousel --> 
         
          <!-- /.blog-slider-container --> 
         </div> 
        </section>
        <!-- /.section --> 
        <!-- ============================================== BLOG SLIDER : END ============================================== --> 
        
        
        
      </div>
      <!-- /.homebanner-holder --> 
      <!-- ============================================== CONTENT : END ============================================== --> 
    </div>



<div class="modal fade" id="m_editlelang" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
   
      <form action="{{ route('customer.update-barang') }}" method="POST" enctype="multipart/form-data">
          @csrf
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Barang</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
    
      <!-- Modal body -->
      <div class="form-row">
                <div class="form-group col-md-6 chobi">
                    <label for="idhobi">Kategori</label><span style="color:red">*</span>
                    <select id="idhobi" name="idhobi" class="form-control">
                        @foreach($hobbies as $hobi)
                        <option value="{{ $hobi->id }}">{{ $hobi->name }}</option>
                      
                        @endforeach
                     
                    </select>
                    </div>
                    <div class="form-group col-md-6 chobidet">
                    <label for="idhobidet">Lebih Spesifik</label><span style="color:red">*</span>
                    <select id="idhobidet" name="idhobidet" class="form-control">                     
                    </select>
                    </div>   
                    
                   
                </div>
      <div class="form-row">
                    <div class="form-group col-md-12">
                    <label>Nama Barang</label><span style="color:red">*</span>
                    <input type="text" class="form-control" id="nmbrg" name="nmbrg" placeholder="Nama barang yang akan dijual">
                    </div>
                   
                </div>
          <div class="form-row">
                  
                    <div class="form-group col-md-12">
                    <label>SKU</label><span style="color:red">*</span>
                    <input type="text" class="form-control" id="kdbrg" name="kdbrg" placeholder="SKU (Stock Keeping Unit) adalah nomor unik pada tiap barang">
                    </div>
                   
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                    <label>Stok</label><span style="color:red">*</span>
                    <input type="number" class="form-control" id="stok" name="stok">
                    </div>
                   
                    <div class="form-group col-md-6">
                    <label>Perkiraan Berat</label><span style="color:red">*</span>
                    <input type="number" class="form-control" id="berat" name="berat" placeholder="Satuan Gram (gr)">
                    </div>
                </div>
            
                <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="kondisi_barang">Kondisi</label><span style="color:red">*</span>
                    <select id="kondisi_barang" name="kondisi_barang" class="form-control">
                        <option selected>Pilih...</option>
                        <option>Baru</option>
                        <option>Bekas</option>
                    </select>
                    </div>
                    <div class="form-group col-md-6">
                    <label for="harga">Harga</label><span style="color:red">*</span>
                    <input type="number" class="form-control" id="harga" name="harga" placeholder="">
                    </div>
                   
                </div>
                <div class="form-group col-md-12">
                    <label for="deskripsi">Deskripsi</label><span style="color:red">*</span>
                    <textarea class="form-control" id="deskripsi" name="deskripsi" rows="5"></textarea>
                </div>

                 <div class="form-row">
                    <div class="form-group col-md-12">
                    <label>Lokasi Barang</label><span style="color:red">*</span>
                    <input type="text" class="form-control" id="lokasi_barang" name="lokasi_barang">
                    <input type="text" autocomplete="off" id="idbarang_edit" name="idbarang_edit" class="form-control">
                    </div>
                 
                </div>    

                <div class="form-row">
                <div class="form-group col-md-12">
                    <!-- <label for="status">Status</label><span style="color:red">*</span> -->
                    <select id="status" name="status" class="form-control">
                        <option selected>Pilih...</option>
                        <option>ACTIVE</option>
                        <option>NON ACTIVE</option>
                    </select>
                    </div>
                  
                   
                </div>
               


      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
      </form>
 
    </div>
  </div>
</div>
    <!-- /.row --> 
    <!-- ============================================== BRANDS CAROUSEL ============================================== -->
    <div id="brands-carousel" class="logo-slider wow fadeInUp">
      <div class="logo-slider-inner">
        <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
          <div class="item m-t-15"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item m-t-10"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand3.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand6.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item--> 
        </div>
        <!-- /.owl-carousel #logo-slider --> 
      </div>
      <!-- /.logo-slider-inner --> 
      
    </div>
    <!-- /.logo-slider --> 
    <!-- ============================================== BRANDS CAROUSEL : END ============================================== --> 
  </div>
  <!-- /.container --> 
</div>

</div>
<!-- /#top-banner-and-menu --> 

<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="footer color-bg">
@include('web.footerweb')
</footer>
<!-- ============================================================= FOOTER : END============================================================= --> 

<!-- For demo purposes – can be removed on production --> 

<!-- For demo purposes – can be removed on production : End --> 

<!-- JavaScripts placed at the end of the document so the pages load faster -->
<script src="{{asset('js/app.js')}}"></script> 
<script src="{{asset('js/jquery-1.11.1.min.js')}}"></script> 
<script src="{{asset('js/bootstrap.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-hover-dropdown.min.js')}}"></script> 
<script src="{{asset('js/owl.carousel.min.js')}}"></script> 
<script src="{{asset('js/echo.min.js')}}"></script> 
<script src="{{asset('js/jquery.easing-1.3.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-slider.min.js')}}"></script> 
<script src="{{asset('js/jquery.rateit.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/lightbox.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-select.min.js')}}"></script> 
<script src="{{asset('js/wow.min.js')}}"></script> 
<script src="{{asset('js/scripts.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.id.js')}}"></script>


<script type="text/javascript">
var id = '';
  Vue.component('my-comp', {
    template: '<div></div>',
    created: function () {
      id = this.$attrs['my-attr']; // And here is - in $attrs object
      //alert(this.$attrs['my-attr'][1]);
    }
  })

   var extTime = new Vue({
  el: '#ext-time',
  data: {
    message: '',
    idbrg: '',
    exttime: '',

  },
  methods: {
    formSubmit(e) {     
                e.preventDefault();
                let currentObj = this;
                axios.post("{{ url('/lelang/update-exttime') }}", {
                    idbrg: this.idbrg,
                    exttime:  this.exttime,
                   
                })
                .then(function (response) {
                    currentObj.output = response.data;
                   // location.reload();
                    alert('berhasil');
                    location.reload();
                })
                .catch(function (error) {
                    currentObj.output = error;
                });
    }
  }  
}); 


</script>

<script>
var SITEURL = '{{URL::to('')}}';
$(document).ready( function () {

//STATUS SEMENTARA DI HIDE DULU
$("#status").hide();
  

$(".modalEditBrg").click(function () {
    var ids = $(this).attr('data-id');
    var nmbrg = $(this).attr('data-nmbrg');
    var kdbrg = $(this).attr('data-kdbrg');
    var berat = $(this).attr('data-berat');
    var stok = $(this).attr('data-stok');
    var harga = $(this).attr('data-harga');
    var deskripsi = $(this).attr('data-deskripsi');
    var lokasi = $(this).attr('data-lokasi');
    var kondisi = $(this).attr('data-kondisi');
    var idhobi = $(this).attr('data-idhobi');
    var idhobidet = $(this).attr('data-idhobidet');
    var status = $(this).attr('data-status');
    $("#nmbrg").val(nmbrg);
    $("#kdbrg").val(kdbrg);
    $("#stok").val(stok);
    $("#berat").val(berat);
    $("textarea#deskripsi").text(deskripsi);
    $("#harga").val(harga);
    $("#lokasi_barang").val(lokasi);
    $("#kondisi_barang").val(kondisi);
    $("#status").val(status);
    $("#idbarang_edit").val(ids);
     $("div.chobi select").val(idhobi);
    // $("div.chobidet select").val(idhobidet);
     loadHobDet(idhobi,idhobidet)
    // $('.chobi option')
    //  .removeAttr('selected')
    //  .filter("[value='"+idhobi+"']")
    //      .attr('selected', true)
    $("#idbarang_edit").hide();
    $('#m_editlelang').modal('show');
});
    

    $('#add_time').datetimepicker({
    //  language: 'id',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1
    
    })


    $('select[name="idhobi"]').on('change', function() {
          var idhobby = $(this).val();
          if(idhobby) {
              $.ajax({
               //   url: "{{ url('/lelang/hobbydet/') }}/"+idhobby, //'/lelang/detail_hobby/'+provID,
                  url: SITEURL +"/lelang/hobbydet/"+idhobby,
                  type: "GET",
                  dataType: "json",
                  success:function(data) {                      
                      $('select[name="idhobidet"]').empty();
                      // $.each(data, function(key, value) {
                      //     $('select[name="idhobidet"]').append('<option value="'+ key +'">'+ value +'</option>');
                      // });
                      $.each(data.dethobby, function(key, value) {
                      $('#idhobidet').append('<option value="'+ value.id+'">'+ value.name +'</option>');
                      });
                  }
              });
          }else{
              $('select[name="idhobidet"]').empty();
          }
      });
});

function loadHobDet(idhobby,idhobidet){
  $.ajax({
               //   url: "{{ url('/lelang/hobbydet/') }}/"+idhobby, //'/lelang/detail_hobby/'+provID,
                  url: SITEURL +"/lelang/hobbydet/"+idhobby,
                  type: "GET",
                  dataType: "json",
                  success:function(data) {                      
                      $('select[name="idhobidet"]').empty();
                      // $.each(data, function(key, value) {
                      //     $('select[name="idhobidet"]').append('<option value="'+ key +'">'+ value +'</option>');
                      // });
                      $.each(data.dethobby, function(key, value) {
                      $('#idhobidet').append('<option value="'+ value.id+'">'+ value.name +'</option>');
                      });

                      $("div.chobidet select").val(idhobidet);
                  }
              });
}


</script>

</body>
</html>