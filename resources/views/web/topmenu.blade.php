@extends('index_web')

@section('topmenu')
<div class="top-bar animate-dropdown">
    <div class="container">
      <div class="header-top-inner">
        <div class="cnt-account">
          <ul class="list-unstyled">
            <!-- <li><a href="#"><i class="icon fa fa-user"></i>My Account</a></li> -->
            <li><a href="{{ url('login')}}"><i class="icon fa fa-lock"></i>Login</a></li>
              <li><a href="#"><i class="icon fa fa-user"></i>Daftar</a></li>
          </ul>
        </div>
        <!-- /.cnt-account -->
        
        <!-- /.cnt-cart -->
        <div class="clearfix"></div>
      </div>
      <!-- /.header-top-inner --> 
    </div>
    <!-- /.container --> 
  </div>
@endsection