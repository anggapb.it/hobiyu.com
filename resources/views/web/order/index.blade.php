<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('ui-backend/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@include('web.headweb')
</head>
<body class="cnt-home">
<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1"> 
  
  <!-- ============================================== TOP MENU ============================================== -->
  <div class="top-bar animate-dropdown">
    <div class="container">
      <div class="header-top-inner">
        <div class="cnt-account">
          <ul class="list-unstyled">
            <!-- <li><a href="#"><i class="icon fa fa-user"></i>My Account</a></li> -->
            
              @if(!Auth::guard('customer')->check()) 
              <li><a href="{{ url('login/customer') }}"><i class="icon fa fa-lock"></i>Login</a></li>
              <li><a href="{{ url('register/customer') }}"><i class="icon fa fa-user"></i>Daftar Member</a></li>
              @endif      
              @if(Auth::guard('customer')->check())  
              <li><a href="{{ url('home/customer') }}"><i class="icon fa fa-home"></i>Dashboard</a></li>
                <li><a href="#"><i class="icon fa fa-user"></i> 
                  Selamat Datang {{ Auth::guard('customer')->user()->name  }}
                    </li> </a>
                    <li><a href="{{ url('logout/customer') }}"><i class="icon fa fa-lock"></i> 
                     Logout
                    </a></li> 
              @endif      
              
          </ul>
        </div>
        <!-- /.cnt-account -->
        
        <!-- /.cnt-cart -->
        <div class="clearfix"></div>
      </div>
      <!-- /.header-top-inner --> 
    </div>
    <!-- /.container --> 
  </div>
  <!-- /.header-top --> 
  <!-- ============================================== TOP MENU : END ============================================== -->
  <div class="main-header">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 logo-holder"> 
          <!-- ============================================================= LOGO ============================================================= -->
          <div class="logo"> <a href="{{ url('/') }}"> <img src="{{asset('images/logo.png')}}" alt="logo"> </a> </div>
          <!-- /.logo --> 
          <!-- ============================================================= LOGO : END ============================================================= --> </div>
        <!-- /.logo-holder -->
        
        <div class="col-xs-12 col-sm-12 col-md-7 top-search-holder"> 
          <!-- /.contact-row --> 
          <!-- ============================================================= SEARCH AREA ============================================================= -->
          @include('web.searchbar')
          <!-- /.search-area --> 
          <!-- ============================================================= SEARCH AREA : END ============================================================= --> </div>
        <!-- /.top-search-holder -->
        
        @include('web.cart')
      <!-- /.row --> 
      
    </div>
    <!-- /.container --> 
    
  </div>
  <!-- /.main-header --> 
  
  <!-- ============================================== NAVBAR ============================================== -->
  @include('web.menuatas')
  <!-- /.header-nav --> 
  <!-- ============================================== NAVBAR : END ============================================== --> 
  
</header>

<!-- ============================================== HEADER : END ============================================== -->
<div id="ext-time">
<div class="body-content outer-top-xs" id="top-banner-and-menu">
  <div class="container">
    <div class="row"> 
      <!-- ============================================== SIDEBAR ============================================== -->
      <div class="col-xs-12 col-sm-12 col-md-3 sidebar"> 
        
        <!-- ================================== TOP NAVIGATION ================================== -->
        <div class="sidebar-module-container">
          <div class="sidebar-filter"> 
            <!-- ============================================== SIDEBAR CATEGORY ============================================== -->
                @include('web.sidebarcust')            
            </div>
        
          </div>
          <!-- /.sidebar-filter --> 
        </div>
        <!-- /.side-menu --> 
        <!-- ================================== TOP NAVIGATION : END ================================== --> 
      </div>
      <!-- /.sidemenu-holder --> 
      <!-- ============================================== SIDEBAR : END ============================================== --> 
      
      <!-- ============================================== CONTENT ============================================== -->
      <div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder"> 
      
        
        <!-- ============================================== BLOG SLIDER ============================================== -->
        <section class="section latest-blog outer-bottom-vs wow fadeInUp">
          <h3 class="section-title">My Order</h3>
          <br>
        <div id="container_order">  
       
          <div class="table-responsive">
          @if ($message = Session::get('success'))
              <div class="alert alert-success">
                  <p>{{ $message }}</p>
              </div>
          @endif
            <table class="table table-striped">
                <thead>
                    <tr>
                    <td><strong>No</strong></td>
                    <td><strong>No Transaksi</strong></td>
                   
                    <td><strong>Total</strong></td>
                    <td><strong>Metode Bayar </strong></td>              
                    <td><strong>Batas Bayar</strong></td>              
                    <td><strong>Status</strong></td>
                    <td><strong>Action</strong></td>
                    </tr>
                </thead>
                <tbody>
                <!-- <tr> 
                            <td colspan="6">Belum ada barang yang anda jual...</td>
                           
                </tr>          -->
                    
                @foreach ($orders as $order)
  
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $order->noorder }}</td>
                   
                    <td>{{ number_format($order->total,0,',','.') }}</td>
                    <td>{{ $paymethod::where('id',$order->idmethod_bayar)->first()->name }}</td>
                    <td>{{ $order->batas_bayar }}</td>
                    <td>{{ $storder::where('id',$order->status)->first()->name }}</td>
                  
                    <td>
                    <button type="button" class="btn btn-info btn-xs modalOrderdet" 
                         data-id="{{$order->id}}">
                      <span class="icon fa fa-cart"> Lihat Barang</span></button>
                         
                        @if($order->status == 1)
                        <form action="{{ route('customer.batalorder',$order->id) }}" method="POST">
                          @csrf
                          <br>
                            <button type="submit" class="btn btn-danger btn-xs">Batalkan</button>
                           
                        </form>

                        <br>
                        <a href="{{route('jual.cart.success-checkout',$order->id)}}" target="_blank" class="btn btn-warning btn-xs" > Bayar </a>
                        @endif
                    </td>
                  
                </tr>
                @endforeach
                 
                </tbody>
                </table>
                {!! $orders->links() !!}
             </div>
            <!-- /.owl-carousel --> 
         
          <!-- /.blog-slider-container --> 
         </div> 
        </section>
        <!-- /.section --> 
        <!-- ============================================== BLOG SLIDER : END ============================================== --> 
        
        
        
      </div>
      <!-- /.homebanner-holder --> 
      <!-- ============================================== CONTENT : END ============================================== --> 
    </div>



<div class="modal fade" id="mdlOrder" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
   
      <!-- <form action="{{ route('customer.update-barang') }}" method="POST" enctype="multipart/form-data">
          @csrf -->
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Barang yang di order</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
    
      <!-- Modal body -->
      <table class="table table-bordered table-striped" id="tblDetailOrder">
                        <thead>
                            <tr>
                              
                                <th>No.</th>
                                <th>Barang</th>
                                <th>Qty</th>
                                <th>Harga</th>
                                <th>Penjual</th>
                            </tr>
                        </thead>
                    
                  </table>
                    
      


      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
      
 
    </div>
  </div>
</div>
    <!-- /.row --> 
    <!-- ============================================== BRANDS CAROUSEL ============================================== -->
   
    <!-- /.logo-slider --> 
    <!-- ============================================== BRANDS CAROUSEL : END ============================================== --> 
  </div>
  <!-- /.container --> 
</div>

</div>
<!-- /#top-banner-and-menu --> 

<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="footer color-bg">
@include('web.footerweb')
</footer>
<!-- ============================================================= FOOTER : END============================================================= --> 

<!-- For demo purposes – can be removed on production --> 

<!-- For demo purposes – can be removed on production : End --> 

<!-- JavaScripts placed at the end of the document so the pages load faster -->
<!-- <script src="{{asset('js/app.js')}}"></script>  -->
<script src="{{asset('js/jquery-1.11.1.min.js')}}"></script> 
<script src="{{asset('js/bootstrap.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-hover-dropdown.min.js')}}"></script> 
<script src="{{asset('js/owl.carousel.min.js')}}"></script> 
<script src="{{asset('js/echo.min.js')}}"></script> 
<script src="{{asset('js/jquery.easing-1.3.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-slider.min.js')}}"></script> 
<script src="{{asset('js/jquery.rateit.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/lightbox.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-select.min.js')}}"></script> 
<script src="{{asset('js/wow.min.js')}}"></script> 
<script src="{{asset('js/scripts.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.id.js')}}"></script>
<script src="{{ asset('ui-backend/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('ui-backend/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>



<script>
var SITEURL = '{{URL::to('')}}';
$(document).ready( function () {

//STATUS SEMENTARA DI HIDE DULU
$("#status").hide();
  

$(".modalOrderdet").click(function () {
    var order_id = $(this).attr('data-id');
 
    $('#tblDetailOrder').DataTable({
         processing: true,
         serverSide: true,
         destroy: true,
         paging: false,
         searching: false,
         ajax: {
          url: SITEURL + "/customer/orderdet/"+order_id,
          type: 'GET',
         },
          columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                    { data: 'nmbrg', name: 'nmbrg' },
                    { data: 'qty', name: 'qty' },
                    { data: 'harga', name: 'harga' },
                    { data: 'nmpenjual', name: 'nmpenjual' },      
                ],
          order: [[0, 'desc']]
         });


    $('#mdlOrder').modal('show');
});
    

    $('#add_time').datetimepicker({
    //  language: 'id',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1
    
    })


  
});

</script>

</body>
</html>