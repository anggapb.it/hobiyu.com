<div class="side-menu animate-dropdown outer-bottom-xs">
          <div class="head"><a href="{{ url('lelang/all') }}" ><i class="icon fa fa-align-justify fa-fw"></i>All Categories</a></div>
          <nav class="yamm megamenu-horizontal">
            <ul class="nav">
            @foreach($hobbies as $hobby)
            
            <li class="dropdown menu-item"> 
            <!-- <a href="{{ url('lelang',$hobby->id) }}"><i class="{{$hobby->icon}}" aria-hidden="true"></i>{{ $hobby->name }}</a> -->
            @if(!empty($hobi::find($hobby->id)->hobbydets()->first()->id))
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="{{$hobby->icon}}" aria-hidden="true"></i>{{ $hobby->name }}</a>
                <ul class="dropdown-menu mega-menu">
                  <li class="yamm-content">
                    <div class="row">
                      <div class="col-sm-12 col-md-3">
                        <ul class="links list-unstyled">
                      
                         @foreach($hobi::find($hobby->id)->hobbydets()->get() as $hobbydet)
                             <li><a href="{{ url('lelang',$hobbydet->id) }}">{{ $hobbydet->name }}</a></li>
                          @endforeach 
                         
                        </ul>
                      </div>
                      <!-- /.col --> 
                    </div>
                    <!-- /.row --> 
                  </li>
                  <!-- /.yamm-content -->
                </ul>
            @else            <!-- H artinya kirim parameter untuk hobby header -->
            <!-- <a href="{{ url('lelang', 'H'.$hobby->id) }}" class="dropdown-toggle"><i class="{{$hobby->icon}}" aria-hidden="true"></i>{{ $hobby->name }}</a> -->
            <a class="dropdown-toggle"><i class="{{$hobby->icon}}" aria-hidden="true"></i>{{ $hobby->name }}</a>

            @endif
            
                <!-- /.dropdown-menu --> </li>
            @endforeach
             
              <!-- /.menu-item -->
              
            
              <!-- /.menu-item -->

              <!-- /.menu-item -->
              
            </ul>
            <!-- /.nav --> 
          </nav>
          <!-- /.megamenu-horizontal --> 
        </div>
      @foreach($sponsors as $item)
      <div class="home-banner"> <a href="{{$item->link}}" target="_blank"><img style="width:260px" src="{{asset('uploads/banner/'.$item->banner)}}" alt="Image"> </a> </div>   
      @endforeach
           
       