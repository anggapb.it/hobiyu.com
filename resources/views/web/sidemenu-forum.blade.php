<div class="side-menu animate-dropdown outer-bottom-xs">
          <div class="head"><a href="{{ url('forum/list/all') }}" ><i class="icon fa fa-align-justify fa-fw"></i>All Categories</a></div>
          <nav class="yamm megamenu-horizontal">
            <ul class="nav">
            @foreach($hobbies as $hobby)
            <li class="dropdown menu-item"> 
            <!-- <a href="{{ url('lelang',$hobby->id) }}"><i class="{{$hobby->icon}}" aria-hidden="true"></i>{{ $hobby->name }}</a> -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="{{$hobby->icon}}" aria-hidden="true"></i>{{ $hobby->name }}</a>
                <ul class="dropdown-menu mega-menu">
                  <li class="yamm-content">
                    <div class="row">
                      <div class="col-sm-12 col-md-3">
                        <ul class="links list-unstyled">
                      
                         @foreach($hobi::find($hobby->id)->hobbydets()->get() as $hobbydet)
                             <li><a href="{{ url('forum/list',$hobbydet->id) }}">{{ $hobbydet->name }}</a></li>
                          @endforeach 
                          <!-- <li><a href="#">Anggrek</a></li>
                          <li><a href="#">Bonsai</a></li>
                          <li><a href="#">Tanaman Buah</a></li>
                          <li><a href="#">Tanaman Aquascape</a></li> -->
                        </ul>
                      </div>
                      <!-- /.col --> 
                    </div>
                    <!-- /.row --> 
                  </li>
                  <!-- /.yamm-content -->
                </ul>
                <!-- /.dropdown-menu --> </li>
            @endforeach
             
              <!-- /.menu-item -->
              
            
              <!-- /.menu-item -->

              <!-- /.menu-item -->
              
            </ul>
            <!-- /.nav --> 
          </nav>
          <!-- /.megamenu-horizontal --> 
        </div>

        @foreach($sponsors as $item)
      <div class="home-banner"> <a href="{{$item->link}}" target="_blank"><img style="width:260px" src="{{asset('uploads/banner/'.$item->banner)}}" alt="Image"> </a> </div>   
      @endforeach

     