<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta -->
<meta name="csrf-token" content="{{ csrf_token() }}">
@include('web.headweb')
</head>
<body class="cnt-home">
<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1"> 
  
  <!-- ============================================== TOP MENU ============================================== -->
  <div class="top-bar animate-dropdown">
    <div class="container">
      <div class="header-top-inner">
        <div class="cnt-account">
          <ul class="list-unstyled">
            <!-- <li><a href="#"><i class="icon fa fa-user"></i>My Account</a></li> -->
            
              @if(!Auth::guard('customer')->check()) 
              <li><a href="{{ url('login/customer') }}"><i class="icon fa fa-lock"></i>Login</a></li>
              <li><a href="{{ url('register/customer') }}"><i class="icon fa fa-user"></i>Daftar Member</a></li>
               
              @endif      
              @if(Auth::guard('customer')->check())  
              <li><a href="{{ url('home/customer') }}"><i class="icon fa fa-home"></i>Dashboard</a></li>
                <li><a href="#"><i class="icon fa fa-user"></i> 
                  Selamat Datang {{ Auth::guard('customer')->user()->name  }}
                    </li> </a>
                    <li><a href="{{ url('logout/customer') }}"><i class="icon fa fa-lock"></i> 
                     Logout
                    </a></li> 
                    
              @endif      
              
          </ul>
        </div>
        <!-- /.cnt-account -->
        
        <!-- /.cnt-cart -->
        <div class="clearfix"></div>
      </div>
      <!-- /.header-top-inner --> 
    </div>
    <!-- /.container --> 
  </div>
  <!-- /.header-top --> 
  <!-- ============================================== TOP MENU : END ============================================== -->
  <div class="main-header">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 logo-holder"> 
          <!-- ============================================================= LOGO ============================================================= -->
          <div class="logo"> <a href="{{ url('/') }}"> <img src="{{asset('images/logo.png')}}" alt="logo"> </a> </div>
          <!-- /.logo --> 
          <!-- ============================================================= LOGO : END ============================================================= --> </div>
        <!-- /.logo-holder -->
        
        <div class="col-xs-12 col-sm-12 col-md-7 top-search-holder"> 
          <!-- /.contact-row --> 
          <!-- ============================================================= SEARCH AREA ============================================================= -->
          @include('web.searchbar')
          <!-- /.search-area --> 
          <!-- ============================================================= SEARCH AREA : END ============================================================= --> </div>
        <!-- /.top-search-holder -->
        
        @include('web.cart')
      <!-- /.row --> 
      
    </div>
    <!-- /.container --> 
    
  </div>
  <!-- /.main-header --> 
  
  <!-- ============================================== NAVBAR ============================================== -->
  @include('web.menuatas')
  <!-- /.header-nav --> 
  <!-- ============================================== NAVBAR : END ============================================== --> 
  
</header>

<!-- ============================================== HEADER : END ============================================== -->

<div class="body-content">
	<div class="container">
    <div class="contact-page">
		<div class="row">
			
				<div class="col-md-12 contact-map outer-bottom-vs">
				<center><h3>Segera selesaikan pembayaran anda sebelum stok habis.</h3></center>
				<br>
				<br>
				<center><h5>Batas Pembayaran : <b><span style="color: #ff7878" data-countdown="{{ $dataorder->batas_bayar }}"></span><b></h5></center>
        <center><span>Transaksi akan dibatalkan jika pembayaran belum diterima setelah melewati batas pembayaran</span></center>
				<br>
				<center><h5>Jumlah Tagihan</h5></center>
				<center><h2><b>Rp. {{number_format($dataorder->total,0,',','.')}}</b></h2></center>
				<br>
				<center><h5>Nomor Tagihan</h5></center>
				<center><h4><b>{{$dataorder->noorder}}</b></h4></center>
				<br>
			
				
				</div>
<div class="col-md-12">
<div class="contact-title">
		<h4>Pembayaran dapat dilakukan ke salahsatu rekening dibawah ini : </h4>
	</div>
</div>
<div class="col-md-4 contact-info">
	<div class="clearfix address">
		<span class="contact-i"><i class="fa fa-bank"></i></span>
		<span class="contact-span">Bank {{$bank1}} <br> A/n {{$profile->atasnama_satu}}</span>
</div>
	<div class="clearfix phone-no">
		<span class="contact-i"><i class="fa fa-book"></i></span>
		<span class="contact-span"><b>Rek : {{$profile->kdrek_satu}}</b></span>
	</div>
</div>			
<div class="col-md-4 contact-info">
	<div class="clearfix address">
		<span class="contact-i"><i class="fa fa-bank"></i></span>
		<span class="contact-span">Bank {{$bank2}} <br> A/n {{$profile->atasnama_dua}}</span>
</div>
	<div class="clearfix phone-no">
		<span class="contact-i"><i class="fa fa-book"></i></span>
		<span class="contact-span"><b>Rek : {{$profile->kdrek_dua}}</b></span>
	</div>
</div>			
<div class="col-md-4 contact-info">
	<div class="clearfix address">
		<span class="contact-i"><i class="fa fa-bank"></i></span>
		<span class="contact-span">Bank {{$bank3}} <br> A/n {{$profile->atasnama_tiga}}</span>
</div>
	<div class="clearfix phone-no">
		<span class="contact-i"><i class="fa fa-book"></i></span>
		<span class="contact-span"><b>Rek : {{$profile->kdrek_tiga}}</b></span>
	</div>
</div>			

</div><!-- /.contact-page -->
		</div><!-- /.row -->
		<!-- ============================================== BRANDS CAROUSEL ============================================== -->
<!-- /.logo-slider -->
<!-- ============================================== BRANDS CAROUSEL : END ============================================== -->	</div>
<!-- ============================================================= FOOTER ============================================================= -->
<!-- <script src="{{asset('js/app.js')}}"></script> -->
@include('web.footerwebfix')


<script type="text/javascript">
$(document).ready(function(){
    
    $('[data-countdown]').each(function() {
      var $this = $(this), finalDate = $(this).data('countdown');
      $this.countdown(finalDate, function(event) {
        if (event.elapsed) {
          $this.html(event.strftime('Waktu Habis'));
        }else{
          $this.html(event.strftime('%H jam %M menit %S detik'));
        }
       
      });
    });
   
});  

//   var SITEURL = '{{URL::to('')}}';
//   var cekSession = "{{Auth::guard('customer')->check()}}";
  
 
//   var vmdetjual = new Vue({
//     el: '#vmdetjual',
//     data: {
//         stok: '',
//         qty: 1
//     },
//     methods: {
//         kurangStok: function(e) {    

//             if(this.qty > 1){
//             this.qty -= 1; 
//             } else{
//               alert('Qty tidak boleh kurang dari satu...');
//             }

//              e.preventDefault(); 
//         }
//         , AddStok: function(e) {    

//             if(this.qty >= Number(this.stok)){
//                 alert('Qty tidak boleh melebihi stok yang tersedia...');
//             } else{
//               this.qty += 1; 
//             }

//              e.preventDefault(); 
//         }
//     }
//   });
</script>    


</body>


</html>


