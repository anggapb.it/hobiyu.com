<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta -->
<meta name="csrf-token" content="{{ csrf_token() }}">
@include('web.headweb')
</head>
<body class="cnt-home">
<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1"> 
  
  <!-- ============================================== TOP MENU ============================================== -->
  <div class="top-bar animate-dropdown">
    <div class="container">
      <div class="header-top-inner">
        <div class="cnt-account">
          <ul class="list-unstyled">
            <!-- <li><a href="#"><i class="icon fa fa-user"></i>My Account</a></li> -->
            
              @if(!Auth::guard('customer')->check()) 
              <li><a href="{{ url('login/customer') }}"><i class="icon fa fa-lock"></i>Login</a></li>
              <li><a href="{{ url('register/customer') }}"><i class="icon fa fa-user"></i>Daftar Member</a></li>
               
              @endif      
              @if(Auth::guard('customer')->check())  
              <li><a href="{{ url('home/customer') }}"><i class="icon fa fa-home"></i>Dashboard</a></li>
                <li><a href="#"><i class="icon fa fa-user"></i> 
                  Selamat Datang {{ Auth::guard('customer')->user()->name  }}
                    </li> </a>
                    <li><a href="{{ url('logout/customer') }}"><i class="icon fa fa-lock"></i> 
                     Logout
                    </a></li> 
                    
              @endif      
              
          </ul>
        </div>
        <!-- /.cnt-account -->
        
        <!-- /.cnt-cart -->
        <div class="clearfix"></div>
      </div>
      <!-- /.header-top-inner --> 
    </div>
    <!-- /.container --> 
  </div>
  <!-- /.header-top --> 
  <!-- ============================================== TOP MENU : END ============================================== -->
  <div class="main-header">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 logo-holder"> 
          <!-- ============================================================= LOGO ============================================================= -->
          <div class="logo"> <a href="{{ url('/') }}"> <img src="{{asset('images/logo.png')}}" alt="logo"> </a> </div>
          <!-- /.logo --> 
          <!-- ============================================================= LOGO : END ============================================================= --> </div>
        <!-- /.logo-holder -->
        
        <div class="col-xs-12 col-sm-12 col-md-7 top-search-holder"> 
          <!-- /.contact-row --> 
          <!-- ============================================================= SEARCH AREA ============================================================= -->
          @include('web.searchbar')
          <!-- /.search-area --> 
          <!-- ============================================================= SEARCH AREA : END ============================================================= --> </div>
        <!-- /.top-search-holder -->
        
        @include('web.cart')
      <!-- /.row --> 
      
    </div>
    <!-- /.container --> 
    
  </div>
  <!-- /.main-header --> 
  
  <!-- ============================================== NAVBAR ============================================== -->
  @include('web.menuatas')
  <!-- /.header-nav --> 
  <!-- ============================================== NAVBAR : END ============================================== --> 
  
</header>

 
<!-- ============================================== HEADER : END ============================================== -->
<div class="body-content outer-top-xs">
  
   
    <div class='container'>
        <div class='row single-product'>
            <div class='col-md-3 sidebar'>
                <div class="sidebar-module-container">
    
  
    
    
        <!-- ============================================== HOT DEALS ============================================== -->

        @foreach($sponsors as $item)
      <div class="home-banner"> <a href="{{$item->link}}"><img style="width:260px" src="{{asset('uploads/banner/'.$item->banner)}}" alt="Image"> </a> </div>   
       <br> 
      @endforeach
<!-- ============================================== HOT DEALS: END ============================================== -->
    </div>
</div><!-- /.sidebar -->


<!-- ============================================== DETAIl PRODUCT ============================================== -->
<div class='col-md-8'>

   

<div class="detail-block">
   <!-- @if ($message = Session::get('success'))
              <div class="alert alert-success">
                  <p>{{ $message }}</p>
              </div>
          @endif -->
    <div class="row  wow fadeInUp">
                
<div class="col-xs-12 col-sm-6 col-md-5 gallery-holder">
    <div class="product-item-holder size-big single-product-gallery small-gallery">

        <div id="owl-single-product">
            <div class="single-product-gallery-item" id="slide1">
                <a data-lightbox="image-1" data-title="{{$barang->nmbrg}}" href="{{asset('uploads/jual_barang/'.$barang->gambar_satu)}}">
                    <img class="img-responsive" alt="" src="{{asset('images/blank.gif')}}" data-echo="{{asset('uploads/jual_barang/'.$barang->gambar_satu)}}" style="width:100%;"/>
                </a>
            </div><!-- /.single-product-gallery-item -->
            <div class="single-product-gallery-item" id="slide2">
                <a data-lightbox="image-2" data-title="{{$barang->nmbrg}}" href="{{asset('uploads/jual_barang/'.$barang->gambar_dua)}}">
                    <img class="img-responsive" alt="" src="{{asset('images/blank.gif')}}" data-echo="{{asset('uploads/jual_barang/'.$barang->gambar_dua)}}" style="width:100%;"/>
                </a>
            </div><!-- /.single-product-gallery-item -->
            <div class="single-product-gallery-item" id="slide3">
                <a data-lightbox="image-3" data-title="{{$barang->nmbrg}}" href="{{asset('uploads/jual_barang/'.$barang->gambar_tiga)}}">
                    <img class="img-responsive" alt="" src="{{asset('images/blank.gif')}}" data-echo="{{asset('uploads/jual_barang/'.$barang->gambar_tiga)}}" style="width:100%;"/>
                </a>
            </div><!-- /.single-product-gallery-item -->
           

            

        </div><!-- /.single-product-slider -->


        <div class="single-product-gallery-thumbs gallery-thumbs">

            <div id="owl-single-product-thumbnails">
            @if(!empty($barang->gambar_satu))
                <div class="item">
                    <a class="horizontal-thumb active" data-target="#owl-single-product" data-slide="1" href="#slide1">
                        <img class="img-responsive" width="85" alt="" src="{{asset('uploads/jual_barang/'.$barang->gambar_satu)}}" data-echo="{{asset('uploads/jual_barang/'.$barang->gambar_satu)}}" />
                    </a>
                </div>
                @endif
                @if(!empty($barang->gambar_dua))
                <div class="item">
                    <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="2" href="#slide2">
                        <img class="img-responsive" width="85" alt="" src="{{asset('uploads/jual_barang/'.$barang->gambar_dua)}}" data-echo="{{asset('uploads/jual_barang/'.$barang->gambar_dua)}}" />
                    </a>
                </div>
                @endif
                @if(!empty($barang->gambar_tiga))
                <div class="item">
                    <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="3" href="#slide3">
                        <img class="img-responsive" width="85" alt="" src="{{asset('uploads/jual_barang/'.$barang->gambar_tiga)}}" data-echo="{{asset('uploads/jual_barang/'.$barang->gambar_tiga)}}" />
                    </a>
                </div>
                @endif
             

                
            </div><!-- /#owl-single-product-thumbnails -->

            

        </div><!-- /.gallery-thumbs -->

    </div><!-- /.single-product-gallery -->



</div><!-- /.gallery-holder --> 
    
      <form action="{{ route('jual.addcart') }}" method="POST">    
   
       @csrf             
                 <div class='col-sm-6 col-md-7 product-info-block'>
                        <div class="product-info">
                            <h3 class="name">{{ $barang->nmbrg }}</h3>
                            
                            <div class="description-container m-t-20">
                                <h4 class="section-title">Kode SKU : #{{ $barang->kdbrg }}</h4>
                                
                                 <h4 class="section-title">Kondisi : {{ $barang->kondisi_barang }}</h4>
                                 <h4 class="section-title">Lokasi : {{ $barang->lokasi_barang }}</h4>
                                 <h4 class="section-title">Posted : {{ $barang->created_at }}</h4>
                                 <h4 class="section-title">Harga : <span style="color: #ff7878">Rp. {{ number_format($barang->harga,0,',','.') }}</span></h4>
                                @if($barang->stok == 0)
                                <h4 class="section-title">Stok : <span style="color: #ff7878">Stok Habis</span></h4>    
                                @else
                                <h4 class="section-title">Stok : <span>{{ $barang->stok }}</span></h4>    
                                @endif 
                                 
                                 <h4 class="section-title">Berat : <span>{{ $barang->berat }} Gr</span></h4>
                                 
                               
                            </div><!-- /.description-container -->
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                    <br>
                                </div>
                            @endif

                          
                            @if(!Auth::guard('customer')->check()) 
                            <div class="quantity-container info-container">
								<div class="row">
									
									<div class="col-sm-2">
										<span class="label">Qty :</span>
									</div>
									
									<div class="col-sm-2">
										<div class="cart-quantity">
											<div class="quant-input">
								                <div class="arrows">
								                  <div class="arrow plus gradient"><span class="ir"><i class="icon fa fa-sort-asc"></i></span></div>
								                  <div class="arrow minus gradient"><span class="ir"><i class="icon fa fa-sort-desc"></i></span></div>
								                </div>
								                <input type="text" readOnly="" class="form-control tnilaibid" id="qty" name="qty" value="1">   
							              </div>
							            </div>
									</div>

									<div class="col-sm-7">
                                       
                                        <button type="button" id="btn_add_cart" class="btn btn-primary"><i class="fa fa-shopping-cart"></i>&nbsp;Add To Cart</button>
									</div>

									
								</div><!-- /.row -->
							</div><!-- /.quantity-container -->
                               
                            @endif
                            @if(Auth::guard('customer')->check())   
                               
                            
                           
                              @if($barang->userid !== Auth::guard('customer')->user()->id && $barang->stok > 0)
                              <div class="quantity-container info-container">
								<div class="row">
									
									<div class="col-sm-2">
										<span class="label">Qty :</span>
									</div>
                                    
                                    <div id="vmdetjual">
									<div class="col-sm-2">
										<div class="cart-quantity">
											<div class="quant-input">
								                <div class="arrows">
								                  <a v-on:click="AddStok"><div class="arrow plus gradient"><span class="ir"><i class="icon fa fa-sort-asc"></i></span></div></a>
								                  <a v-on:click="kurangStok"><div class="arrow minus gradient"><span class="ir"><i class="icon fa fa-sort-desc"></i></span></div></a>
								                </div>
								                <input type="text" readOnly="" class="form-control tnilaibid" v-model="qty" id="qty" name="qty" value="1">   
							              </div>
							            </div>
                                    </div>
                                    </div>

									<div class="col-sm-7">
                                        <!-- <a href="#" class="btn btn-primary"><i class="fa fa-shopping-cart inner-right-vs"></i> ADD TO CART</a> -->
                                        <button type="submit" v-if="seen" class="btn btn-primary"><i class="fa fa-shopping-cart inner-right-vs"></i> Add To Cart</button>
                                        <input type="hidden" class="form-control" name="nmbrg" value="{{$barang->nmbrg}}">    
                                        <input type="hidden" class="form-control" name="idbrg" value="{{$barang->id}}">    
                                        <input type="hidden" class="form-control" name="harga" value="{{$barang->harga}}">    
									</div>

									
								</div><!-- /.row -->
							</div><!-- /.quantity-container -->
                                  
                                     
                                     
                                    
                              @endif      
                          
                      
                            @endif
                            
             
                        </div><!-- /.product-info -->
                    </div><!-- /.col-sm-7 -->
                </div><!-- /.row -->
             </div>

      </form>          
     
                
                <div class="product-tabs inner-bottom-xs  wow fadeInUp">
                    <div class="row">
                        <div class="col-sm-3">
                            <ul id="product-tabs" class="nav nav-tabs nav-tab-cell">
                                <li class="active"><a data-toggle="tab" href="#description">DESCRIPTION</a></li>
                                 <li><a data-toggle="tab" href="#auctioneer">SELLER</a></li>
                                <!-- <li><a data-toggle="tab" href="#review">REVIEW</a></li> -->
                                <!-- <li><a data-toggle="tab" href="#auctioneer">COMMENT</a></li> -->
                            </ul><!-- /.nav-tabs #product-tabs -->
                        </div>
                        <div class="col-sm-9">

                            <div class="tab-content">
                                
                                <div id="description" class="tab-pane in active">
                                    <div class="product-tab">
                                        <p class="text">{{ $barang->deskripsi_barang }}</p>
                                    </div>  
                                    @if($barang->video_satu)
                                    <video height="300px" width="400px" controls>
                                        <source src="{{asset('uploads/jual_barang/'.$barang->video_satu)}}" type="video/ogg">
                                          Your browser does not support the video tag.
                                  </video>
                                  @endif
                                  @if($barang->video_dua)
                                    <video height="300px" width="400px" controls>
                                        <source src="{{asset('uploads/jual_barang/'.$barang->video_dua)}}" type="video/ogg">
                                          Your browser does not support the video tag.
                                  </video>
                                  @endif
                                  @if($barang->video_tiga)
                                    <video height="300px" width="400px" controls>
                                        <source src="{{asset('uploads/jual_barang/'.$barang->video_tiga)}}" type="video/ogg">
                                          Your browser does not support the video tag.
                                  </video>
                                  @endif
                               
                                </div><!-- /.tab-pane -->

                                <div id="review" class="tab-pane">
                                    <div class="product-tab">
                                                                                
                                        <div class="product-reviews">
                                            <h4 class="title">Customer Reviews</h4>

                                            <div class="reviews">
                                                <div class="review">
                                                    <div class="review-title"><span class="summary">We love this product</span><span class="date"><i class="fa fa-calendar"></i><span>1 days ago</span></span></div>
                                                    <div class="text">"Lorem ipsum dolor sit amet, consectetur adipiscing elit.Aliquam suscipit."</div>
                                                                                                        </div>
                                            
                                            </div><!-- /.reviews -->
                                        </div><!-- /.product-reviews -->
                                        

                                        
                                        <div class="product-add-review">
                                            <h4 class="title">Write your own review</h4>
                                            <div class="review-table">
                                                <div class="table-responsive">
                                                    <table class="table">   
                                                        <thead>
                                                            <tr>
                                                                <th class="cell-label">&nbsp;</th>
                                                                <th>1 star</th>
                                                                <th>2 stars</th>
                                                                <th>3 stars</th>
                                                                <th>4 stars</th>
                                                                <th>5 stars</th>
                                                            </tr>
                                                        </thead>    
                                                        <tbody>
                                                            <tr>
                                                                <td class="cell-label">Quality</td>
                                                                <td><input type="radio" name="quality" class="radio" value="1"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="2"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="3"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="4"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="5"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="cell-label">Price</td>
                                                                <td><input type="radio" name="quality" class="radio" value="1"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="2"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="3"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="4"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="5"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="cell-label">Value</td>
                                                                <td><input type="radio" name="quality" class="radio" value="1"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="2"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="3"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="4"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="5"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table><!-- /.table .table-bordered -->
                                                </div><!-- /.table-responsive -->
                                            </div><!-- /.review-table -->
                                            
                                            <div class="review-form">
                                                <div class="form-container">
                                                    <form role="form" class="cnt-form">
                                                        
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="exampleInputName">Your Name <span class="astk">*</span></label>
                                                                    <input type="text" class="form-control txt" id="exampleInputName" placeholder="">
                                                                </div><!-- /.form-group -->
                                                                <div class="form-group">
                                                                    <label for="exampleInputSummary">Summary <span class="astk">*</span></label>
                                                                    <input type="text" class="form-control txt" id="exampleInputSummary" placeholder="">
                                                                </div><!-- /.form-group -->
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="exampleInputReview">Review <span class="astk">*</span></label>
                                                                    <textarea class="form-control txt txt-review" id="exampleInputReview" rows="4" placeholder=""></textarea>
                                                                </div><!-- /.form-group -->
                                                            </div>
                                                        </div><!-- /.row -->
                                                        
                                                        <div class="action text-right">
                                                            <button class="btn btn-primary btn-upper">SUBMIT REVIEW</button>
                                                        </div><!-- /.action -->

                                                    </form><!-- /.cnt-form -->
                                                </div><!-- /.form-container -->
                                            </div><!-- /.review-form -->

                                        </div><!-- /.product-add-review -->                                     
                                        
                                    </div><!-- /.product-tab -->
                                </div><!-- /.tab-pane -->

                                <div id="auctioneer" class="tab-pane">
                                    <div class="product-tag">
                                        
                                        
                                      
                                            <div class="form-container">
                                    
                                                <div class="form-group">
                                                <h4 class="title">{{$customer->name}}</h4>
                                                </div>
                                                <div class="form-group">
                                                    
                                                    <label>Kontak : {{$customer->phone}}</label>
                                                </div>
                                                <div class="form-group">
                                                    <label>Email : {{$customer->email}}</label>
                                                   
                                                </div>

                                               
                                            </div><!-- /.form-container -->
                                      

                                    </div><!-- /.product-tab -->
                                </div><!-- /.tab-pane -->

                            </div><!-- /.tab-content -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.product-tabs -->

               
</div><!-- /.col -->
            <div class="clearfix"></div>
        </div><!-- /.row -->


        </div><!-- /.container -->
</div><!-- /.body-content -->
<!-- /#top-banner-and-menu --> 

<!-- ============================================================= FOOTER ============================================================= -->
<script src="{{asset('js/app.js')}}"></script>
@include('web.footerwebfix')


<script type="text/javascript">

  var SITEURL = '{{URL::to('')}}';
  var cekSession = "{{Auth::guard('customer')->check()}}";
  $(document).ready(function(){
      $("#btn_add_cart").click(function(){
        if(!cekSession){
            alert("Anda harus register/login terlebih dahulu..");
         }
      });
      
  });  
 
  var vmdetjual = new Vue({
    el: '#vmdetjual',
    data: {
        stok: '{{ $barang->stok }}',
        qty: 1
    },
    methods: {
        kurangStok: function(e) {    

            if(this.qty > 1){
            this.qty -= 1; 
            } else{
              alert('Qty tidak boleh kurang dari satu...');
            }

             e.preventDefault(); 
        }
        , AddStok: function(e) {    

            if(this.qty >= Number(this.stok)){
                alert('Qty tidak boleh melebihi stok yang tersedia...');
            } else{
              this.qty += 1; 
            }

             e.preventDefault(); 
        }
    }
  });
</script>    


</body>


</html>


