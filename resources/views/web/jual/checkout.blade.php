<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta -->
<meta name="csrf-token" content="{{ csrf_token() }}">
@include('web.headweb')
</head>
<body class="cnt-home">
<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1"> 
  
  <!-- ============================================== TOP MENU ============================================== -->
  <div class="top-bar animate-dropdown">
    <div class="container">
      <div class="header-top-inner">
        <div class="cnt-account">
          <ul class="list-unstyled">
            <!-- <li><a href="#"><i class="icon fa fa-user"></i>My Account</a></li> -->
            
              @if(!Auth::guard('customer')->check()) 
              <li><a href="{{ url('login/customer') }}"><i class="icon fa fa-lock"></i>Login</a></li>
              <li><a href="{{ url('register/customer') }}"><i class="icon fa fa-user"></i>Daftar Member</a></li>
               
              @endif      
              @if(Auth::guard('customer')->check())  
              <li><a href="{{ url('home/customer') }}"><i class="icon fa fa-home"></i>Dashboard</a></li>
                <li><a href="#"><i class="icon fa fa-user"></i> 
                  Selamat Datang {{ Auth::guard('customer')->user()->name  }}
                    </li> </a>
                    <li><a href="{{ url('logout/customer') }}"><i class="icon fa fa-lock"></i> 
                     Logout
                    </a></li> 
                    
              @endif      
              
          </ul>
        </div>
        <!-- /.cnt-account -->
        
        <!-- /.cnt-cart -->
        <div class="clearfix"></div>
      </div>
      <!-- /.header-top-inner --> 
    </div>
    <!-- /.container --> 
  </div>
  <!-- /.header-top --> 
  <!-- ============================================== TOP MENU : END ============================================== -->
  <div class="main-header">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 logo-holder"> 
          <!-- ============================================================= LOGO ============================================================= -->
          <div class="logo"> <a href="{{ url('/') }}"> <img src="{{asset('images/logo.png')}}" alt="logo"> </a> </div>
          <!-- /.logo --> 
          <!-- ============================================================= LOGO : END ============================================================= --> </div>
        <!-- /.logo-holder -->
        
        <div class="col-xs-12 col-sm-12 col-md-7 top-search-holder"> 
          <!-- /.contact-row --> 
          <!-- ============================================================= SEARCH AREA ============================================================= -->
          @include('web.searchbar')
          <!-- /.search-area --> 
          <!-- ============================================================= SEARCH AREA : END ============================================================= --> </div>
        <!-- /.top-search-holder -->
        
        @include('web.cart')
      <!-- /.row --> 
      
    </div>
    <!-- /.container --> 
    
  </div>
  <!-- /.main-header --> 
  
  <!-- ============================================== NAVBAR ============================================== -->
  @include('web.menuatas')
  <!-- /.header-nav --> 
  <!-- ============================================== NAVBAR : END ============================================== --> 
  
</header>

<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="#">Home</a></li>
				<li class='active'>Checkout</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->
<!-- ============================================== HEADER : END ============================================== -->
@if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
<form action="{{ route('jual.cart.insert-checkout') }}" method="POST"> 
		@csrf	
<div class="body-content">
	<div class="container">
		<div class="checkout-box ">
			<div class="row">
				<div class="col-md-8">
					<div class="panel-group checkout-steps" id="accordion">
						<!-- checkout-step-01  -->
	<div class="panel panel-default checkout-step-01">

		<!-- panel-heading -->
			<div class="panel-heading">
			<h4 class="unicase-checkout-title">
				<a data-toggle="collapse" class="" data-parent="#accordion" href="#collapseOne">
				<span>1</span>Alamat Pengiriman
				</a>
			</h4>
		</div>
		<!-- panel-heading -->

		<div id="collapseOne" class="panel-collapse collapse in">

			<!-- panel-body  -->
			<div class="panel-body">
				<div class="row">		

				

					<!-- already-registered-login -->
					<div class="col-md-12 col-sm-12 already-registered-login">
						<h4 class="checkout-subtitle"><b>{{Auth::guard('customer')->user()->name}}</b></h4>
						<p class="text title-tag-line">{{Auth::guard('customer')->user()->phone}}</p>
						<hr>
						
							<div class="form-group">
							<textarea class="form-control" id="alamat" name="alamat" rows="3">{{Auth::guard('customer')->user()->alamat}}</textarea>   
						</div>
					
					</div>	
					<!-- already-registered-login -->		

				</div>			
			</div>
			<!-- panel-body  -->

		</div><!-- row -->
	</div>
<!-- checkout-step-01  -->
					    <!-- checkout-step-02  -->
					  	<div class="panel panel-default checkout-step-02">
						    <div class="panel-heading">
						      <h4 class="unicase-checkout-title">
						        <a data-toggle="collapse" class="" data-parent="#accordion" href="#collapseTwo">
						          <span>2</span>Detail Belanja
						        </a>
						      </h4>
						    </div>
						    <div id="collapseTwo" class="panel-collapse collapse in">
						      <div class="panel-body">
								
							  <div class="shopping-cart">
								<div class="shopping-cart-table ">
                              <div class="table-responsive">
                                    <table class="table">
                                      
                                        <tbody>
                                        <tr>
                                                <th>#</th>
                                                <th>Photo</th>
                                                <th>Barang</th>
                                                <th>Qty</th>
                                                <th>Harga</th>
                                               
                                            </tr>
                                          @foreach(Cart::session(Auth::guard('customer')->user()->id)->getContent() as $cart)
                                            <tr>
                                                <td>
												<form action="{{ route('jual.remove-cart', $cart->id) }}" method="POST"> 
                   								@csrf 
													<button type="submit" id="btnRemove"><i class="fa fa-trash"></i></button> 
												</form>	
												</td>
                                                <td>
                                                    <a class="entry-thumbnail" href="#">
                                                        <img src="{{asset('uploads/jual_barang/'.DB::table('barang')->where('id', $cart->id)->first()->gambar_satu)}}" width="80" height="80" alt="">
                                                    </a>
                                                </td>
                                                <td>
												
                                                    <p class='cart-product-description'><a href="#">{{$cart->name}}</a></p>
													<input type="hidden" name="idbrg[]" value="{{$cart->id}}" class="form-control">
													<input type="hidden" name="nmbrg[]" value="{{$cart->name}}" class="form-control">
													<input type="hidden" name="quantity[]" value="{{$cart->quantity}}" class="form-control">
													<input type="hidden" name="harga[]" value="{{$cart->price}}" class="form-control">
                                                </td>
                                                
                                                {{-- <td class="cart-quantity">
													
														<div class="quant-input">
															<div class="arrows">
															<div class="arrow plus gradient"><span class="ir"><i class="icon fa fa-sort-asc"></i></span></div>
															<div class="arrow minus gradient"><span class="ir"><i class="icon fa fa-sort-desc"></i></span></div>
															</div>
															<input type="text" readOnly="" class="form-control tnilaibid" id="qty" name="qty" value="{{$cart->quantity}}">   
												
                                                </td> --}}
                                                <td>{{$cart->quantity}}</td>
                                                <td>Rp. {{number_format($cart->price,0,',','.')}}</td>
                                               
                                            </tr>
											@endforeach
											<tr>
												<td colspan="4" align="right">Total : </td>
												<td>Rp. {{number_format(Cart::session(Auth::guard('customer')->user()->id)->getTotal(),0,',','.')}}</td>
                                               
                                            </tr>
                                        </tbody><!-- /tbody -->
                                    </table><!-- /table -->
                                </div>
                              

							  </div>
							  
							</div>
							</div>
						    </div>
					  	</div>
					  	<!-- checkout-step-02  -->

						<!-- checkout-step-03  -->
					  	<div class="panel panel-default checkout-step-03">
						    <div class="panel-heading">
						      <h4 class="unicase-checkout-title">
						        <a data-toggle="collapse" class="" data-parent="#accordion" href="#collapseThree">
						       		<span>3</span>Metode Pembayaran
						        </a>
						      </h4>
						    </div>
						    <div id="collapseThree" class="panel-collapse collapse in">
						      <div class="panel-body">
							  <div class="row">		

			

									<!-- already-registered-login -->
									<div class="col-md-12 col-sm-12 already-registered-login">
										
											<div class="form-group">
											{{-- <input type="text" class="form-control" id="method_bayar" name="method_bayar" value="Transfer Bank" readOnly=""> --}}
											<select id="method_bayar" name="method_bayar" class="form-control">
												{{-- <option value="" selected>Pilih...</option> --}}
												@foreach($method_byrs as $item)
												<option value="{{ $item->id }}" {{ isset($selected_item) && $selected_item->id == $item->id ? 'selected="selected"' : '' }}>{{ $item->name }}</option>
											  
												@endforeach
											 
											</select>	
										</div>

									</div>	
									<!-- already-registered-login -->		

									</div>			
						      </div>
						    </div>
					  	</div>
					  	<!-- checkout-step-03  -->

					
					  	
					</div><!-- /.checkout-steps -->
				</div>
				<div class="col-md-4">
					<!-- checkout-progress-sidebar -->
<div class="checkout-progress-sidebar ">
	<div class="panel-group">
	
		<div class="panel panel-default">
			<div class="panel-heading">
		    	<h4 class="unicase-checkout-title">Ringkasan Belanja</h4>
		    </div>
		    <table>
				<tr>
					<td width="40%">Total Harga ({{Cart::session(Auth::guard('customer')->user()->id)->getContent()->count()}} barang)</td>
					<td width="10%">&nbsp;</td>
					<td width="15%"><b>Rp. {{number_format(Cart::session(Auth::guard('customer')->user()->id)->getTotal(),0,',','.')}}</b></td>
				</tr>
				<!-- <tr>
					<td width="40%">Ongkir</td>
					<td width="10%">&nbsp;</td>
					<td width="15%"><b>Rp. {{number_format(Cart::session(Auth::guard('customer')->user()->id)->getTotal(),0,',','.')}}</b></td>
				</tr> -->
			</table>
			<hr>
			<table>
				<tr>
					<td width="40%"><h5><b>Total Tagihan</b></h5></td>
					<td width="10%">&nbsp;</td>
					<td width="15%"><b>Rp. {{number_format(Cart::session(Auth::guard('customer')->user()->id)->getTotal(),0,',','.')}}</b></td>
				</tr>
			</table>
			<hr>
			<br>
			<center><button type="submit" class="btn checkout-btn">Selesaikan Pembayaran</button></center>
			
		</div>
	
	</div>
</div> 
<!-- checkout-progress-sidebar -->				</div>
			</div><!-- /.row -->
		</div><!-- /.checkout-box -->
	
<!-- ============================================== BRANDS CAROUSEL : END ============================================== -->	</div><!-- /.container -->
</div><!-- /.body-content -->
</form>

<!-- ============================================================= FOOTER ============================================================= -->
<!-- <script src="{{asset('js/app.js')}}"></script> -->
@include('web.footerwebfix')


<script type="text/javascript">

//   var SITEURL = '{{URL::to('')}}';
//   var cekSession = "{{Auth::guard('customer')->check()}}";
  
 
//   var vmdetjual = new Vue({
//     el: '#vmdetjual',
//     data: {
//         stok: '',
//         qty: 1
//     },
//     methods: {
//         kurangStok: function(e) {    

//             if(this.qty > 1){
//             this.qty -= 1; 
//             } else{
//               alert('Qty tidak boleh kurang dari satu...');
//             }

//              e.preventDefault(); 
//         }
//         , AddStok: function(e) {    

//             if(this.qty >= Number(this.stok)){
//                 alert('Qty tidak boleh melebihi stok yang tersedia...');
//             } else{
//               this.qty += 1; 
//             }

//              e.preventDefault(); 
//         }
//     }
//   });
</script>    


</body>


</html>


