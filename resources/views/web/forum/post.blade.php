<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta -->
@include('web.headweb')
</head>
<body class="cnt-home">
<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1"> 
  
  <!-- ============================================== TOP MENU ============================================== -->
  <div class="top-bar animate-dropdown">
    <div class="container">
      <div class="header-top-inner">
        <div class="cnt-account">
          <ul class="list-unstyled">
            <!-- <li><a href="#"><i class="icon fa fa-user"></i>My Account</a></li> -->
            
              @if(!Auth::guard('customer')->check()) 
              <li><a href="{{ url('login/customer') }}"><i class="icon fa fa-lock"></i>Login</a></li>
              <li><a href="{{ url('register/customer') }}"><i class="icon fa fa-user"></i>Daftar Member</a></li>
              @endif      
              @if(Auth::guard('customer')->check())  
              <li><a href="{{ url('home/customer') }}"><i class="icon fa fa-home"></i>Dashboard</a></li>
                <li><a href="#"><i class="icon fa fa-user"></i> 
                  Selamat Datang {{ Auth::guard('customer')->user()->name  }}
                    </li> </a>
                    <li><a href="{{ url('logout/customer') }}"><i class="icon fa fa-lock"></i> 
                     Logout
                    </a></li> 
              @endif      
              
          </ul>
        </div>
        <!-- /.cnt-account -->
        
        <!-- /.cnt-cart -->
        <div class="clearfix"></div>
      </div>
      <!-- /.header-top-inner --> 
    </div>
    <!-- /.container --> 
  </div>
  <!-- /.header-top --> 
  <!-- ============================================== TOP MENU : END ============================================== -->
  <div class="main-header">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 logo-holder"> 
          <!-- ============================================================= LOGO ============================================================= -->
          <div class="logo"> <a href="{{ url('/') }}"> <img src="{{asset('images/logo.png')}}" alt="logo"> </a> </div>
          <!-- /.logo --> 
          <!-- ============================================================= LOGO : END ============================================================= --> </div>
        <!-- /.logo-holder -->
        
        <div class="col-xs-12 col-sm-12 col-md-7 top-search-holder"> 
          <!-- /.contact-row --> 
          <!-- ============================================================= SEARCH AREA ============================================================= -->
          @include('web.searchbar')
          <!-- /.search-area --> 
          <!-- ============================================================= SEARCH AREA : END ============================================================= --> </div>
        <!-- /.top-search-holder -->
        
        @include('web.cart')
      <!-- /.row --> 
      
    </div>
    <!-- /.container --> 
    
  </div>
  <!-- /.main-header --> 
  
  <!-- ============================================== NAVBAR ============================================== -->
  @include('web.menuatas')
  <!-- /.header-nav --> 
  <!-- ============================================== NAVBAR : END ============================================== --> 
  
</header>

<!-- ============================================== HEADER : END ============================================== -->
<div class="body-content">
    <div class="container">
        <div class="row">
            <div class="blog-page">
                <div class="col-md-12">
                    <div class="blog-post wow fadeInUp">
                        <h3>New Thread</h3>
                    </div>
           
                    @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

<form action="{{ route('forum.store') }}" method="POST" enctype="multipart/form-data">
@csrf
<div class="blog-write-comment outer-bottom-xs outer-top-xs">
    <div class="row">
      
        <div class="col-md-12">
           
                <div class="form-group">
                <label class="info-title" for="InputKategoru"><b>Kategori</b></label>
                <select id="id_category_forum" name="id_category_forum" class="form-control">
                        <option selected>Pilih...</option>
                        @foreach($hobbies as $item)
                        <option value="{{ $item->id }}" {{ isset($selected_item) && $selected_item->id == $item->id ? 'selected="selected"' : '' }}>{{ $item->name }}</option>
            
                        @endforeach
                     
                    </select>  
            </div>
            
        </div>
        <div class="col-md-12">
           
                <div class="form-group">
                <label class="info-title" for="InputKategoru"><b>Lebih Spesifik</b></label>
                <select id="idhobidet" name="idhobidet" class="form-control">
                      
                    </select>  
            </div>
            
        </div>
        <div class="col-md-12">
            
                <div class="form-group">
                <label class="info-title"><b>Judul</b></label>
                <input type="text" class="form-control unicase-form-control" name="judul" placeholder="Judul...">
            
              </div>
           
        </div>
        
        <div class="col-md-12">
           
                <div class="form-group">
                <label class="info-title" for="InputDeskripsi"><b>Deskripsi</b></label>
                <textarea class="form-control summernote" id="deskripsi" name="deskripsi" rows="6"></textarea>
              </div>
         
        </div>
        <!-- <div class="col-md-12">
           
                <div class="form-group">
                <label class="info-title" for="InputTag"><b>Tag</b></label>
                <input type="text" class="form-control unicase-form-control text-input" id="tag" name="tag" placeholder="Tag (Min.1 & Max.3) , untuk pemisah gunakan tanda ';'" >
              </div>
            
        </div> -->
        <div class="col-md-12 outer-bottom-small m-t-20">
            <button type="submit" class="btn-upper btn btn-primary checkout-page-button">Post Thread</button>
        </div>
    </div>
</div>
</form>
                </div>
     

                <!-- ==============================================CATEGORY============================================== -->


                   </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /#top-banner-and-menu --> 

<!-- ============================================================= FOOTER ============================================================= -->
@include('web.footerwebfix')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>    
 
<script>
var SITEURL = '{{URL::to('')}}';
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 300,   //set editable area's height
                codemirror: { // codemirror options
                  theme: 'monokai'
                }
            });

            $('select[name="id_category_forum"]').on('change', function() {
              var idhobby = $(this).val();
              if(idhobby) {
                  $.ajax({
                  //   url: "{{ url('/lelang/hobbydet/') }}/"+idhobby, //'/lelang/detail_hobby/'+provID,
                      url: SITEURL +"/lelang/hobbydet/"+idhobby,
                      type: "GET",
                      dataType: "json",
                      success:function(data) {                      
                          $('select[name="idhobidet"]').empty();
                          // $.each(data, function(key, value) {
                          //     $('select[name="idhobidet"]').append('<option value="'+ key +'">'+ value +'</option>');
                          // });
                          $.each(data.dethobby, function(key, value) {
                          $('#idhobidet').append('<option value="'+ value.id+'">'+ value.name +'</option>');
                          });
                      }
                  });
              }else{
                  $('select[name="idhobidet"]').empty();
              }
          });


        });
</script>
 
<script type="text/javascript">
   $(document).ready(function(){
        $('[data-countdown]').each(function() {
        var $this = $(this), finalDate = $(this).data('countdown');
        $this.countdown(finalDate, function(event) {
          $this.html(event.strftime('%D Hari %H:%M:%S'));
        });
      });
});

</script>

</body>
</html>