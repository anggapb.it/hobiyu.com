<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('ui-backend/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@include('web.headweb')
</head>
<body class="cnt-home">
<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1"> 
  
  <!-- ============================================== TOP MENU ============================================== -->
  <div class="top-bar animate-dropdown">
    <div class="container">
      <div class="header-top-inner">
        <div class="cnt-account">
          <ul class="list-unstyled">
            <!-- <li><a href="#"><i class="icon fa fa-user"></i>My Account</a></li> -->
            
              @if(!Auth::guard('customer')->check()) 
              <li><a href="{{ url('login/customer') }}"><i class="icon fa fa-lock"></i>Login</a></li>
              <li><a href="{{ url('register/customer') }}"><i class="icon fa fa-user"></i>Daftar Member</a></li>
              @endif      
              @if(Auth::guard('customer')->check())  
              <li><a href="{{ url('home/customer') }}"><i class="icon fa fa-home"></i>Dashboard</a></li>
                <li><a href="#"><i class="icon fa fa-user"></i> 
                  Selamat Datang {{ Auth::guard('customer')->user()->name  }}
                    </li> </a>
                    <li><a href="{{ url('logout/customer') }}"><i class="icon fa fa-lock"></i> 
                     Logout
                    </a></li> 
              @endif      
              
          </ul>
        </div>
        <!-- /.cnt-account -->
        
        <!-- /.cnt-cart -->
        <div class="clearfix"></div>
      </div>
      <!-- /.header-top-inner --> 
    </div>
    <!-- /.container --> 
  </div>
  <!-- /.header-top --> 
  <!-- ============================================== TOP MENU : END ============================================== -->
  <div class="main-header">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 logo-holder"> 
          <!-- ============================================================= LOGO ============================================================= -->
          <div class="logo"> <a href="{{ url('/') }}"> <img src="{{asset('images/logo.png')}}" alt="logo"> </a> </div>
          <!-- /.logo --> 
          <!-- ============================================================= LOGO : END ============================================================= --> </div>
        <!-- /.logo-holder -->
        
        <div class="col-xs-12 col-sm-12 col-md-7 top-search-holder"> 
          <!-- /.contact-row --> 
          <!-- ============================================================= SEARCH AREA ============================================================= -->
          @include('web.searchbar')
          <!-- /.search-area --> 
          <!-- ============================================================= SEARCH AREA : END ============================================================= --> </div>
        <!-- /.top-search-holder -->
        
        @include('web.cart')
      <!-- /.row --> 
      
    </div>
    <!-- /.container --> 
    
  </div>
  <!-- /.main-header --> 
  
  <!-- ============================================== NAVBAR ============================================== -->
  @include('web.menuatas')
  <!-- /.header-nav --> 
  <!-- ============================================== NAVBAR : END ============================================== --> 
  
</header>

<!-- ============================================== HEADER : END ============================================== -->
<div id="ext-time">
<div class="body-content outer-top-xs" id="top-banner-and-menu">
  <div class="container">
    <div class="row"> 
      <!-- ============================================== SIDEBAR ============================================== -->
      <div class="col-xs-12 col-sm-12 col-md-3 sidebar"> 
        
        <!-- ================================== TOP NAVIGATION ================================== -->
        <div class="sidebar-module-container">
          <div class="sidebar-filter"> 
            <!-- ============================================== SIDEBAR CATEGORY ============================================== -->
                @include('web.sidebarcust')            
            </div>
        
          </div>
          <!-- /.sidebar-filter --> 
        </div>
        <!-- /.side-menu --> 
        <!-- ================================== TOP NAVIGATION : END ================================== --> 
      </div>
      <!-- /.sidemenu-holder --> 
      <!-- ============================================== SIDEBAR : END ============================================== --> 
      
      <!-- ============================================== CONTENT ============================================== -->
      <div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder"> 
      
        
        <!-- ============================================== BLOG SLIDER ============================================== -->
        <section class="section latest-blog outer-bottom-vs wow fadeInUp">
          <h3 class="section-title">My Forum</h3>
          <br>
        <div id="container_forum">  
       
          <div class="table-responsive">
          @if ($message = Session::get('success'))
              <div class="alert alert-success">
                  <p>{{ $message }}</p>
              </div>
          @endif
            <table class="table table-striped">
                <thead>
                    <tr>
                    <td><strong>No</strong></td>
                    <td><strong>Judul</strong></td>
                    <td><strong>Action</strong></td>
                 
                    </tr>
                </thead>
                <tbody>
                <!-- <tr> 
                            <td colspan="6">Belum ada barang yang anda jual...</td>
                           
                </tr>          -->
                    
                @foreach ($forums as $forum)
  
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $forum->judul }}</td>                   
                       
                  
                    <td>
                    <button type="button" class="btn btn-info btn-xs modal_edit_forums" 
                         data-id="{{$forum->id}}"
                         data-judul="{{$forum->judul}}"
                         data-deskripsi="{{$forum->deskripsi}}">
                      <span class="icon fa fa-pencil"> Edit</span></button>
                
                    </td>
                  
                </tr>
                @endforeach
                 
                </tbody>
                </table>
                {!! $forums->links() !!}
             </div>
            <!-- /.owl-carousel --> 
         
          <!-- /.blog-slider-container --> 
         </div> 
        </section>
        <!-- /.section --> 
        <!-- ============================================== BLOG SLIDER : END ============================================== --> 
        
        
        
      </div>
      <!-- /.homebanner-holder --> 
      <!-- ============================================== CONTENT : END ============================================== --> 
    </div>



<div class="modal fade" id="mdlOrder" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
   
      <form action="{{ route('customer.update-forum') }}" method="POST" enctype="multipart/form-data">
          @csrf
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Forum</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
    
      <!-- Modal body -->
      <div class="form-row">
                  
            <div class="form-group col-md-12">
            <label>Judul</label>
            <input type="text" class="form-control" id="judul" name="judul">
            <input type="hidden" class="form-control" id="idforum" name="idforum">
            </div>
           
        </div>
      <div class="form-row">
        <div class="form-group col-md-12">
            <label for="deskripsi">Isi</label>
            <textarea class="form-control" id="deskripsi" name="deskripsi" rows="5"></textarea>
        </div>
      </div>
  
      


      <!-- Modal footer -->
      <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </form>
 
    </div>
  </div>
</div>
    <!-- /.row --> 
    <!-- ============================================== BRANDS CAROUSEL ============================================== -->
   
    <!-- /.logo-slider --> 
    <!-- ============================================== BRANDS CAROUSEL : END ============================================== --> 
  </div>
  <!-- /.container --> 
</div>

</div>
<!-- /#top-banner-and-menu --> 

<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="footer color-bg">
@include('web.footerweb')
</footer>
<!-- ============================================================= FOOTER : END============================================================= --> 

<!-- For demo purposes – can be removed on production --> 

<!-- For demo purposes – can be removed on production : End --> 

<!-- JavaScripts placed at the end of the document so the pages load faster -->
<!-- <script src="{{asset('js/app.js')}}"></script>  -->
<script src="{{asset('js/jquery-1.11.1.min.js')}}"></script> 
<script src="{{asset('js/bootstrap.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-hover-dropdown.min.js')}}"></script> 
<script src="{{asset('js/owl.carousel.min.js')}}"></script> 
<script src="{{asset('js/echo.min.js')}}"></script> 
<script src="{{asset('js/jquery.easing-1.3.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-slider.min.js')}}"></script> 
<script src="{{asset('js/jquery.rateit.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/lightbox.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-select.min.js')}}"></script> 
<script src="{{asset('js/wow.min.js')}}"></script> 
<script src="{{asset('js/scripts.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.id.js')}}"></script>
<script src="{{ asset('ui-backend/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('ui-backend/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>    



<script>
var SITEURL = '{{URL::to('')}}';
$(document).ready( function () {

    $('.summernote').summernote({
                height: 300,   //set editable area's height
                codemirror: { // codemirror options
                  theme: 'monokai'
                }
            });

//STATUS SEMENTARA DI HIDE DULU
$("#status").hide();
  

$(".modal_edit_forums").click(function () {
    var forum_id = $(this).attr('data-id');
    var judul = $(this).attr('data-judul');
    var deskripsi = $(this).attr('data-deskripsi');
    $("#idforum").val(forum_id);
    $("#judul").val(judul);
    $("#deskripsi").val(deskripsi);
 
    $('#mdlOrder').modal('show');
});
    

    $('#add_time').datetimepicker({
    //  language: 'id',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1
    
    })


  
});

</script>

</body>
</html>