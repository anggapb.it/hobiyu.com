<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta -->
@include('web.headweb')
</head>
<body class="cnt-home">
<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1"> 
  
  <!-- ============================================== TOP MENU ============================================== -->
  <div class="top-bar animate-dropdown">
    <div class="container">
      <div class="header-top-inner">
        <div class="cnt-account">
          <ul class="list-unstyled">
            <!-- <li><a href="#"><i class="icon fa fa-user"></i>My Account</a></li> -->
            
              @if(!Auth::guard('customer')->check()) 
              <li><a href="{{ url('login/customer') }}"><i class="icon fa fa-lock"></i>Login</a></li>
              <li><a href="{{ url('register/customer') }}"><i class="icon fa fa-user"></i>Daftar Member</a></li>
              @endif      
              @if(Auth::guard('customer')->check())  
              <li><a href="{{ url('home/customer') }}"><i class="icon fa fa-home"></i>Dashboard</a></li>
                <li><a href="#"><i class="icon fa fa-user"></i> 
                  Selamat Datang {{ Auth::guard('customer')->user()->name  }}
                    </li> </a>
                    <li><a href="{{ url('logout/customer') }}"><i class="icon fa fa-lock"></i> 
                     Logout
                    </a></li> 
              @endif      
              
          </ul>
        </div>
        <!-- /.cnt-account -->
        
        <!-- /.cnt-cart -->
        <div class="clearfix"></div>
      </div>
      <!-- /.header-top-inner --> 
    </div>
    <!-- /.container --> 
  </div>
  <!-- /.header-top --> 
  <!-- ============================================== TOP MENU : END ============================================== -->
  <div class="main-header">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 logo-holder"> 
          <!-- ============================================================= LOGO ============================================================= -->
          <div class="logo"> <a href="{{ url('/') }}"> <img src="{{asset('images/logo.png')}}" alt="logo"> </a> </div>
          <!-- /.logo --> 
          <!-- ============================================================= LOGO : END ============================================================= --> </div>
        <!-- /.logo-holder -->
        
        <div class="col-xs-12 col-sm-12 col-md-7 top-search-holder"> 
          <!-- /.contact-row --> 
          <!-- ============================================================= SEARCH AREA ============================================================= -->
          @include('web.searchbar')
          <!-- /.search-area --> 
          <!-- ============================================================= SEARCH AREA : END ============================================================= --> </div>
        <!-- /.top-search-holder -->
        
        @include('web.cart')
      <!-- /.row --> 
      
    </div>
    <!-- /.container --> 
    
  </div>
  <!-- /.main-header --> 
  
  <!-- ============================================== NAVBAR ============================================== -->
  @include('web.menuatas')
  <!-- /.header-nav --> 
  <!-- ============================================== NAVBAR : END ============================================== --> 
  
</header>

<!-- ============================================== HEADER : END ============================================== -->
<div class="body-content outer-top-xs" id="top-banner-and-menu">
  <div class="container">
    <div class="row"> 
      <!-- ============================================== SIDEBAR ============================================== -->
      <div class="col-xs-12 col-sm-12 col-md-3 sidebar"> 
        
        <!-- ================================== TOP NAVIGATION ================================== -->
        <div class="sidebar-module-container">
          <div class="sidebar-filter"> 
            <!-- ============================================== SIDEBAR CATEGORY ============================================== -->
                @include('web.sidebarcust')            
            </div>
        
          </div>
          <!-- /.sidebar-filter --> 
        </div>
        <!-- /.side-menu --> 
        <!-- ================================== TOP NAVIGATION : END ================================== --> 
      </div>
      <!-- /.sidemenu-holder --> 
      <!-- ============================================== SIDEBAR : END ============================================== --> 
      
      <!-- ============================================== CONTENT ============================================== -->
      <div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder"> 

      @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if ($message = Session::get('success'))
              <div class="alert alert-success">
                  <p>{{ $message }}</p>
              </div>
          @endif

      <form action="{{ route('customer.update-profile',$cust->id) }}" method="POST" enctype="multipart/form-data">
          @csrf
         
        <!-- ============================================== BLOG SLIDER ============================================== -->
        <section class="section latest-blog outer-bottom-vs wow fadeInUp">
          <h3 class="section-title">My Profile</h3>
          <div class="blog-slider-container outer-top-xs">
            <div class="owl-carousel blog-slider custom-carousel">
              <div class="item">
               @if(empty($cust->photo_profile))   
              <img src="{{asset('images/blog-post/post1.jpg')}}" width="200" height="200" alt=""/>
              @endif
              @if(!empty($cust->photo_profile))   
              <img src="{{asset('uploads/member_profile/'.$cust->photo_profile)}}" width="200" height="200" alt=""/>
              @endif
             
                <div class="blog-post">
                <br>
                  <div class="blog-post-info text-left">
                  <div class="form-row">
                    <input type="file" class="form-control-file" id="photo_profile" name="photo_profile"  value="{{$cust->photo_profile}}">{{$cust->photo_profile}}
                    <input type="hidden"  class="form-control" id="photo" name="photo" value="{{$cust->photo_profile}}">
                    <span>Maksimal 8 Mb</span>
                    </div> 
                <br>
                    <div class="form-row">
                    {{-- <label>Password :</label> --}}
                    <input type="hidden" class="form-control" id="password" name="password" value="{{$cust->password}}">
                   <span>&nbsp;</span>
                    </div>

                    <div class="form-row">
                    <label>Password Baru :</label>
                    <input type="password" class="form-control" id="new_password" name="new_password" >
                    <span class="info">Password minimal harus 8 karakter</span>
                    </div>
                    
                    <div class="form-row">
                    <label>Bank :</label>
                    <select  id="idbank" name="idbank" class="form-control">
                      
                          <option value="0" selected>Pilih...</option>
                                        
                        @foreach($bank as $item)
                        <option value="{{ $item->id }}" {{ isset($cust->idbank) && $cust->idbank == $item->id ? 'selected="selected"' : '' }}>{{ $item->name }}</option>
                      
                        @endforeach
                     
                    </select>
                    <span>&nbsp;</span>
                    </div>

                    <div class="form-row">
                    <label>Jenis Kelamin :</label>
                    <select id="kelamin" name="kelamin" class="form-control">
                    
                          <option value="LAKI-LAKI" {{ isset($cust->kelamin) && $cust->kelamin == 'LAKI-LAKI' ? 'selected="selected"' : '' }}>LAKI-LAKI</option>
                          <option value="PEREMPUAN" {{ isset($cust->kelamin) && $cust->kelamin == 'PEREMPUAN' ? 'selected="selected"' : '' }}>PEREMPUAN</option>
                     
                    
                    </select>
                    <span class="info"></span>
                    </div>
 
                    <div class="form-row">
                    <label>Tempat Lahir :</label>
                    <input type="text" class="form-control" id="tpt_lahir" name="tpt_lahir" value="{{$cust->tpt_lahir}}">
                    <span class="info"></span>
                    </div> 
                   
                   
                    <div class="form-row">
                    <label>Pekerjaan :</label>
                    <select id="idpekerjaan" name="idpekerjaan" class="form-control">
                   
                          <option value="0">Pilih...</option>
                                     
                        @foreach($jobs as $item)
                        <option value="{{ $item->id }}" {{ isset($cust->idpekerjaan) && $cust->idpekerjaan == $item->id ? 'selected="selected"' : '' }}>{{ $item->name }}</option>
                      
                        @endforeach
                     </select>   
                    <span class="info"></span>
                    </div>
                   
                    <div class="form-row">
                    <label>Provinsi :</label>
                    <select id="idprovinsi" name="idprovinsi" class="form-control">
                   
                          <option value="0">Pilih...</option>
                                     
                        @foreach($provs as $item)
                        <option value="{{ $item->id }}" {{ isset($cust->idprovinsi) && $cust->idprovinsi == $item->id ? 'selected="selected"' : '' }}>{{ $item->name }}</option>
                      
                        @endforeach
                     </select>   
                    <span class="info"></span>
                    </div>
                   
                    <div class="form-row">
                    <label>Kota / Kabupaten :</label>
                    <select id="idkota" name="idkota" class="form-control">
                        
                        
                     </select>   
                    <span class="info"></span>
                    </div>
    

                  <br>
                    <button class="lnk btn btn-primary">Submit </button>
   
                </div>
                  <!-- /.blog-post-info --> 
                  
                </div>
                <!-- /.blog-post --> 
              </div>
              <!-- /.item -->
              
              <div class="item">
                <div class="blog-post">
                  
                  <!-- /.blog-post-image -->
                  
                  <div class="blog-post-info text-left">
                  <div class="form-row">
                    <label>Nama :</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{$cust->name}}">
                    <span class="info">Member Sejak &nbsp;:&nbsp; {{$cust->created_at}} </span>
                    </div>

                    <div class="form-row">
                    <label>Email :</label>
                    <input type="text" class="form-control" id="email" name="email" value="{{$cust->email}}">
                    <span>&nbsp;</span>
                    </div>
                  
                    <div class="form-row">
                    <label>No. Handphone :</label>
                    <input type="text" class="form-control" id="phone" name="phone" value="{{$cust->phone}}">
                    <span>&nbsp;</span>
                    </div>
                   
                    <div class="form-row">
                    <label>Alamat :</label>
                    <textarea class="form-control" id="alamat" name="alamat" rows="3" >{{$cust->alamat}}</textarea>
                    <span>&nbsp;</span>
                    </div>
                    
                    <div class="form-row">
                    <label>Kodepos :</label>
                    <input type="text" class="form-control" id="kodepos" name="kodepos" value="{{$cust->kodepos}}">
                    <span></span>
                    </div>
                   <br>
                   <div class="form-row">
                    <label>Kode Rekening :</label>
                    <input type="text" class="form-control" id="kdrek" name="kdrek" value="{{$cust->kdrek}}">
                   <span>&nbsp;</span>
                    </div>

                    <div class="form-row">
                    <label>No. KTP :</label>
                    <input type="text" class="form-control" id="noktp" name="noktp" value="{{$cust->noktp}}">
                    <span class="info"></span>
                    </div>

                    <div class="form-row">
                    <label>Tgl Lahir :</label>
                    <input type="text" class="form-control"  autocomplete="off" id="tgl_lahir" name="tgl_lahir" value="{{$cust->tgl_lahir}}">
                    <span class="info"></span>
                    </div>

                    <div class="form-row">
                    <label>Tgl Aktivasi :</label>
                    <input type="text" readOnly="true" class="form-control" id="tglaktivasi" name="tglaktivasi" value="{{$cust->tglaktivasi}}">
                    <span class="info"></span>
                    </div>

                    <div class="form-row">
                    <label>Kecamatan :</label>
                    <select id="idkecamatan" name="idkecamatan" class="form-control">
                   
                       
                     </select>   
                    <span class="info"></span>
                    </div>
                  
                    <div class="form-row">
                    <label>Kelurahan :</label>
                    <select id="idkelurahan" name="idkelurahan" class="form-control">
                
                     </select>   
                    <span class="info"></span>
                    </div>
                  
                  <!-- /.blog-post-info --> 
                  
                </div>
                <!-- /.blog-post --> 
              </div>
              <!-- /.item --> 
              
              <!-- /.item -->
        
            </div>
            <!-- /.owl-carousel --> 
          </div>
          <!-- /.blog-slider-container --> 
        </section>
        <!-- /.section --> 
</form>
        <!-- ============================================== BLOG SLIDER : END ============================================== --> 
        
        
        
      </div>
      <!-- /.homebanner-holder --> 
      <!-- ============================================== CONTENT : END ============================================== --> 
    </div>
    <!-- /.row --> 
    <!-- ============================================== BRANDS CAROUSEL ============================================== -->
    <div id="brands-carousel" class="logo-slider wow fadeInUp">
      <div class="logo-slider-inner">
        <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
          <div class="item m-t-15"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item m-t-10"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand3.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand6.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item--> 
        </div>
        <!-- /.owl-carousel #logo-slider --> 
      </div>
      <!-- /.logo-slider-inner --> 
      
    </div>
    <!-- /.logo-slider --> 
    <!-- ============================================== BRANDS CAROUSEL : END ============================================== --> 
  </div>
  <!-- /.container --> 
</div>
<!-- /#top-banner-and-menu --> 

<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="footer color-bg">
@include('web.footerweb')
</footer>
<!-- ============================================================= FOOTER : END============================================================= --> 

<!-- For demo purposes – can be removed on production --> 

<!-- For demo purposes – can be removed on production : End --> 

<!-- JavaScripts placed at the end of the document so the pages load faster --> 
<script src="{{asset('js/jquery-1.11.1.min.js')}}"></script> 
<script src="{{asset('js/bootstrap.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-hover-dropdown.min.js')}}"></script> 
<script src="{{asset('js/owl.carousel.min.js')}}"></script> 
<script src="{{asset('js/echo.min.js')}}"></script> 
<script src="{{asset('js/jquery.easing-1.3.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-slider.min.js')}}"></script> 
<script src="{{asset('js/jquery.rateit.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/lightbox.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-select.min.js')}}"></script> 
<script src="{{asset('js/wow.min.js')}}"></script> 
<script src="{{asset('js/scripts.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.id.js')}}"></script>

<script>
var SITEURL = '{{URL::to('')}}';
$(document).ready( function () {
  idprov = $('#idprovinsi').val();
  idkota = $('#idkota').val();
  loadKota(idprov);
 
  $('#tgl_lahir').datepicker({
    //  language: 'id',
        autoclose: 1,  
        format:'yyyy-mm-dd'  
    })
 
  // $('#tglaktivasi').datepicker({
  //   //  language: 'id',
  //       autoclose: 1,  
  //       format:'dd/mm/yyyy'  
  //   })


  $('select[name="idprovinsi"]').on('change', function() {
          var id = $(this).val();
          if(id) {
              $.ajax({
               //   url: "{{ url('/lelang/hobbydet/') }}/"+idhobby, //'/lelang/detail_hobby/'+provID,
                  url: SITEURL +"/customer/kota/"+id,
                  type: "GET",
                  dataType: "json",
                  success:function(data) {                      
                      $('select[name="idkota"]').empty();
                      // $.each(data, function(key, value) {
                      //     $('select[name="idhobidet"]').append('<option value="'+ key +'">'+ value +'</option>');
                      // });
                      $.each(data.kota, function(key, value) {
                      $('#idkota').append('<option value="'+ value.id+'">'+ value.name +'</option>');
                      });
                  }
              });
          }else{
              $('select[name="idkota"]').empty();
          }
      });
 
  $('select[name="idkota"]').on('change', function() {
          var id = $(this).val();
          if(id) {
              $.ajax({
               //   url: "{{ url('/lelang/hobbydet/') }}/"+idhobby, //'/lelang/detail_hobby/'+provID,
                  url: SITEURL +"/customer/kecamatan/"+id,
                  type: "GET",
                  dataType: "json",
                  success:function(data) {                      
                      $('select[name="idkecamatan"]').empty();
                      // $.each(data, function(key, value) {
                      //     $('select[name="idhobidet"]').append('<option value="'+ key +'">'+ value +'</option>');
                      // });
                      $.each(data.kec, function(key, value) {
                      $('#idkecamatan').append('<option value="'+ value.id+'">'+ value.name +'</option>');
                      });
                  }
              });
          }else{
              $('select[name="idkecamatan"]').empty();
          }
      });
 
  $('select[name="idkecamatan"]').on('change', function() {
          var id = $(this).val();
          if(id) {
              $.ajax({
               //   url: "{{ url('/lelang/hobbydet/') }}/"+idhobby, //'/lelang/detail_hobby/'+provID,
                  url: SITEURL +"/customer/kelurahan/"+id,
                  type: "GET",
                  dataType: "json",
                  success:function(data) {                      
                      $('select[name="idkelurahan"]').empty();
                      // $.each(data, function(key, value) {
                      //     $('select[name="idhobidet"]').append('<option value="'+ key +'">'+ value +'</option>');
                      // });
                      $.each(data.kel, function(key, value) {
                      $('#idkelurahan').append('<option value="'+ value.id+'">'+ value.name +'</option>');
                      });
                  }
              });
          }else{
              $('select[name="idkelurahan"]').empty();
          }
      });

});



function loadKota(id){
  $.ajax({
               //   url: "{{ url('/lelang/hobbydet/') }}/"+idhobby, //'/lelang/detail_hobby/'+provID,
                  url: SITEURL +"/customer/kota/"+id,
                  type: "GET",
                  dataType: "json",
                  success:function(data) {                      
                      $('select[name="idkota"]').empty();
                      // $.each(data, function(key, value) {
                      //     $('select[name="idhobidet"]').append('<option value="'+ key +'">'+ value +'</option>');
                      // });
                      $.each(data.kota, function(key, value) {
                      $('#idkota').append('<option value="'+ value.id+'" {{ isset($cust->idkota) && $cust->idkota == "'+value.id+'" ? "selected=selected" : '' }} >'+ value.name +'</option>');
                        
                      });

                      loadKec({{$cust->idkota}});
                  }
              });
}

function loadKec(id){
  $.ajax({
               //   url: "{{ url('/lelang/hobbydet/') }}/"+idhobby, //'/lelang/detail_hobby/'+provID,
                  url: SITEURL +"/customer/kecamatan/"+id,
                  type: "GET",
                  dataType: "json",
                  success:function(data) {                      
                      $('select[name="idkecamatan"]').empty();
                      // $.each(data, function(key, value) {
                      //     $('select[name="idhobidet"]').append('<option value="'+ key +'">'+ value +'</option>');
                      // });
                      $.each(data.kec, function(key, value) {
                      $('#idkecamatan').append('<option value="'+ value.id+'" {{ isset($cust->idkecamatan) && $cust->idkecamatan == "'+value.id+'" ? "selected=selected" : '' }}>'+ value.name +'</option>');
                      });
                      loadKel({{$cust->idkecamatan}})
                  }
              });

 
}

function loadKel(id){
  $.ajax({
               //   url: "{{ url('/lelang/hobbydet/') }}/"+idhobby, //'/lelang/detail_hobby/'+provID,
                  url: SITEURL +"/customer/kelurahan/"+id,
                  type: "GET",
                  dataType: "json",
                  success:function(data) {                      
                      $('select[name="idkelurahan"]').empty();
                      // $.each(data, function(key, value) {
                      //     $('select[name="idhobidet"]').append('<option value="'+ key +'">'+ value +'</option>');
                      // });
                      $.each(data.kel, function(key, value) {
                      $('#idkelurahan').append('<option value="'+ value.id+'" {{ isset($cust->idkelurahan) && $cust->idkelurahan == "'+value.id+'" ? "selected=selected" : '' }}>'+ value.name +'</option>');
                      });
                  }
              });

 
}

</script>

</body>
</html>