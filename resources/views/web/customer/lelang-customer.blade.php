<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta -->
<meta name="csrf-token" content="{{ csrf_token() }}">
@include('web.headweb')
</head>
<body class="cnt-home">
<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1"> 
  
  <!-- ============================================== TOP MENU ============================================== -->
  <div class="top-bar animate-dropdown">
    <div class="container">
      <div class="header-top-inner">
        <div class="cnt-account">
          <ul class="list-unstyled">
            <!-- <li><a href="#"><i class="icon fa fa-user"></i>My Account</a></li> -->
            
              @if(!Auth::guard('customer')->check()) 
              <li><a href="{{ url('login/customer') }}"><i class="icon fa fa-lock"></i>Login</a></li>
              <li><a href="{{ url('register/customer') }}"><i class="icon fa fa-user"></i>Daftar Member</a></li>
              @endif      
              @if(Auth::guard('customer')->check())  
              <li><a href="{{ url('home/customer') }}"><i class="icon fa fa-home"></i>Dashboard</a></li>
                <li><a href="#"><i class="icon fa fa-user"></i> 
                  Selamat Datang {{ Auth::guard('customer')->user()->name  }}
                    </li> </a>
                    <li><a href="{{ url('logout/customer') }}"><i class="icon fa fa-lock"></i> 
                     Logout
                    </a></li> 
              @endif      
              
          </ul>
        </div>
        <!-- /.cnt-account -->
        
        <!-- /.cnt-cart -->
        <div class="clearfix"></div>
      </div>
      <!-- /.header-top-inner --> 
    </div>
    <!-- /.container --> 
  </div>
  <!-- /.header-top --> 
  <!-- ============================================== TOP MENU : END ============================================== -->
  <div class="main-header">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 logo-holder"> 
          <!-- ============================================================= LOGO ============================================================= -->
          <div class="logo"> <a href="{{ url('/') }}"> <img src="{{asset('images/logo.png')}}" alt="logo"> </a> </div>
          <!-- /.logo --> 
          <!-- ============================================================= LOGO : END ============================================================= --> </div>
        <!-- /.logo-holder -->
        
        <div class="col-xs-12 col-sm-12 col-md-7 top-search-holder"> 
          <!-- /.contact-row --> 
          <!-- ============================================================= SEARCH AREA ============================================================= -->
          @include('web.searchbar')
          <!-- /.search-area --> 
          <!-- ============================================================= SEARCH AREA : END ============================================================= --> </div>
        <!-- /.top-search-holder -->
        
        @include('web.cart')
      <!-- /.row --> 
      
    </div>
    <!-- /.container --> 
    
  </div>
  <!-- /.main-header --> 
  
  <!-- ============================================== NAVBAR ============================================== -->
  @include('web.menuatas')
  <!-- /.header-nav --> 
  <!-- ============================================== NAVBAR : END ============================================== --> 
  
</header>

<!-- ============================================== HEADER : END ============================================== -->
<div id="ext-time">
<div class="body-content outer-top-xs" id="top-banner-and-menu">
  <div class="container">
    <div class="row"> 
      <!-- ============================================== SIDEBAR ============================================== -->
      <div class="col-xs-12 col-sm-12 col-md-3 sidebar"> 
        
        <!-- ================================== TOP NAVIGATION ================================== -->
        <div class="sidebar-module-container">
          <div class="sidebar-filter"> 
            <!-- ============================================== SIDEBAR CATEGORY ============================================== -->
                @include('web.sidebarcust')            
            </div>
        
          </div>
          <!-- /.sidebar-filter --> 
        </div>
        <!-- /.side-menu --> 
        <!-- ================================== TOP NAVIGATION : END ================================== --> 
      </div>
      <!-- /.sidemenu-holder --> 
      <!-- ============================================== SIDEBAR : END ============================================== --> 
      
      <!-- ============================================== CONTENT ============================================== -->
      <div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder"> 
      
        
        <!-- ============================================== BLOG SLIDER ============================================== -->
        <section class="section latest-blog outer-bottom-vs wow fadeInUp">
          <h3 class="section-title">My Lelang</h3>
          <br>
        <div id="container_list_lelang">  
          <div class="row">
            <div class="col-lg-11 margin-tb">
                <div class="pull-right">
                    <a class="btn-sm btn-primary" href="{{ url('customer/formlelang') }}"> Buat Lelang</a>
                </div>
            </div>
            </div>
          <div class="table-responsive">
          @if ($message = Session::get('success'))
              <div class="alert alert-success">
                  <p>{{ $message }}</p>
              </div>
          @endif
            <table class="table table-striped">
                <thead>
                    <tr>
                    <td><strong>No</strong></td>
                    <td><strong>Item</strong></td>
                    <td><strong>Buka Harga</strong></td>
                 
                    <td><strong>Jml Bid</strong></td>
                    <td><strong>End Date</strong></td>
                    <td><strong>Pemenang</strong></td>
                    <td><strong>Action</strong></td>
                    </tr>
                </thead>
                <tbody>
                @foreach ($lelangs as $lelang)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $lelang->nmitem }}</td>
                    <td>{{ number_format($lelang->hrgori,0,',','.') }}</td>
                   
                    <td>{{ $bid::where("idlelang", $lelang->id)->count() }}</td>
                    <td>{{ $lelang->tgl_bts_penawaran }}</td>
                    <td>
                    @if(!empty($lelang->pemenang_bid))
                    {{ $user::where("id",$lelang->pemenang_bid)->first()->name }}
                    @else 
                      Belum Ada
                    @endif  
                    </td>
                    <td>
                    <button type="button" v-on:click="idlelang = {{$lelang->id}}" class="btn btn-warning btn-xs modal_add_time" data-tglclose="{{$lelang->tgl_bts_penawaran}}" data-id="{{$lelang->id}}"><span class="icon fa fa-clock-o"> Add Time</span></button><br>
                   <br>
                    <button type="button" v-on:click="idlelang = {{$lelang->id}}" class="btn btn-info btn-xs modal_update_lelang" data-lokasi="{{$lelang->lokasi_barang}}" data-hrgori="{{$lelang->hrgori}}" data-deskripsi="{{$lelang->deskripsi}}" data-nmitem="{{$lelang->nmitem}}" data-id="{{$lelang->id}}"><span class="icon fa fa-pencil"> Edit</span></button>
                        <form action="{{ route('lelang.destroy',$lelang->id) }}" method="POST">
          
                          
                            <!-- <a class="btn btn-primary" href="{{ route('lelang.edit',$lelang->id) }}">Edit</a> -->
          
                            @csrf
                          
              
                            
                            <!-- <button type="submit" class="btn btn-danger btn-xs">Delete</button> -->
                           
                        </form>
                    </td>
                </tr>
                @endforeach
                 
                </tbody>
                </table>
                {!! $lelangs->links() !!}
             </div>
            <!-- /.owl-carousel --> 
         
          <!-- /.blog-slider-container --> 
         </div> 
        </section>
        <!-- /.section --> 
        <!-- ============================================== BLOG SLIDER : END ============================================== --> 
        
        
        
      </div>
      <!-- /.homebanner-holder --> 
      <!-- ============================================== CONTENT : END ============================================== --> 
    </div>

<div class="modal fade" id="m_addtime" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
   
      <form @submit="formSubmit">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Set Tambahan Waktu</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      <label>End Date</label>
                 <div class="input-group">
                   <input type="text" autocomplete="off" id="tglclose" name="tglclose" class="form-control">
                 </div>
     <br>
      <label>Waktu</label>
                 <div class="input-group">
                 <select id="exttime" v-model="exttime" name="exttime" class="form-control">
                        
                        @foreach($exttimes as $item)
                        <option value="{{ $item->waktu }}" {{ isset($selected_exttime) && $selected_exttime->waktu == $item->waktu ? 'selected="selected"' : '' }}>{{$item->waktu}} Menit</option>
                      
                        @endforeach
                     
                    </select>
                 </div>
                 <input type="text" v-model="idlelang" name="idlelang" id="idlelang"/>
               
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
      </form>
 
    </div>
  </div>
</div>


<div class="modal fade" id="m_editlelang" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
   
      <form action="{{ route('lelang.update-lelang') }}" method="POST" enctype="multipart/form-data">
          @csrf
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit data lelang</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body mx-3">
            
               <div class="md-form mb-5">
               <label>Nama Item</label>
                   <input type="text" autocomplete="off" id="nmitem" name="nmitem" class="form-control">
                 </div>

                 <div class="md-form mb-5">
                 <label>Deskripsi</label>
                    <textarea class="form-control" id="deskripsi" name="deskripsi" rows="3"></textarea>
                 </div>
             
               <div class="md-form mb-5">
               <label>Buka Harga</label>
                   <input type="text" autocomplete="off" id="hrgori" name="hrgori" class="form-control">
                 </div>
                 
                 <div class="md-form mb-5">
               <label>Lokasi Barang</label>
                   <input type="text" autocomplete="off" id="lokasi" name="lokasi" class="form-control">
                   <input type="text" autocomplete="off" id="idlelang_edit" name="idlelang_edit" class="form-control">
                 </div>

               

     <br>
     
               
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
      </form>
 
    </div>
  </div>
</div>
    <!-- /.row --> 
    <!-- ============================================== BRANDS CAROUSEL ============================================== -->
    <div id="brands-carousel" class="logo-slider wow fadeInUp">
      <div class="logo-slider-inner">
        <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
          <div class="item m-t-15"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item m-t-10"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand3.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand6.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item--> 
        </div>
        <!-- /.owl-carousel #logo-slider --> 
      </div>
      <!-- /.logo-slider-inner --> 
      
    </div>
    <!-- /.logo-slider --> 
    <!-- ============================================== BRANDS CAROUSEL : END ============================================== --> 
  </div>
  <!-- /.container --> 
</div>

</div>
<!-- /#top-banner-and-menu --> 

<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="footer color-bg">
@include('web.footerweb')
</footer>
<!-- ============================================================= FOOTER : END============================================================= --> 

<!-- For demo purposes – can be removed on production --> 

<!-- For demo purposes – can be removed on production : End --> 

<!-- JavaScripts placed at the end of the document so the pages load faster -->
<script src="{{asset('js/app.js')}}"></script> 
<script src="{{asset('js/jquery-1.11.1.min.js')}}"></script> 
<script src="{{asset('js/bootstrap.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-hover-dropdown.min.js')}}"></script> 
<script src="{{asset('js/owl.carousel.min.js')}}"></script> 
<script src="{{asset('js/echo.min.js')}}"></script> 
<script src="{{asset('js/jquery.easing-1.3.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-slider.min.js')}}"></script> 
<script src="{{asset('js/jquery.rateit.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/lightbox.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-select.min.js')}}"></script> 
<script src="{{asset('js/wow.min.js')}}"></script> 
<script src="{{asset('js/scripts.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.id.js')}}"></script>


<script type="text/javascript">
var id = '';
  Vue.component('my-comp', {
    template: '<div></div>',
    created: function () {
      id = this.$attrs['my-attr']; // And here is - in $attrs object
      //alert(this.$attrs['my-attr'][1]);
    }
  })

   var extTime = new Vue({
  el: '#ext-time',
  data: {
    message: '',
    idlelang: '',
    exttime: '',

  },
  methods: {
    formSubmit(e) {     
                e.preventDefault();
                let currentObj = this;
                axios.post("{{ url('/lelang/update-exttime') }}", {
                    idlelang: this.idlelang,
                    exttime:  this.exttime,
                   
                })
                .then(function (response) {
                    currentObj.output = response.data;
                   // location.reload();
                    alert('berhasil');
                    location.reload();
                })
                .catch(function (error) {
                    currentObj.output = error;
                });
    }
  }  
}); 


</script>

<script>
$(document).ready( function () {
    
     
$(".modal_add_time").click(function () {
    var ids = $(this).attr('data-id');
    var tglclose = $(this).attr('data-tglclose');
    $("#tglclose").val(tglclose);
   // $("#idlelang").val(ids);
    $("#idlelang").hide();
    $('#m_addtime').modal('show');
});

$(".modal_update_lelang").click(function () {
    var ids = $(this).attr('data-id');
    var nmitem = $(this).attr('data-nmitem');
    var deskripsi = $(this).attr('data-deskripsi');
    var hrgori = $(this).attr('data-hrgori');
    var lokasi = $(this).attr('data-lokasi');
    $("#nmitem").val(nmitem);
    $("#deskripsi").val(deskripsi);
    $("#hrgori").val(hrgori);
    $("#lokasi").val(lokasi);
    $("#idlelang_edit").val(ids);
    $("#idlelang_edit").hide();
    $('#m_editlelang').modal('show');
});
    

    $('#add_time').datetimepicker({
    //  language: 'id',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1
    
    })
});


</script>

</body>
</html>