<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta -->
<meta name="csrf-token" content="{{ csrf_token() }}">
@include('web.headweb')
</head>
<body class="cnt-home">
<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1"> 
  
  <!-- ============================================== TOP MENU ============================================== -->
  <div class="top-bar animate-dropdown">
    <div class="container">
      <div class="header-top-inner">
        <div class="cnt-account">
          <ul class="list-unstyled">
            <!-- <li><a href="#"><i class="icon fa fa-user"></i>My Account</a></li> -->
            
              @if(!Auth::guard('customer')->check()) 
              <li><a href="{{ url('login/customer') }}"><i class="icon fa fa-lock"></i>Login</a></li>
              <li><a href="{{ url('register/customer') }}"><i class="icon fa fa-user"></i>Daftar Member</a></li>
               
              @endif      
              @if(Auth::guard('customer')->check())  
              <li><a href="{{ url('home/customer') }}"><i class="icon fa fa-home"></i>Dashboard</a></li>
                <li><a href="#"><i class="icon fa fa-user"></i> 
                  Selamat Datang {{ Auth::guard('customer')->user()->name  }}
                    </li> </a>
                    <li><a href="{{ url('logout/customer') }}"><i class="icon fa fa-lock"></i> 
                     Logout
                    </a></li> 
                    
              @endif      
              
          </ul>
        </div>
        <!-- /.cnt-account -->
        
        <!-- /.cnt-cart -->
        <div class="clearfix"></div>
      </div>
      <!-- /.header-top-inner --> 
    </div>
    <!-- /.container --> 
  </div>
  <!-- /.header-top --> 
  <!-- ============================================== TOP MENU : END ============================================== -->
  <div class="main-header">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 logo-holder"> 
          <!-- ============================================================= LOGO ============================================================= -->
          <div class="logo"> <a href="{{ url('/') }}"> <img src="{{asset('images/logo.png')}}" alt="logo"> </a> </div>
          <!-- /.logo --> 
          <!-- ============================================================= LOGO : END ============================================================= --> </div>
        <!-- /.logo-holder -->
        
        <div class="col-xs-12 col-sm-12 col-md-7 top-search-holder"> 
          <!-- /.contact-row --> 
          <!-- ============================================================= SEARCH AREA ============================================================= -->
          @include('web.searchbar')
          <!-- /.search-area --> 
          <!-- ============================================================= SEARCH AREA : END ============================================================= --> </div>
        <!-- /.top-search-holder -->
        
        @include('web.cart')
      <!-- /.row --> 
      
    </div>
    <!-- /.container --> 
    
  </div>
  <!-- /.main-header --> 
  
  <!-- ============================================== NAVBAR ============================================== -->
  @include('web.menuatas')
  <!-- /.header-nav --> 
  <!-- ============================================== NAVBAR : END ============================================== --> 
  
</header>

 
<!-- ============================================== HEADER : END ============================================== -->
<div class="body-content outer-top-xs">
  
   
    <div class='container'>
        <div class='row single-product'>
            <div class='col-md-4 sidebar'>
                <div class="sidebar-module-container">
    
  
    
    
        <!-- ============================================== HOT DEALS ============================================== -->
 <div class="sidebar-widget hot-deals wow fadeInUp outer-top-vs">
    <h3 class="section-title"><center>List Biders</center></h3>
    <div class="owl-carousel sidebar-carousel custom-carousel owl-theme outer-top-xs">
            <table border="1">
             
                <tr style="font-size: 11px;">
                
                  
                  <td width="12px"><center>No</center></td>
                  <td width="130px"><center>Nama</center></td>
                  <td width="110px"><center>Tgl. Bid</center></td>
                  <td width="80px"><center>Nilai Bid</center></td>
                </tr>
                
                @foreach ($biders as $bider)
                  @if($lelang->pemenang_bid != '' && $lelang->hrgawal == $bider->harga)
                <tr style="font-size: 10px;" bgcolor="#00FF00">
                   
                   
                    <td>&nbsp;{{ ++$i }}</td>
                    <td>&nbsp;{{ $bider->nmcustomer }}</td>
                    <td>&nbsp;{{ $bider->created_at }}</td>
                    <td>&nbsp;Rp. {{ number_format($bider->harga,0,',','.') }}</td>
                </tr>
                  @else
                  <tr style="font-size: 10px;">
                   
                   
                   <td>&nbsp;{{ ++$i }}</td>
                   <td>&nbsp;{{ $bider->nmcustomer }}</td>
                   <td>&nbsp;{{ $bider->created_at }}</td>
                   <td>&nbsp;Rp. {{ number_format($bider->harga,0,',','.') }}</td>
                  </tr>
                  @endif
                @endforeach    
            
            </table>
            
              
        
    </div><!-- /.sidebar-widget -->
</div>
<br>

<div class="home-banner outer-top-n">
</div>
<!-- ============================================== HOT DEALS: END ============================================== -->
    </div>
</div><!-- /.sidebar -->


<!-- ============================================== DETAIl PRODUCT ============================================== -->
<div class='col-md-8'>

   

<div class="detail-block">
   <!-- @if ($message = Session::get('success'))
              <div class="alert alert-success">
                  <p>{{ $message }}</p>
              </div>
          @endif -->
    <div class="row  wow fadeInUp">
                
<div class="col-xs-12 col-sm-6 col-md-5 gallery-holder">
    <div class="product-item-holder size-big single-product-gallery small-gallery">

        <div id="owl-single-product">
            <div class="single-product-gallery-item" id="slide1">
                <a data-lightbox="image-1" data-title="{{$lelang->nmitem}}" href="{{asset('uploads/item_lelang/'.$lelang->photo_item)}}">
                    <img class="img-responsive" alt="" src="{{asset('images/blank.gif')}}" data-echo="{{asset('uploads/item_lelang/'.$lelang->photo_item)}}" style="width:100%;"/>
                </a>
            </div><!-- /.single-product-gallery-item -->
            <div class="single-product-gallery-item" id="slide2">
                <a data-lightbox="image-2" data-title="{{$lelang->nmitem}}" href="{{asset('uploads/item_lelang/'.$lelang->photo_item2)}}">
                    <img class="img-responsive" alt="" src="{{asset('images/blank.gif')}}" data-echo="{{asset('uploads/item_lelang/'.$lelang->photo_item2)}}" style="width:100%;"/>
                </a>
            </div><!-- /.single-product-gallery-item -->
            <div class="single-product-gallery-item" id="slide3">
                <a data-lightbox="image-3" data-title="{{$lelang->nmitem}}" href="{{asset('uploads/item_lelang/'.$lelang->photo_item3)}}">
                    <img class="img-responsive" alt="" src="{{asset('images/blank.gif')}}" data-echo="{{asset('uploads/item_lelang/'.$lelang->photo_item3)}}" style="width:100%;"/>
                </a>
            </div><!-- /.single-product-gallery-item -->
            <div class="single-product-gallery-item" id="slide4">
                <a data-lightbox="image-4" data-title="{{$lelang->nmitem}}" href="{{asset('uploads/item_lelang/'.$lelang->photo_item4)}}">
                    <img class="img-responsive" alt="" src="{{asset('images/blank.gif')}}" data-echo="{{asset('uploads/item_lelang/'.$lelang->photo_item4)}}" style="width:100%;"/>
                </a>
            </div><!-- /.single-product-gallery-item -->
            <div class="single-product-gallery-item" id="slide5">
                <a data-lightbox="image-5" data-title="{{$lelang->nmitem}}" href="{{asset('uploads/item_lelang/'.$lelang->photo_item5)}}">
                    <img class="img-responsive" alt="" src="{{asset('images/blank.gif')}}" data-echo="{{asset('uploads/item_lelang/'.$lelang->photo_item5)}}" style="width:100%;"/>
                </a>
            </div><!-- /.single-product-gallery-item -->

            

        </div><!-- /.single-product-slider -->


        <div class="single-product-gallery-thumbs gallery-thumbs">

            <div id="owl-single-product-thumbnails">
            @if(!empty($lelang->photo_item))
                <div class="item">
                    <a class="horizontal-thumb active" data-target="#owl-single-product" data-slide="1" href="#slide1">
                        <img class="img-responsive" width="85" alt="" src="{{asset('uploads/item_lelang/'.$lelang->photo_item)}}" data-echo="{{asset('uploads/item_lelang/'.$lelang->photo_item)}}" />
                    </a>
                </div>
                @endif
                @if(!empty($lelang->photo_item2))
                <div class="item">
                    <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="2" href="#slide2">
                        <img class="img-responsive" width="85" alt="" src="{{asset('uploads/item_lelang/'.$lelang->photo_item2)}}" data-echo="{{asset('uploads/item_lelang/'.$lelang->photo_item2)}}" />
                    </a>
                </div>
                @endif
                @if(!empty($lelang->photo_item3))
                <div class="item">
                    <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="3" href="#slide3">
                        <img class="img-responsive" width="85" alt="" src="{{asset('uploads/item_lelang/'.$lelang->photo_item3)}}" data-echo="{{asset('uploads/item_lelang/'.$lelang->photo_item3)}}" />
                    </a>
                </div>
                @endif
                @if(!empty($lelang->photo_item4))
                <div class="item">
                    <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="4" href="#slide4">
                        <img class="img-responsive" width="85" alt="" src="{{asset('uploads/item_lelang/'.$lelang->photo_item4)}}" data-echo="{{asset('uploads/item_lelang/'.$lelang->photo_item4)}}" />
                    </a>
                </div>
                @endif
                @if(!empty($lelang->photo_item5))
                <div class="item">
                    <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="5" href="#slide5">
                        <img class="img-responsive" width="85" alt="" src="{{asset('uploads/item_lelang/'.$lelang->photo_item5)}}" data-echo="{{asset('uploads/item_lelang/'.$lelang->photo_item5)}}" />
                    </a>
                </div>
                @endif

                
            </div><!-- /#owl-single-product-thumbnails -->

            

        </div><!-- /.gallery-thumbs -->

    </div><!-- /.single-product-gallery -->



</div><!-- /.gallery-holder --> 
 <div id="app-5">
      <form @submit="formSubmit">   
    <!--   <form action="{{ route('lelang.makebid') }}" method="POST" enctype="multipart/form-data">   --> 
    <!--    @csrf       -->       
                 <div class='col-sm-6 col-md-7 product-info-block'>
                        <div class="product-info">
                            <h3 class="name">{{ $lelang->nmitem }}</h3>
                            
                            <div class="description-container m-t-20">
                                <h4 class="section-title">ID : #{{ $lelang->id }}</h4>
                                 <h4 style="font-size9px" class="section-title">Bids : {{ $bid::where("idlelang", $lelang->id)->count() }}</h4>
                                 
                                 <h4 class="section-title">Kondisi : {{ $lelang->kondisi_item }}</h4>
                                 <h4 class="section-title">Lokasi : {{ $lelang->lokasi_barang }}</h4>
                                 <h4 class="section-title">Posted : {{ $lelang->created_at }}</h4>
                                 <h4 class="section-title">End Date/Berakhir Lelang : {{ $lelang->tgl_bts_penawaran }}</h4>
                                 <h4 class="section-title">Extension Date/Waktu Tambahan : <span>{{($lelang->wkt_tambahan)?$lelang->wkt_tambahan:0}} menit</span></h4>
                                 <h4 class="section-title">Kelipatan Bid : Rp. {{ number_format($lelang->kelipatan,0,',','.') }}</h4>
                                 <h4 class="section-title">Buka Harga : Rp. {{ number_format($lelang->hrgori,0,',','.') }}</h4>
                                 <h4 class="section-title">Potition Last Bid : <span style="color: #ff7878">Rp. {{ number_format($lelang->hrgawal,0,',','.') }}</span></h4>
                                 <h4 class="section-title">Status : <span>{{ $stlelang->name }}</span></h4>
                                 <h4 class="section-title">Time Limit : <span style="color: #ff7878" data-countdown="{{ $lelang->tgl_bts_penawaran }}"></span></h4>
                                 
                               
                            </div><!-- /.description-container -->

                            <!--center>
                                <br><button class="btn checkout-btn">Proses lelang telah selesai</button>
                            </center --> 
                            @if(!Auth::guard('customer')->check()) 
                                <center>
                                    <br><button id="btn_make_bid" class="btn btn-danger">Make Bid</button>
                                </center> 
                            @endif
                            @if(Auth::guard('customer')->check())   
                               
                            <!--     <input class="form-control" v-model.lazy="hrg_bid" v-money="money" @input="checkBid" id="harga" name="harga" placeholder="Masukan Harga Penawaran Anda">    
                             -->    <!--   <money v-model="hrg_bid"
                             v-bind="money"
                             class="form-control" ></money>  -->
                            
                            @if($lelang->idstlelang != 3) 
                              @if($lelang->userid !== Auth::guard('customer')->user()->id)
                             <div class="quantity-container info-container">
                                <div class="row">
                                  
                                <div class="col-sm-2">
                                    <a v-on:click="kurangBid" class="btn btn-primary btn_min"><i class="fa fa-minus-square inner-right-vs"></i></a>
                                  </div>
                                  
                                  <div class="col-sm-4">
                                  <input class="form-control tnilaibid" readOnly="" v-model="hrg_bid" id="harga" name="harga" placeholder="Harga Bid">    
                                  </div>

                                  <div class="col-sm-2">
                                    <a  v-on:click="hrg_bid += {{$lelang->kelipatan}}" class="btn btn-primary btn_plus"><i class="fa fa-plus-square inner-right-vs"></i></a>
                                  </div>

                                  
                                </div><!-- /.row -->
                              </div><!-- /.quantity-container -->
                                  
                                      <h5>@{{ message }}</h5>

                                      <h5>@{{ output }}</h5>
                                     
                                    <button type="submit" v-if="seen" class="btn btn-danger btn_make_bid">Make Bid</button>
                              @endif      
                            @endif
                            @if($lelang->idstlelang == 3)
                                  <h4>Proses Lelang Selesai</h4>
                            @endif
                                  

                               

                               

                      
                            @endif
                            
                            
                            
                        </div><!-- /.product-info -->
                    </div><!-- /.col-sm-7 -->
                </div><!-- /.row -->
             </div>

      </form>          
    </div>        
                
                <div class="product-tabs inner-bottom-xs  wow fadeInUp">
                    <div class="row">
                        <div class="col-sm-3">
                            <ul id="product-tabs" class="nav nav-tabs nav-tab-cell">
                                <li class="active"><a data-toggle="tab" href="#description">DESCRIPTION</a></li>
                                 <li><a data-toggle="tab" href="#auctioneer">AUCTIONEER</a></li>
                                <!-- <li><a data-toggle="tab" href="#review">REVIEW</a></li> -->
                                <!-- <li><a data-toggle="tab" href="#auctioneer">COMMENT</a></li> -->
                            </ul><!-- /.nav-tabs #product-tabs -->
                        </div>
                        <div class="col-sm-9">

                            <div class="tab-content">
                                
                                <div id="description" class="tab-pane in active">
                                    <div class="product-tab">
                                        <p class="text">{{ $lelang->deskripsi }}</p>
                                    </div>  
                                    @if($lelang->video1)
                                    <video height="300px" width="400px" controls>
                                        <source src="{{asset('uploads/item_lelang/'.$lelang->video1)}}" type="video/ogg">
                                          Your browser does not support the video tag.
                                  </video>
                                  @endif
                                  @if($lelang->video2)
                                    <video height="300px" width="400px" controls>
                                        <source src="{{asset('uploads/item_lelang/'.$lelang->video2)}}" type="video/ogg">
                                          Your browser does not support the video tag.
                                  </video>
                                  @endif
                                  @if($lelang->video3)
                                    <video height="300px" width="400px" controls>
                                        <source src="{{asset('uploads/item_lelang/'.$lelang->video3)}}" type="video/ogg">
                                          Your browser does not support the video tag.
                                  </video>
                                  @endif
                                  @if($lelang->video4)
                                    <video height="300px" width="400px" controls>
                                        <source src="{{asset('uploads/item_lelang/'.$lelang->video4)}}" type="video/ogg">
                                          Your browser does not support the video tag.
                                  </video>
                                  @endif
                                  @if($lelang->video5)
                                    <video height="300px" width="400px" controls>
                                        <source src="{{asset('uploads/item_lelang/'.$lelang->video5)}}" type="video/ogg">
                                          Your browser does not support the video tag.
                                  </video>
                                  @endif
                                </div><!-- /.tab-pane -->

                                <div id="review" class="tab-pane">
                                    <div class="product-tab">
                                                                                
                                        <div class="product-reviews">
                                            <h4 class="title">Customer Reviews</h4>

                                            <div class="reviews">
                                                <div class="review">
                                                    <div class="review-title"><span class="summary">We love this product</span><span class="date"><i class="fa fa-calendar"></i><span>1 days ago</span></span></div>
                                                    <div class="text">"Lorem ipsum dolor sit amet, consectetur adipiscing elit.Aliquam suscipit."</div>
                                                                                                        </div>
                                            
                                            </div><!-- /.reviews -->
                                        </div><!-- /.product-reviews -->
                                        

                                        
                                        <div class="product-add-review">
                                            <h4 class="title">Write your own review</h4>
                                            <div class="review-table">
                                                <div class="table-responsive">
                                                    <table class="table">   
                                                        <thead>
                                                            <tr>
                                                                <th class="cell-label">&nbsp;</th>
                                                                <th>1 star</th>
                                                                <th>2 stars</th>
                                                                <th>3 stars</th>
                                                                <th>4 stars</th>
                                                                <th>5 stars</th>
                                                            </tr>
                                                        </thead>    
                                                        <tbody>
                                                            <tr>
                                                                <td class="cell-label">Quality</td>
                                                                <td><input type="radio" name="quality" class="radio" value="1"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="2"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="3"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="4"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="5"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="cell-label">Price</td>
                                                                <td><input type="radio" name="quality" class="radio" value="1"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="2"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="3"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="4"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="5"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="cell-label">Value</td>
                                                                <td><input type="radio" name="quality" class="radio" value="1"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="2"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="3"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="4"></td>
                                                                <td><input type="radio" name="quality" class="radio" value="5"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table><!-- /.table .table-bordered -->
                                                </div><!-- /.table-responsive -->
                                            </div><!-- /.review-table -->
                                            
                                            <div class="review-form">
                                                <div class="form-container">
                                                    <form role="form" class="cnt-form">
                                                        
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="exampleInputName">Your Name <span class="astk">*</span></label>
                                                                    <input type="text" class="form-control txt" id="exampleInputName" placeholder="">
                                                                </div><!-- /.form-group -->
                                                                <div class="form-group">
                                                                    <label for="exampleInputSummary">Summary <span class="astk">*</span></label>
                                                                    <input type="text" class="form-control txt" id="exampleInputSummary" placeholder="">
                                                                </div><!-- /.form-group -->
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="exampleInputReview">Review <span class="astk">*</span></label>
                                                                    <textarea class="form-control txt txt-review" id="exampleInputReview" rows="4" placeholder=""></textarea>
                                                                </div><!-- /.form-group -->
                                                            </div>
                                                        </div><!-- /.row -->
                                                        
                                                        <div class="action text-right">
                                                            <button class="btn btn-primary btn-upper">SUBMIT REVIEW</button>
                                                        </div><!-- /.action -->

                                                    </form><!-- /.cnt-form -->
                                                </div><!-- /.form-container -->
                                            </div><!-- /.review-form -->

                                        </div><!-- /.product-add-review -->                                     
                                        
                                    </div><!-- /.product-tab -->
                                </div><!-- /.tab-pane -->

                                <div id="auctioneer" class="tab-pane">
                                    <div class="product-tag">
                                        
                                        
                                      
                                            <div class="form-container">
                                    
                                                <div class="form-group">
                                                <h4 class="title">{{$customer->name}}</h4>
                                                </div>
                                                <div class="form-group">
                                                    
                                                    <label>Kontak : {{$customer->phone}}</label>
                                                </div>
                                                <div class="form-group">
                                                    <label>Email : {{$customer->email}}</label>
                                                   
                                                </div>

                                               
                                            </div><!-- /.form-container -->
                                      

                                    </div><!-- /.product-tab -->
                                </div><!-- /.tab-pane -->

                            </div><!-- /.tab-content -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.product-tabs -->

               
</div><!-- /.col -->
            <div class="clearfix"></div>
        </div><!-- /.row -->


        </div><!-- /.container -->
</div><!-- /.body-content -->
<!-- /#top-banner-and-menu --> 

<!-- ============================================================= FOOTER ============================================================= -->
<script src="{{asset('js/app.js')}}"></script>
@include('web.footerwebfix')




<script type="text/javascript">

   var app5 = new Vue({
  el: '#app-5',
  data: {
    message: '',
    hrg_max: '{{ $lelang->hrgawal }}',
    kelipatan: '{{$lelang->kelipatan}}',
    hrg_bid: {{$lelang->hrgawal + $lelang->kelipatan}},
    events: [],
    seen: true,
    isIdlelang:false,
    isHrgmax:false,
    isUserid:false,
      money: {
        decimal: ',',
        thousands: '.',
        prefix: '',
        suffix: '',
        precision: 0,
        masked: true
      },
      idlelang: '{{ $lelang->id }}',
      output: '',

  },
  computed: {
   
  },
  methods: {
    formSubmit(e) {
                e.preventDefault();
                let currentObj = this;
                axios.post("{{ url('/lelang/makebid') }}", {
                    harga: this.hrg_bid,
                    idlelang:  this.idlelang,
                   
                })
                .then(function (response) {
                    
//                    currentObj.output = response.data;
                    if(response.data == 1){
                        alert('Bid gagal.. Server sibuk, silahkan reload/refresh lalu ulangi bid Anda');
                      //  currentObj.output = 'Bid gagal.. Server sibuk, silahkan reload/refresh lalu ulangi bid Anda';
                    }else{
                        currentObj.output = response.data;
                    
                        location.reload();
                    }
                 
                   
                    //alert('Bid berhasil');
                    
                })
                .catch(function (error) {
                    currentObj.output = error;
                });
    },

    checkBid: function(e){
       if(Number(this.hrg_bid) <= Number(this.hrg_max)){
         this.message = 'Harga bid harus lebih dari harga minimum';
       //  this.hrg_bid = '';
         this.seen = false;
      }
      if(Number(this.hrg_bid) > Number(this.hrg_max)){
        this.message = '';
        this.seen = true
      } 

      e.preventDefault(); 
    },
    kurangBid: function(e) {
    
    

       if(this.hrg_bid > (Number(this.hrg_max) + Number(this.kelipatan))){
        this.hrg_bid -= Number(this.kelipatan); 
       } else{
         alert('Nilai bid harus lebih besar dari nilai bid terakhir');
       }

       e.preventDefault(); 
    }
      
  }  
}); 



var SITEURL = '{{URL::to('')}}';
  $(document).ready(function(){
    
      $('[data-countdown]').each(function() {
        var $this = $(this), finalDate = $(this).data('countdown');
        $this.countdown(finalDate, function(event) {
          if (event.elapsed) {
            $this.html(event.strftime('Waktu Habis'));
            //  $('.btn_make_bid').hide(); 
            $(".btn_make_bid").hide();
            $(".btn_min").hide();
            $(".btn_plus").hide();
            $(".tnilaibid").hide();
          }else{
            $this.html(event.strftime('%D Hari %H:%M:%S'));
          }
         
        });
      });
     
      $('#btn_make_bid').click(function () {
          alert('Anda Harus Login / Register Dulu Sebelum Bid');
      });
  });  

 
</script>    


</body>


</html>


