<div class="col-xs-12 col-sm-12 col-md-2 animate-dropdown top-cart-row">
    <!-- ============================================================= SHOPPING CART DROPDOWN ============================================================= -->
    @if(!Auth::guard('customer')->check())
    <div class="dropdown dropdown-cart"> <a href="#" class="dropdown-toggle lnk-cart" data-toggle="dropdown">
            <div class="items-cart-inner">
                <div class="basket"> <i class="glyphicon glyphicon-shopping-cart"></i> </div>
                <div class="basket-item-count"><span class="count">0</span></div>
                <div class="total-price-basket"> <span class="lbl">cart -</span> <span class="total-price"> <span
                            class="sign">Rp. </span><span class="value">0</span> </span> </div>
            </div>
        </a>
        <ul class="dropdown-menu">
            <li>
                <!-- <div class="cart-item product-summary">
                  <div class="row">
                    <div class="col-xs-4">
                      <div class="image"> <a href="detail.html"><img src="{{asset('images/cart.jpg')}}" alt=""></a> </div>
                    </div>
                    <div class="col-xs-7">
                      <h3 class="name"><a href="index.php?page-detail">Simple Product</a></h3>
                      <div class="price">$600.00</div>
                    </div>
                    <div class="col-xs-1 action"> <a href="#"><i class="fa fa-trash"></i></a> </div>
                  </div>
                </div> -->
                <!-- /.cart-item -->
                <div class="clearfix"></div>
                <hr>
                <div class="clearfix cart-total">
                    <div class="pull-right"> <span class="text">Sub Total :</span><span class='price'>0</span> </div>
                    <div class="clearfix"></div>
                    <!-- <a href="checkout.html" class="btn btn-upper btn-primary btn-block m-t-20">Checkout</a> </div> -->
                    <!-- /.cart-total-->

            </li>
        </ul>
        <!-- /.dropdown-menu-->
    </div>
    <!-- /.dropdown-cart -->
    @endif

    @if(Auth::guard('customer')->check())
    <div class="dropdown dropdown-cart"> <a href="#" class="dropdown-toggle lnk-cart" data-toggle="dropdown">
            <div class="items-cart-inner">
                <div class="basket"> <i class="glyphicon glyphicon-shopping-cart"></i> </div>
                <div class="basket-item-count"><span
                        class="count">{{Cart::session(Auth::guard('customer')->user()->id)->getContent()->count()}}</span>
                </div>
                <div class="total-price-basket">
                    <span class="lbl"></span> <span class="total-price"> <span class="sign">Rp. </span><span
                            class="value">{{number_format(Cart::session(Auth::guard('customer')->user()->id)->getTotal(),0,',','.')}}</span>
                    </span> </div>
            </div>
        </a>
        <ul class="dropdown-menu">
            <li>
                <div class="cart-item product-summary">

                    <div class="row">
                        @foreach(Cart::session(Auth::guard('customer')->user()->id)->getContent() as $cart)
                        <div class="col-xs-4">
                            <div class="image"> <img
                                    src="{{asset('uploads/jual_barang/'.DB::table('barang')->where('id', $cart->id)->first()->gambar_satu)}}"
                                    alt=""> </div>
                        </div>
                        <div class="col-xs-7">
                            <h3 class="name">{{$cart->name}}</h3>
                            <div class="price">{{number_format($cart->price,0,',','.')}}</div>
                            <h3 class="name">Qty : {{$cart->quantity}}</h3>
                            <hr>
                        </div>
                        <div class="col-xs-1 action">
                            <form action="{{ route('jual.remove-cart', $cart->id) }}" method="POST">
                                @csrf
                                <button type="submit" id="btnRemove"><i class="fa fa-trash"></i></button>
                            </form>
                        </div>

                        @endforeach
                    </div>
                </div>

                <!-- /.cart-item -->
                <div class="clearfix"></div>
                <hr>
                <div class="clearfix cart-total">
                    <div class="pull-right"> <span class="text">Sub Total :</span><span class='price'>Rp.
                            {{number_format(Cart::session(Auth::guard('customer')->user()->id)->getTotal(),0,',','.')}}</span>
                    </div>
                    <div class="clearfix"></div>
                    @if(Cart::session(Auth::guard('customer')->user()->id)->getContent()->count() > 0)
                    <a href="{{route('jual.cart.checkout')}}"
                        class="btn btn-upper btn-primary btn-block m-t-20">Checkout</a>
                </div>
                @endif
                <!-- /.cart-total-->

            </li>
        </ul>
        <!-- /.dropdown-menu-->
    </div>
    <!-- /.dropdown-cart -->
    @endif


    <!-- ============================================================= SHOPPING CART DROPDOWN : END============================================================= -->
</div>
<!-- /.top-cart-row -->
</div>

<!--  <script src="{{asset('js/app.js')}}"></script> -->

<script>
    // var vmcart = new Vue({
    //   el: '#vmcart',
    //   data: {},
    //   methods: {
    //     removeCart: function(idbrg) {
    //   //    alert(idbrg);
    //    //   e.preventDefault();
    //       let currentObj = this;
    //       axios.post("{{ url('/jual/remove-cart/') }}", {
    //           id: idbrg     
    //       })
    //       .then(function (response) {
    //           currentObj.output = response.data;

    //       })
    //       .catch(function (error) {
    //           currentObj.output = error;
    //       });
    //     }
    //   }  
    // });

</script>
