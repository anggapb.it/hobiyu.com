<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta -->
<meta name="csrf-token" content="{{ csrf_token() }}">
@include('web.headweb')
</head>


<body class="cnt-home">
<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1"> 
  
  <!-- ============================================== TOP MENU ============================================== -->
  <div class="top-bar animate-dropdown">
    <div class="container">
      <div class="header-top-inner">
        <div class="cnt-account">
          <ul class="list-unstyled">
            <!-- <li><a href="#"><i class="icon fa fa-user"></i>My Account</a></li> -->
            
              @if(!Auth::guard('customer')->check()) 
              <li><a href="{{ url('login/customer') }}"><i class="icon fa fa-lock"></i>Login</a></li>
              <li><a href="{{ url('register/customer') }}"><i class="icon fa fa-user"></i>Daftar Member</a></li>
               
              @endif      
              @if(Auth::guard('customer')->check())  
              <li><a href="{{ url('home/customer') }}"><i class="icon fa fa-home"></i>Dashboard</a></li>
                <li><a href="#"><i class="icon fa fa-user"></i> 
                  Selamat Datang {{ Auth::guard('customer')->user()->name  }}
                    </li> </a>
                    <li><a href="{{ url('logout/customer') }}"><i class="icon fa fa-lock"></i> 
                     Logout
                    </a></li> 
                    
              @endif      
              
          </ul>
        </div>
        <!-- /.cnt-account -->
        
        <!-- /.cnt-cart -->
        <div class="clearfix"></div>
      </div>
      <!-- /.header-top-inner --> 
    </div>
    <!-- /.container --> 
  </div>
  <!-- /.header-top --> 
  <!-- ============================================== TOP MENU : END ============================================== -->
  <div class="main-header">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 logo-holder"> 
          <!-- ============================================================= LOGO ============================================================= -->
          <div class="logo"> <a href="{{ url('/') }}"> <img src="{{asset('images/logo.png')}}" alt="logo"> </a> </div>
          <!-- /.logo --> 
          <!-- ============================================================= LOGO : END ============================================================= --> </div>
        <!-- /.logo-holder -->
        
        <div class="col-xs-12 col-sm-12 col-md-7 top-search-holder"> 
          <!-- /.contact-row --> 
          <!-- ============================================================= SEARCH AREA ============================================================= -->
          @include('web.searchbar')
          <!-- /.search-area --> 
          <!-- ============================================================= SEARCH AREA : END ============================================================= --> </div>
        <!-- /.top-search-holder -->
        
        @include('web.cart')
      <!-- /.row --> 
      
    </div>
    <!-- /.container --> 
    
  </div>
  <!-- /.main-header --> 
  
  <!-- ============================================== NAVBAR ============================================== -->
  @include('web.menuatas')
  <!-- /.header-nav --> 
  <!-- ============================================== NAVBAR : END ============================================== --> 
  
</header>

 
<!-- ============================================== HEADER : END ============================================== -->


<div class="breadcrumb">
    <div class="container">
        <div class="breadcrumb-inner">
            <ul class="list-inline list-unstyled">
                <li><a href="{{url('customer/complaint')}}">Complaint</a></li>
                <li class='active'>Detail</li>
            </ul>
        </div><!-- /.breadcrumb-inner -->
    </div><!-- /.container -->
</div><!-- /.breadcrumb -->
<div class="body-content">
    <div class="container">
        <div class="row">
            <div class="blog-page">
                <div class="col-md-9">
                    <div class="blog-post wow fadeInUp">
                        <h1>{{$complaint->judul}}</h1>
                        <span class="author">{{$complaint->notiket}}</span>
                        <span class="review">{{$count_answer}} Response</span>
                        <span class="date-time">{{$complaint->created_at->diffForHumans()}}</span>
                    </div>

                    <div class="blog-post-author-details wow fadeInUp">
    <div class="row">
      
        <div class="col-md-12">
       
            {{ $complaint->keterangan }}
            
      
        </div>
        <div class="col-md-12">
       
          
            
            @if(isset($complaint->file))
                <a href="{{asset('uploads/komplain/'.$complaint->file)}}"><img class="img-responsive" alt="" src="{{asset('uploads/komplain/'.$complaint->file)}}" style="width:50%;"/></a>
                @endif
          
        </div>
    </div>
</div>
 


<div class="blog-write-comment outer-bottom-xs outer-top-xs">
    
    <div class="row">
    <form action="{{ route('customer.insert-answer-complaint',$complaint->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="col-md-12">
                <div class="form-group">
                <label class="info-title" for="comment"><b>Keterangan</b></label>
                <textarea class="form-control unicase-form-control" id="answer" name="answer" placeholder="Tulis disini ..." style="height:80px"></textarea>
              </div>
        </div>

        <div class="col-md-12 outer-bottom-small m-t-7">
             <button type="submit" id="btn_comment_after_login" class="btn-upper btn btn-primary checkout-page-button">Reply</button>
        </div>
        </form>
    </div>

    @if ($message = Session::get('success'))
              <div class="alert alert-success">
                  <p>{{ $message }}</p>
              </div>
          @endif
    <div class="row">
        <div class="col-md-12">
            <h3 class="title-review-comments">{{$count_answer}} response</h3>
           
        <br>
        </div>
        <div class="col-md-12 blog-comments outer-bottom-xs">
        <div class="blog-comments inner-bottom-xs">
        @foreach($answers as $item)
       
            
                <h4><strong>{{$item->iduser}}</strong></h4>
                <span class="review-action pull-right">
                    {{$item->created_at}}
                </span>
                <p>{{$item->answer}}</p>
                @if(isset($item->file))
                <a href="{{asset('uploads/komplain/'.$item->file)}}"><img class="img-responsive" alt="" src="{{asset('uploads/komplain/'.$item->file)}}" style="width:50%;"/></a>
                @endif
                <hr>
            
           
        
       
        @endforeach
        </div>
        </div>
    </div>
</div>
                </div>

<div class="col-md-3 sidebar">
                               

<div class="home-banner outer-top-n outer-bottom-xs">
</div>
                <!-- ==============================================CATEGORY============================================== -->

<!-- ============================================== PRODUCT TAGS ============================================== -->

@foreach($sponsors as $item)
      <div class="home-banner"> <a href="{{$item->link}}"><img style="width:260px" src="{{asset('uploads/banner/'.$item->banner)}}" alt="Image"> </a> </div>   
      @endforeach
<!-- ============================================== PRODUCT TAGS : END ============================================== -->   
<br>
<br>

                   </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ============================================================= FOOTER ============================================================= -->
<script src="{{asset('js/app.js')}}"></script>
@include('web.footerwebfix')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>    
 
  
</body>
<script type="text/javascript">
$(document).ready(function(){
  $('.summernote').summernote({
                height: 200,   //set editable area's height
                airMode:true
            });
      
      $('#btn_comment').click(function () {
          alert('Anda Harus Login / Register Dulu...');
      }); 
});
</script>

</html>


