<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta -->
<meta name="csrf-token" content="{{ csrf_token() }}">
@include('web.headweb')
</head>
<body class="cnt-home">
<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1"> 
  
  <!-- ============================================== TOP MENU ============================================== -->
  <div class="top-bar animate-dropdown">
    <div class="container">
      <div class="header-top-inner">
        <div class="cnt-account">
          <ul class="list-unstyled">
            <!-- <li><a href="#"><i class="icon fa fa-user"></i>My Account</a></li> -->
            
              @if(!Auth::guard('customer')->check()) 
              <li><a href="{{ url('login/customer') }}"><i class="icon fa fa-lock"></i>Login</a></li>
              <li><a href="{{ url('register/customer') }}"><i class="icon fa fa-user"></i>Daftar Member</a></li>
              @endif      
              @if(Auth::guard('customer')->check())  
              <li><a href="{{ url('home/customer') }}"><i class="icon fa fa-home"></i>Dashboard</a></li>
                <li><a href="#"><i class="icon fa fa-user"></i> 
                  Selamat Datang {{ Auth::guard('customer')->user()->name  }}
                    </li> </a>
                    <li><a href="{{ url('logout/customer') }}"><i class="icon fa fa-lock"></i> 
                     Logout
                    </a></li> 
              @endif      
              
          </ul>
        </div>
        <!-- /.cnt-account -->
        
        <!-- /.cnt-cart -->
        <div class="clearfix"></div>
      </div>
      <!-- /.header-top-inner --> 
    </div>
    <!-- /.container --> 
  </div>
  <!-- /.header-top --> 
  <!-- ============================================== TOP MENU : END ============================================== -->
  <div class="main-header">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 logo-holder"> 
          <!-- ============================================================= LOGO ============================================================= -->
          <div class="logo"> <a href="{{ url('/') }}"> <img src="{{asset('images/logo.png')}}" alt="logo"> </a> </div>
          <!-- /.logo --> 
          <!-- ============================================================= LOGO : END ============================================================= --> </div>
        <!-- /.logo-holder -->
        
        <div class="col-xs-12 col-sm-12 col-md-7 top-search-holder"> 
          <!-- /.contact-row --> 
          <!-- ============================================================= SEARCH AREA ============================================================= -->
          @include('web.searchbar')
          <!-- /.search-area --> 
          <!-- ============================================================= SEARCH AREA : END ============================================================= --> </div>
        <!-- /.top-search-holder -->
        
        @include('web.cart')
      <!-- /.row --> 
      
    </div>
    <!-- /.container --> 
    
  </div>
  <!-- /.main-header --> 
  
  <!-- ============================================== NAVBAR ============================================== -->
  @include('web.menuatas')
  <!-- /.header-nav --> 
  <!-- ============================================== NAVBAR : END ============================================== --> 
  
</header>

<!-- ============================================== HEADER : END ============================================== -->
<div id="ext-time">
<div class="body-content outer-top-xs" id="top-banner-and-menu">
  <div class="container">
    <div class="row"> 
      <!-- ============================================== SIDEBAR ============================================== -->
      <div class="col-xs-12 col-sm-12 col-md-3 sidebar"> 
        
        <!-- ================================== TOP NAVIGATION ================================== -->
        <div class="sidebar-module-container">
          <div class="sidebar-filter"> 
            <!-- ============================================== SIDEBAR CATEGORY ============================================== -->
                @include('web.sidebarcust')            
            </div>
        
          </div>
          <!-- /.sidebar-filter --> 
        </div>
        <!-- /.side-menu --> 
        <!-- ================================== TOP NAVIGATION : END ================================== --> 
      </div>
      <!-- /.sidemenu-holder --> 
      <!-- ============================================== SIDEBAR : END ============================================== --> 
      
      <!-- ============================================== CONTENT ============================================== -->
      <div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder"> 
      
        
        <!-- ============================================== BLOG SLIDER ============================================== -->
        <section class="section latest-blog outer-bottom-vs wow fadeInUp">
          <h3 class="section-title">Complaint</h3>
          <br>
        <div id="container_list_lelang">  
          <div class="row">
            <div class="col-lg-11 margin-tb">
                <div class="pull-right">
                    <button class="btn-sm btn-primary btnAddComplaint"> Buka Tiket</button>
                </div>
            </div>
            </div>
          <div class="table-responsive">
          @if ($message = Session::get('success'))
              <div class="alert alert-success">
                  <p>{{ $message }}</p>
              </div>
          @endif
          

          <div class="search-result-container ">
          <div id="myTabContent" class="tab-content category-list">
       
            <!-- /.tab-pane -->
            
            <div class="tab-pane active"  id="list-container">
              <div class="category-product">
              @if($complaints->isEmpty())
                  <p>Belum ada data...</p>
                  @endif
                  
                  @foreach($complaints as $item)
                <div class="category-product-inner wow fadeInUp">
                  <div class="products">
                    <div class="product-list product">
                      <div class="row product-list-row">
                      <h2 class="name"><a href="{{route('customer.complaint-detail',$item->id)}}">&nbsp;{{$item->judul}}</a></h2>
                        <!-- /.col -->
                        <div class="col col-sm-8 col-lg-12">
                          <div class="product-info">
                            <h5>{{$item->keterangan}}</h5>
                            <br>
                            <!-- /.product-price -->
                            <div class="post m-t-10"><i>No. Tiket : {{$item->notiket}} || Status : {{$item->status}} || Created At : {{$item->created_at}}</i></div>
                            <hr>
                          </div>
                          <!-- /.product-info --> 
                        </div>
                        <!-- /.col --> 
                      </div>
                      <!-- /.product-list-row -->
                    
                     
                    </div>
                    <!-- /.product-list --> 
                  </div>
                  <!-- /.products --> 
                </div>
                @endforeach
                <!-- /.category-product-inner -->
                
               

                
              </div>
              <!-- /.category-product --> 
            </div>
            <!-- /.tab-pane #list-container --> 
          </div>
          <!-- /.tab-content -->
          <div class="clearfix filters-container">
            <div class="text-right">
         
              <!-- /.pagination-container --> </div>
            <!-- /.text-right --> 
            
          </div>
          <!-- /.filters-container --> 
          
        </div>



         </div> 
        </section>
        <!-- /.section --> 
        <!-- ============================================== BLOG SLIDER : END ============================================== --> 
        
        
        
      </div>
      <!-- /.homebanner-holder --> 
      <!-- ============================================== CONTENT : END ============================================== --> 
    </div>



<div class="modal fade" id="mdlComplaint" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
   
      <form action="{{ route('customer.insert-complaint') }}" method="POST" enctype="multipart/form-data">
          @csrf
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Form Complaint</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
      <!-- Modal body -->
      <div class="form-row">
                    <div class="form-group col-md-12">
                    <label>Judul</label><span style="color:red">*</span>
                    <input type="text" class="form-control" id="judul" name="judul" placeholder="">
                    </div>
                   
                </div>
      <div class="form-row">
                    <div class="form-group col-md-12">
                    <label>Complaint</label><span style="color:red">*</span>
                    <textarea class="form-control" id="keterangan" name="keterangan" rows="5"></textarea>
                    </div>
                   
                </div>
      <div class="form-row">
                    <div class="form-group col-md-12">
                    <label>File Pendukung</label>
                    <input type="file" class="form-control-file" id="file" name="file">
                    </div>
                   
                </div>
               


      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
      </form>
 
    </div>
  </div>
</div>
    <!-- /.row --> 
    <!-- ============================================== BRANDS CAROUSEL ============================================== -->
   
    <!-- /.logo-slider --> 
    <!-- ============================================== BRANDS CAROUSEL : END ============================================== --> 
  </div>
  <!-- /.container --> 
</div>

</div>
<!-- /#top-banner-and-menu --> 

<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="footer color-bg">
@include('web.footerweb')
</footer>
<!-- ============================================================= FOOTER : END============================================================= --> 

<!-- For demo purposes – can be removed on production --> 

<!-- For demo purposes – can be removed on production : End --> 

<!-- JavaScripts placed at the end of the document so the pages load faster -->
<script src="{{asset('js/app.js')}}"></script> 
<script src="{{asset('js/jquery-1.11.1.min.js')}}"></script> 
<script src="{{asset('js/bootstrap.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-hover-dropdown.min.js')}}"></script> 
<script src="{{asset('js/owl.carousel.min.js')}}"></script> 
<script src="{{asset('js/echo.min.js')}}"></script> 
<script src="{{asset('js/jquery.easing-1.3.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-slider.min.js')}}"></script> 
<script src="{{asset('js/jquery.rateit.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/lightbox.min.js')}}"></script> 
<script src="{{asset('js/bootstrap-select.min.js')}}"></script> 
<script src="{{asset('js/wow.min.js')}}"></script> 
<script src="{{asset('js/scripts.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{ asset('ui-backend/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.id.js')}}"></script>


<script type="text/javascript">
var id = '';
  Vue.component('my-comp', {
    template: '<div></div>',
    created: function () {
      id = this.$attrs['my-attr']; // And here is - in $attrs object
      //alert(this.$attrs['my-attr'][1]);
    }
  })

   var extTime = new Vue({
  el: '#ext-time',
  data: {
    message: '',
    idbrg: '',
    exttime: '',

  },
  methods: {
    formSubmit(e) {     
                e.preventDefault();
                let currentObj = this;
                axios.post("{{ url('/lelang/update-exttime') }}", {
                    idbrg: this.idbrg,
                    exttime:  this.exttime,
                   
                })
                .then(function (response) {
                    currentObj.output = response.data;
                   // location.reload();
                    alert('berhasil');
                    location.reload();
                })
                .catch(function (error) {
                    currentObj.output = error;
                });
    }
  }  
}); 


</script>

<script>
var SITEURL = '{{URL::to('')}}';
$(document).ready( function () {

//STATUS SEMENTARA DI HIDE DULU

  

$(".btnAddComplaint").click(function () {
   
    $('#mdlComplaint').modal('show');
});
    

});




</script>

</body>
</html>