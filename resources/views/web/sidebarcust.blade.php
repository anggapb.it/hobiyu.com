 <div class="sidebar-widget wow fadeInUp">
              <h3 class="section-title">Menu</h3>
              <div class="sidebar-widget wow fadeInUp">
           
              <div class="sidebar-widget-body">
                <ul class="list">
                  <li><a href="{{ route('customer.profile') }}"><span class="icon fa fa-user"></span> My Profile</a></li>
                  <li><a href="{{ route('customer.lelang') }}"><span class="icon fa fa-list"></span> My Lelang</a></li>
                  <li><a href="{{ route('customer.toko') }}"><span class="icon fa fa-opencart"></span> My Store</a></li>
                  <li><a href="{{ route('customer.saldo') }}"><span class="icon fa fa-money"></span> My Saldo (Rp. {{ number_format(Auth::guard('customer')->user()->saldo,0,',','.') }})</a></li>
                  <li><a href="{{ route('customer.order') }}"><span class="icon fa fa-cart-plus"></span> My Order</a></li>
                  <li><a href="{{ route('customer.sales') }}"><span class="icon fa fa-sellsy"></span> My Sales</a></li>
                  <li><a href="{{ route('customer.forum') }}"><span class="icon fa fa-users"></span> My Forum</a></li>
                  <li><a href="{{ route('customer.history-bid') }}"><span class="icon fa fa-history"></span> History Bids</a></li>
                  <li><a href="{{ route('customer.complaint') }}"><span class="icon fa fa-book"></span> Complaint</a></li>
                </ul>
                <!--<a href="#" class="lnk btn btn-primary">Show Now</a>--> 
              </div>
              <!-- /.sidebar-widget-body --> 
            </div>