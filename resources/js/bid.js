window.Vue = require('vue');

Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr("value");


new Vue({
  el: '#app-5',
  data: {
    message: '',
    hrg_max: '',
    hrg_bid: '',
    events: [],
    seen: true,
    isIdlelang:false,
    isHrgmax:false,
    isUserid:false,
      money: {
        decimal: ',',
        thousands: '.',
        prefix: '',
        suffix: '',
        precision: 0,
        masked: true
      },
      idlelang: '',
      userid: '',
      output: '',
  },
  
  methods: {
    formSubmit(e) {
                e.preventDefault();
                let currentObj = this;
                axios.post("{{ url('/lelang/makebid') }}", {
                    harga: this.hrg_bid,
                    idlelang:  this.tidlelang,
                    userid:  this.tuserid,
                })
                .then(function (response) {
                    currentObj.output = response.data;
                })
                .catch(function (error) {
                    currentObj.output = error;
                });
    },

    checkBid: function(e){
       if(Number(this.hrg_bid) <= Number(this.hrg_max)){
         this.message = 'Harga bid harus lebih dari harga minimum';
       //  this.hrg_bid = '';
         this.seen = false;
      }
      if(Number(this.hrg_bid) > Number(this.hrg_max)){
        this.message = '';
        this.seen = true
      } 

      e.preventDefault(); 
    }
      
  }  
}); 