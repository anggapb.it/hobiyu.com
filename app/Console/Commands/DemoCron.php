<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Master\Bid;
use App\Lelang;
use App\Customer;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailBid;


class DemoCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'demo:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        date_default_timezone_set('Asia/Jakarta');
        $date = date("Y-m-d H:i:s");
        //get lelang yang sudah habis batas waktu
        $lelangs = \DB::table('lelangs')->where('tgl_bts_penawaran','<',$date)->where('idstlelang','<>','3')->get();
            foreach($lelangs as $lelang){
                   //get harga tertinggi
                    $harga = \DB::table('bids')->where('idlelang',$lelang->id)->max('harga');
                    //get user pemenang bid
                    $bid = Bid::where("idlelang", $lelang->id)->where("harga",$harga)->first();   
                    if(!empty($bid->userid)){
                        $user = Customer::where("id", $bid->userid)->first();   
                        $hobiyu = \DB::table('profiles')
                                   ->join('banks AS b1', 'profiles.bank_satu', '=' , 'b1.id') 
                                   ->join('banks AS b2', 'profiles.bank_dua', '=' , 'b2.id') 
                                   ->join('banks AS b3', 'profiles.bank_tiga', '=' , 'b3.id')
                                   ->select('profiles.*',\DB::raw('b1.name as nmb1'),\DB::raw('b2.name as nmb2'),\DB::raw('b3.name as nmb3'))
                                   ->first(); 

                        $bank_satu = $hobiyu->nmb1;           
                        $bank_dua = $hobiyu->nmb2;           
                        $bank_tiga = $hobiyu->nmb3;           
                        $kdrek_satu = $hobiyu->kdrek_satu;           
                        $kdrek_dua = $hobiyu->kdrek_dua;           
                        $kdrek_tiga = $hobiyu->kdrek_tiga;           
                        $atasnama_satu = $hobiyu->atasnama_satu;           
                        $atasnama_dua = $hobiyu->atasnama_dua;           
                        $atasnama_tiga = $hobiyu->atasnama_tiga;           

                        $update_pemenang = \DB::table('lelangs')
                             ->where('id', $lelang->id)
                             ->update(['pemenang_bid' => $bid->userid, 'idstlelang' => 3]);
                         Mail::to($user->email)->send(new SendMailBid($user->name,$bid->idlelang,$lelang->nmitem,$bank_satu,$kdrek_satu,$atasnama_satu)); 
                    } else{
                        \DB::table('lelangs')
                             ->where('id', $lelang->id)
                             ->update(['idstlelang' => 3]);
                    }

                       
            }
           
            //$pemenang_bid = $bid->userid;

    //    $test_insert = \DB::table('bids')->insert(
    //         ['harga' => '4200000', 'userid' => 12, 'idlelang' => 8]
    //     );
        

       // \Log::info($update_pemenang);
     
        /*
           Write your database logic we bellow:
           Item::create(['name'=>'hello new']);
        */
      
        $this->info('Demo:Cron Cummand Run successfully!');
    }
}
