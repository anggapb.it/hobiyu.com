<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Lelang extends Model
{
    use Notifiable;


    protected $fillable = [
        'nmitem', 'userid','idkatitem', 'kondisi_item', 'hrgawal', 'deskripsi', 'photo_item', 'tgl_bts_penawaran', 
        'notelp', 'beban_biaya_kirim', 'idstlelang', 'pemenang_bid', 'jmlbid','kelipatan','hrgori','idhobidet','photo_item2',
        'photo_item3','photo_item4','photo_item5','video1','video2','video3','video4','video5','lokasi_barang'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

  
    public function bids()
	{
	    return $this->hasMany('App\Models\Master\Bid','idlelang');
	}
}
