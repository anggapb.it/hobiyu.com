<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\FormatEmails;

class SendMailBid extends Mailable
{
    use Queueable, SerializesModels;
    public $nmpemenang;
    public $idlelang;
    public $nmitem;
    public $bank_satu;
    public $kdrek_satu;
    public $atasnama_satu;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($nmpemenang,$idlelang,$nmitem,$bank_satu,$kdrek_satu,$atasnama_satu)
    {
        //
        $this->nmpemenang = $nmpemenang;
        $this->idlelang = $idlelang;
        $this->nmitem = $nmitem;
        $this->bank_satu = $bank_satu;
        $this->kdrek_satu = $kdrek_satu;
        $this->atasnama_satu = $atasnama_satu;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
     //   return $this->view('web.lelang.email_biders');
        $isi_email = FormatEmails::where('id','=',1)->first()->format;

        return $this->from('admin@hobiyu.com')
                    ->subject("Pengumuman Pemenang Lelang")
                   ->view('web.lelang.email_biders',compact('isi_email'));
                //    ->with(
                //     [
                //         'nama' => 'Admin',
                //         'website' => 'www.hobiyu.com',
                //     ]);
    }
}
