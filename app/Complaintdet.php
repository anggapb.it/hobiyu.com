<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Complaintdet extends Model
{
    //
    use Notifiable;

    protected $fillable = [
        'id','idcomplaint','answer','file','iduser'
    ];
}
