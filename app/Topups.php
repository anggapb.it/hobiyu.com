<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Topups extends Model
{
    //
    use Notifiable;

    protected $fillable = [
        'id','idcustomer','nominal','status','bukti_transfer','keterangan'
    ];
}
