<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ExtensionTime extends Model
{
    //
    use Notifiable;

    protected $fillable = [
        'id','waktu',
    ];
}
