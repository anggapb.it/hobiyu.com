<?php

namespace App\Http\Middleware;

use Closure;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \Menu::make('mainMenu', function ($menu) {
            //DASHBOARD
          //  $menu->add('Dashboard', 'home')->data('icon', 'dashboard');

            //Masters
            $menu->add('Master')
                ->data('icon', 'cubes');


            //Child Master
          //  $menu->master->add('User', 'master/user');
            $menu->master->add('Bank', 'master/bank');
            $menu->master->add('Provinsi', 'master/provinsi');
            $menu->master->add('Kota / Kabupaten', 'master/kota');
            $menu->master->add('Kecamatan', 'master/kecamatan');
            $menu->master->add('Kelurahan', 'master/kelurahan');
         //   $menu->master->add('Kategori Forum', 'master/category-forum');
            $menu->master->add('Pekerjaan', 'master/job');
            $menu->master->add('Sponsor', 'master/sponsor');
        //    $menu->master->add('Bid', 'master/bid');
            $menu->master->add('Tipe Transaksi', 'master/type-transaction');
            $menu->master->add('Header Hobi', 'master/hobby');
            $menu->master->add('Detail Hobi', 'master/hobby-det');
           // $menu->master->add('Kategori Hobi', 'master/category-hobby');
            $menu->master->add('Kelipatan', 'master/kelipatan');
            $menu->master->add('Setting ', 'master/setting');
            $menu->master->add('Slider', 'master/slider');
            $menu->master->add('Profile', 'master/profile');
            $menu->master->add('Member', 'master/member');
            $menu->master->add('User Backend', 'master/user');
            $menu->master->add('Daftar Forum', 'master/mstforum');
         
            

            //Articles
            // $menu->add('Articles')
            //     ->data('icon', 'file');
            // //Child Articles
            // $menu->get('articles')->add('Articles', 'articles/article');
            // $menu->articles->add('Categories', 'articles/category');

            // Medical Store
            $menu->add('Transaksi')->data('icon', 'shopping cart');

            $menu->transaksi->add('Daftar Barang', 'transaksi/barang');
            $menu->transaksi->add('Daftar Lelang', 'transaksi/list-lelang');
            
            $menu->transaksi->add('Daftar Jual', 'transaksi/list-jual');
          //  $menu->transaksi->add('Daftar Forum', 'transaksi/list-forum');
            $menu->transaksi->add('Daftar Komplain', 'transaksi/list-complaint');
            $menu->transaksi->add('Top Up Saldo Member', 'transaksi/topup-saldo');
            //$menu->store->add('Items', 'store/items');
            // // Medical Consultations
            // $menu->add('Consultation', 'consultation/index')->data('icon', 'question circle');
            //Header
            // $menu->add('Transactions')->data('icon', 'file');
            // //Child
            // $menu->transactions->add('Orders')->data('icon', 'file');
            //  $menu->orders->add('Tes Order Det', 'transaction/order-medical-equip/index');
        });


        return $next($request);
    }
}
