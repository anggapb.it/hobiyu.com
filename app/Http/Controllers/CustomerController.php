<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Redirect,Response;
use App\Models\Master\Hobby;
use App\Lelang;
use App\Stlelang;
use App\Customer;
use App\Kelipatan;
use App\Topups;
use App\Profile;
use App\Setting;
use App\Order;
use App\Orderdet;
use App\StatusOrder;
use App\PayMethod;
use App\Complaint;
use App\Complaintdet;
use App\Forums;
use App\Models\Master\Sponsor;
use App\Models\Master\Bank;
use App\Models\Master\Job;
use App\Models\Master\Provinsi;
use App\Models\Master\Kota;
use App\Models\Master\Kecamatan;
use App\Models\Master\Kelurahan;
use App\Models\Transaksi\Barang;
use App\Models\Master\TypeTransaction;
use App\ExtensionTime;
use App\Models\Master\HobbyDet; 
use App\Models\Master\Members;
use App\Models\Master\Bid;
use Carbon\Carbon;
use Auth;
use DataTables;

class CustomerController extends Controller
{

    public function homeCustomer(Hobby $hobi){
        $hobbies = $hobi::where("id_status", "ACTIVE")->get();
        $profile = Profile::where('id',1)->first();
        return view('web.customer.home-customer',compact('hobbies','hobi','profile'));
    }

    //  public function historyBid(Hobby $hobi){
    //     $hobbies = $hobi::where("id_status", "ACTIVE")->get();
    //     $profile = Profile::where('id',1)->first();
    //     return view('web.customer.history-bid-customer',compact('hobbies','hobi','profile'));
    // }

    public function historyBid(Hobby $hobi, Bid $bid, Stlelang $stlelang, Customer $user, StatusOrder $storder, PayMethod $paymethod){
        $hobbies = $hobi::where("id_status", "ACTIVE")->get();
        $profile = Profile::where('id',1)->first();
        $bids = \DB::table('bids')
        ->leftjoin('lelangs', 'lelangs.id', '=', 'bids.idlelang')
        ->leftjoin('customers', 'customers.id', '=', 'lelangs.userid')
        ->where('bids.userid','=',Auth::guard('customer')->user()->id)
       ->select('bids.*','customers.name as nmpelelang','lelangs.nmitem as nmitem')
       ->paginate(5);
  
        return view('web.customer.history-bid-new',compact('hobbies','hobi','user','profile','bids'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    } 

    public function lelangCustomer(Hobby $hobi, Bid $bid, Stlelang $stlelang, Customer $user){
        $hobbies = $hobi::where("id_status", "ACTIVE")->get();
        $profile = Profile::where('id',1)->first();
        $lelangs = Lelang::where("userid", Auth::guard('customer')->user()->id)->orderBy('id','desc')->paginate(5);
        $exttimes = ExtensionTime::all();
  
        return view('web.customer.lelang-customer',compact('hobbies','hobi','lelangs','bid','stlelang','user','exttimes','profile'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
   
    public function toko(Hobby $hobi, Bid $bid, Stlelang $stlelang, Customer $user){
        $hobbies = $hobi::where("id_status", "ACTIVE")->get();
        $profile = Profile::where('id',1)->first();
        $barangs = Barang::where("userid", Auth::guard('customer')->user()->id)->orderBy('id','desc')->paginate(5);
  
        return view('web.toko.barang',compact('hobbies','hobi','user','profile','barangs'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
   
    public function complaint(Hobby $hobi, Bid $bid, Stlelang $stlelang, Customer $user){
        $hobbies = $hobi::where("id_status", "ACTIVE")->get();
        $profile = Profile::where('id',1)->first();
        $complaints = Complaint::where("iduser", Auth::guard('customer')->user()->id)->orderBy('id','desc')->paginate(5);
  
        return view('web.complaint.index',compact('hobbies','hobi','user','profile','complaints'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
   
    public function complaint_detail($id, Hobby $hobi, Bid $bid, Stlelang $stlelang, Customer $user){
        $hobbies = $hobi::where("id_status", "ACTIVE")->get();
        $profile = Profile::where('id',1)->first();
        $complaint = Complaint::where('id',$id)->first();
        $count_answer = Complaintdet::where('idcomplaint',$complaint->id)->count();
        $answers = Complaintdet::where('idcomplaint',$complaint->id)->get();
        $sponsors = Sponsor::where('id_status','=','ACTIVE')->get();
       
        return view('web.complaint.detail',compact('hobbies','hobi','user','profile','complaint','count_answer','answers','sponsors'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    
    
    public function order(Hobby $hobi, Bid $bid, Stlelang $stlelang, Customer $user, StatusOrder $storder, TypeTransaction $paymethod){
        $hobbies = $hobi::where("id_status", "ACTIVE")->get();
        $profile = Profile::where('id',1)->first();
        $orders = Order::where("idcustomer", Auth::guard('customer')->user()->id)->orderBy('id','desc')->paginate(5);
  
        return view('web.order.index',compact('hobbies','hobi','user','profile','orders','storder','paymethod'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    } 
   
    public function forum(Hobby $hobi, Bid $bid, Stlelang $stlelang, Customer $user, TypeTransaction $paymethod){
        $hobbies = $hobi::where("id_status", "ACTIVE")->get();
        $profile = Profile::where('id',1)->first();
        $forums = Forums::where("userid", Auth::guard('customer')->user()->id)->orderBy('id','desc')->paginate(5);
  
        return view('web.customer.forum-customer',compact('hobbies','hobi','user','profile','forums','paymethod'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    } 
   
    public function sales(Hobby $hobi, Bid $bid, Stlelang $stlelang, Customer $user, StatusOrder $storder, TypeTransaction $paymethod){
        $hobbies = $hobi::where("id_status", "ACTIVE")->get();
        $profile = Profile::where('id',1)->first();
     //   $orders = Order::where("idpenjual", Auth::guard('customer')->user()->id)->orderBy('id','desc')->paginate(5);

        $orders = \DB::table('orders')
        ->leftjoin('orderdets', 'orderdets.idorder', '=', 'orders.id')
        ->leftjoin('customers', 'customers.id', '=', 'orders.idcustomer')
        ->leftjoin('status_orders', 'status_orders.id', '=', 'orders.status')
        ->where('orderdets.idpenjual','=',Auth::guard('customer')->user()->id)
        ->groupby('orderdets.idorder')
       ->select('orders.*','status_orders.name as nmstatus','customers.name as nmcustomer')
       ->paginate(5);
  
        return view('web.sales.index',compact('hobbies','hobi','user','profile','orders','storder','paymethod'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    } 
    
    public function batalorder($id){
        $order = Order::find($id);
        $order->status =  5; // Transaksi dibatalkan (table status_order)
        $order->update();  
        
        $orderdet = Orderdet::where('idorder','=',$id)->get();
        foreach($orderdet as $item) { 
            $brg = Barang::find($item->idbarang);
            $brg->stok = $brg->stok + $item->qty;
            $brg->update();
        }
                    
        return redirect()->back()->with('success','Transaksi dibatalkan...');
    }

    public function update_noresi(Request $req){
        // $orderdet =  Orderdet::where('idorder','=',$req->order_id)->first();
        // $orderdet->noresi =  $req->noresi; 
        // $orderdet->update();  

        $orderdet = \DB::table('orderdets')
            ->where('idorder', $req->order_id)
            ->where('idpenjual', Auth::guard('customer')->user()->id)
            ->update(['noresi' => $req->noresi]);
          
        return redirect()->back()->with('success','No resi berhasil disimpan...');
    }
   
    public function kirimorder($id){
        $orderdet = Orderdet::where('idorder', $id)->first();
        $orderdet->stbrg =  3; // Barang sudah dikirim (table status_order)
        $orderdet->update();  
        
        return redirect()->back()->with('success','Barang sudah dikirim...');
    }

    public function orderdet($id){
        $orderdets = \DB::table('orderdets')
        ->leftjoin('customers', 'customers.id', '=', 'orderdets.idpenjual')
       ->where('orderdets.idorder','=',$id)
       ->select('orderdets.*','customers.name as nmpenjual')
       ->get();

       if(request()->ajax()) {
           return datatables()->of($orderdets)
           ->addIndexColumn()
           ->make(true);
       }
       return view('web.order.index');
    }
    
    public function orderdet_forsales($id){
        $orderdets = \DB::table('orderdets')
        ->leftjoin('customers', 'customers.id', '=', 'orderdets.idpenjual')
       ->where('orderdets.idorder','=',$id)
       ->where('orderdets.idpenjual','=',Auth::guard('customer')->user()->id)
       ->select('orderdets.*','customers.name as nmpenjual')
       ->get();

       if(request()->ajax()) {
           return datatables()->of($orderdets)
           ->addIndexColumn()
           ->make(true);
       }
       return view('web.order.index');
    }

    public function profile(Hobby $hobi, Customer $customer, Bank $banks, Job $job){
        $hobbies = $hobi::where("id_status", "ACTIVE")->get();
        $cust = $customer::where('id',Auth::guard('customer')->user()->id)->first();
        $profile = Profile::where('id',1)->first();
        $bank = Bank::all();
        $jobs = Job::all();
        $provs = Provinsi::all();

        return view('web.customer.profile',compact('hobbies','hobi','cust','profile','bank','banks','jobs','job','provs'));
    }
 
    public function saldo(Hobby $hobi, Customer $customer){
        $hobbies = $hobi::where("id_status", "ACTIVE")->get();
        $cust = $customer::where('id',Auth::guard('customer')->user()->id)->first();
        $profile = Profile::where('id',1)->first();

        return view('web.customer.saldo',compact('hobbies','hobi','cust','profile'));
    }

    public function formLelang(Hobby $hobi, HobbyDet $hobidet){
        $hobbies = $hobi::where("id_status", "ACTIVE")->get();
        $hobidets = $hobidet::where("id_status", "ACTIVE")->get();
        $kelipatans = Kelipatan::all();
        $profile = Profile::where('id',1)->first();
        return view('web.customer.add-lelang',compact('hobidets','hobbies','hobi','kelipatans','profile'));
    }
   
    public function formBrg(Hobby $hobi, HobbyDet $hobidet){
        $hobbies = $hobi::where("id_status", "ACTIVE")->get();
        $hobidets = $hobidet::where("id_status", "ACTIVE")->get();
        $profile = Profile::where('id',1)->first();
        return view('web.toko.add-brg',compact('hobidets','hobbies','hobi','profile'));
    }

    public function insertTopup(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');     
        $request->validate([
            'bukti_transfer' => 'image|mimes:jpeg,png,jpg,gif,svg|max:8192',
            'nominal' => 'required',
        ]);

        if(empty($request->bukti_transfer)){
            $bukti_transfer = null;
        } else {
            $bukti_transfer = time().'.'.$request->bukti_transfer->getClientOriginalExtension();
        }

        $topup = new Topups([
            'idcustomer' => Auth::guard('customer')->user()->id,
            'nominal' => $request->get('nominal'),
            'bukti_transfer'=> $bukti_transfer,
            'keterangan'=> $request->get('keterangan'),
            'status'=> 'WAITING',
          
          ]);

          if(!empty($request->bukti_transfer)){
            $request->bukti_transfer->move(public_path('uploads/bukti_transfer'), time().'.'.$request->bukti_transfer->getClientOriginalExtension());
          }  

        $topup->save();  

        return redirect()->route('customer.saldo')
        ->with('success','Bukti transfer sedang di cek oleh admin kami, silahkan tunggu beberapa saat');

    }

    public function updateProfile($id, Request $request)
    {
        date_default_timezone_set('Asia/Jakarta'); 
     
        $request->validate([
            'photo_profile' => 'image|mimes:jpeg,png,jpg,gif,svg|max:8192',
            'email' => 'email|unique:customers,email,'. $id.',id',
            'noktp' => 'unique:customers,noktp,'. $id.',id',
            'kdrek' => 'unique:customers,kdrek,'. $id.',id',
            'phone' => 'unique:customers,phone,'. $id.',id',

            //'email' => 'unique:customers',
       //    'idpekerjaan' => 'required|not_in:0'
        ]);

        if(empty($request->photo_profile)){
            $photo_profile = null;
        } else {
            $photo_profile = time().'.'.$request->photo_profile->getClientOriginalExtension();
        }
        // if(empty($request->new_password)){
        //     $password = $request->password;
        // } else {
        //     $password = $request->new_password;
        // }

        $customer = Customer::find($id);
        $customer->photo_profile =  $photo_profile;
        if(!empty($request->new_password)){
            $customer->password =  Hash::make($request->new_password);
        }
        $customer->name =  $request->name;
        $customer->email =  $request->email;
        $customer->phone =  $request->phone;
        $customer->alamat =  $request->alamat;
        $customer->kodepos =  $request->kodepos;
        $customer->kdrek =  $request->kdrek;
        $customer->idbank =  $request->idbank;
        $customer->kelamin =  $request->kelamin;
        $customer->noktp =  $request->noktp;
        $customer->tglaktivasi =  $request->tglaktivasi;
        $customer->tpt_lahir =  $request->tpt_lahir;
        $customer->tgl_lahir =  $request->tgl_lahir;
        $customer->idpekerjaan =  $request->idpekerjaan;
        $customer->idprovinsi =  $request->idprovinsi;
        $customer->idkota =  $request->idkota;
        $customer->idkecamatan =  $request->idkecamatan;
        $customer->idkelurahan =  $request->idkelurahan;
        $customer->update();

        if(!empty($request->photo_profile)){
            $request->photo_profile->move(public_path('uploads/member_profile'), time().'.'.$request->photo_profile->getClientOriginalExtension());
          }
  

        return redirect()->route('customer.profile')
                        ->with('success','Update Profile successfully.');
    }

    public function storeLelang(Request $request)
    {
        $arr_setting = \DB::table('settings')->where('id','=',1)->first();  //AMBIL NILAI SETTING PEMOTONGAN BIAYA LELANG
        
        $biaya_lelang = $arr_setting->nilai;

       // $arr_customer = \DB::table('customers')->where('id','=',1)->first();

        date_default_timezone_set('Asia/Jakarta');
        $customMessages = [
            'required' => ':attribute field is required.',
            'min' => 'SALDO ANDA TIDAK CUKUP, silahkan TOP UP agar bisa melakukan proses lelang',
        ];
    
        $request->validate([
            'saldo' => 'numeric|min:"'.$biaya_lelang.'"',
            'nmitem' => 'required',
            'idkatitem' => 'required',
            'idhobidet' => 'required',
            'kondisi_item' => 'required',
            'hrgawal' => 'required',
            'deskripsi' => 'required',
            'tgl_bts_penawaran' => 'required',
            'kelipatan' => 'required',
            'photo_item' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:8192',
            'photo_item2' => 'image|mimes:jpeg,png,jpg,gif,svg|max:8192',
            'photo_item3' => 'image|mimes:jpeg,png,jpg,gif,svg|max:8192',
            'photo_item4' => 'image|mimes:jpeg,png,jpg,gif,svg|max:8192',
            'photo_item5' => 'image|mimes:jpeg,png,jpg,gif,svg|max:8192',
            'video1' => 'mimes:mp4,mov,ogg|max:15360', // max 15 Mb
            'video2' => 'mimes:mp4,mov,ogg|max:15360', // max 15 Mb
            'video3' => 'mimes:mp4,mov,ogg|max:15360', // max 15 Mb
            'video4' => 'mimes:mp4,mov,ogg|max:15360', // max 15 Mb
            'video5' => 'mimes:mp4,mov,ogg|max:15360', // max 15 Mb
            'lokasi_barang' => 'required'
        ],$customMessages);
        
        //photo 1
        if(empty($request->photo_item)){
            $photo_item = null;
        } else {
            $photo_item = time().'satu.'.$request->photo_item->getClientOriginalExtension();
        }
        //photo 2
        if(empty($request->photo_item2)){
            $photo_item2 = null;
        } else {
            $photo_item2 = time().'dua.'.$request->photo_item2->getClientOriginalExtension();
        }
        //photo 3
        if(empty($request->photo_item3)){
            $photo_item3 = null;
        } else {
            $photo_item3 = time().'tiga.'.$request->photo_item3->getClientOriginalExtension();
        }
        //photo 4
        if(empty($request->photo_item4)){
            $photo_item4 = null;
        } else {
            $photo_item4 = time().'empat.'.$request->photo_item4->getClientOriginalExtension();
        }
        //photo 5
        if(empty($request->photo_item5)){
            $photo_item5 = null;
        } else {
            $photo_item5 = time().'lima.'.$request->photo_item5->getClientOriginalExtension();
        }

        //VIDEO
         //video 1
         if(empty($request->video1)){
            $video1 = null;
        } else {
            $video1 = time().'video-satu.'.$request->video1->getClientOriginalExtension();
        }
         //video 2
         if(empty($request->video2)){
            $video2 = null;
        } else {
            $video2 = time().'video-dua.'.$request->video2->getClientOriginalExtension();
        }
         //video 3
         if(empty($request->video3)){
            $video3 = null;
        } else {
            $video3 = time().'video-tiga.'.$request->video3->getClientOriginalExtension();
        }
         //video 4
         if(empty($request->video4)){
            $video4 = null;
        } else {
            $video4 = time().'video-empat.'.$request->video4->getClientOriginalExtension();
        }
         //video 5
         if(empty($request->video5)){
            $video5 = null;
        } else {
            $video5 = time().'video-lima.'.$request->video5->getClientOriginalExtension();
        }
        ///

        $lelang = new Lelang([
            'nmitem' => $request->get('nmitem'),
            'idkatitem'=> $request->get('idkatitem'),
            'idhobidet'=> $request->get('idhobidet'),
            'kondisi_item'=> $request->get('kondisi_item'),
            'hrgawal'=> $request->get('hrgawal'),
            'hrgori'=> $request->get('hrgawal'),
            'deskripsi'=> $request->get('deskripsi'),
            'kelipatan'=> $request->get('kelipatan'),
            'tgl_bts_penawaran'=> $request->get('tgl_bts_penawaran'),
            'photo_item'=>  $photo_item,
            'photo_item2'=>  $photo_item2,
            'photo_item3'=>  $photo_item3,
            'photo_item4'=>  $photo_item4,
            'photo_item5'=>  $photo_item5,
            'video1'=>  $video1,
            'video2'=>  $video2,
            'video3'=>  $video3,
            'video4'=>  $video4,
            'video5'=>  $video5,
            'notelp'=> $request->get('notelp'),
            'userid'=> Auth::guard('customer')->user()->id,
            'idstlelang'=> 1,
            'jmlbid'=> 0,
            'lokasi_barang'=> $request->get('lokasi_barang'),
          ]);
         // $request->photo_item->move(public_path('uploads/item_lelang'), time().'.'.$request->photo_item->getClientOriginalExtension());
          
          if(!empty($request->photo_item)){
            $request->photo_item->move(public_path('uploads/item_lelang'), time().'satu.'.$request->photo_item->getClientOriginalExtension());
          }
          if(!empty($request->photo_item2)){
            $request->photo_item2->move(public_path('uploads/item_lelang'), time().'dua.'.$request->photo_item2->getClientOriginalExtension());
          }
          if(!empty($request->photo_item3)){
            $request->photo_item3->move(public_path('uploads/item_lelang'), time().'tiga.'.$request->photo_item3->getClientOriginalExtension());
          }
          if(!empty($request->photo_item4)){
            $request->photo_item4->move(public_path('uploads/item_lelang'), time().'empat.'.$request->photo_item4->getClientOriginalExtension());
          }
          if(!empty($request->photo_item5)){
            $request->photo_item5->move(public_path('uploads/item_lelang'), time().'lima.'.$request->photo_item5->getClientOriginalExtension());
          }

          //VIDEO
          if(!empty($request->video1)){
            $request->video1->move(public_path('uploads/item_lelang'), time().'video-satu.'.$request->video1->getClientOriginalExtension());
          }  
          if(!empty($request->video2)){
            $request->video2->move(public_path('uploads/item_lelang'), time().'video-dua.'.$request->video2->getClientOriginalExtension());
          }  
          if(!empty($request->video3)){
            $request->video3->move(public_path('uploads/item_lelang'), time().'video-tiga.'.$request->video3->getClientOriginalExtension());
          }  
          if(!empty($request->video4)){
            $request->video4->move(public_path('uploads/item_lelang'), time().'video-empat.'.$request->video4->getClientOriginalExtension());
          }  
          if(!empty($request->video5)){
            $request->video5->move(public_path('uploads/item_lelang'), time().'video-lima.'.$request->video5->getClientOriginalExtension());
          }  
          //

          $lelang->save();

          $customer = Customer::find(Auth::guard('customer')->user()->id);
          $customer->saldo =  $customer->saldo - $biaya_lelang;
          $customer->update();         
  
   
        return redirect()->route('customer.lelang')
                        ->with('success','Created successfully.');
    }


    public function destroyLelang($id)
    {
        $lelang = Lelang::find($id);
        $lelang->delete();
  
        return redirect()->route('customer.lelang')
                        ->with('success','Deleted successfully');
    }

    public function editLelang(Hobby $hobi,Lelang $lelang)
    {
        $hobbies = $hobi::where("id_status", "ACTIVE")->get();
        return view('web.customer.edit-lelang',compact('hobbies','hobi','lelang'));
       
    }

    public function updateLelang(Request $request, Lelang $lelang)
    {
        $request->validate([
            'nmitem' => 'required',
            'idkatitem' => 'required',
            'kondisi_item' => 'required',
            'hrgawal' => 'required',
            'deskripsi' => 'required',
            'tgl_bts_penawaran' => 'required',
            'photo_item' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

  
        $product->update($request->all());
  
        return redirect()->route('customer.lelang')
                        ->with('success','Updated successfully');
    }
    
    public function login_member(){
        return view('web.signin');
    }

    public function getHistoryBid(){
        $bid = \DB::table('bids')->join('lelangs', 'lelangs.id', '=', 'bids.idlelang')->select('bids.*','lelangs.nmitem')
            ->where('bids.userid', '=', Auth::guard('customer')->user()->id)->paginate(5);
        //$data = Bid::where("userid", Auth::guard('customer')->user()->id)->orderBy('created_at')->paginate(5);
        return response()->json($bid);
    }


    public function addExttime(Request $request)
    {
         date_default_timezone_set('Asia/Jakarta');
         $this->validate($request, [
            'idlelang' => 'required',
        ]);
        $minutes = $request->get('exttime');    
        $lelang = Lelang::where('id',$request->get('idlelang'))->first();
       
       // $date = $lelang->tgl_bts_penawaran;
        $date = date('Y-m-d H:i:s',strtotime('+{$minutes} minutes',strtotime($lelang->tgl_bts_penawaran)));
        //$date = strtotime("+5 minute", $date);
        //$time->modify("+{$minutes} minutes");

        $time = new \DateTime($lelang->tgl_bts_penawaran);
        $time->add(new \DateInterval('PT' . $minutes . 'M'));

        $stamp = $time->format('Y-m-d H:i:s');

        $data = array(
            'wkt_tambahan' => $request->get('exttime'),   
            'tgl_bts_penawaran' => $stamp,   
        );
        \DB::table('lelangs')->where('id', $request->get('idlelang'))->update($data);


        return response()->json('Add Extension Time Success!');         
    }
   
   
    public function updateLelangNew(Request $request)
    {
         date_default_timezone_set('Asia/Jakarta');
       
        $lelang = Lelang::find($request->idlelang_edit);
        $lelang->nmitem =  $request->nmitem;
        $lelang->deskripsi =  $request->deskripsi;
        $lelang->hrgori =  $request->hrgori;
        $lelang->lokasi_barang =  $request->lokasi;
        $lelang->update();         
  
        return redirect()->route('customer.lelang')
                        ->with('success','Updated successfully');
    }

    public function getKota($id)
    {
      $hd = Kota::where("id_provinsi","=",$id)->get();
      return response()->json(['kota' => $hd], 200);
    }
    public function getKec($id)
    {
      $hd = Kecamatan::where("id_kota","=",$id)->get();
      return response()->json(['kec' => $hd], 200);
    }
    public function getKel($id)
    {
      $hd = Kelurahan::where("id_kecamatan","=",$id)->get();
      return response()->json(['kel' => $hd], 200);
    }

    public function generateRandomString($length = 5) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString.date('y');
    }

    public function insertComplaint(Request $request){
        date_default_timezone_set('Asia/Jakarta');     
        $request->validate([
            'file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:8192',
            'judul' => 'required',
            'keterangan' => 'required',
        ]);

        if(empty($request->file)){
            $file = null;
        } else {
            $file = time().'.'.$request->file->getClientOriginalExtension();
        }

        $complaint = new Complaint([
            'iduser' => Auth::guard('customer')->user()->id,
            'judul' => $request->get('judul'),
            'keterangan' => $request->get('keterangan'),
            'file'=> $file,
            'status'=> 'OPEN',
            'notiket'=>$this->generateRandomString()
          
          ]);

          if(!empty($request->file)){
            $request->file->move(public_path('uploads/komplain'), time().'.'.$request->file->getClientOriginalExtension());
          }  

        $complaint->save();  

        return redirect()->route('customer.complaint')
        ->with('success','Buka tiket komplain berhasil dibuat dan sedang di cek oleh tim kami...');
    }

    public function insertBrg(Request $request)
    {
       
        date_default_timezone_set('Asia/Jakarta');
        $customMessages = [
            'required' => ':attribute harus diisi...',
        ];
    
        $request->validate([
            'nmbrg' => 'required',
            'kdbrg' => 'required',
            'idhobi' => 'required',
            'idhobidet' => 'required',
            'kondisi_barang' => 'required',
            'stok' => 'required',
            'berat' => 'required',
            'harga' => 'required',
            'deskripsi' => 'required',
            'photo_item' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:8192',
            'photo_item2' => 'image|mimes:jpeg,png,jpg,gif,svg|max:8192',
            'photo_item3' => 'image|mimes:jpeg,png,jpg,gif,svg|max:8192', //max 8 Mb
            'video1' => 'mimes:mp4,mov,ogg|max:15360', // max 15 Mb
            'video2' => 'mimes:mp4,mov,ogg|max:15360', // max 15 Mb
            'video3' => 'mimes:mp4,mov,ogg|max:15360', // max 15 Mb
            'lokasi_barang' => 'required'
        ],$customMessages);
        
        //photo 1
        if(empty($request->photo_item)){
            $photo_item = null;
        } else {
            $photo_item = $request->photo_item->getClientOriginalName().'satu.'.$request->photo_item->getClientOriginalExtension();
        }
        //photo 2
        if(empty($request->photo_item2)){
            $photo_item2 = null;
        } else {
            $photo_item2 = $request->photo_item2->getClientOriginalName().'dua.'.$request->photo_item2->getClientOriginalExtension();
        }
        //photo 3
        if(empty($request->photo_item3)){
            $photo_item3 = null;
        } else {
            $photo_item3 = $request->photo_item3->getClientOriginalName().'tiga.'.$request->photo_item3->getClientOriginalExtension();
        }

        //VIDEO
         //video 1
         if(empty($request->video1)){
            $video1 = null;
        } else {
            $video1 = $request->video1->getClientOriginalName().'video-satu.'.$request->video1->getClientOriginalExtension();
        }
         //video 2
         if(empty($request->video2)){
            $video2 = null;
        } else {
            $video2 = $request->video2->getClientOriginalName().'video-dua.'.$request->video2->getClientOriginalExtension();
        }
         //video 3
         if(empty($request->video3)){
            $video3 = null;
        } else {
            $video3 = $request->video3->getClientOriginalName().'video-tiga.'.$request->video3->getClientOriginalExtension();
        }
        ///

        $brg = new Barang([
            'nmbrg' => $request->get('nmbrg'),
            'kdbrg' => $request->get('kdbrg'),
            'idhobi'=> $request->get('idhobi'),
            'idhobidet'=> $request->get('idhobidet'),
            'kondisi_barang'=> $request->get('kondisi_barang'),
            'harga'=> $request->get('harga'),
            'deskripsi_barang'=> $request->get('deskripsi'),
            'stok'=> $request->get('stok'),
            'berat'=> $request->get('berat'),
            'gambar_satu'=>  $photo_item,
            'gambar_dua'=>  $photo_item2,
            'gambar_tiga'=>  $photo_item3,
            'video_satu'=>  $video1,
            'video_dua'=>  $video2,
            'video_tiga'=>  $video3,
            'userid'=> Auth::guard('customer')->user()->id,
            'status'=> 'ACTIVE',
            'lokasi_barang'=> $request->get('lokasi_barang'),
          ]);
         // $request->photo_item->move(public_path('uploads/item_lelang'), time().'.'.$request->photo_item->getClientOriginalExtension());
          
          if(!empty($request->photo_item)){
            $request->photo_item->move(public_path('uploads/jual_barang'), $request->photo_item->getClientOriginalName().'satu.'.$request->photo_item->getClientOriginalExtension());
          }
          if(!empty($request->photo_item2)){
            $request->photo_item2->move(public_path('uploads/jual_barang'), $request->photo_item2->getClientOriginalName().'dua.'.$request->photo_item2->getClientOriginalExtension());
          }
          if(!empty($request->photo_item3)){
            $request->photo_item3->move(public_path('uploads/jual_barang'), $request->photo_item3->getClientOriginalName().'tiga.'.$request->photo_item3->getClientOriginalExtension());
          }
        

          //VIDEO
          if(!empty($request->video1)){
            $request->video1->move(public_path('uploads/jual_barang'), $request->video1->getClientOriginalName().'video-satu.'.$request->video1->getClientOriginalExtension());
          }  
          if(!empty($request->video2)){
            $request->video2->move(public_path('uploads/jual_barang'), $request->video2->getClientOriginalName().'video-dua.'.$request->video2->getClientOriginalExtension());
          }  
          if(!empty($request->video3)){
            $request->video3->move(public_path('uploads/jual_barang'), $request->video3->getClientOriginalName().'video-tiga.'.$request->video3->getClientOriginalExtension());
          }  
          //

          $brg->save();      
  
   
        return redirect()->route('customer.toko')
                        ->with('success','Barang berhasil ditambahkan');
    }

    public function updateForum(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $forum = Forums::find($request->idforum);
        $forum->judul =  $request->judul;
        $forum->deskripsi =  $request->deskripsi;
       
        $forum->update();         
  
        return redirect()->route('customer.forum')
                        ->with('success','Updated berhasil');
    }

    public function updateBrg(Request $request)
    {
         date_default_timezone_set('Asia/Jakarta');
    
        $brg = Barang::find($request->idbarang_edit);
        $brg->nmbrg =  $request->nmbrg;
        $brg->kdbrg =  $request->kdbrg;
        $brg->deskripsi_barang =  $request->deskripsi;
        $brg->harga =  $request->harga;
        $brg->berat =  $request->berat;
        $brg->stok =  $request->stok;
        $brg->kondisi_barang =  $request->kondisi_barang;
        $brg->lokasi_barang =  $request->lokasi_barang;
        $brg->idhobi =  $request->idhobi;
        $brg->idhobidet =  $request->idhobidet;
        $brg->status =  $request->status;
        $brg->update();         
  
        return redirect()->route('customer.toko')
                        ->with('success','Updated barang berhasil');
    }

    public function insertAnswerComplaint($id, Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $request->validate([
            'answer' => 'required',
        ]);

        $answer = new Complaintdet([
            'idcomplaint' => $id,//$request->get('id_forum'),
            'answer'=> $request->get('answer'),
            'iduser'=> Auth::guard('customer')->user()->name,
          ]);
          $answer->save();
  
   
        return redirect()->route('customer.complaint-detail',$id)
                        ->with('success','Berhasil ditambahkan..');
    }
    
}
