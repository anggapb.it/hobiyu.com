<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
use App\Models\Master\Hobby; 
use App\Models\Master\HobbyDet;
use App\Models\Master\Bid;
use App\Models\Master\Sponsor;
use App\Lelang; 
use App\Customer;
use App\Members;
use App\Stlelang;
use App\Forums;
use App\Profile;
use App\CategoryForums;
use App\Answers;
use Auth;
use Carbon\Carbon;

class ForumController extends Controller
{
    public function list($id, Request $request, Hobby $hobi, HobbyDet $hobidet, Customer $customer,CategoryForums $category_forum)
    {
        $profile = Profile::where('id',1)->first();
        date_default_timezone_set('Asia/Jakarta');
        $hobbies = $hobi::where("id_status", "ACTIVE")->get();
        $sponsors = Sponsor::where('id_status','=','ACTIVE')->get();
        $cari = $request->cari;
    if($id == 'all'){
        if(!empty($cari)){ 
            $forums = \DB::table('forums')
            ->join('customers', 'customers.id', '=', 'forums.userid')
            ->join('category_forums', 'category_forums.id', '=', 'forums.id_category_forum')
            ->select('forums.*', 'customers.name', 'category_forums.name')
            ->where('judul','like',"%".$cari."%")
            ->orWhere('deskripsi', 'like', "%{$cari}%")
            ->orWhere('tag', 'like', "%{$cari}%")
            ->orWhere('customers.name', 'like', "%{$cari}%")
            ->orWhere('category_forums.name', 'like', "%{$cari}%")
            ->paginate(6);
        }else{
            $forums = Forums::where('status','=','ACTIVE')->orderby('created_at','desc')->paginate(6); 
        }

    } else{
        if(!empty($cari)){ 
            $forums = \DB::table('forums')
            ->join('customers', 'customers.id', '=', 'forums.userid')
            ->join('category_forums', 'category_forums.id', '=', 'forums.id_category_forum')
            ->select('forums.*', 'customers.name', 'category_forums.name')
            ->where('judul','like',"%".$cari."%")
            ->orWhere('deskripsi', 'like', "%{$cari}%")
            ->orWhere('tag', 'like', "%{$cari}%")
            ->orWhere('customers.name', 'like', "%{$cari}%")
            ->orWhere('category_forums.name', 'like', "%{$cari}%")
            ->paginate(6);
        }else{
            $forums = Forums::where('idhobidet',$id)->where('status','=','ACTIVE')->orderby('created_at','desc')->paginate(6);   
        }
    }
      
       
        
        return view('web.forum.index',compact('hobbies','hobi','forums','customer','category_forum','profile','hobidet','sponsors'))
        ->with('i', (request()->input('page', 1) - 1) * 6);
    }

    public function post(Hobby $hobi, HobbyDet $hobidet, CategoryForums $category_forum)
    {
        $profile = Profile::where('id',1)->first();
        $hobbies = $hobi::where("id_status", "ACTIVE")->get();
        $category_forums = $category_forum::all();
        return view('web.forum.post',compact('hobbies','category_forums','profile','hobidet'));
    }
   

    public function store_bak(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $request->validate([
            'id_category_forum' => 'required',
            'judul' => 'required',
            'deskripsi' => 'required',
            //'tag' => 'required',
             // 'photo_item' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $forum = new Forums([
            'id_category_forum' => $request->get('id_category_forum'),
            'judul'=> $request->get('judul'),
            'deskripsi'=> $request->get('deskripsi'),
            'tag'=> '', //$request->get('tag'),
            'userid'=> Auth::guard('customer')->user()->id,
            'status'=> 'ACTIVE',
          ]);
      //    $request->photo_item->move(public_path('uploads/item_lelang'), time().'.'.$request->photo_item->getClientOriginalExtension());
         
          $forum->save();
  
      //  Lelang::create($request->all());
   
        return redirect()->route('forum')
                        ->with('success','Post thread successfully.');
    }

    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $request->validate([
            'id_category_forum' => 'required',
            'idhobidet' => 'required',
            'judul' => 'required',
            'deskripsi' => 'required',
            //'tag' => 'required',
             // 'photo_item' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $detail=$request->deskripsi;
 
        $dom = new \DOMDocument();
        $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getelementsbytagname('img');
 
        //loop over img elements, decode their base64 src and save them to public folder,
        //and then replace base64 src with stored image URL.
        foreach($images as $k => $img){
            $data = $img->getattribute('src');
           // $style = $img->getattribute('style');
 
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
 
            $data = base64_decode($data);
            $image_name= time().$k.'.png';
            $path = public_path() .'/'. $image_name;
 
            file_put_contents($path, $data);
 
            $img->removeattribute('src');
            $img->setattribute('src', \URL::to('/').'/'.$image_name);
        }
 
        $detail = $dom->savehtml();

       

        $forum = new Forums;
        $forum->id_category_forum = $request->id_category_forum;
        $forum->idhobidet = $request->idhobidet;
        $forum->judul = $request->judul;
        $forum->deskripsi = $detail;
        $forum->userid = Auth::guard('customer')->user()->id;
        $forum->status = 'ACTIVE';
        $forum->save();

        return redirect()->route('forum.list','all')
        ->with('success','Post thread successfully.');

        //return view('summernote_display',compact('forum'));
    }

    public function detail($id,Customer $user_answer){
        $profile = Profile::where('id',1)->first();
        $hobbies = Hobby::where("id_status", "ACTIVE")->get();
         $forum = Forums::where('id',$id)->first();
         $customer = Customer::where('id',$forum->userid)->first();
         $category_forum = CategoryForums::where('id',$forum->id_category_forum)->first();
         $count_answer = Answers::where('id_forum',$forum->id)->count();

         $answers = Answers::where('id_forum',$forum->id)->get();
         $sponsors = Sponsor::where('id_status','=','ACTIVE')->get();
        
                    

        return view('web.forum.detail',compact('id','hobbies','forum','customer','category_forum',
        'count_answer','answers','user_answer','profile','sponsors'))->with('i');
    }

    public function comment($id, Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $request->validate([
            'comment' => 'required',
        ]);

        $answer = new Answers([
            'id_forum' => $id,//$request->get('id_forum'),
            'comment'=> $request->get('comment'),
            'userid'=> Auth::guard('customer')->user()->id,
          ]);
          $answer->save();
  
   
        return redirect()->route('forum.detail',$id)
                        ->with('success','Komentar berhasil ditambahkan..');
    }

   
}
