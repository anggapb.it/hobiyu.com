<?php

namespace App\Http\Controllers\Transaksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaksi\Barang;
use App\Models\Master\HobbyDet;
use Redirect,Response;
use DataTables;
use App\Http\Requests\Master\BarangsRequest;
use DB;
// use App\Image_uploaded;
// use Carbon\Carbon;
// use Image;
use File;
use Illuminate\Support\Facades\Input;
use Validator;

class BarangController extends Controller
{ 
    public $path;
    public $dimensions;

    public function __construct()
    {
        //DEFINISIKAN PATH
        $this->path = storage_path('app/public/images');
        //DEFINISIKAN DIMENSI
        $this->dimensions = ['245', '300', '500'];
    }
    public function create(){
        $kategori = HobbyDet::all();
        return view('backend.transaksi.barang.create', compact('kategori'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barang = DB::table('barang')
        ->leftjoin('hobby_det', 'barang.idkatbrg', '=', 'hobby_det.id')
        ->select('barang.*', 'hobby_det.name as nmkatbrg')
        ->get();

        if(request()->ajax()) {
            return datatables()->of($barang)
            ->addColumn('action', 'backend.transaksi.barang.action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        return view('backend.transaksi.barang.index');
    }

    public function upload($request)
    {
        // $this->validate($image, [
        //     'image' => 'required|image|mimes:jpg,png,jpeg'
        // ]);
		
        //JIKA FOLDERNYA BELUM ADA
        if (!File::isDirectory($this->path)) {
            //MAKA FOLDER TERSEBUT AKAN DIBUAT
            File::makeDirectory($this->path);
        }
		
        //MENGAMBIL FILE IMAGE DARI FORM
        $file = $request->file('image');
        //MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
       // $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
        //UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
        Image::make($request)->save($this->path . '/' . $file);
		
        //LOOPING ARRAY DIMENSI YANG DI-INGINKAN
        //YANG TELAH DIDEFINISIKAN PADA CONSTRUCTOR
        foreach ($this->dimensions as $row) {
            //MEMBUAT CANVAS IMAGE SEBESAR DIMENSI YANG ADA DI DALAM ARRAY 
            $canvas = Image::canvas($row, $row);
            //RESIZE IMAGE SESUAI DIMENSI YANG ADA DIDALAM ARRAY 
            //DENGAN MEMPERTAHANKAN RATIO
            $resizeImage  = Image::make($file)->resize($row, $row, function($constraint) {
                $constraint->aspectRatio();
            });
			
            //CEK JIKA FOLDERNYA BELUM ADA
            if (!File::isDirectory($this->path . '/' . $row)) {
                //MAKA BUAT FOLDER DENGAN NAMA DIMENSI
                File::makeDirectory($this->path . '/' . $row);
            }
        	
            //MEMASUKAN IMAGE YANG TELAH DIRESIZE KE DALAM CANVAS
            $canvas->insert($resizeImage, 'center');
            //SIMPAN IMAGE KE DALAM MASING-MASING FOLDER (DIMENSI)
            $canvas->save($this->path . '/' . $row . '/' . $fileName);
        }
        
        //SIMPAN DATA IMAGE YANG TELAH DI-UPLOAD
        Image_uploaded::create([
            'name' => $fileName,
            'dimensions' => implode('|', $this->dimensions),
            'path' => $this->path
        ]);
        //return redirect()->back()->with(['success' => 'Gambar Telah Di-upload']);
        return false;
    }
  
    public function store(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'nmbrg' => 'required',
        'gambar_satu' => 'image|mimes:jpeg,png,jpg,gif,svg',
        'gambar_dua' => 'image|mimes:jpeg,png,jpg,gif,svg',
        'gambar_tiga' => 'image|mimes:jpeg,png,jpg,gif,svg',
      ]);

      if ($validator->passes()) {
        $input = $request->all();
        $input['gambar_satu'] = time().'.'.$request->gambar_satu->getClientOriginalExtension();
        $input['gambar_dua'] = time().'.'.$request->gambar_dua->getClientOriginalExtension();
        $input['gambar_tiga'] = time().'.'.$request->gambar_tiga->getClientOriginalExtension();
        //$destination = base_path() . '/public/uploads';
       // $request->gambar_satu->move($destination, $input['gambar_satu']);
        $request->gambar_satu->move(public_path('uploads/mbarang/gambar_satu'), $input['gambar_satu']);
        $request->gambar_dua->move(public_path('uploads/mbarang/gambar_dua'), $input['gambar_dua']);
        $request->gambar_tiga->move(public_path('uploads/mbarang/gambar_tiga'), $input['gambar_tiga']);

        Barang::create($input);
      //  return response()->json(['success'=>'Berhasil']);
        return redirect()->route('barang.index')
                        ->with('success','Barang created successfully.');
      }

      return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function store_bak(Request $request)
    {  
        $validator = Validator::make($request->all(), [
            'judul' => 'required',
            'gambar_satu' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
          ]);
          if ($validator->passes()) {
            $input = $request->all();
            $input['gambar_satu'] = time().'.'.$request->gambar_satu->getClientOriginalExtension();
            $destination = base_path() . '/public/uploads';
            $request->gambar_satu->move($destination, $input['gambar_satu']);
    
            Upload_file::create($input);
            return response()->json(['success'=>'Berhasil']);
          }
      //  $this->upload($request);
        $arr   =   Barang::updateOrCreate([
                    'id' => $request->barang_id],
                    ['nmbrg' => $request->name
                    , 'idkatbrg' => $request->id_kategori_brg
                    , 'stok' => $request->stok
                    , 'harga' => $request->harga
                    , 'berat' => $request->berat
                    , 'kondisi_barang' => $request->cb_kondisi_brg
                    , 'deskripsi_barang' => $request->deskripsi_barang
                    , 'status' => $request->status
                    , 'gambar_satu' => $request->image
                    ]
        );        
        
     //   if ($request->hasFile('image')) {
            $photo = $request->file('image')->getClientOriginalName();
            $destination = base_path() . '/public/uploads';
            $request->file('image')->move($destination, $photo);
       // }

        return Response::json($arr);
    
    }

    public function update(Request $request, $id)
    {
        
        $arr = Barang::find($id);
  
        $arr->update([
            'nmbrg' => $request->name
                    , 'idkatbrg' => $request->id_kategori_brg
                    , 'stok' => $request->stok
                    , 'harga' => $request->harga
                    , 'berat' => $request->berat
                    , 'kondisi_barang' => $request->cb_kondisi_brg
                    , 'deskripsi_barang' => $request->deskripsi_barang
                    , 'status' => $request->status
        ]);

        return Response::json($arr);
      // return redirect()->route('users.index')
          // ->with('success','User updated successfully');
    }

    public function edit($id)
    {   
        $where = array('id' => $id);
        $arr  = Barang::where($where)->first();
     
        return Response::json($arr);
    }

    public function destroy($id)
    {
        $arr = Barang::where('id',$id)->delete();
    
        return Response::json($arr);
    }

    public function show()
    {
         return response()->json(HobbyDet::orderBy('name', 'ASC')->get());
    }
}
