<?php

namespace App\Http\Controllers\Transaksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaksi\Barang;
use App\Models\Master\HobbyDet;
use App\Lelang;
use App\Customer;
use Redirect,Response;
use DataTables;
use App\Http\Requests\Master\BarangsRequest;
use DB;
// use App\Image_uploaded;
// use Carbon\Carbon;
// use Image;
use File;
use Illuminate\Support\Facades\Input;
use Validator;

class TLelangController extends Controller
{ 
    public $path;
    public $dimensions;

    public function __construct()
    {
        //DEFINISIKAN PATH
        $this->path = storage_path('app/public/images');
        //DEFINISIKAN DIMENSI
        $this->dimensions = ['245', '300', '500'];
    }
    public function create(){
        $kategori = HobbyDet::all();
        return view('backend.transaksi.lelang.create', compact('kategori'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lelang = DB::table('lelangs')
        ->leftjoin('customers', 'lelangs.userid', '=', 'customers.id')
        ->leftjoin('hobby', 'lelangs.idkatitem', '=', 'hobby.id')
        ->leftjoin('stlelangs', 'lelangs.idstlelang', '=', 'stlelangs.id')
        ->leftjoin('customers as winbid', 'lelangs.pemenang_bid', '=', 'winbid.id')
        ->select('lelangs.*','customers.name as nmcustomer', 'hobby.name as nmkatitem', 'stlelangs.name as nmstlelang', 'winbid.name as pemenang')
        ->get();

        if(request()->ajax()) {
            return datatables()->of($lelang)
            ->addColumn('view_pemenang', function ($lelang) {
                if(!empty($lelang->pemenang_bid)){
                    return '
                    <a href="javascript:void(0);" id="lihat-member" data-toggle="tooltip" data-original-title="Cancel" data-id="'.$lelang->pemenang_bid.'" class="btn_view btn-sm btn-primary">
                    '.$lelang->pemenang.'
                     </a>';   
                } else {
                    return '-';   
                }
                   
               
                })
            ->addColumn('action', 'backend.transaksi.lelang.action')
            ->rawColumns(['view_pemenang','action'])
            ->addIndexColumn()
            ->make(true);
        }
        return view('backend.transaksi.lelang.index');
    }

    public function upload($request)
    {
        // $this->validate($image, [
        //     'image' => 'required|image|mimes:jpg,png,jpeg'
        // ]);
		
        //JIKA FOLDERNYA BELUM ADA
        if (!File::isDirectory($this->path)) {
            //MAKA FOLDER TERSEBUT AKAN DIBUAT
            File::makeDirectory($this->path);
        }
		
        //MENGAMBIL FILE IMAGE DARI FORM
        $file = $request->file('image');
        //MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
       // $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
        //UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
        Image::make($request)->save($this->path . '/' . $file);
		
        //LOOPING ARRAY DIMENSI YANG DI-INGINKAN
        //YANG TELAH DIDEFINISIKAN PADA CONSTRUCTOR
        foreach ($this->dimensions as $row) {
            //MEMBUAT CANVAS IMAGE SEBESAR DIMENSI YANG ADA DI DALAM ARRAY 
            $canvas = Image::canvas($row, $row);
            //RESIZE IMAGE SESUAI DIMENSI YANG ADA DIDALAM ARRAY 
            //DENGAN MEMPERTAHANKAN RATIO
            $resizeImage  = Image::make($file)->resize($row, $row, function($constraint) {
                $constraint->aspectRatio();
            });
			
            //CEK JIKA FOLDERNYA BELUM ADA
            if (!File::isDirectory($this->path . '/' . $row)) {
                //MAKA BUAT FOLDER DENGAN NAMA DIMENSI
                File::makeDirectory($this->path . '/' . $row);
            }
        	
            //MEMASUKAN IMAGE YANG TELAH DIRESIZE KE DALAM CANVAS
            $canvas->insert($resizeImage, 'center');
            //SIMPAN IMAGE KE DALAM MASING-MASING FOLDER (DIMENSI)
            $canvas->save($this->path . '/' . $row . '/' . $fileName);
        }
        
        //SIMPAN DATA IMAGE YANG TELAH DI-UPLOAD
        Image_uploaded::create([
            'name' => $fileName,
            'dimensions' => implode('|', $this->dimensions),
            'path' => $this->path
        ]);
        //return redirect()->back()->with(['success' => 'Gambar Telah Di-upload']);
        return false;
    }
  
    public function store(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'nmbrg' => 'required',
        'gambar_satu' => 'image|mimes:jpeg,png,jpg,gif,svg',
        'gambar_dua' => 'image|mimes:jpeg,png,jpg,gif,svg',
        'gambar_tiga' => 'image|mimes:jpeg,png,jpg,gif,svg',
      ]);

      if ($validator->passes()) {
        $input = $request->all();
        $input['gambar_satu'] = time().'.'.$request->gambar_satu->getClientOriginalExtension();
        $input['gambar_dua'] = time().'.'.$request->gambar_dua->getClientOriginalExtension();
        $input['gambar_tiga'] = time().'.'.$request->gambar_tiga->getClientOriginalExtension();
        //$destination = base_path() . '/public/uploads';
       // $request->gambar_satu->move($destination, $input['gambar_satu']);
        $request->gambar_satu->move(public_path('uploads/mlelang/gambar_satu'), $input['gambar_satu']);
        $request->gambar_dua->move(public_path('uploads/mlelang/gambar_dua'), $input['gambar_dua']);
        $request->gambar_tiga->move(public_path('uploads/mlelang/gambar_tiga'), $input['gambar_tiga']);

        Barang::create($input);
      //  return response()->json(['success'=>'Berhasil']);
        return redirect()->route('lelang.index')
                        ->with('success','Barang created successfully.');
      }

      return response()->json(['error'=>$validator->errors()->all()]);
    }

   
    public function update(Request $request)
    {
        
        $arr = Lelang::find($request->list_lelang_id);
  
        $arr->update([
            'idstlelang' => $request->idstlelang
        ]);

       // return Response::json($arr);
       return redirect()->route('list-lelang.index');
          // ->with('success','User updated successfully');
    }

    public function edit($id)
    {   
         $where = array('id' => $id);
         $arr  = Lelang::where($where)->first();

        
        return Response::json($arr);
    }

    public function get_member_by_id($id)
    {   
        //  $where = array('id' => $id);
        //  $arr  = Customer::where($where)->first();

         $arr = DB::table('customers')
         ->leftjoin('banks', 'banks.id', '=', 'customers.idbank')
         ->leftjoin('jobs', 'jobs.id', '=', 'customers.idpekerjaan')
         ->leftjoin('provinsis', 'provinsis.id', '=', 'customers.idprovinsi')
         ->leftjoin('kota', 'kota.id', '=', 'customers.idkota')
         ->leftjoin('kecamatan', 'kecamatan.id', '=', 'customers.idkecamatan')
         ->leftjoin('kelurahan', 'kelurahan.id', '=', 'customers.idkelurahan')
         ->select('customers.*','banks.name as nmbank'
                 ,'jobs.name as nmpekerjaan'
                 ,'provinsis.name as provinsi'
                 ,'kota.name as kotkab'
                 ,'kecamatan.name as kecamatan'
                 ,'kelurahan.name as kelurahan'
                 )
         ->where('customers.id','=',$id)
         ->first();

        
        return Response::json($arr);
    }


  
    public function show()
    {
         return response()->json(HobbyDet::orderBy('name', 'ASC')->get());
    }
}
