<?php

namespace App\Http\Controllers\Transaksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaksi\Barang;
use App\Models\Master\HobbyDet;
use App\Lelang;
use App\Topups;
use App\Customer;
use App\Post;
use Redirect,Response;
use DataTables;
use App\Http\Requests\Master\BarangsRequest;
use DB;
// use App\Image_uploaded;
// use Carbon\Carbon;
// use Image;
use File;
use Illuminate\Support\Facades\Input;
use App\Http\Resources\PostCollection;
use Validator;

class TopupController extends Controller
{
    public function index(){
        $topup = DB::table('topups')
        ->leftjoin('customers', 'customers.id', '=', 'topups.idcustomer')
        //->where('topups.status','<>','CANCELED')
        ->select('topups.*', 'customers.name as nmcustomer')
        ->get();

        if(request()->ajax()) {
            return datatables()->of($topup)
            ->addColumn('image', function ($topup) { 
                $url= asset('uploads/bukti_transfer/'.$topup->bukti_transfer);
                return '<a href="'.$url.'"><img src="'.$url.'" border="0" height="100" width="100" class="img-rounded" align="center" /></a>';
            })
            ->addColumn('action', function ($topup) {
                if($topup->status == 'SUCCESS'){
                    return '<span class="confirm btn-sm btn-info">
                    Top Up Berhasil
                    </span>&nbsp;
                    <a href="javascript:void(0);" id="delete-topup" data-toggle="tooltip" data-original-title="Cancel" data-id="'.$topup->id.'" class="delete btn-sm btn-warning">
                    Batalkan
                     </a>';
                } else if($topup->status == 'CANCELED'){
                    return '<span class="confirm btn-sm btn-danger">
                    Top Up Batal
                    </span>';
                } else{
                    return '
                    <a href="javascript:void(0);" id="confirm-topup" data-toggle="tooltip" data-original-title="Confirm" data-id="'.$topup->id.'" class="confirm btn-sm btn-success">
                    Confirm
                    </a>&nbsp;
                    <a href="javascript:void(0);" id="delete-topup" data-toggle="tooltip" data-original-title="Cancel" data-id="'.$topup->id.'" class="delete btn-sm btn-warning">
                    Batalkan
                     </a>';   
                }
              
                })
         //   ->addColumn('action', 'backend.transaksi.topup.action')
            ->rawColumns(['image','action'])
            ->addIndexColumn()
            ->make(true);
        }
        return view('backend.transaksi.topup.index');
    }

    // public function index(){
    //     return new PostCollection(Post::all());
    // }

    public function store(Request $request)
    {  
        date_default_timezone_set('Asia/Jakarta');
        $arr   =   Topups::updateOrCreate(['id' => $request->topup_id],
                    ['idcustomer' => $request->member
                    , 'nominal' => $request->nominal
                    , 'status' => 'SUCCESS'
                    ]
                    );
        
        $customer = Customer::find($request->member);
        $customer->saldo =  $customer->saldo + $request->nominal;
        $customer->update();            
                    
        return Response::json($arr);
    
    }

    public function edit($id)
    {   

        $where = array('id' => $id);
        $arr  = Topups::where($where)->first();
     
        return Response::json($arr);
    }

    public function destroy($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $arr = Topups::find($id);
  
        $arr->update([
             'status' => 'CANCELED'
        ]);

        $arrtopup  = Topups::where('id',$id)->first();
        $idcustomer = $arrtopup->idcustomer;
        $nominal = $arrtopup->nominal;

        $customer = Customer::find($idcustomer);
        $customer->saldo =  $customer->saldo - $nominal;
        $customer->update();         

        return Response::json($arr);
    }
   
    public function confirm($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $arr = Topups::find($id);
  
        $arr->update([
             'status' => 'SUCCESS'
        ]);

        $arrtopup  = Topups::where('id',$id)->first();
        $idcustomer = $arrtopup->idcustomer;
        $nominal = $arrtopup->nominal;

        $customer = Customer::find($idcustomer);
        $customer->saldo =  $customer->saldo + $nominal;
        $customer->update();         

        return Response::json($arr);
    }

    public function getmember()
    {
     
         return response()->json(Customer::orderBy('name', 'ASC')->get());
    }
}