<?php

namespace App\Http\Controllers\Transaksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaksi\Barang;
use App\Models\Master\HobbyDet;
use App\Lelang;
use App\Topups;
use App\Customer;
use App\Post;
use App\Order;
use App\Orderdet;
use App\StatusOrder;
use App\Complaint;
use App\Complaintdet;
use Redirect,Response;
use DataTables;
use App\Http\Requests\Master\BarangsRequest;
use DB;
// use App\Image_uploaded;
// use Carbon\Carbon;
use Image;
use File;
use Auth;
use Illuminate\Support\Facades\Input;
use App\Http\Resources\PostCollection;
use Validator;

class TComplaintController extends Controller
{
    public function index()
    {
        $complaints = DB::table('complaints')
        ->leftjoin('customers', 'customers.id', '=', 'complaints.iduser')
        ->leftjoin('complaintdets', 'complaintdets.idcomplaint', '=', 'complaints.id')
       //->where('topups.status','<>','CANCELED')
       ->groupby('complaints.id')
       ->select('complaints.*','customers.name as nmcustomer')
       ->get();

       if(request()->ajax()) {
           return datatables()->of($complaints)
           ->addColumn('image', function ($complaints) { 
                $url= asset('uploads/komplain/'.$complaints->file);
                return '<a href="'.$url.'"><img src="'.$url.'" border="0" height="100" width="100" class="img-rounded" align="center" /></a>';
            })
             ->addColumn('action', 'backend.transaksi.complaint.action')
           ->rawColumns(['image','action'])
           ->addIndexColumn()
           ->make(true);
       }
       return view('backend.transaksi.complaint.index');
    }

    public function answer(Request $request)
    {  
        date_default_timezone_set('Asia/Jakarta');     
        $request->validate([
            'file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:8192',
        ]); 

        if ($files = $request->file('file')) {
            $ImageUpload = Image::make($files);
            $originalPath = public_path('uploads/komplain/');
            $ImageUpload->save($originalPath.time().$files->getClientOriginalName());
            
            $insert_complaintdets = new Complaintdet();
            $insert_complaintdets->file = time().$files->getClientOriginalName();
            $insert_complaintdets->answer = $request->answer;
            $insert_complaintdets->idcomplaint = $request->complaint_id;
            $insert_complaintdets->iduser = Auth::user()->name;
            $insert_complaintdets->save();
        } else{
            $insert_complaintdets = new Complaintdet();
           // $insert_complaintdets->file = time().$files->getClientOriginalName();
            $insert_complaintdets->answer = $request->answer;
            $insert_complaintdets->idcomplaint = $request->complaint_id;
            $insert_complaintdets->iduser =  Auth::user()->name;
            $insert_complaintdets->save();
        }    
        
         
      

          return Response::json($insert_complaintdets);
    }

    public function complaintdet($id){
        $complaintdet = DB::table('complaintdets')
        ->leftjoin('complaints', 'complaints.id', '=', 'complaintdets.idcomplaint')
      //  ->leftjoin('users', 'users.id', '=', 'complaintdets.iduser')
       ->where('complaintdets.idcomplaint','=',$id)
       ->select('complaintdets.*','complaints.notiket as notiket')
       ->get();

       if(request()->ajax()) {
           return datatables()->of($complaintdet)
           ->addColumn('file', function ($complaintdet) { 
            $url= asset('uploads/komplain/'.$complaintdet->file);
            return '<a href="'.$url.'"><img src="'.$url.'" border="0" height="100" width="100" class="img-rounded" align="center" /></a>';
             })
            ->rawColumns(['file']) 
           ->addIndexColumn()
           ->make(true);
       }
       return view('backend.transaksi.complaint.index');
    }

}