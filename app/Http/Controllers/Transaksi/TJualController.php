<?php

namespace App\Http\Controllers\Transaksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaksi\Barang;
use App\Models\Master\HobbyDet;
use App\Lelang;
use App\Topups;
use App\Customer;
use App\Post;
use App\Order;
use App\Orderdet;
use App\StatusOrder;
use Redirect,Response;
use DataTables;
use App\Http\Requests\Master\BarangsRequest;
use DB;
// use App\Image_uploaded;
// use Carbon\Carbon;
// use Image;
use File;
use Illuminate\Support\Facades\Input;
use App\Http\Resources\PostCollection;
use Validator;

class TJualController extends Controller
{

    public function index()
    {

             $orders = DB::table('orders')
         ->leftjoin('customers', 'customers.id', '=', 'orders.idcustomer')
         ->leftjoin('status_orders', 'status_orders.id', '=', 'orders.status')
        //->where('topups.status','<>','CANCELED')
        ->select('orders.*','customers.name as nmcustomer','status_orders.name as nmstatus')
        ->get();

        if(request()->ajax()) {
            return datatables()->of($orders)
            // ->addColumn('action', function ($orders) {
            //         return '
            //         <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$orders->id.'" data-original-title="Edit" class="btn-sm btn-success modalDaftarJual">
            //             Konfirmasi
            //         </a>';                 
            //     })
             ->addColumn('action', 'backend.transaksi.daftarjual.action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        $storders = StatusOrder::all();
        return view('backend.transaksi.daftarjual.index',compact('storders'));
    }

    public function orderdet($id){
        $orderdets = DB::table('orderdets')
        ->leftjoin('customers', 'customers.id', '=', 'orderdets.idpenjual')
        ->leftjoin('status_orders', 'status_orders.id', '=', 'orderdets.stbrg')
       ->where('orderdets.idorder','=',$id)
       ->select('orderdets.*','customers.name as nmpenjual', 'status_orders.name as nmstbrg')
       ->get();

       if(request()->ajax()) {
           return datatables()->of($orderdets)
           ->addIndexColumn()
           ->make(true);
       }
       return view('backend.transaksi.daftarjual.index');
    }


    public function verifikasi($id)
    {   
        $orders = DB::table('orders')
        ->leftjoin('customers', 'customers.id', '=', 'orders.idcustomer')
        ->leftjoin('status_orders', 'status_orders.id', '=', 'orders.status')
        ->where('orders.id','=',$id)
       ->select('orders.*','customers.name as nmcustomer','status_orders.name as nmstatus')
       ->first();
     
        return Response::json($orders);
    }

    public function update(Request $request){
        $jual = Order::find($request->order_id);
        $jual->status =  $request->status;
        $jual->update();            
                    
        return Response::json($jual);
    }

   
}