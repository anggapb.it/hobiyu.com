<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
use App\Models\Master\Hobby; 
use App\Models\Master\HobbyDet;
use App\Models\Master\Bid;
use App\Models\Master\Sponsor;
use App\Lelang; 
use App\Customer;
use App\Members;
use App\Profile;
use App\Stlelang;
use Auth;
use Carbon\Carbon; 
use jpmurray\LaravelCountdown\Countdown;


class LelangController extends Controller
{
	 public function index(Request $request, $id, Hobby $hobi, Lelang $lelang, Bid $bid, Stlelang $stlelang, HobbyDet $hbdt, Customer $usr)
    {
        $profile = Profile::where('id',1)->first();
        date_default_timezone_set('Asia/Jakarta');
        $hobbies = $hobi::where("id_status", "ACTIVE")->get();
        $sponsors = Sponsor::where("id_status", "ACTIVE")->get();
        $cari = $request->cari;
        
        if($id == 'all'){
            if(!empty($cari)){ 
                $lelang = \DB::table('lelangs')
                ->select("lelangs.*")
                ->where('nmitem','like',"%".$cari."%")
                ->orWhere('id', 'like', "%{$cari}%")
                ->paginate(6);
            } else{
                $lelang = Lelang::latest()->paginate(6); 
            }
        } else if(strpos($id, 'H') !== false){
            if(!empty($cari)){ 
                $lelang = \DB::table('lelangs')
                ->select("lelangs.*")
                ->where('nmitem','like',"%".$cari."%")
                ->orWhere('id', 'like', "%{$cari}%")
                ->paginate(6);
            } else{
                $lelang = Lelang::where('idkatitem',substr($id,1))->paginate(6);     
            }
        } else{
            if(!empty($cari)){ 
                $lelang = \DB::table('lelangs')
                ->select("lelangs.*")
                ->where('nmitem','like',"%".$cari."%")
                ->orWhere('id', 'like', "%{$cari}%")
                ->paginate(6);
            } else{
                $lelang = Lelang::where('idhobidet',$id)->paginate(6);     
            }
        }

       
        
        return view('web.lelang.index',compact('hobbies','hobi','lelang','bid','stlelang','profile','hbdt','sponsors','usr'))
        ->with('i', (request()->input('page', 1) - 1) * 6);
    }

    public function detail_hobby($id)
      {
        $hd = HobbyDet::where("id_hobi","=",$id)->get();
        return response()->json(['dethobby' => $hd], 200);
      }
}