<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
use App\Models\Master\Hobby; 
use App\Models\Master\HobbyDet;
use App\Models\Master\Bid;
use App\Models\Master\Sponsor;
use App\Models\Master\TypeTransaction;
use App\Models\Transaksi\Barang;
use App\Lelang; 
use App\Customer;
use App\Members;
use App\Profile;
use App\Stlelang;
use App\Order;
use App\Orderdet;
use App\Setting;
use Auth;
use Carbon\Carbon; 
use jpmurray\LaravelCountdown\Countdown;
use Cart;
use DB;


class JualController extends Controller
{
    public function index(Request $request, $id, Hobby $hobi, Lelang $lelang, Bid $bid, Stlelang $stlelang, HobbyDet $hbdt)
    {
        $profile = Profile::where('id',1)->first();
        date_default_timezone_set('Asia/Jakarta');
        $hobbies = $hobi::where("id_status", "ACTIVE")->get();
        $sponsors = Sponsor::where("id_status", "ACTIVE")->get();
        $cari = $request->cari;
        
        if($id == 'all'){
            if(!empty($cari)){ 
                $barang = \DB::table('barang')
                ->select("barang.*")
                ->where('nmbrg','like',"%".$cari."%")
                ->orWhere('kdbrg', 'like', "%{$cari}%")
                ->orWhere('lokasi_barang', 'like', "%{$cari}%")
                ->paginate(6);
            } else{
                $barang = Barang::latest()->paginate(6); 
            }
        } else if(strpos($id, 'H') !== false){
            if(!empty($cari)){ 
                $barang = \DB::table('barang')
                ->select("barang.*")
                ->where('nmbrg','like',"%".$cari."%")
                ->orWhere('kdbrg', 'like', "%{$cari}%")
                ->orWhere('lokasi_barang', 'like', "%{$cari}%")
                ->paginate(6);
            } else{
                $barang = Barang::where('idhobi',substr($id,1))->paginate(6);     
            }
        } else{
            if(!empty($cari)){ 
                $barang = \DB::table('barang')
                ->select("barang.*")
                ->where('nmbrg','like',"%".$cari."%")
                ->orWhere('kdbrg', 'like', "%{$cari}%")
                ->orWhere('lokasi_barang', 'like', "%{$cari}%")
                ->paginate(6);
            } else{
                $barang = Barang::where('idhobidet',$id)->paginate(6);     
            }
        }

    
        return view('web.jual.index',compact('hobbies','hobi','barang','profile','hbdt','sponsors'))
        ->with('i', (request()->input('page', 1) - 1) * 6);
    }

    public function detail($id){ 
        $profile = Profile::where('id',1)->first();
        $hobbies = Hobby::where("id_status", "ACTIVE")->get();
        $barang = Barang::where('id',$id)->first();
        $customer = Customer::where('id',$barang->userid)->first();
        $sponsors = Sponsor::where("id_status", "ACTIVE")->get();
        

        return view('web.jual.detail',compact('id','hobbies','barang','customer','profile','sponsors'))->with('i');
    }


    public function addCart(Request $res){
        $userid = Auth::guard('customer')->user()->id;
        $add = Cart::session($userid)->add([
             'id' => $res->idbrg,   
             'name' => $res->nmbrg,   
             'price' => $res->harga,   
             'quantity' => $res->qty,   
        ]);   

        if($add){
            // return view('web.jual.detail', [
            //     'cart' => Cart::session($userid)->getContent()
            // ]);

            return redirect()->route('jual.detail',$res->idbrg)
                    ->with('success','Berhasil ditambahkan ke keranjang');
           // return $add;
        }
    }
    
    public function removeCart($id){
        $userid = Auth::guard('customer')->user()->id;
       
        $remove = Cart::session($userid)->remove($id);   

        if($remove){
            // return view('web.jual.detail', [
            //     'cart' => Cart::session($userid)->getContent()
            // ]);

            return redirect()->back();
           // return $add;
        }
    }

    public function updateCart(Request $request){
        $userid = Auth::guard('customer')->user()->id;
        $barang = Barang::where('id','=',$request->id)->first()->stok;

        $update = Cart::session($userid)->update($request->id, array(
            'quantity' => array(
                'relative' => false,
                'value' => $request->qty
            ),
          ));
        
          if($update){
              return redirect()->back();
          }
    }

    public function checkout(Barang $barang){ 
        $profile = Profile::where('id',1)->first();
        $hobbies = Hobby::where("id_status", "ACTIVE")->get();
        $sponsors = Sponsor::where("id_status", "ACTIVE")->get();
        $method_byrs = TypeTransaction::all();
        
        if(Cart::session(Auth::guard('customer')->user()->id)->getContent()->count() > 0){
            return view('web.jual.checkout',compact('hobbies','profile','sponsors','barang','method_byrs'))->with('i');
        }else {
            return redirect('/');
        }
        
    }

   
   
    public function insertCheckout(Request $request){ 
        $request->validate([
            'alamat' => 'required',
        ]);

        date_default_timezone_set('Asia/Jakarta');
        $profile = Profile::where('id',1)->first();
        $getBtsByr = Setting::where('id',2)->first()->nilai; // batas bayar dalam menit
        $hobbies = Hobby::where("id_status", "ACTIVE")->get();
        $sponsors = Sponsor::where("id_status", "ACTIVE")->get();
        $bank1 = DB::table('banks')->where('id', $profile->bank_satu)->first()->name;
        $bank2 = DB::table('banks')->where('id', $profile->bank_dua)->first()->name;
        $bank3 = DB::table('banks')->where('id', $profile->bank_tiga)->first()->name;

        $time = new \DateTime(date('Y-m-d H:i:s'));
        $time->add(new \DateInterval('PT' . $getBtsByr . 'M'));

        $stamp = $time->format('Y-m-d H:i:s');

        $order = Order::firstOrCreate([
            'noorder' => $this->generateRandomString(),
            'idcustomer' => Auth::guard('customer')->user()->id,
            'idmethod_bayar' => $request->method_bayar,
            'idkurir' => 1,
            'ongkir' => 0,
            'total' => Cart::session(Auth::guard('customer')->user()->id)->getTotal(),
            'alamat_kirim' => $request->alamat,
            'status' => 1,
            'batas_bayar' => $stamp,
            'biayaadmin' => 0
           ]);

           
    
             foreach($request->get('idbrg') as $i => $idbrg){
                
                 $nmbrg = $request->get('nmbrg');
                 $quantity = $request->get('quantity');
                 $harga = $request->get('harga');
               

                 $brg = Barang::find($idbrg);

                 if($brg->stok !== 0){
                    $order->orderdets_many()->create([
                        'idbarang' => $idbrg,
                        'nmbrg' => $nmbrg[$i],
                        'qty' => $quantity[$i],
                        'harga' => $harga[$i],
                        'idpenjual' => $brg->userid
                     ]);
                     $brg->stok = $brg->stok - $quantity[$i]; 
                     $brg->update();   
                 }
              
             }

             Cart::session(Auth::guard('customer')->user()->id)->clear();

        if($order){
            return  redirect()->route('jual.cart.success-checkout', $order->id);
           // return view('web.jual.bayar',compact('id','hobbies','profile','sponsors','bank1','bank2','bank3','dataorder'))->with('i');
        } 
        
    }

    public function generateRandomString($length = 9) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString.date('y');
    }

    public function successCheckout($id){
        $dataorder = Order::where("id", $id)->first();
        $profile = Profile::where('id',1)->first();
        $hobbies = Hobby::where("id_status", "ACTIVE")->get();
        $sponsors = Sponsor::where("id_status", "ACTIVE")->get();
        $bank1 = DB::table('banks')->where('id', $profile->bank_satu)->first()->name;
        $bank2 = DB::table('banks')->where('id', $profile->bank_dua)->first()->name;
        $bank3 = DB::table('banks')->where('id', $profile->bank_tiga)->first()->name;

        return view('web.jual.bayar',compact('id','hobbies','profile','sponsors','bank1','bank2','bank3','dataorder'))->with('i');
    }

}