<?php

namespace App\Http\Controllers\Auth;
use App\User;
use App\Customer;
use App\Writer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Profile;
use App\Setting;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('guest:customer');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'status' => 'A',
            'klpuser' => 'ADMIN'
        ]);
    }

    public function showCustomerRegisterForm()
    {
        $profile = Profile::where('id',1)->first();
        return view('auth.memberregister', ['url' => 'customer'],compact('profile'));
    }

    public function showWriterRegisterForm()
    {
        return view('auth.register', ['url' => 'writer']);
    }

    protected function createCustomer(Request $request)
    {
       
        // $this->validator($request->all())->validate();
        // $admin = Customer::create([
        //     'name' => $request['name'],
        //     'email' => $request['email'],
        //     'phone' => $request['phone'],
        //     'password' => Hash::make($request['password']),
        // ]);
        // return redirect()->intended('login/customer');

        $rules=[
            'name' => 'required|unique:customers',
            'email' => 'required|email|unique:customers,email',
            'phone'=>'required|min:11|numeric|unique:customers',
            'password' => 'required|min:8',
            'password_confirmation' => 'required|min:8|same:password',
            'policy' => 'required'
        ];    

        $valPromo = Setting::where('id',3)->first();
        $saldo;
        if($valPromo->nilai == 1){
            $saldo = $valPromo->attribute;
        } else{
           $saldo = 0;  
        }
       
        $validator = \Validator::make($request->all(), $rules);
    
        if ($validator->fails()) {
          
            // get the error messages from the validator
            $messages = $validator->messages();

            // redirect our user back to the form with the errors from the validator
            return \Redirect::to('register/customer')
            ->withErrors($validator);
        } else {
            $admin = Customer::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'phone' => $request['phone'],
                'password' => Hash::make($request['password']),
                'status' => 'ACTIVE',
                'saldo' => $saldo
            ]);
            return redirect()->intended('login/customer');    
            // return response(array(
            //     'error' => false,
            //     'message' => 'Patient added successfully', 'patientid' => $post->patient_id,), 200);
        }
    }
}
