<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Customer;
use Auth;
use App\Profile;

class LoginController extends Controller
{
    
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:customer')->except('logout')->except('customerLogout');
     //   $this->middleware('guest:writer')->except('logout');
    }
    public function sms_login()
    {
        return view('auth.sms_login');
    }

    

    public function showCustomerLoginForm()
    {
        $profile = Profile::where('id',1)->first();
        return view('auth.memberlogin', ['url' => 'customer'],compact('profile'));
    }

    public function customerLogout()
    {
        if (Auth::guard('customer')->check()) {
        Auth::guard('customer')->logout();
        } 
        // elseif (Auth::guard('user')->check()) {
        // Auth::guard('user')->logout();
        // }
        return redirect('/');
    }

    public function customerLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|exists:customers|email',
            'password' => 'required|min:8' //confirmed
        ]);

        if (Auth::guard('customer')->attempt(['email' => $request->email, 'password' => $request->password, 'status' => 'ACTIVE'])) {

            return redirect()->intended('/customer/saldo');
        }
        return back()->withInput($request->only('email', 'remember'));
    }
}
