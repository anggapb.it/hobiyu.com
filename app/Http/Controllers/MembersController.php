<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Master\Members;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class MembersController extends Controller
{
    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest:member')->except('logout')->except('index');
    }
    
    // public function index(){
    //       return view('member.home');
    // }
    
    public function showLoginForm()
    {
        return view('web.signin');
    }
    
    public function showRegisterForm()
    {
        return view('web.signup');
    }
    
    // public function username()
    // {
    //         return 'username';
    // }
    
    protected function guard()
    {
          return Auth::guard('member');
    }
    
    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:50',
            'email' => 'required|email|max:100|unique:users,email',
            'password' => 'required|min:6',
            'confirm_password' => 'required|same:password'
        ]);
          $member = Members::create([
            'name' => $request->name,
            'email' => $request->email,
            'notelp' => $request->notelp,
            'password' => bcrypt($request->password),
            'status'    => 'A',
            'klpuser' => 'MEMBER'
        ]);
          $member->save();
       //   Auth::loginUsingId($member->id);
        //  return redirect('/');
          return redirect('/sign-member')->with('alert', 'Members berhasil ditambahkan, silahkan login!');
    }
}
