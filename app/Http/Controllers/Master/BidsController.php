<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
use App\Models\Master\Bid;
use Datatables;

class BidsController extends Controller
{
    public function index()
    {
        if(request()->ajax()) {
            return datatables()->of(Bid::select('*'))
            ->addColumn('action', 'backend.master.bids.action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        return view('backend.master.bids.index');
    }

    public function store(Request $request)
    {  
        $id = $request->bid_id;
        $arr   =   Bid::updateOrCreate(['id' => $id],
                    [
                        'harga' => $request->harga,
                        'keterangan' => $request->keterangan
                    ]
                    );        
        return Response::json($arr);
    
    }

    public function edit($id)
    {   
        $where = array('id' => $id);
        $arr  = Bid::where($where)->first();
     
        return Response::json($arr);
    }

    public function destroy($id)
        {
            $arr = Bid::where('id',$id)->delete();
        
            return Response::json($arr);
        }
}
