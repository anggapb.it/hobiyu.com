<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaksi\Barang;
use App\Models\Master\HobbyDet;
use App\Lelang;
use App\Topups;
use App\Customer;
use App\Post;
use Redirect,Response;
use DataTables;
use App\Http\Requests\Master\BarangsRequest;
use DB;
// use App\Image_uploaded;
// use Carbon\Carbon;
// use Image;
use File;
use Illuminate\Support\Facades\Input;
use App\Http\Resources\PostCollection;
use Validator;

class MemberController extends Controller
{
    public function index(){
        $member = DB::table('customers')
        ->leftjoin('banks', 'banks.id', '=', 'customers.idbank')
        //->where('members.status','<>','CANCELED')
        ->select('customers.*','banks.name as nmbank')
        ->get();

        if(request()->ajax()) {
            return datatables()->of($member)
            // ->addColumn('image', function ($member) { 
            //     $url= asset('uploads/bukti_transfer/'.$member->bukti_transfer);
            //     return '<a href="'.$url.'"><img src="'.$url.'" border="0" height="100" width="100" class="img-rounded" align="center" /></a>';
            // })
            ->addColumn('action', function ($member) {
                if($member->status == 'ACTIVE'){
                    return '
                    <a href="javascript:void(0);" id="banned-member" data-toggle="tooltip" data-original-title="Cancel" data-id="'.$member->id.'" class="delete btn-sm btn-danger">
                    Banned?
                     </a>';   
                } else {
                    return '
                    <a href="javascript:void(0);" id="active-member" data-toggle="tooltip" data-original-title="Confirm" data-id="'.$member->id.'" class="confirm btn-sm btn-success">
                    Aktifkan?
                    </a>';   
                }
                   
               
                })
         //   ->addColumn('action', 'backend.master.member.action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        return view('backend.master.member.index');
    }

    // public function index(){
    //     return new PostCollection(Post::all());
    // }

    

    public function banned($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $arr = Customer::find($id);
  
        $arr->update([
             'status' => 'BANNED'
        ]);


        return Response::json($arr);
    }
   
    public function active($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $arr = Customer::find($id);
  
        $arr->update([
             'status' => 'ACTIVE'
        ]);

        return Response::json($arr);
    }

    // public function getmember()
    // {
     
    //      return response()->json(Customer::orderBy('name', 'ASC')->get());
    // }
}