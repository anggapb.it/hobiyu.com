<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
use App\Models\Master\Hobby;
use Datatables;
use DB;

class HobbyController extends Controller
{
    public function index()
    {
        $hobby = DB::table('hobby')
        ->select('hobby.*')
        ->get();

        if(request()->ajax()) {
            return datatables()->of($hobby)
            ->addColumn('icon', function ($hobby) { 
                return '<center><span class="'.$hobby->icon.'"></span></center>';
            })
            ->addColumn('action', 'backend.master.hobbies.action')
            ->rawColumns(['icon','action']) 
            ->addIndexColumn()
            ->make(true);
        }
        return view('backend.master.hobbies.index');
    }

    public function store(Request $request)
    {  
        $id = $request->hobby_id;
        $arr   =   Hobby::updateOrCreate(['id' => $id],
                    [
                        'name' => $request->name,
                        'keterangan' => $request->keterangan,
                        'id_status' => $request->id_status,
                        'icon' => 'icon fa '.$request->icon
                    ]
                    );        
        return Response::json($arr);
    
    }

    public function edit($id)
    {   
        $where = array('id' => $id);
        $arr  = Hobby::where($where)->first();
     
        return Response::json($arr);
    }

    public function destroy($id)
        {
            $arr = Hobby::where('id',$id)->delete();
        
            return Response::json($arr);
        }
}
