<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
use App\Profile;
use App\Models\Master\Bank;
use Datatables;

class ProfileController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()) {
            return datatables()->of(Profile::select('*'))
            ->addColumn('action', 'backend.master.profile.action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        return view('backend.master.profile.index');
    }

    public function store(Request $request)
    {  
        $arr   =   Profile::updateOrCreate(['id' => $request->profile_id],
                    [
                        'brand_name' => $request->brand_name,
                        'phone1' => $request->phone1,
                        'phone2' => $request->phone2,
                        'address' => $request->address,
                        'email' => $request->email,
                        'facebook' => $request->facebook,
                        'twitter' => $request->twitter, 
                        'youtube' => $request->youtube,
                        'bank_satu' => $request->idbank1,
                        'bank_dua' => $request->idbank2,
                        'bank_tiga' => $request->idbank3,
                        'kdrek_satu' => $request->kdrek_satu,
                        'kdrek_dua' => $request->kdrek_dua,
                        'kdrek_tiga' => $request->kdrek_tiga,
                        'atasnama_satu' => $request->atasnama_satu,
                        'atasnama_dua' => $request->atasnama_dua,
                        'atasnama_tiga' => $request->atasnama_tiga,
                    ]
                    );        
        return Response::json($arr);
    
    }

    public function update(Request $request, $id)
    {
        
        $arr = Profile::find($id);
  
        $arr->update([
            'name' => $request->name 
        ]);

        return Response::json($arr);
      // return redirect()->route('users.index')
          // ->with('success','User updated successfully');
    }

    public function edit($id)
    {   
        $where = array('id' => $id);
        $arr  = Profile::where($where)->first();
     
        return Response::json($arr);
    }

    public function destroy($id)
    {
        $arr = Profile::where('id',$id)->delete();
    
        return Response::json($arr);
    }

    public function getbank()
    {
     
         return response()->json(Bank::orderBy('name', 'ASC')->get());
    }
}
