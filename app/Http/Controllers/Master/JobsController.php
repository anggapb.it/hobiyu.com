<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
use App\Models\Master\Job;
use Datatables;


class JobsController extends Controller
{
    public function index()
    {
        if(request()->ajax()) {
            return datatables()->of(Job::select('*'))
            ->addColumn('action', 'backend.master.jobs.action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        return view('backend.master.jobs.index');
    }

    public function store(Request $request)
    {  
        $id = $request->job_id;
        $arr   =   Job::updateOrCreate(['id' => $id],
                    ['name' => $request->name
                    ]
                    );        
        return Response::json($arr);
    
    }

    public function edit($id)
    {   
        $where = array('id' => $id);
        $arr  = Job::where($where)->first();
     
        return Response::json($arr);
    }

    public function destroy($id)
        {
            $arr = Job::where('id',$id)->delete();
        
            return Response::json($arr);
        }
}
