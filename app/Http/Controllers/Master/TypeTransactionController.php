<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
use App\Models\Master\TypeTransaction;
use Datatables;

class TypeTransactionController extends Controller
{
    public function index()
    {
        if(request()->ajax()) {
            return datatables()->of(TypeTransaction::select('*'))
            ->addColumn('action', 'backend.master.typetransactions.action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        return view('backend.master.typetransactions.index');
    }

    public function store(Request $request)
    {  
        $id = $request->type_transaction_id;
        $arr   =   TypeTransaction::updateOrCreate(['id' => $id],
                    [
                        'name' => $request->name,
                        'keterangan' => $request->keterangan
                    ]
                    );        
        return Response::json($arr);
    
    }

    public function edit($id)
    {   
        $where = array('id' => $id);
        $arr  = TypeTransaction::where($where)->first();
     
        return Response::json($arr);
    }

    public function destroy($id)
        {
            $arr = TypeTransaction::where('id',$id)->delete();
        
            return Response::json($arr);
        }
}
