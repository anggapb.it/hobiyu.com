<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
use App\Kelipatan;
use Datatables;

class KelipatanController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()) {
            return datatables()->of(Kelipatan::select('*'))
            ->addColumn('action', 'backend.master.kelipatan.action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        return view('backend.master.kelipatan.index');
    }

    public function store(Request $request)
    {  
        $arr   =   Kelipatan::updateOrCreate(['id' => $request->kelipatan_id],
                    ['name' => $request->name
                    ]
                    );        
        return Response::json($arr);
    
    }

    public function update(Request $request, $id)
    {
        
        $arr = Kelipatan::find($id);
  
        $arr->update([
            'name' => $request->name 
        ]);

        return Response::json($arr);
      // return redirect()->route('users.index')
          // ->with('success','User updated successfully');
    }

    public function edit($id)
    {   
        $where = array('id' => $id);
        $arr  = Kelipatan::where($where)->first();
     
        return Response::json($arr);
    }

    public function destroy($id)
    {
        $arr = Kelipatan::where('id',$id)->delete();
    
        return Response::json($arr);
    }
}
