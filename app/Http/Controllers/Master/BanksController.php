<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
use App\Models\Master\Bank;
use Datatables;
use App\Http\Requests\Master\BanksRequest;

class BanksController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()) {
            return datatables()->of(Bank::select('*'))
            ->addColumn('action', 'backend.master.banks.action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        return view('backend.master.banks.index');
    }

    public function store(BanksRequest $request)
    {  
        $bank   =   Bank::updateOrCreate(['id' => $request->bank_id],
                    ['name' => $request->name
                    , 'keterangan' => $request->keterangan
                    ]
                    );        
        return Response::json($bank);
    
    }

    public function update(BanksRequest $request, $id)
    {
        
        $bank = Bank::find($id);
  
        $bank->update([
            'name' => $request->name 
            , 'keterangan' => $request->keterangan
        ]);

        return Response::json($bank);
      // return redirect()->route('users.index')
          // ->with('success','User updated successfully');
    }

    public function edit($id)
    {   
        $where = array('id' => $id);
        $bank  = Bank::where($where)->first();
     
        return Response::json($bank);
    }

    public function destroy($id)
    {
        $bank = Bank::where('id',$id)->delete();
    
        return Response::json($bank);
    }
}
