<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
use App\Setting;
use Datatables;


class SettingController extends Controller
{
    public function index()
    {
        if(request()->ajax()) {
            return datatables()->of(Setting::select('*'))
            ->addColumn('action', 'backend.master.setting.action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
       
        return view('backend.master.setting.index');
    }

    public function store(Request $request)
    {  
        $id = $request->setting_id;
        $arr   =   Setting::updateOrCreate(['id' => $id],
                    [
                        'name' => $request->name,
                        'nilai' => $request->nilai,
                        'keterangan' => $request->keterangan, 
                        'attribute' => $request->attribute, 
                    ]
                    );        
        return Response::json($arr);
    
    }

    public function edit($id)
    {   
        $where = array('id' => $id);
        $arr  = Setting::where($where)->first();
     
        return Response::json($arr);
    }

    public function destroy($id)
        {
            $arr = Setting::where('id',$id)->delete();
        
            return Response::json($arr);
        }
}
