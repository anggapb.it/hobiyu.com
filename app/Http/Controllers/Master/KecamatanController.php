<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
use App\Models\Master\Kota;
use App\Models\Master\Kecamatan;
use Datatables;
use DB;


class KecamatanController extends Controller
{
    public function index()
    {
        $kecamatan = DB::table('kecamatan')
        ->leftjoin('kota', 'kecamatan.id_kota', '=', 'kota.id')
        ->select('kecamatan.*', 'kota.name as nmkota')
        ->get();

        if(request()->ajax()) {
            return datatables()->of($kecamatan)
            ->addColumn('action', 'backend.master.kecamatan.action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        return view('backend.master.kecamatan.index');
    }

    public function store(Request $request)
    {  
        $kecId = $request->kecamatan_id;
        $kec   =   Kecamatan::updateOrCreate(['id' => $kecId],
                    [
                        'name' => $request->name,
                        'id_kota' => $request->id_kota
                    ]
                    );        
        return Response::json($kec);
    
    }

    public function show()
    {
     
         return response()->json(Kota::orderBy('name', 'ASC')->get());
    }

    public function edit($id)
    {   
        $where = array('id' => $id);
        $arr  = Kecamatan::where($where)->first();
     
        return Response::json($arr);
    }


    public function destroy($id)
        {
            $arr = Kecamatan::where('id',$id)->delete();
        
            return Response::json($arr);
        }
}
