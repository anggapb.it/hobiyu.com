<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
use App\CategoryForums;
use Datatables;

class CategoryForumController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()) {
            return datatables()->of(CategoryForums::select('*'))
            ->addColumn('action', 'backend.master.categoryforum.action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        return view('backend.master.categoryforum.index');
    }

    public function store(Request $request)
    {  
        $arr   =   CategoryForums::updateOrCreate(['id' => $request->catforum_id],
                    ['name' => $request->name
                    ]
                    );        
        return Response::json($arr);
    
    }

    public function update(Request $request, $id)
    {
        
        $arr = CategoryForums::find($id);
  
        $arr->update([
            'name' => $request->name 
        ]);

        return Response::json($arr);
      // return redirect()->route('users.index')
          // ->with('success','User updated successfully');
    }

    public function edit($id)
    {   
        $where = array('id' => $id);
        $arr  = CategoryForums::where($where)->first();
     
        return Response::json($arr);
    }

    public function destroy($id)
    {
        $arr = CategoryForums::where('id',$id)->delete();
    
        return Response::json($arr);
    }
}
