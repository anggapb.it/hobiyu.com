<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response,File;
use App\Models\Master\Sponsor;
use Datatables;
Use Image;


class SponsorController extends Controller
{
    public function index()
    {
        if(request()->ajax()) {
            return datatables()->of(Sponsor::select('*'))
            ->addColumn('banner', function ($data) { 
                $url= asset('uploads/banner/'.$data->banner);
                return '<a href="'.$url.'"><img src="'.$url.'" border="0" height="200" width="100" class="img-rounded" align="center" /></a>';
            })
            ->addColumn('action', 'backend.master.sponsor.action')
            ->rawColumns(['banner','action'])
            ->addIndexColumn()
            ->make(true);
        }
       
        return view('backend.master.sponsor.index');
    }

    public function store(Request $request)
    {  
        $request->validate([
            'banner' => 'image|mimes:jpeg,png,jpg,gif,svg|max:8192',
        ]); 

       

        if ($files = $request->file('banner')) {
            $ImageUpload = Image::make($files);
            $originalPath = public_path('uploads/banner/');
            $ImageUpload->save($originalPath.time().$files->getClientOriginalName());
            
             $arr   =   Sponsor::updateOrCreate(['id' => $request->sponsor_id],
                    [
                        'name' => $request->name,
                        'type_sponsor' => $request->type_sponsor,
                        'id_status' => $request->id_status, 
                        'tglactive' => date("Y-m-d", strtotime($request->tglactive)),
                        'no_contract' => $request->no_contract, 
                        'time_contract' => $request->time_contract, 
                     //   'status_contract' => $request->status_contract,
                        'banner' => time().$files->getClientOriginalName(),
                        'link' => $request->link
                    ]
                    );        
        }

        return Response::json($arr);
    
    }

    public function edit($id)
    {   
        $where = array('id' => $id);
        $arr  = Sponsor::where($where)->first();
     
        return Response::json($arr);
    }

    public function destroy($id)
        {
            $arr = Sponsor::where('id',$id)->delete();
        
            return Response::json($arr);
        }
}
