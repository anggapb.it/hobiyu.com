<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
use App\Models\Master\Kota;
use App\Models\Master\Provinsi;
use Datatables;
use DB;

class KotaController extends Controller
{
    public function index()
    {
        $kota = DB::table('kota')
        ->leftjoin('provinsis', 'kota.id_provinsi', '=', 'provinsis.id')
        ->select('kota.*', 'provinsis.name as nmprovinsi')
        ->get();

        if(request()->ajax()) {
            return datatables()->of($kota)
            ->addColumn('action', 'backend.master.kota.action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        return view('backend.master.kota.index');
    }

    public function store(Request $request)
    {  
        $kotaId = $request->kota_id;
        $kota   =   Kota::updateOrCreate(['id' => $kotaId],
                    [
                        'name' => $request->name,
                        'id_provinsi' => $request->id_provinsi
                    ]
                    );        
        return Response::json($kota);
    
    }

    public function show()
    {
     
         return response()->json(Provinsi::orderBy('name', 'ASC')->get());
    }

    public function edit($id)
    {   
        $where = array('id' => $id);
        $arr  = Kota::where($where)->first();
     
        return Response::json($arr);
    }


    public function destroy($id)
        {
            $arr = Kota::where('id',$id)->delete();
        
            return Response::json($arr);
        }
}
