<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response,File;
use App\Slider;
use Datatables;
Use Image;

class SliderController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = \DB::table('sliders')
        ->select('sliders.*')
        ->get();

        if(request()->ajax()) {
            return datatables()->of(Slider::select('*'))
            ->addColumn('slider_image', function ($data) { 
                $url= asset('uploads/slider/'.$data->slider);
                return '<a href="'.$url.'"><img src="'.$url.'" border="0" height="100" width="250" class="img-rounded" align="center" /></a>';
            })
            ->addColumn('action', 'backend.master.slider.action')
            ->rawColumns(['slider_image','action'])
            ->addIndexColumn()
            ->make(true);
        }
        return view('backend.master.slider.index');
    }

    public function store(Request $request)
    {  
        $request->validate([
            'slider' => 'image|mimes:jpeg,png,jpg,gif,svg|max:8192',
        ]); 

       

        if ($files = $request->file('slider_image')) {
            $ImageUpload = Image::make($files);
            $originalPath = public_path('uploads/slider/');
            $ImageUpload->save($originalPath.time().$files->getClientOriginalName());
            
        
            $ins_slider = new Slider();
            $ins_slider->slider = time().$files->getClientOriginalName();
            $ins_slider->keterangan = $request->keterangan;
            $ins_slider->status = 'ACTIVE';
            $ins_slider->save();
        }

        // $ImageUpload = Image::make($files);
        // $originalPath = 'public/uploads/';
        // $ImageUpload->save($originalPath.time().$files->getClientOriginalName());

        // $ins_slider = new Slider([
        //     'slider' => $slider,
        //     'keterangan'=> $request->keterangan,
        //     'status'=> 'ACTIVE',
        // ]);

        // if(!empty($request->slider)){
        //     $request->slider->move(public_path('uploads/slider'), time().'.'.$request->slider_image->getClientOriginalExtension());
        //   }

        // $ins_slider->save();  

         // return redirect()->route('master.slider')->with('success','Created successfully.');

          return Response::json($ins_slider);
    }

    public function update(Request $request, $id)
    {
        
        $arr = Slider::find($id);
  
        $arr->update([
            'name' => $request->name 
        ]);

        return Response::json($arr);
      // return redirect()->route('users.index')
          // ->with('success','User updated successfully');
    }

    public function edit($id)
    {   
        $where = array('id' => $id);
        $arr  = Slider::where($where)->first();
     
        return Response::json($arr);
    }

    public function destroy($id)
    {
        $arr = Slider::where('id',$id)->delete();
    
        return Response::json($arr);
    }
}
