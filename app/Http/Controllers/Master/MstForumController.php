<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
use App\Forums;
use App\Models\Master\HobbyDet;
use Datatables;

class MstForumController extends Controller
{
    public function index()
    {
        if(request()->ajax()) {
            return datatables()->of(Forums::select('*'))
            ->addColumn('action', 'backend.master.mstforum.action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        $dethobi = HobbyDet::all();
        return view('backend.master.mstforum.index',compact('dethobi'));
    }

    public function store(Request $request)
    {  
        $id = $request->mstforum_id;
        $arr   =   Forums::updateOrCreate(['id' => $id],
                    [
                        'judul' => $request->judul,
                        'status' => $request->id_status
                    ]
                    );        
        return Response::json($arr);
    
    }

    public function edit($id)
    {   
        $where = array('id' => $id);
        $arr  = Forums::where($where)->first();
     
        return Response::json($arr);
    }

    public function destroy($id)
        {
            $arr = Forums::where('id',$id)->delete();
        
            return Response::json($arr);
        }
}
