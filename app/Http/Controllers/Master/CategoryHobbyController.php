<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
use App\Models\Master\CategoryHobby;
use Datatables;

class CategoryHobbyController extends Controller
{
    public function index()
    {
        if(request()->ajax()) {
            return datatables()->of(CategoryHobby::select('*'))
            ->addColumn('action', 'backend.master.category_hobbies.action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        return view('backend.master.category_hobbies.index');
    }

    public function store(Request $request)
    {  
        $id = $request->category_hobby_id;
        $arr   =   CategoryHobby::updateOrCreate(['id' => $id],
                    [
                        'name' => $request->name,
                        'keterangan' => $request->keterangan
                    ]
                    );        
        return Response::json($arr);
    
    }

    public function edit($id)
    {   
        $where = array('id' => $id);
        $arr  = CategoryHobby::where($where)->first();
     
        return Response::json($arr);
    }

    public function destroy($id)
        {
            $arr = CategoryHobby::where('id',$id)->delete();
        
            return Response::json($arr);
        }
}
