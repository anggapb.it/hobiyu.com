<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
use App\Models\Master\Provinsi;
use Datatables;

class ProvinsiController extends Controller
{
    public function index()
    {
        if(request()->ajax()) {
            return datatables()->of(Provinsi::select('*'))
            ->addColumn('action', 'backend.master.provinsis.action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        return view('backend.master.provinsis.index');
    }

    public function store(Request $request)
    {  
        $provId = $request->provinsi_id;
        $prov   =   Provinsi::updateOrCreate(['id' => $provId],
                    ['name' => $request->name
                    ]
                    );        
        return Response::json($prov);
    
    }

    public function edit($id)
    {   
        $where = array('id' => $id);
        $arr  = Provinsi::where($where)->first();
     
        return Response::json($arr);
    }

    public function destroy($id)
        {
            $arr = Provinsi::where('id',$id)->delete();
        
            return Response::json($arr);
        }
}
