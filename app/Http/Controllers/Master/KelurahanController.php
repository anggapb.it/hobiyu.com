<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
use App\Models\Master\Kecamatan;
use App\Models\Master\Kelurahan;
use Datatables;
use DB;


class KelurahanController extends Controller
{
    public function index()
    {
        $kelurahan = DB::table('kelurahan')
        ->leftjoin('kecamatan', 'kelurahan.id_kecamatan', '=', 'kelurahan.id')
        ->select('kelurahan.*', 'kecamatan.name as nmkecamatan')
        ->get();

        if(request()->ajax()) {
            return datatables()->of($kelurahan)
            ->addColumn('action', 'backend.master.kelurahan.action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        return view('backend.master.kelurahan.index');
    }

    public function store(Request $request)
    {  
        $kecId = $request->kelurahan_id;
        $kec   =   Kelurahan::updateOrCreate(['id' => $kecId],
                    [
                        'name' => $request->name,
                        'id_kecamatan' => $request->id_kecamatan
                    ]
                    );        
        return Response::json($kec);
    
    }

    public function show()
    {
     
         return response()->json(Kecamatan::orderBy('name', 'ASC')->get());
    }

    public function edit($id)
    {   
        $where = array('id' => $id);
        $arr  = Kelurahan::where($where)->first();
     
        return Response::json($arr);
    }


    public function destroy($id)
        {
            $arr = Kelurahan::where('id',$id)->delete();
        
            return Response::json($arr);
        }
}
