<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
use App\Models\Master\HobbyDet;
use App\Models\Master\Hobby;
use App\Models\Master\CategoryHobby;
use Datatables;
use DB;


class HobbyDetController extends Controller
{
    public function index()
    {
        $hobbydet = DB::table('hobby_det')
            ->leftjoin('hobby', 'hobby_det.id_hobi', '=', 'hobby.id')
            ->leftjoin('category_hobby AS cathobisatu', 'cathobisatu.id', '=', 'hobby_det.id_kathobi_satu')
            ->leftjoin('category_hobby AS cathobidua', 'hobby_det.id_kathobi_dua', '=', 'cathobidua.id')
            ->select('hobby_det.*', 'hobby.name as nmhobi', 'cathobisatu.name as nmkathobi1', 'cathobidua.name as nmkathobi2')
            ->get();

        if(request()->ajax()) {
            return datatables()->of($hobbydet)
            ->addColumn('action', 'backend.master.hobidet.action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        $hobi = Hobby::all();
        $cathobi = CategoryHobby::all();
        return view('backend.master.hobidet.index',compact('cathobi','hobi'));
    }

    public function store(Request $request)
    {  
        $key = $request->hobby_det_id;
        $arr   =   HobbyDet::updateOrCreate(['id' => $key],
                    [
                        'name' => $request->name,
                        'id_hobi' => $request->id_hobby,
                        'id_kathobi_satu' => $request->id_kathobi_satu,
                        'id_kathobi_dua' => $request->id_kathobi_dua,
                        'id_status' => $request->id_status,
                    ]
                    );        
        return Response::json($arr);
    
    }

    public function edit($id)
    {   
        $where = array('id' => $id);
        $arr  = HobbyDet::where($where)->first();
     
        return Response::json($arr);
    }


    public function destroy($id)
        {
            $arr = HobbyDet::where('id',$id)->delete();
        
            return Response::json($arr);
        }

        public function show()
        {
             return response()->json(Hobby::orderBy('name', 'ASC')->get());
        }

        public function category()
        {
             return response()->json(CategoryHobby::orderBy('name', 'ASC')->get());
        }
}
