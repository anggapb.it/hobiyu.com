<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
use App\Models\Master\Hobby; 
use App\Models\Master\HobbyDet;
use App\Models\Master\Bid;
use App\Models\Master\Sponsor;
use App\Lelang;
use App\Customer;
use App\Members;
use App\Stlelang;
use App\Slider;
use App\Profile;
use Auth;
use DB;
use Carbon\Carbon;
use jpmurray\LaravelCountdown\Countdown;


class HomeBaseWebController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function index(Hobby $hobi, Lelang $lelang, Bid $bid)
    {
        date_default_timezone_set('Asia/Jakarta');
        $hobbies = $hobi::where("id_status", "ACTIVE")->get();
        $lelang_new = DB::table('lelangs')
        ->offset(0)
        ->limit(8)
        ->orderByRaw('created_at DESC')
        ->get();
      //  $lelang_new = DB::table('lelangs')->where([['created_at','<',"DATE_ADD(DATE_FORMAT(created_at,'%Y-%m-%d'),INTERVAL 1 MONTH)"], ['idstlelang','=','2']])->get();
       // $lelang_will_end = $lelang::all();

        $lelang_will_end = DB::table('lelangs')
        ->where('idstlelang','<>','3') //lelang yang sudah closed tidak di tampilkan
        ->offset(0)
        ->limit(8)
        ->orderByRaw('tgl_bts_penawaran ASC')
        ->get();

        $profile = Profile::where('id',1)->first();
        $sponsors = Sponsor::where('id_status','=','ACTIVE')->get();
        $sliders = Slider::all();
      

        return view('index_web',compact('hobbies','hobi','lelang_new','lelang_will_end','bid','profile','sliders','sponsors'));
    }

    public function detailLelang($id, Bid $bid){
        $profile = Profile::where('id',1)->first();
        $hobbies = Hobby::where("id_status", "ACTIVE")->get();
        $lelang = Lelang::where('id',$id)->first();
        $customer = Customer::where('id',$lelang->userid)->first();
        $stlelang = Stlelang::where('id',$lelang->idstlelang)->first();
      //  $biders = Bid::where('idlelang',$lelang->id)->get();
        $biders = \DB::table('bids')->join('customers', 'customers.id', '=', 'bids.userid')
                    ->select('bids.*','customers.name as nmcustomer')
                    ->where('idlelang',$lelang->id)->get();

        return view('web.lelang.detail_lelang',compact('id','hobbies','lelang','customer','bid','biders','stlelang','profile'))->with('i');
    }

    public function homeCustomer(Hobby $hobi){
        $profile = Profile::where('id',1)->first();
        $hobbies = $hobi::where("id_status", "ACTIVE")->get();

        return view('web.customer.home-customer',compact('hobbies','hobi','profile'));
    }

    public function signup(){
        return view('web.signup');
    }
    
    public function login_member(){
        return view('web.signin');
    }

    public function bid(Hobby $hobi){
        $hobbies = $hobi::where("id_status", "ACTIVE")->get();

        return view('web.lelang.bid',compact('hobbies'));
    }

     public function makeBid(Request $request)
    {
         date_default_timezone_set('Asia/Jakarta');
         $this->validate($request, [
            'harga' => 'required',
            'idlelang' => 'required',
           // 'userid' => 'required',
        ]);

        $message = '';


        $val = Bid::where('idlelang','=',$request->get('idlelang'))
               ->where('harga','=',$request->get('harga'))->first();  
        
        if(empty($val->id)){
            
                $bid = Bid::firstOrCreate([
                    'harga' => $request->get('harga'),
                    'idlelang'=> $request->get('idlelang'),
                    'userid'=> Auth::guard('customer')->user()->id,
                ]);

            $lelang = Lelang::find($request->idlelang);
            $lelang->hrgawal =  $request->harga;
            $lelang->update();

            $message = 'Bid Berhasil';
        } else{
            $message = '1';
        }

  


        return response()->json($message);

    }

    
}
