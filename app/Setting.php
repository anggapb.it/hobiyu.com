<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Setting extends Model
{
    use Notifiable;

    protected $fillable = [
        'id','name','nilai','keterangan','attribute'
    ];
}
