<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Forums extends Model
{
    //
    use Notifiable;

    protected $fillable = [
        'id','id_category_forum','judul','deskripsi','tag','status','userid','idhobidet'
    ];
}
