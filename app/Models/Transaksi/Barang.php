<?php

namespace App\Models\Transaksi;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $guarded = [];
    
    protected $table 		= 'barang';

    protected $fillable 	= ['userid','kdbrg','status',
                               'nmbrg','idkatbrg','stok','harga','berat','kondisi_barang','video_satu','video_dua','video_tiga',
                             'deskripsi_barang','gambar_satu','gambar_dua','idhobi','idhobidet','cost_jual',
                             'gambar_tiga', 'update_at', 'created_at','lokasi_barang'];
}
