<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Sponsor extends Model
{
     /**
     * @var string
     */
    protected $table 		= 'sponsor';

    /**
     * @var array
     */
    protected $fillable 	= ['name','type_sponsor','tglactive',
                               'no_contract','time_contract','status_contract',
                               'id_status','created_at', 'update_at','banner','link'];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];
}
