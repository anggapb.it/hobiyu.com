<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
     /**
     * @var string
     */
    protected $table 		= 'kelurahan';

    /**
     * @var array
     */
    protected $fillable 	= ['name','id_kecamatan','created_at', 'update_at'];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];
}
