<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class TypeTransaction extends Model
{
      /**
     * @var string
     */
    protected $table 		= 'type_transaction';

    /**
     * @var array
     */
    protected $fillable 	= ['name','keterangan', 'created_at', 'update_at'];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];
}
