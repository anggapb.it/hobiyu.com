<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table 		= 'banks';

    protected $fillable 	= ['name', 'keterangan', 'update_at', 'created_at'];
}
