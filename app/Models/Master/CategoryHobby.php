<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class CategoryHobby extends Model
{
     /**
     * @var string
     */
    protected $table 		= 'category_hobby';

    /**
     * @var array
     */
    protected $fillable 	= ['name','keterangan', 'created_at', 'update_at'];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];
}
