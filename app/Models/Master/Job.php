<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
       /**
     * @var string
     */
    protected $table 		= 'jobs';

    /**
     * @var array
     */
    protected $fillable 	= ['id','name', 'created_at', 'update_at'];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];
}
