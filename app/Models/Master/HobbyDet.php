<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class HobbyDet extends Model
{
        /**
     * @var string
     */
    protected $table 		= 'hobby_det';

    /**
     * @var array
     */
    protected $fillable 	= ['name','id_hobi','id_status','id_kathobi_satu', 'id_kathobi_dua', 'created_at', 'update_at'];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];

    public function hobby()
    {
       return $this->belongsTo(Hobby::class,'id_hobi');
    }
}
