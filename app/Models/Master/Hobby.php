<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use App\Models\Master\HobbyDet;


class Hobby extends Model
{
      /**
     * @var string
     */
    protected $table 		= 'hobby';

    /**
     * @var array
     */
    protected $fillable 	= ['name','id_status','keterangan', 'created_at', 'update_at','icon'];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];

    public function hobbydets()
    {
       return $this->hasMany(HobbyDet::class,'id_hobi');
    }
}
