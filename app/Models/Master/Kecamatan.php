<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
     /**
     * @var string
     */
    protected $table 		= 'kecamatan';

    /**
     * @var array
     */
    protected $fillable 	= ['name','id_kota','created_at', 'update_at'];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];
}
