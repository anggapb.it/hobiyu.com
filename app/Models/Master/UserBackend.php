<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class UserBackend extends Model
{
       /**
     * @var string
     */
    protected $table 		= 'users';

    /**
     * @var array
     */
    protected $fillable 	= ['id','name', 'email','password','created_at', 'update_at'];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];
}
