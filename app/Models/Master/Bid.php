<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
      /**
     * @var string
     */
    protected $table 		= 'bids';

    /**
     * @var array
     */
    protected $fillable 	= ['harga','userid','idlelang', 'created_at', 'update_at'];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];
}
