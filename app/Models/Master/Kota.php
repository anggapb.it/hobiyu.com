<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
      /**
     * @var string
     */
    protected $table 		= 'kota';

    /**
     * @var array
     */
    protected $fillable 	= ['name','id_provinsi','created_at', 'update_at'];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];
}
