<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class MstForum extends Model
{
     /**
     * @var string
     */
    protected $table 		= 'mst_forum';

    /**
     * @var array
     */
    protected $fillable 	= ['name','id_status','id_detail_hobi', 'created_at', 'update_at'];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];
}
