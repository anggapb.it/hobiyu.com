<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class StatusOrder extends Model
{
    //
    use Notifiable;

    protected $table 		= 'status_orders';

    protected $fillable = [
        'id','name'
    ];
}
