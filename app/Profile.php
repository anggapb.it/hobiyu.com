<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Profile extends Model
{
    //
    use Notifiable;

    protected $fillable = ['brand_name', 'address','phone1',
        'phone2','email','facebook','twitter','youtube','kdrek',
        'bank_satu','kdrek_satu','atasnama_satu','bank_dua','kdrek_dua','atasnama_dua','bank_tiga','kdrek_tiga','atasnama_tiga'];
}
