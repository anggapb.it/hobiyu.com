<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Answers extends Model
{
    //
    use Notifiable;

    protected $fillable = [
        'id','id_forum','comment','userid'
    ];
}
