<?php

    namespace App;

    use Illuminate\Notifications\Notifiable;
    use Illuminate\Foundation\Auth\User as Authenticatable;

    class Customer extends Authenticatable
    {
        use Notifiable;

        protected $guard = 'customer';

        protected $fillable = [
            'name', 'email', 'password',  'phone', 'photo_profile','alamat','kodepos','status','idbank','kdrek','saldo'
        ];

        protected $hidden = [
            'password', 'remember_token',
        ];

        public function lelang()
        {
            return $this->hasOne('App\Lelang');
        }

       
    }