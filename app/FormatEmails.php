<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class FormatEmails extends Model
{
    //
    use Notifiable;

    protected $fillable = [
        'id','format'
    ];
}
