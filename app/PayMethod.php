<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class PayMethod extends Model
{
    //
    use Notifiable;

    protected $table 		= 'pay_methods';

    protected $fillable = [
        'id','name'
    ];
}
