<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Order extends Model
{
    
    use Notifiable;

    protected $fillable = [
        'id','noorder','idcustomer','biayaadmin','idmethod_bayar','idkurir','ongkir','total','alamat_kirim','status','batas_bayar'
        ,'estimasi_kirim','idbank_transfer'
    ];

    public function orderdets_many()
    {
       return $this->hasMany(Orderdet::class,'idorder');
    }

}
