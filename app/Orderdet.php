<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Orderdet extends Model
{
    //
    use Notifiable;

    protected $fillable = [
        'id','idorder','idbarang','nmbrg','qty','harga','idpenjual','stbrg','noresi','noorderdet'
    ];
}
