<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Members extends Authenticatable
{
    use Notifiable;
    /**
     * @var string
     */
  //  protected $table 		= 'members';

    /**
     * @var array
     */
    protected $fillable 	= ['name','email','notelp','password','klpuser','status','created_at', 'update_at'];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($val)
    {
        return $this->attributes['password'] = bcrypt($val);
    }

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];
}
