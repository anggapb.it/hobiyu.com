<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Complaint extends Model
{
    //
    use Notifiable;

    protected $fillable = [
        'id','notiket','judul','keterangan','file','status','iduser'
    ];
}
